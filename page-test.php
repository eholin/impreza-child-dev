<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * /* Template name: Page for testing only
 */
$us_layout = US_Layout::instance();
get_header();
us_load_template( 'templates/titlebar' );
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<div class="l-content g-html">

			<?php do_action( 'us_before_page' ) ?>

			<?php
			while ( have_posts() ){
				the_post();

				$the_content = apply_filters( 'the_content', get_the_content() );

				// The page may be paginated itself via <!--nextpage--> tags
				$pagination = us_wp_link_pages( array(
					'before' => '<div class="w-blog-pagination"><nav class="navigation pagination" role="navigation">',
					'after' => '</nav></div>',
					'next_or_number' => 'next_and_number',
					'nextpagelink' => '>',
					'previouspagelink' => '<',
					'link_before' => '<span>',
					'link_after' => '</span>',
					'echo' => 0,
				) );

				// If content has no sections, we'll create them manually
				$has_own_sections = ( strpos( $the_content, ' class="l-section' ) !== FALSE );
				if ( ! $has_own_sections ) {
					$the_content = '<section class="l-section"><div class="l-section-h i-cf">' . $the_content . $pagination . '</div></section>';
				} elseif ( ! empty( $pagination ) ) {
					$the_content .= '<section class="l-section"><div class="l-section-h i-cf">' . $pagination . '</div></section>';
				}

				echo $the_content;

				/* all tests start here */
				/*
								global $wpdb;
								$query = 'SELECT download_post_id FROM acas4u_download_indexes WHERE download_permalink="" LIMIT 0,15';
								$downloads = $wpdb->get_results( $query );

								if ( count( $downloads ) > 0 ) {
									foreach ( $downloads as $download ) {
										$post_id = $download->download_post_id;
										$permalink = get_the_permalink( $post_id );
										$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
										$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
										$download_count = get_post_meta( $post_id, '_download_count', TRUE );
										$date_uploaded = get_the_date( 'Y-m-d H:i:s', $post_id );
										$query_update = 'UPDATE acas4u_download_indexes SET
														download_permalink = "' . $permalink . '",
														download_votes="' . $download_votes . '",
														download_duration="' . $download_duration . '",
														download_count="' . $download_count . '",
														download_post_date="' . $date_uploaded . '"
													 WHERE download_post_id = "' . $post_id . '"';
										echo $query_update . '<br>';
										$wpdb->query( $query_update );
									}
								}
								*/
			}
			?>

			<?php do_action( 'us_after_page' ) ?>

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos . ' ' . us_dynamic_sidebar_id(); ?>"<?php echo ( us_get_option( 'schema_markup' ) ) ? ' itemscope itemtype="https://schema.org/WPSideBar"' : ''; ?>>
				<?php
				// Sidebar for Events Calendar pages
				$post_type = get_post_type();
				if ( is_singular( array( 'tribe_events' ) ) OR is_tax( 'tribe_events_cat' ) OR is_post_type_archive( 'tribe_events' ) ) {
					$default_events_sidebar_id = us_get_option( 'event_sidebar_id', 'default_sidebar' );
					us_dynamic_sidebar( $default_events_sidebar_id );
				} elseif ( in_array( $post_type, us_get_option( 'custom_post_types_support', array() ) ) ) {
					$default_post_sidebar_id = us_get_option( 'sidebar_' . $post_type . '_id', 'default_sidebar' );
					us_dynamic_sidebar( $default_post_sidebar_id );
				} else {
					us_dynamic_sidebar();
				}
				?>
			</aside>
		<?php endif; ?>

	</div>
</div>

<?php get_footer() ?>
