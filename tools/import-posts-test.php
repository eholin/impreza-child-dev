<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$conf_path = '/srv/new.acapellas4u.co.uk/public_html';
$log_path = '/srv/new.acapellas4u.co.uk/public_html/wp-content/themes/Impreza-child/tools/reply_ids.txt';
$errors = 0;
$total = 0;
$step = 0;
$max_steps = 150; // 1000 posts per 1 step
$last_post_id = 0;

include( $conf_path . '/' . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = DB_NAME;

function clean( $string ) {
	$string = str_replace( ' ', '-', $string ); // Replaces all spaces with hyphens.
	$string = preg_replace( '/[^A-Za-z0-9\-]/', '', $string );
	$string = str_replace( 'amp', 'and', $string ); // Replaces all spaces with hyphens.

	return trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
}

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$phpbb = new mysqli( "localhost", 'pilchbeta', 'tabritis joint health' );

while ( $last_post_id != - 1 ){

	// ### topics starts here ###
	$query = 'SELECT post_id, topic_id, forum_id, poster_id, post_time, post_subject, post_text FROM ' . $phpbb_dbname . '.phpbb_posts WHERE post_id > "' . $last_post_id . '" ORDER BY post_id ASC LIMIT 0,1000';
	$result = $phpbb->query( $query );
	if ( $result ) {
		$post_count = $result->num_rows;
		if ( $post_count > 0 ) {
			$posts = [ ];
			$post_meta = [ ];
			while ( $row = $result->fetch_object() ){
				$post_id = $row->post_id;
				$posts[ $post_id ]['poster_id'] = $row->poster_id;
				$post_meta[ $post_id ]['_bbp_topic_id'] = $row->topic_id;

				$last_post_id = $post_id;
			}
		} else {
			$last_post_id = - 1;
		}
	}

	echo '<p>Last post ID: ' . $last_post_id . '</p>';

	if ( $last_post_id > 0 ) {
		$t = 0;
		foreach ( $posts as $post_id => $post_value ) {

			// get post topic
			$tquery = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_topic_id" AND meta_value="' . $post_meta[ $post_id ]['_bbp_topic_id'] . '"';
			$tresult = $mysqli->query( $tquery );
			if ( $tresult ) {
				if ( $tresult->num_rows ) {
					$trow = $tresult->fetch_assoc();
					$post_parent = $trow['post_id'];
				}
			}
			if ( $post_parent ) {
				file_put_contents( $log_path, $post_id . "\r\n", FILE_APPEND );
			}
		}

		echo '<p>' . $t . ' posts added</p>';
	} // if ( $last_post_id > 0 ) {

	if ( $step >= $max_steps ) {
		$last_post_id = - 1;
	}
	$step ++;
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled</p>';
