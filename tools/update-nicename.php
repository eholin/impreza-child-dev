<?php
/* update meta value if source is zero (0) */

$time_start = microtime( TRUE );
$srv_path = '/srv/site/new.acapellas4u.co.uk/www';
include( $srv_path . '/' . 'wp-config.php' );
include( $srv_path . '/' . 'wp-includes/formatting.php.' );
$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$total = 0;
$query = 'SELECT ID, user_login FROM acas4u_wpnew.wp_users WHERE user_nicename = ""';
//echo $query . "<br>";
$result = $mysqli->query( $query );

if ( $result ) {
	$post_count = $result->num_rows;
	//echo 'Count: ' . $post_count . '<br>';
	if ( $post_count > 0 ) {
		while ( $row = $result->fetch_object() ){
			$user_id = $row->ID;
			$user_login = $row->user_login;
			$nicename = sanitize_title( $user_login );

			$query_update = 'UPDATE acas4u_wpnew.wp_users SET user_nicename="' . $nicename . '" WHERE ID="' . $user_id . '"';
			//echo $query_update . '<br>';
			$mysqli->query( $query_update );

			$total ++;
		}
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled.</p>';

?>
