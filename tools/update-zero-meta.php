<?php
/* update meta value if source is zero (0) */

$source_field = 'mode';
$destination_field = '_download_mode';

$time_start = microtime( TRUE );
$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
include( $srv_path . 'wp-config.php' );

$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$total = 0;
if ( $source_field != '' ) {

	$query = 'SELECT id FROM ' . $phpbb_dbname . '.phpbb_download_files WHERE `' . $source_field . '` = "0"';
	//echo $query . "<br>";
	$result = $mysqli->query( $query );

	if ( $result ) {
		$post_count = $result->num_rows;
		//echo 'Count: ' . $post_count . '<br>';
		if ( $post_count > 0 ) {
			while ( $row = $result->fetch_object() ){
				$download_id = $row->id;

				$query_id = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_download_old_file_id" AND meta_value="' . $download_id . '"';
				//echo $query_id . '<br>';
				$result_id = $mysqli->query( $query_id );
				if ( $result_id ) {
					$row_id = $result_id->fetch_assoc();
					$post_id = $row_id['post_id'];

					$query_insert = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta (post_id, meta_key, meta_value) VALUES ("' . $post_id . '", "' . $destination_field . '", "0")';
					//echo $query_insert . '<br>';
					$mysqli->query( $query_insert );

					$total ++;
				}
			}
		}
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled. Source field: <strong>' . $source_field . '</strong></p>';

?>
