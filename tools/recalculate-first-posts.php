<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$log_path = $srv_path . 'wp-content/themes/Impreza-child/tools/first_posts.txt';
$errors = 0;
$total = 0;
$step = 0;
$max_steps = 150; // 1000 posts per 1 step
$last_post_id = 0;

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$query = 'SELECT topic_first_post_id FROM ' . $phpbb_dbname . '.phpbb_topics WHERE topic_first_post_id > "0" ORDER BY topic_id ASC';
$result = $mysqli->query( $query );
if ( $result ) {
	$post_count = $result->num_rows;
	echo '<p>Count of posts: ' . $post_count . '</p>';
	if ( $post_count > 0 ) {
		while ( $row = $result->fetch_object() ){
			$first_post_id = $row->topic_first_post_id;
			$query_pm = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_post_id" AND meta_value="' . $first_post_id . '"';
			$result_pm = $mysqli->query( $query_pm );
			if ( $result_pm ) {
				if ( $result_pm->num_rows ) {
					$row_pm = $result_pm->fetch_assoc();
					$post_id = $row_pm['post_id'];
					if ( $post_id ) {
						$query_up = 'UPDATE ' . $wp_dbname . '.wp_posts SET post_status="draft" WHERE ID="' . $post_id . '"';
						$update = $mysqli->query( $query_up );
						file_put_contents( $log_path, $post_id . "\r\n", FILE_APPEND );
						$total ++;
					}
				}
			}
		}
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled</p>';

?>
