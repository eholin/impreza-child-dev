<?php
$time_start = microtime( TRUE );
setlocale( LC_ALL, 'en_US.UTF8' );
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.
	return $string;
}

$srv_path = '/srv/site/acas4u/www';
include( $srv_path . '/' . 'wp-config.php' );
$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$last_post_id = 0;
$total = 0;
while ( $last_post_id != - 1 ){

	$query = 'SELECT ID, post_name FROM acas4u.wp_posts WHERE ID > "' . $last_post_id . '" AND post_type="download" ORDER BY ID ASC LIMIT 0,500';
	$result = $mysqli->query( $query );

	if ( $result ) {
		$post_count = $result->num_rows;
		if ( $post_count > 0 ) {
			while ( $row = $result->fetch_object() ){
				$post_id = $row->ID;
				$url_fname = clean( $row->post_name );
				$query_u = 'UPDATE acas4u.wp_posts SET post_name="' . $url_fname . '" WHERE ID = "' . $post_id . '"';
				//echo $query_u.'<br>';
				$mysqli->query( $query_u );

				//echo $url_fname . '<br>';
				$last_post_id = $post_id;
				$total ++;
			}
		} else {
			$last_post_id = - 1;
		}
	} else {
		$last_post_id = - 1;
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled</p>';

/*
$urls = Array(
	'Å¡patnej_wliw_-_jestli_(czech_acapella).mp3',
	'_Å¡patnej_wliw__---__jestli_(czech_acapella).mp3-',
	'four_tops_-_i’ll_be_there_(acapella).mp3',
	"four_tops_-_i'll_be_there_(acapella).mp3",
	'four_tops_-_i"ll_be_there_(acapella).mp3',
	'madonna_-_4_minutes_(jamey’s_mixshow_acapella).mp3',
	'nÃ¡ksi_vs._brunner_-_nÃ©zz_az_Ã‰g_felÃ©_(acapella).mp3',
	'nolan_srong_&_the_diablos_-_jump,_shake_&_move_(acapella).mp3',
);

foreach ( $urls as $key => $value ) {
	echo clean( $value ) . '<br>';
}
*/

?>
