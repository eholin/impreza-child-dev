<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$total = 0;
$dbname = 'acas4u_wpnew';

$mysqli = new mysqli( 'localhost', 'acas4u', '1d7YIH6r8WSspf6W1kQw' );
$query = 'SELECT id FROM ' . $dbname . '.phpbb_download_files WHERE timestamp < "1131598803" ORDER BY id ASC';
$result = $mysqli->query( $query );
if ( $result ) {
	$records_count = $result->num_rows;
	if ( $records_count > 0 ) {
		while ( $row = $result->fetch_object() ){
			$record_id = $row->id;

			$query_next = 'SELECT timestamp FROM ' . $dbname . '.phpbb_download_files WHERE id > "' . $record_id . '" ORDER BY id ASC LIMIT 1';
			$result_next = $mysqli->query( $query_next );
			if ( $result_next ) {
				$next_row = $result_next->fetch_assoc();
				$timestamp_next = $next_row['timestamp'];
				if ( $timestamp_next < 1131598803 ) {
					$timestamp_next = 1131598803;
				}
			}

			$query_prev = 'SELECT timestamp FROM ' . $dbname . '.phpbb_download_files WHERE id < "' . $record_id . '" ORDER BY id DESC LIMIT 1';
			$result_prev = $mysqli->query( $query_prev );
			if ( $result_next ) {
				$prev_row = $result_next->fetch_assoc();
				$timestamp_prev = $next_row['timestamp'];
				if ( $timestamp_prev < 1131598803 ) {
					$timestamp_prev = 1131598803;
				}
			}

			if ( $timestamp_next AND $timestamp_prev ) {
				$new_timestamp = floor( ( $timestamp_next + $timestamp_prev ) / 2 );
			} else if ( $timestamp_next AND ! $timestamp_prev ) {
				$new_timestamp = $timestamp_next - 1;
			} else if ( ! $timestamp_next AND $timestamp_prev ) {
				$new_timestamp = $timestamp_prev + 1;
			} else {
				$new_timestamp = 0;
			}

			if ( $new_timestamp ) {
				$query_update = 'UPDATE ' . $dbname . '.phpbb_download_files SET timestamp="' . $new_timestamp . '" WHERE id = "' . $record_id . '" ';
				$mysqli->query( $query_update );
				$total ++;
				echo '<p>Record #' . $record_id . ', new timestamp: ' . $new_timestamp . '</p>';
			}
		}
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> records handled</p>';

?>
