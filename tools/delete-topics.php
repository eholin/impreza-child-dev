<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$errors = 0;
$total = 0;

include( $srv_path . 'wp-config.php' );
$wp_dbname = constant( 'DB_NAME' );

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$query = 'SELECT ID FROM ' . $wp_dbname . '.wp_posts WHERE post_type="topic" LIMIT 0,5000';
$result = $mysqli->query( $query );
if ( $result ) {
	$count = $result->num_rows;
	if ( $count > 0 ) {
		$p = 0;
		while ( $row = $result->fetch_object() ){
			$post_id = $row->ID;
			$query_delete_meta = 'DELETE FROM ' . $wp_dbname . '.wp_postmeta WHERE post_id="' . $post_id . '"';
			$res_delete_meta = $mysqli->query( $query_delete_meta );

			$query_delete_post = 'DELETE FROM ' . $wp_dbname . '.wp_posts WHERE ID="' . $post_id . '"';
			$res_delete_post = $mysqli->query( $query_delete_post );

			if ( $res_delete_post === FALSE ) {
				echo '<pre>';
				var_dump( $res_delete_post );
				echo '</pre>';
				$errors ++;
			} else {
				$total ++;
			}
		}
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> topics deleted</p>';

