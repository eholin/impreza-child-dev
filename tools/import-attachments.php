<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$errors = 0;
// id of last attachment in phpbb
$last_attachment_id = 0;
$debug = FALSE;
// relative to wordpress uploads path
$attachment_dir = 'phpbb_attachments/';
// uploads uri
$website = 'http://acapellas4u.co.uk/wp-content/uploads/';

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

setlocale( LC_ALL, 'en_US.UTF8' );
// clean url for post
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.

	return $string;
}

$total = 0;

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$phpbb = new mysqli( "localhost", 'pilchbeta', 'tabritis joint health' );
while ( $last_attachment_id != - 1 ){

	// ### topics starts here ###
	$query = 'SELECT attach_id, post_msg_id, poster_id, physical_filename, real_filename, extension, mimetype, filetime FROM ' . $phpbb_dbname . '.phpbb_attachments WHERE attach_id > "' . $last_attachment_id . '" AND post_msg_id > 0 ORDER BY attach_id ASC LIMIT 0,1000';
	$result = $phpbb->query( $query );
	if ( $result ) {
		$attach_count = $result->num_rows;
		if ( $attach_count > 0 ) {
			$attachments = [];
			$attachment_meta = [];
			while ( $row = $result->fetch_object() ){
				$attachment_id = $row->attach_id;

				// post_parent
				$ppquery = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_post_id" AND meta_value="' . $row->post_msg_id . '"';
				$ppresult = $mysqli->query( $ppquery );
				if ( $ppresult ) {
					if ( $ppresult->num_rows ) {
						$pprow = $ppresult->fetch_assoc();
						$attachments[ $attachment_id ]['post_parent'] = $pprow['post_id'];
						$attachments[ $attachment_id ]['poster_id'] = $row->poster_id;
						$attachments[ $attachment_id ]['filetime'] = $row->filetime;
						$attachments[ $attachment_id ]['extension'] = $row->extension;
						$attachments[ $attachment_id ]['mimetype'] = $row->mimetype;
						$attachments[ $attachment_id ]['physical_filename'] = $row->physical_filename;

						$attachment_meta[ $attachment_id ]['_bbp_old_attach_id'] = $attachment_id;
						$attachment_meta[ $attachment_id ]['_bbp_old_parent_post_id'] = $row->post_msg_id;
						$attachment_meta[ $attachment_id ]['_wp_attached_file'] = $attachment_dir . $row->physical_filename;
						$attachment_meta[ $attachment_id ]['_bbp_attachment'] = 1;
					}
				}

				$last_attachment_id = $attachment_id;
			}
		} else {
			$last_attachment_id = - 1;
		}
	}

	echo '<p>Last attachment ID: ' . $last_attachment_id . '</p>';

	if ( $last_attachment_id > 0 ) {
		$t = 0;
		foreach ( $attachments as $attachment_id => $topic_value ) {
			$dotext = '.' . $attachments[ $attachment_id ]['extension'];
			$attachment_name = trim( $attachments[ $attachment_id ]['physical_filename'], $dotext );
			$post_title = $mysqli->real_escape_string( $attachment_name );
			$post_name = strtolower( clean( $attachment_name ) );
			$post_date = date( 'Y-m-d H:i:s', $attachments[ $attachment_id ]['filetime'] );
			$guid = $website . $attachment_meta[ $attachment_id ]['_wp_attached_file'];
			$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_content,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			guid,
			menu_order,
			post_type,
			post_mime_type,
			comment_count
			) VALUES (
			'" . $attachments[ $attachment_id ]['poster_id'] . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'',
			'" . $post_title . "',
			'inherit',
			'closed',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $attachments[ $attachment_id ]['post_parent'] . "',
			'" . $guid . "',
			'0',
			'attachment',
			'" . $attachments[ $attachment_id ]['mimetype'] . "',
			'0')";
			if ( ! $debug ) {
				$res = $mysqli->query( $query_posts );
				if ( $res === FALSE ) {
					echo '<pre>';
					var_dump( $res );
					echo '</pre>';
					$errors ++;
				} else {
					$t ++;
					$total ++;
					$attachment_meta[ $attachment_id ]['_bbp_attachment_id'] = $mysqli->insert_id;
				}
			} else {
				echo '<pre>' . $query_posts . ' <br></pre>';
			}
		}

		echo '<p>' . $t . ' attachments added</p>';

		if ( ! $debug ) {
			// ############## postmeta starts here ##############
			$m = 0;
			$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta( post_id, meta_key, meta_value ) VALUES ';
			foreach ( $attachment_meta as $attachment_id => $meta_value ) {
				$new_attachment_id = $attachment_meta[ $attachment_id ]['_bbp_attachment_id'];
				foreach ( $attachment_meta[ $attachment_id ] as $key => $value ) {
					if ( $value != '' ) {
						$value = $mysqli->real_escape_string( $value );
						$query_meta .= "('" . $new_attachment_id . "', '" . $key . "', '" . $value . "'), ";
					}
					$m ++;
				}
			}
			$query_meta = substr( $query_meta, 0, - 2 );
			$query_meta .= ';';

			$res = $mysqli->query( $query_meta );

			if ( $res === FALSE ) {
				echo '<pre>';
				var_dump( $res );
				echo '</pre>';
				$errors ++;
			} else {
				echo '<p>' . $m . ' attachments meta added</p>';
			}
		}
	} // if ( $last_topic_id > 0 )
	if ( $debug ) {
		$last_attachment_id = - 1;
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors . Script working time: ' . $time . ' seconds . <strong>' . $total . '</strong> attachments handled</p>';
