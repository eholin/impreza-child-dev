<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$topics_file = 'topics.txt';
$errors = 0;
$total = 0;
$step = 0;
$max_steps = 50; // 1000 topics per 1 step
$t_filename = $srv_path . 'wp-content/themes/Impreza-child/tools/' . $topics_file;

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME');

setlocale( LC_ALL, 'en_US.UTF8' );
// clean url for post
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.

	return $string;
}

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$last_topic_id = 0;
while ( $last_topic_id != - 1 ){

	// ### topics starts here ###
	if ( file_exists( $t_filename ) ) {
		$last_topic_id = (int) file_get_contents( $t_filename );
	}
	$query = 'SELECT topic_id, topic_title, topic_first_post_id, topic_poster, topic_time, forum_id, topic_replies, topic_replies_real, topic_type, topic_last_post_time FROM ' . $phpbb_dbname . '.phpbb_topics WHERE topic_id > "' . $last_topic_id . '" ORDER BY topic_id ASC LIMIT 0,1000';
	$result = $mysqli->query( $query );
	if ( $result ) {
		$topic_count = $result->num_rows;
		if ( $topic_count > 0 ) {
			$topics = [ ];
			$topic_meta = [ ];
			while ( $row = $result->fetch_object() ){
				$topic_id = $row->topic_id;
				$topics[ $topic_id ]['topic_title'] = $row->topic_title;

				// here we get a description from replies (the first reply)
				$topic_first_post_id = $row->topic_first_post_id;
				$rfreply = $mysqli->query( 'SELECT post_text FROM ' . $phpbb_dbname . '.phpbb_posts WHERE topic_id="' . $topic_id . '"' );
				if ( $rfreply ) {
					if ( $rfreply->num_rows ) {
						$rfrow = $rfreply->fetch_assoc();
						$topics[ $topic_id ]['post_text'] = $rfrow['post_text'];
					}
				}

				$topics[ $topic_id ]['topic_poster'] = $row->topic_poster;
				$topics[ $topic_id ]['topic_time'] = $row->topic_time;

				$topic_meta[ $topic_id ]['_bbp_old_topic_id'] = $topic_id;

				// post_parent
				$ppquery = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_forum_id" AND meta_value="' . $row->forum_id . '"';
				$ppresult = $mysqli->query( $ppquery );
				if ( $ppresult ) {
					if ( $ppresult->num_rows ) {
						$pprow = $ppresult->fetch_assoc();
						$topics[ $topic_id ]['post_parent'] = $pprow['post_id'];
					}
				}

				$topic_meta[ $topic_id ]['_bbp_forum_id'] = $topics[ $topic_id ]['post_parent'];
				$topic_meta[ $topic_id ]['_bbp_old_parent_forum_id'] = $row->forum_id;
				$topic_meta[ $topic_id ]['_bbp_reply_count'] = $row->topic_replies;
				$topic_meta[ $topic_id ]['_bbp_total_reply_count'] = $row->topic_replies_real;
				$topic_meta[ $topic_id ]['_bbp_old_sticky_status'] = $row->topic_type;
				$topic_meta[ $topic_id ]['_bbp_last_active_time'] = date( 'Y-m-d H:i:s', $row->topic_last_post_time );
				$last_topic_id = $topic_id;
			}
		} else {
			$last_topic_id = - 1;
		}
		file_put_contents( $t_filename, $last_topic_id );
	}

	echo '<p>Last topic ID: ' . $last_topic_id . '</p>';

	if ( $last_topic_id > 0 ) {
		$t = 0;
		foreach ( $topics as $topic_id => $topic_value ) {
			$post_content = $mysqli->real_escape_string( $topics[ $topic_id ]['post_text'] );
			$post_title = $mysqli->real_escape_string( $topics[ $topic_id ]['topic_title'] );
			$post_name = strtolower( clean( $topics[ $topic_id ]['topic_title'] ) );
			$post_date = date( 'Y-m-d H:i:s', $topics[ $topic_id ]['topic_time'] );
			$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_content,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			menu_order,
			post_type,
			comment_count
			) VALUES (
			'" . $topics[ $topic_id ]['topic_poster'] . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_content . "',
			'" . $post_title . "',
			'publish',
			'closed',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $topics[ $topic_id ]['post_parent'] . "',
			'0',
			'topic',
			'0')";
			$res = $mysqli->query( $query_posts );
			if ( $res === FALSE ) {
				echo '<pre>';
				var_dump( $res );
				echo '</pre>';
				$errors ++;
			} else {
				$t ++;
				$total ++;
				$topic_meta[ $topic_id ]['_bbp_topic_id'] = $mysqli->insert_id;
			}
			//echo '<pre>' . $query_posts . ' <br></pre>';
		}

		echo '<p>' . $t . ' topics added</p>';

		// ############## postmeta starts here ##############
		$m = 0;
		$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta( post_id, meta_key, meta_value ) VALUES ';
		foreach ( $topic_meta as $topic_id => $meta_value ) {
			$new_topic_id = $topic_meta[ $topic_id ]['_bbp_topic_id'];
			foreach ( $topic_meta[ $topic_id ] as $key => $value ) {
				if ( $value != '' ) {
					$value = $mysqli->real_escape_string( $value );
					$query_meta .= "('" . $new_topic_id . "', '" . $key . "', '" . $value . "'), ";
				}
				$m ++;
			}
		}
		$query_meta = substr( $query_meta, 0, - 2 );
		$query_meta .= ';';

		$res = $mysqli->query( $query_meta );

		if ( $res === FALSE ) {
			echo '<pre>';
			var_dump( $res );
			echo '</pre>';
			$errors ++;
		} else {
			echo '<p>' . $m . ' topics meta added</p>';
		}
	} // if ( $last_topic_id > 0 )
	if ( $step >= $max_steps ) {
		$last_topic_id = - 1;
	}
	$step ++;
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors . Script working time: ' . $time . ' seconds . <strong>' . $total . '</strong> topics handled</p>';
