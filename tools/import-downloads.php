<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$errors = 0;
// in file is ID of the last uploaded file in table phpbb_download_files
$downloads_file = 'downloads.txt';
$debug = FALSE;
$step = 1;
$max_steps = 50; // 1000 downloads per 1 step

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = DB_NAME;

setlocale( LC_ALL, 'en_US.UTF8' );
// clean url for post
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.

	return $string;
}

$total = 0;
$d_filename = $srv_path . 'wp-content/themes/Impreza-child/tools/' . $downloads_file;

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$last_download_id = 30260;
while ( $last_download_id != - 1 ){

	// ### download files starts here ###
	if ( $debug AND file_exists( $d_filename ) ) {
		$last_download_id = (int) file_get_contents( $d_filename );
	}
	$query = 'SELECT * FROM ' . $phpbb_dbname . '.phpbb_download_files WHERE id > ' . $last_download_id . '  ORDER BY id ASC LIMIT 0,1000';
	$result = $mysqli->query( $query );
	if ( $result ) {
		$download_count = $result->num_rows;
		if ( $download_count > 0 ) {
			$downloads = [];
			$download_meta = [];
			while ( $row = $result->fetch_object() ){
				$download_id = $row->id;

				// need to clean from specialcharachters
				$url_fname = clean( $row->filename );

				$downloads[ $download_id ]['post_title'] = $row->filename;
				$downloads[ $download_id ]['post_name'] = $download_id . '-' . strtolower( $url_fname );
				$downloads[ $download_id ]['post_date'] = $row->timestamp;

				$download_meta[ $download_id ]['_download_old_file_id'] = $download_id;
				$download_meta[ $download_id ]['_download_filename'] = $row->filename;
				$download_meta[ $download_id ]['_download_filehash'] = $row->filehash;
				$download_meta[ $download_id ]['_download_path'] = $row->path;
				$download_meta[ $download_id ]['_download_size'] = $row->size;
				$download_meta[ $download_id ]['_download_lenght'] = $row->lenght;
				$download_meta[ $download_id ]['_download_bpm'] = $row->bpm;
				$download_meta[ $download_id ]['_download_bitrate'] = $row->bitrate;
				$download_meta[ $download_id ]['_download_count'] = $row->count;
				$download_meta[ $download_id ]['_download_rating'] = $row->rating;
				$download_meta[ $download_id ]['_download_votes'] = $row->votes;
				$download_meta[ $download_id ]['_download_live'] = $row->live;
				$download_meta[ $download_id ]['_download_genre_1'] = $row->genre_1;
				$download_meta[ $download_id ]['_download_genre_2'] = $row->genre_2;
				$download_meta[ $download_id ]['_download_artist1'] = $row->artist1;
				$download_meta[ $download_id ]['_download_artist1id'] = $row->artist1id;
				$download_meta[ $download_id ]['_download_artist2'] = $row->artist2;
				$download_meta[ $download_id ]['_download_artist2id'] = $row->artist2id;
				$download_meta[ $download_id ]['_download_trackname'] = $row->trackname;
				$download_meta[ $download_id ]['_download_echonestid'] = $row->echonestid;
				$download_meta[ $download_id ]['_download_trackstatus'] = $row->trackstatus;
				$download_meta[ $download_id ]['_download_samplerate'] = $row->samplerate;
				$download_meta[ $download_id ]['_download_audio_md5'] = $row->audio_md5;
				$download_meta[ $download_id ]['_download_key'] = $row->key;
				$download_meta[ $download_id ]['_download_energy'] = $row->energy;
				$download_meta[ $download_id ]['_download_tempo'] = $row->tempo;
				$download_meta[ $download_id ]['_download_mode'] = $row->mode;
				$download_meta[ $download_id ]['_download_time_signature'] = $row->time_signature;
				$download_meta[ $download_id ]['_download_duration'] = $row->duration;
				$download_meta[ $download_id ]['_download_loudness'] = $row->loudness;
				$download_meta[ $download_id ]['_download_danceability'] = $row->danceability;
				$download_meta[ $download_id ]['_download_md5'] = $row->md5;
				$download_meta[ $download_id ]['_download_keymode_id'] = $row->keymode_id;
				$download_meta[ $download_id ]['_download_term_id'] = $row->term_id;
				$last_download_id = $download_id;
			}
		} else {
			$last_download_id = - 1;
		}
		if ( $debug ) {
			file_put_contents( $d_filename, $last_download_id );
		}
	}

	echo '<p>Last download ID: ' . $last_download_id . '</p>';

	if ( $last_download_id > 0 ) {
		$t = 0;
		foreach ( $downloads as $download_id => $download_value ) {
			$post_title = $mysqli->real_escape_string( $downloads[ $download_id ]['post_title'] );
			$post_name = $mysqli->real_escape_string( $downloads[ $download_id ]['post_name'] );
			$post_date = date( 'Y-m-d H:i:s', $downloads[ $download_id ]['post_date'] );
			$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			menu_order,
			post_type,
			comment_count
			) VALUES (
			'2',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_title . "',
			'publish',
			'open',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'0',
			'0',
			'download',
			'0')";
			if ( ! $debug ) {
				$res = $mysqli->query( $query_posts );
				if ( $res === FALSE ) {
					echo '<pre>';
					var_dump( $res );
					echo '</pre>';
					$errors ++;
				} else {
					$t ++;
					$total ++;
					$download_meta[ $download_id ]['_download_id'] = $mysqli->insert_id;
				}
			} else {
				echo '<pre>' . $query_posts . ' <br></pre>';
			}
		}

		echo '<p>' . $t . ' attachments added</p>';

		if ( ! $debug ) {
			// ############## postmeta starts here ##############
			$m = 0;
			$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta( post_id, meta_key, meta_value ) VALUES ';
			foreach ( $download_meta as $download_id => $meta_value ) {
				$new_download_id = $download_meta[ $download_id ]['_download_id'];
				foreach ( $download_meta[ $download_id ] as $key => $value ) {
					if ( $value != '' AND $key != '_download_id' ) {
						$value = $mysqli->real_escape_string( $value );
						$query_meta .= "('" . $new_download_id . "', '" . $key . "', '" . $value . "'), ";
					}
					$m ++;
				}
			}
			$query_meta = substr( $query_meta, 0, - 2 );
			$query_meta .= ';';

			$res = $mysqli->query( $query_meta );

			if ( $res === FALSE ) {
				echo '<pre>';
				var_dump( $res );
				echo '</pre>';
				$errors ++;
			} else {
				echo '<p>' . $m . ' attachments meta added</p>';
			}
		}
	} // if ( $last_topic_id > 0 )
	if ( $debug OR $step >= $max_steps ) {
		$last_download_id = - 1;
	}
	$step ++;
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors . Script working time: ' . $time . ' seconds . <strong>' . $total . '</strong> download files handled</p>';
