<?php
$srv_path = '/srv/site/acas4u/www';
include( $srv_path . '/' . 'wp-config.php' );
$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$total = 0;

$query = 'SELECT ID, guid FROM acas4u_wpnew.wp_posts WHERE post_type="attachment" ORDER BY ID ASC';
$result = $mysqli->query( $query );
if ( $result ) {
	if ( $result->num_rows > 0 ) {
		while ( $row = $result->fetch_assoc() ){
			$post_id = $row['ID'];
			$guid = $row['guid'];
			$guid = str_replace( 'http://acas4u.codelights.net/phpbb_attachments/', 'http://acas4u.codelights.net/wp-content/uploads/phpbb_attachments/', $guid );
			$update = 'UPDATE acas4u.wp_posts SET guid="' . $guid . '" WHERE ID="' . $post_id . '"';
			$res = $mysqli->query( $update );
			$total ++;
		}
	}
}

echo '<p>Done. <strong>' . $total . '</strong> attachments handled</p>';
