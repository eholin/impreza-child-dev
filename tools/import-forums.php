<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$errors = 0;

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

setlocale( LC_ALL, 'en_US.UTF8' );
// clean url for post
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.

	return $string;
}

$parents = TRUE;
$childrens = TRUE;
$total = 0;

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );

// ### only first level forums ###
if ( $parents === TRUE ) {
	$query = 'SELECT * FROM ' . $phpbb_dbname . '.phpbb_forums WHERE parent_id="0"';
	$result = $mysqli->query( $query );
	if ( $result ) {
		$count = $result->num_rows;
		if ( $count > 0 ) {
			$forums = [];
			$forum_meta = [];
			while ( $row = $result->fetch_object() ){
				$forum_id = $row->forum_id;

				// check is forum exists in WP
				//$forum_title = $mysqli->real_escape_string( $row->forum_name );
				//$query_check = "SELECT * FROM " . $wp_dbname . ".wp_posts WHERE post_title = '$forum_title'";

				$forums[ $forum_id ]['forum_name'] = $row->forum_name;
				$forums[ $forum_id ]['forum_desc'] = $row->forum_desc;

				$forum_meta[ $forum_id ]['_bbp_old_forum_id'] = $forum_id;
				$forum_meta[ $forum_id ]['_bbp_old_parent_id'] = $forum_id;
				$forum_meta[ $forum_id ]['_bbp_forum_type'] = $row->forum_type;
				$forum_meta[ $forum_id ]['_bbp_reply_count'] = $row->forum_posts;
				$forum_meta[ $forum_id ]['_bbp_total_topic_count'] = $row->forum_topics_real;
				$forum_meta[ $forum_id ]['_bbp_last_topic_id'] = $row->forum_last_post_id;
				$forum_meta[ $forum_id ]['_bbp_forum_flags'] = $row->forum_flags;
				$forum_meta[ $forum_id ]['_bbp_last_active_time'] = $row->forum_last_post_time;
				$forum_meta[ $forum_id ]['_bbp_forum_options'] = $row->forum_options;
				$forum_meta[ $forum_id ]['sbg_selected_sidebar'] = serialize( array( 0 => '0' ) );
				$forum_meta[ $forum_id ]['sbg_selected_sidebar_replacement'] = serialize( array( 0 => 'Default Sidebar' ) );
				$forum_meta[ $forum_id ]['_edit_last'] = 1;
				$forum_meta[ $forum_id ]['_bbp_topic_count_hidden'] = 0;
			}
		}
	}

	// only one date for all first level forums
	$post_date = date( 'Y-m-d H:i:s' ); // default date - today
	$qdate = 'SELECT topic_time FROM ' . $phpbb_dbname . '.phpbb_topics ORDER BY topic_id ASC LIMIT 0,1';
	$rdate = $mysqli->query( $qdate );
	if ( $rdate ) {
		if ( $rdate->num_rows ) {
			$row = $rdate->fetch_assoc();
			$post_date = date( 'Y-m-d H:i:s', $row['topic_time'] );
		}
	}

	$f = 0;
	foreach ( $forums as $forum_id => $forum_value ) {
		$post_content = $mysqli->real_escape_string( $forums[ $forum_id ]['forum_desc'] );
		$post_title = $mysqli->real_escape_string( $forums[ $forum_id ]['forum_name'] );
		$post_name = strtolower( clean( $forums[ $forum_id ]['forum_name'] ) );
		$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_content,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			menu_order,
			post_type,
			comment_count
			) VALUES (
			'1',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_content . "',
			'" . $post_title . "',
			'publish',
			'closed',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'0',
			'0',
			'forum',
			'0')";
		$res = $mysqli->query( $query_posts );
		if ( $res === FALSE ) {
			echo '<pre>';
			var_dump( $res );
			echo '</pre>';
			$errors ++;
		} else {
			$f ++;
			$total ++;
			$forum_meta[ $forum_id ]['forum_id'] = $mysqli->insert_id;
		}
	}

	echo '<p>' . $f . ' forums added</p>';

	/*
	echo '<pre>';
	var_dump( $query_posts );
	echo '</pre>';
	*/

	// ############## postmeta starts here ##############

	$m = 0;
	$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta (post_id, meta_key, meta_value) VALUES ';
	foreach ( $forum_meta as $forum_id => $meta_value ) {
		$new_forum_id = $forum_meta[ $forum_id ]['forum_id'];
		foreach ( $forum_meta[ $forum_id ] as $key => $value ) {
			if ( $value != '' ) {
				$value = $mysqli->real_escape_string( $value );
				$query_meta .= "('" . $new_forum_id . "', '" . $key . "', '" . $value . "'), ";
			}
			$m ++;
		}
	}
	$query_meta = substr( $query_meta, 0, - 2 );
	$query_meta .= ';';

	$res = $mysqli->query( $query_meta );

	if ( $res === FALSE ) {
		echo '<pre>';
		var_dump( $res );
		echo '</pre>';
		$errors ++;
	} else {
		echo '<p>' . $m . ' forums meta added</p>';
	}
	/*
	echo '<pre>';
	var_dump( $query_meta );
	echo '</pre>';
	*/
}
// ### forums with parents ###
if ( $childrens === TRUE ) {
	$query = 'SELECT * FROM ' . $phpbb_dbname . '.phpbb_forums WHERE parent_id>"0"';
	$result = $mysqli->query( $query );
	if ( $result ) {
		$count = $result->num_rows;
		if ( $count > 0 ) {
			$forums = [];
			$forum_meta = [];
			while ( $row = $result->fetch_object() ){
				$forum_id = $row->forum_id;
				$forums[ $forum_id ]['forum_name'] = $row->forum_name;
				$forums[ $forum_id ]['forum_desc'] = $row->forum_desc;

				$forum_meta[ $forum_id ]['_bbp_old_forum_id'] = $forum_id;
				$forum_meta[ $forum_id ]['_bbp_old_parent_forum_id'] = $row->parent_id;
				$forum_meta[ $forum_id ]['_bbp_forum_type'] = $row->forum_type;
				$forum_meta[ $forum_id ]['_bbp_reply_count'] = $row->forum_posts;
				$forum_meta[ $forum_id ]['_bbp_total_topic_count'] = $row->forum_topics_real;
				$forum_meta[ $forum_id ]['_bbp_last_topic_id'] = $row->forum_last_post_id;
				$forum_meta[ $forum_id ]['_bbp_forum_flags'] = $row->forum_flags;
				$forum_meta[ $forum_id ]['_bbp_last_active_time'] = $row->forum_last_post_time;
				$forum_meta[ $forum_id ]['_bbp_forum_options'] = $row->forum_options;
				$forum_meta[ $forum_id ]['sbg_selected_sidebar'] = serialize( array( 0 => '0' ) );
				$forum_meta[ $forum_id ]['sbg_selected_sidebar_replacement'] = serialize( array( 0 => 'Default Sidebar' ) );
				$forum_meta[ $forum_id ]['_edit_last'] = 1;
				$forum_meta[ $forum_id ]['_bbp_topic_count_hidden'] = 0;
			}
		}
	}

	$f = 0;
	foreach ( $forums as $forum_id => $forum_value ) {
		// post date by first post topic
		$post_date = date( 'Y-m-d H:i:s' ); // default date - today
		$qdate = 'SELECT topic_time FROM ' . $phpbb_dbname . '.phpbb_topics WHERE forum_id="' . $forum_id . '" ORDER BY topic_id ASC LIMIT 0,1';
		$rdate = $mysqli->query( $qdate );
		if ( $rdate ) {
			if ( $rdate->num_rows ) {
				$row = $rdate->fetch_assoc();
				$post_date = date( 'Y-m-d H:i:s', $row['topic_time'] );
			}
		}

		// post parent
		$qparent = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_parent_id" AND meta_value="' . $forum_meta[ $forum_id ]['_bbp_old_parent_forum_id'] . '"';
		$rparent = $mysqli->query( $qparent );
		if ( $rparent ) {
			if ( $rparent->num_rows ) {
				$row = $rparent->fetch_assoc();
				$post_parent = $row['post_id'];
			}
		}

		$post_content = $mysqli->real_escape_string( $forums[ $forum_id ]['forum_desc'] );
		$post_title = $mysqli->real_escape_string( $forums[ $forum_id ]['forum_name'] );
		$post_name = strtolower( clean( $forums[ $forum_id ]['forum_name'] ) );
		$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_content,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			menu_order,
			post_type,
			comment_count) VALUES (
			'1',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_content . "',
			'" . $post_title . "',
			'publish',
			'closed',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_parent . "',
			'0',
			'forum',
			'0')";
		$res = $mysqli->query( $query_posts );
		if ( $res === FALSE ) {
			echo '<pre>';
			var_dump( $res );
			echo '</pre>';
			$errors ++;
		} else {
			$forum_meta[ $forum_id ]['forum_id'] = $mysqli->insert_id;
			$f ++;
			$total ++;
		}
	}

	echo '<p>' . $f . ' forums added</p>';

	// ############## postmeta starts here ##############

	$m = 0;
	$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta (post_id, meta_key, meta_value) VALUES ';
	foreach ( $forum_meta as $forum_id => $meta_value ) {
		$new_forum_id = $forum_meta[ $forum_id ]['forum_id'];
		foreach ( $forum_meta[ $forum_id ] as $key => $value ) {
			if ( $value != '' ) {
				$value = $mysqli->real_escape_string( $value );
				$query_meta .= "('" . $new_forum_id . "', '" . $key . "', '" . $value . "'), ";
			}
			$m ++;
		}
	}
	$query_meta = substr( $query_meta, 0, - 2 );
	$query_meta .= ';';

	$res = $mysqli->query( $query_meta );

	if ( $res === FALSE ) {
		echo '<pre>';
		var_dump( $res );
		echo '</pre>';
		$errors ++;
	} else {
		echo '<p>' . $m . ' forums meta added</p>';
	}
	/*
	echo '<pre>';
	var_dump( $query_meta );
	echo '</pre>';
	*/
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> forums handled</p>';
