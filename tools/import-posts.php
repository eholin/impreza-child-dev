<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$log_path = $srv_path . 'wp-content/themes/Impreza-child/tools/reply_ids.txt';
$errors = 0;
$total = 0;
$step = 0;
$max_steps = 150; // 1000 posts per 1 step
$last_post_id = 0;

include( $srv_path . 'wp-config.php' );
$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

setlocale( LC_ALL, 'en_US.UTF8' );
// clean url for post
function clean( $string ) {
	$replacement_empty = array(
		')',
		'(',
		']',
		'[',
		'}',
		'{',
		'mp3',
		',',
		'"',
		'’',
		"'",
	);
	$replacement_hyphen = array(
		'~',
		' ',
	);
	$string = strtolower( iconv( 'UTF-8', 'us-ascii//TRANSLIT', $string ) );
	$string = str_replace( '&amp;', '_and_', $string );
	$string = str_replace( '&', '_and_', $string );
	$string = str_replace( $replacement_empty, '', $string );
	$string = str_replace( $replacement_hyphen, '-', $string );
	$string = preg_replace( '/[^a-z0-9\-\_]/', '', $string );

	$string = trim( preg_replace( '/-+/', '-', $string ), '-' ); // Removes special chars.
	$string = trim( preg_replace( '/_+/', '_', $string ), '_' ); // Removes special chars.

	return $string;
}

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
while ( $last_post_id != - 1 ){

	// ### topics starts here ###
	$query = 'SELECT post_id, topic_id, forum_id, poster_id, post_time, post_subject, post_text FROM ' . $phpbb_dbname . '.phpbb_posts WHERE post_id > "' . $last_post_id . '" ORDER BY post_id ASC LIMIT 0,1000';
	$result = $mysqli->query( $query );
	if ( $result ) {
		$post_count = $result->num_rows;
		if ( $post_count > 0 ) {
			$posts = [];
			$post_meta = [];
			while ( $row = $result->fetch_object() ){
				$post_id = $row->post_id;
				$posts[ $post_id ]['poster_id'] = $row->poster_id;
				$posts[ $post_id ]['post_subject'] = $row->post_subject;
				$posts[ $post_id ]['post_text'] = $row->post_text;
				$posts[ $post_id ]['post_time'] = $row->post_time;

				$post_meta[ $post_id ]['_bbp_old_post_id'] = $post_id;
				$post_meta[ $post_id ]['_bbp_topic_id'] = $row->topic_id;

				// get post topic
				$tfquery = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_forum_id" AND meta_value="' . $row->forum_id . '"';
				$tfresult = $mysqli->query( $tfquery );
				if ( $tfresult ) {
					if ( $tfresult->num_rows ) {
						$tfrow = $tfresult->fetch_assoc();
						$post_meta[ $post_id ]['_bbp_forum_id'] = $tfrow['post_id'];
					}
				}

				$last_post_id = $post_id;
			}
		} else {
			$last_post_id = - 1;
		}
	}

	echo '<p>Last post ID: ' . $last_post_id . '</p>';

	if ( $last_post_id > 0 ) {
		$t = 0;
		foreach ( $posts as $post_id => $post_value ) {
			$post_content = $mysqli->real_escape_string( $posts[ $post_id ]['post_text'] );
			$post_title = $mysqli->real_escape_string( $posts[ $post_id ]['post_subject'] );
			if ( $posts[ $post_id ]['post_subject'] ) {
				$post_name = strtolower( clean( $posts[ $post_id ]['post_subject'] ) );
			} else {
				$post_name = 'reply-' . $post_id;
			}
			$post_date = date( 'Y-m-d H:i:s', $posts[ $post_id ]['post_time'] );

			// get post topic
			$tquery = 'SELECT post_id FROM ' . $wp_dbname . '.wp_postmeta WHERE meta_key="_bbp_old_topic_id" AND meta_value="' . $post_meta[ $post_id ]['_bbp_topic_id'] . '"';
			$tresult = $mysqli->query( $tquery );
			if ( $tresult ) {
				if ( $tresult->num_rows ) {
					$trow = $tresult->fetch_assoc();
					$post_parent = $trow['post_id'];
					if ( $post_parent ) {
						$query_posts = "INSERT INTO " . $wp_dbname . ".wp_posts (
			post_author,
			post_date,
			post_date_gmt,
			post_content,
			post_title,
			post_status,
			comment_status,
			ping_status,
			post_name,
			post_modified,
			post_modified_gmt,
			post_parent,
			menu_order,
			post_type,
			comment_count
			) VALUES (
			'" . $posts[ $post_id ]['poster_id'] . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_content . "',
			'" . $post_title . "',
			'publish',
			'closed',
			'closed',
			'" . $post_name . "',
			'" . $post_date . "',
			'" . $post_date . "',
			'" . $post_parent . "',
			'0',
			'reply',
			'0')";
						//echo '<pre>' . $query_posts . '</pre>';

						$res = $mysqli->query( $query_posts );
						if ( $res === FALSE ) {
							echo '<pre>';
							var_dump( $res );
							echo '</pre>';
							$errors ++;
						} else {
							file_put_contents( $log_path, $post_id . "\r\n", FILE_APPEND );
							$t ++;
							$total ++;
							$post_meta[ $post_id ]['_bbp_post_id'] = $mysqli->insert_id;
						}
					}
				}
			}
		}

		echo '<p>' . $t . ' posts added</p>';

		// ############## postmeta starts here ##############
		$m = 0;
		$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_postmeta (post_id, meta_key, meta_value) VALUES ';
		foreach ( $post_meta as $post_id => $meta_value ) {
			$new_post_id = $post_meta[ $post_id ]['_bbp_post_id'];
			foreach ( $post_meta[ $post_id ] as $key => $value ) {
				if ( $value != '' ) {
					$value = $mysqli->real_escape_string( $value );
					$query_meta .= "('" . $new_post_id . "', '" . $key . "', '" . $value . "'), ";
				}
				$m ++;
			}
		}
		$query_meta = substr( $query_meta, 0, - 2 );
		$query_meta .= ';';

		$res = $mysqli->query( $query_meta );

		if ( $res === FALSE ) {
			echo '<pre>';
			var_dump( $res );
			echo '</pre>';
			$errors ++;
		} else {
			echo '<p>' . $m . ' posts meta added</p>';
		}
	} // if ( $last_post_id > 0 ) {
	if ( $step >= $max_steps ) {
		$last_post_id = - 1;
	}
	$step ++;
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> posts handled</p>';
