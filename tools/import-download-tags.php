<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$srv_path = '/srv/site/new.acapellas4u.co.uk/www';
include( $srv_path . '/' . 'wp-config.php' );
$phpbb_dbname = 'acas4u_phpbb';
$wp_dbname = DB_NAME;

//setlocale( LC_ALL, 'en_US.UTF8' );
$mysqli = new mysqli( 'localhost', constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );
$query = 'SELECT * FROM ' . $wp_dbname . '.phpbb_download_tags ORDER BY tag_id ASC';
$result = $mysqli->query( $query );
$updated = 0;
$inserted = 0;
if ( $result ) {
	$tags_count = $result->num_rows;
	if ( $tags_count > 0 ) {
		while ( $row = $result->fetch_object() ){
			$query_ex = 'SELECT tag_id FROM ' . $wp_dbname . '.acas4u_search_tags WHERE tag_name = "' . $row->tag_name . '"';
			$result_ex = $mysqli->query( $query_ex );
			if ( $result_ex ) {
				$tags_count_ex = $result_ex->num_rows;
				$row_ex = $result_ex->fetch_object();
				$query_upd = 'UPDATE ' . $wp_dbname . '.acas4u_search_tags SET tag_quantity = tag_quantity + 1 WHERE tag_id = "' . $row_ex->tag_id . '"';
				$result_upd = $mysqli->query( $query_upd );
				$updated ++;
			} else {
				$query_ins = 'INSERT INTO ' . $wp_dbname . '.acas4u_search_tags (tag_quantity, search_date, search_tag) VALUES ("' . $row->tag_quantity . '", "' . date( 'Y-m-d H:i:s', $row->tag_last_tag_time ) . '", "' . $row->tag_name . '");';
				$result_ins = $mysqli->query( $query_ins );
				$inserted ++;
			}
		}
	}
}

echo '<p>Done, updated ' . $updated . ' tags, inserted ' . $inserted . ' tags</p>';
