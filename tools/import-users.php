<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', E_ALL );
ini_set( 'display_startup_errors', 1 );
$time_start = microtime( TRUE );

$srv_path = '/srv/site/new.acapellas4u.co.uk/www/';
$errors = 0;

include( $srv_path . 'wp-config.php' );
//include( $srv_path . 'wp-includes/formatting.php' );

$phpbb_dbname = 'pilchbeta';
$wp_dbname = constant( 'DB_NAME' );

$incorrect_values = array(
	0 => 'soundcloud.com/',
	1 => 'www.',
);

$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );

$count = 1;
$total = 0;
while ( $count > 0 ){
	$result_id = $mysqli->query( 'SELECT user_id AS phpbb_user_id FROM ' . $phpbb_dbname . '.phpbb_users WHERE user_id NOT IN(SELECT ID FROM ' . $wp_dbname . '.wp_users) ORDER BY user_id ASC LIMIT 0,1' );
	$row = $result_id->fetch_object();
	if ( $result_id ) {
		$count = $result_id->num_rows;
		if ( $count > 0 ) {
			$wp_last_user_id = $row->phpbb_user_id;

			$result = $mysqli->query( 'SELECT
	user_id,
	group_id,
	user_regdate,
	username,
	user_password,
	user_email,
	user_birthday,
	user_timezone,
	user_dateformat,
	user_rank,
	user_options,
	user_avatar,
	user_sig,
	user_from,
	user_website,
	user_occ,
	user_interests,
	user_posts,
	user_form_salt FROM ' . $phpbb_dbname . '.phpbb_users WHERE user_id >= "' . $wp_last_user_id . '" ORDER BY user_id ASC LIMIT 0,25000' );
			$users = [];
			$usermeta = [];
			while ( $phpbb_user = $result->fetch_object() ){
				$user_id = $phpbb_user->user_id;
				$group_id = $phpbb_user->group_id;
				$meta_query = 'SELECT pf_talents, pf_inspiredby, pf_your_genre, pf_soundcloud_page, pf_fav_website, pf_testimonials FROM ' . $phpbb_dbname . '.phpbb_profile_fields_data WHERE user_id="' . $user_id . '"';
				//echo $meta_query . ';<br>';
				$meta_result = $mysqli->query( $meta_query );
				$phpbb_usermeta = $meta_result->fetch_object();

				$users[ $user_id ]['ID'] = $user_id;
				$users[ $user_id ]['user_registered'] = date( 'Y-m-d H:i:s', $phpbb_user->user_regdate );
				$users[ $user_id ]['user_login'] = $phpbb_user->username;
				$users[ $user_id ]['user_pass'] = $phpbb_user->user_password;
				$nicename = sanitize_title( $phpbb_user->username );
				$usermeta[ $user_id ]['_bbp_password'] = serialize( array(
					'hash' => $phpbb_user->user_password,
					'salt' => $phpbb_user->user_form_salt,
				) );
				$users[ $user_id ]['user_email'] = $phpbb_user->user_email;
				$user_rank = $phpbb_user->user_rank;
				if ( $user_rank == '14' ) {
					$users[ $user_id ]['wp_capabilities'] = serialize( array( 'subscriber', 'bbp_moderator' ) );
				} else if ( $user_rank == '18' ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_donator'] = 'TRUE';
				}
				$users[ $user_id ]['user_email'] = $phpbb_user->user_email;
				if ( $phpbb_user->user_website ) {
					$users[ $user_id ]['user_url'] = $phpbb_user->user_website;
				}

				/* --------------------------- start of user capabilities convertation --------------------------- */

				$group_query = 'SELECT group_id FROM ' . $phpbb_dbname . '.phpbb_user_group WHERE user_id = "' . $user_id . '"';
				$group_result = $mysqli->query( $group_query );
				$groups = array( 0 => $group_id );
				while ( $user_groups = $group_result->fetch_object() ){
					$groups[] = $user_groups->group_id;
				}
				$groups = array_unique( $groups );

				$capabilities = [];
				foreach ( $groups as $key => $group ) {
					switch ( $group ) {
						case '4':
							$capabilities[] = 'administrator';
							$capabilities[] = 'bbp_keymaster';
							break;
						case '24013':
							// bbp_donator is a custom role with the same capabilities as bbp_participant
							$capabilities[] = 'subscriber';
							$capabilities[] = 'bbp_donator';
							$usermeta[ $user_id ]['_bbp_phpbb_user_donator'] = 'TRUE';
							break;
						case '24015':
							$capabilities[] = 'subscriber';
							$capabilities[] = 'bbp_participant';
							break;
						case '24018':
							$capabilities[] = 'administrator';
							$capabilities[] = 'bbp_keymaster';
							break;
						case '24019':
							$capabilities[] = 'subscriber';
							$capabilities[] = 'bbp_spectator';
							break;
						case '24020':
							$capabilities[] = 'subscriber';
							$capabilities[] = 'bbp_participant';
							break;
						case '24022':
							$capabilities[] = 'subscriber';
							$capabilities[] = 'bbp_blocked';
							break;
					}
				}

				if ( count( $capabilities ) > 0 ) {
					$capabilities = array_unique( $capabilities );
				} else {
					$capabilities = array( 'subscriber', 'bbp_participant' );
				}

				if ( in_array( 'bbp_participant', $capabilities ) AND in_array( 'bbp_keymaster', $capabilities ) ) {
					if ( ( $key = array_search( 'bbp_participant', $capabilities ) ) !== FALSE ) {
						unset( $capabilities[ $key ] );
					}
				}

				if ( in_array( 'bbp_participant', $capabilities ) AND in_array( 'bbp_donator', $capabilities ) ) {
					if ( ( $key = array_search( 'bbp_participant', $capabilities ) ) !== FALSE ) {
						unset( $capabilities[ $key ] );
					}
				}

				if ( in_array( 'subscriber', $capabilities ) AND in_array( 'administrator', $capabilities ) ) {
					if ( ( $key = array_search( 'subscriber', $capabilities ) ) !== FALSE ) {
						unset( $capabilities[ $key ] );
					}
				}

				$usermeta[ $user_id ]['wp_capabilities'] = serialize( $capabilities );

				/* --------------------------- end of user capabilities convertation --------------------------- */

				if ( $phpbb_user->user_type ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_type'] = $phpbb_user->user_type;
				}

				if ( $phpbb_user->user_birthday ) {
					$user_birthday = $phpbb_user->user_birthday;
					$user_birthday = str_replace( ' ', '', $user_birthday );
					if ( ! strpos( $user_birthday, '0' ) ) {
						$usermeta[ $user_id ]['_bbp_phpbb_user_birthday'] = date( 'Y-m-d', strtotime( $user_birthday ) );
					}
				}
				$usermeta[ $user_id ]['_bbp_phpbb_user_timezone'] = $phpbb_user->user_timezone;
				$usermeta[ $user_id ]['_bbp_phpbb_user_dateformat'] = $phpbb_user->user_dateformat;
				$usermeta[ $user_id ]['_bbp_phpbb_user_rank'] = $phpbb_user->user_rank;
				if ( $phpbb_user->user_avatar ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_avatar'] = $phpbb_user->user_avatar;
				}
				if ( $phpbb_user->user_sig ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_sig'] = $phpbb_user->user_sig;
				}
				if ( $phpbb_user->user_from ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_from'] = $phpbb_user->user_from;
				}
				if ( $phpbb_user->user_occ ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_occ'] = $phpbb_user->user_occ;
				}
				if ( $phpbb_user->user_interests ) {
					$usermeta[ $user_id ]['_bbp_phpbb_user_interests'] = $phpbb_user->user_interests;
				}
				$usermeta[ $user_id ]['wp__bbp_reply_count'] = $phpbb_user->user_posts;

				if ( $phpbb_usermeta ) {
					if ( $phpbb_usermeta->pf_talents ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_talents'] = $phpbb_usermeta->pf_talents;
					}
					if ( $phpbb_usermeta->pf_inspiredby ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_inspiredby'] = $phpbb_usermeta->pf_inspiredby;
					}
					if ( $phpbb_usermeta->pf_your_genre ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_your_genre'] = $phpbb_usermeta->pf_your_genre;
					}
					if ( $phpbb_usermeta->pf_soundcloud_page AND ! in_array( $phpbb_usermeta->pf_soundcloud_page, $incorrect_values ) ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_soundcloud_page'] = $phpbb_usermeta->pf_soundcloud_page;
					}
					if ( $phpbb_usermeta->pf_fav_website AND ! in_array( $phpbb_usermeta->pf_soundcloud_page, $incorrect_values ) ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_fav_website'] = $phpbb_usermeta->pf_fav_website;
					}
					if ( $phpbb_usermeta->pf_testimonials ) {
						$usermeta[ $user_id ]['_bbp_phpbb_pf_testimonials'] = $phpbb_usermeta->pf_testimonials;
					}
				}
				$total ++;
			}

			$u = 0;
			$query_user = 'INSERT INTO ' . $wp_dbname . '.wp_users (ID, user_registered, user_login, display_name, user_pass, user_email, user_url) VALUES ';
			foreach ( $users as $user_id => $meta_value ) {
				if ( $u > 1000 ) {
					$query_user = substr( $query_user, 0, - 2 );
					$query_user .= ';';
					$res = $mysqli->query( $query_user );
					if ( $res === FALSE ) {
						echo '<pre>';
						var_dump( $res );
						echo '</pre>';
						$errors ++;
					} else {
						echo '<p>1000 users added</p>';
					}
					$query_user = 'INSERT INTO ' . $wp_dbname . '.wp_users (ID, user_registered, user_login, display_name, user_pass, user_email, user_url) VALUES ';
					$u = 0;
				}
				$user_login = $mysqli->real_escape_string( $users[ $user_id ]['user_login'] );
				$user_email = $mysqli->real_escape_string( $users[ $user_id ]['user_email'] );
				$user_url = $mysqli->real_escape_string( $users[ $user_id ]['user_url'] );
				$query_user .= "('" . $users[ $user_id ]['ID'] . "', '" . $users[ $user_id ]['user_registered'] . "', '" . $user_login . "', '" . $user_login . "', '" . $users[ $user_id ]['user_pass'] . "', '" . $user_email . "', '" . $user_url . "'), ";
				$u ++;
			}
			$query_user = substr( $query_user, 0, - 2 );
			$query_user .= ';';

			$res = $mysqli->query( $query_user );

			if ( $res === FALSE ) {
				echo '<pre>';
				var_dump( $res );
				echo '</pre>';
				$errors ++;
			} else {
				echo '<p>' . $u . ' users added</p>';
			}

			$um = 0;
			$m = 0;
			$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_usermeta (user_id, meta_key, meta_value) VALUES ';
			foreach ( $usermeta as $user_id => $meta_value ) {
				foreach ( $usermeta[ $user_id ] as $key => $value ) {
					if ( $m > 1000 ) {
						$query_meta = substr( $query_meta, 0, - 2 );
						$query_meta .= ';';
						$res2 = $mysqli->query( $query_meta );
						if ( $res2 === FALSE ) {
							echo '<pre>';
							var_dump( $res2 );
							echo '</pre>';
							$errors ++;
						} else {
							echo '<p>1000 meta added, ' . $um . ' users added</p>';
						}
						$query_meta = 'INSERT INTO ' . $wp_dbname . '.wp_usermeta (user_id, meta_key, meta_value) VALUES ';
						$um = 0;
						$m = 0;
					}
					if ( $value != '' ) {
						$value = $mysqli->real_escape_string( $value );
						$query_meta .= "('" . $user_id . "', '" . $key . "', '" . $value . "'), ";
					}
					$m ++;
				}
				$um ++;
			}
			$query_meta = substr( $query_meta, 0, - 2 );
			$query_meta .= ';';

			//echo '<pre>' . $query_meta . '</pre>';

			$res2 = $mysqli->query( $query_meta );

			if ( $res2 === FALSE ) {
				echo '<pre>';
				var_dump( $res2 );
				echo '</pre>';
				$errors ++;
			} else {
				echo '<p>' . $m . ' meta added, ' . $um . ' users added</p>';
			}
		}
		// for testing purposes!!!
		//$count = 0;
	} else {
		$count = 0;
	}
}

$time_end = microtime( TRUE );
$time = $time_end - $time_start;

echo '<p>Done with ' . $errors . ' errors. Script working time: ' . $time . ' seconds. <strong>' . $total . '</strong> users handled</p>';
