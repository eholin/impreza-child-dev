<?php
$srv_path = '/srv/site/new.acapellas4u.co.uk/www';
$acas4u_upload_path = '/srv/site/new.acapellas4u.co.uk/www/wp-content/uploads';
include( $srv_path . '/' . 'wp-config.php' );
$mysqli = new mysqli( "localhost", constant( 'DB_USER' ), constant( 'DB_PASSWORD' ) );

$query = 'SELECT post_title FROM acas4u_wpnew.wp_posts WHERE post_type="download" AND post_status="publish" ORDER BY post_title ASC';
$result = $mysqli->query( $query );
if ( $result ) {
	if ( $result->num_rows > 0 ) {
		$fp = fopen( $acas4u_upload_path . '/acapellas4u_list.txt', 'w' );
		while ( $row = $result->fetch_assoc() ){
			$post_title = $row['post_title'];

			fwrite( $fp, $post_title . "\r\n" );
		}
		fclose( $fp );
	}
}
