jQuery(document).ready(function($){
	'use strict';

	var $previewContainer = $('.track-preview'),
		audioControls = $('#peaks-audio').get(0),
		fileWaveform = $previewContainer.data('waveform');

	if (audioControls && fileWaveform) {

		window.peaks.init({

			/** REQUIRED OPTIONS **/
			container: document.getElementById('peaks-container'), // Containing element
			audioElement: document.getElementById('peaks-audio'), // HTML5 Audio element for audio track
			dataUri: fileWaveform, // URI to waveform data file in binary or JSON

			/** Optional config with defaults **/

			height: 100, // height of the waveform canvases in pixels
			zoomLevels: [512, 1024, 2048, 4096], // Array of zoom levels in samples per pixel (big >> small)
			keyboard: false, // Bind keyboard controls
			nudgeIncrement: 0.01, // Keyboard nudge increment in seconds (left arrow/right arrow)
			inMarkerColor: '#a0a0a0', // Colour for the in marker of segments
			outMarkerColor: '#a0a0a0', // Colour for the out marker of segments
			zoomWaveformColor: 'rgba(0, 225, 128, 1)', // Colour for the zoomed in waveform
			overviewWaveformColor: 'rgba(0,0,0,0.2)', // Colour for the overview waveform
			segmentColor: 'rgba(255, 161, 39, 1)', // Colour for segments on the waveform
			randomizeSegmentColor: true, // Random colour per segment (overrides segmentColor)

		});

		$("#zoomIn").on("click", window.peaks.zoom.zoomIn);
		$("#zoomOut").on("click", window.peaks.zoom.zoomOut);
		$("#segment").on("click", function(event){
			var startTime = window.peaks.time.getCurrentTime(),
				endTime = startTime + 10,
				segmentEditable = true;
			window.peaks.segments.addSegment(startTime, endTime, segmentEditable);
		});
	}

	$('.acas4u-ratings-single-star').on('mouseover', function(){
		var $starLink = $(this),
			$parent = $starLink.parent('.acas4u-user-ratings-stars'),
			$star = $starLink.find('i.fa'),
			starClass = $starLink.data('class'),
			starIndex = $starLink.data('index');
		if (starClass == 'fa-star-o') {
			$star.removeClass('fa-star-o').addClass('fa-star');
		} else if (starClass == 'fa-star-half-o') {
			$star.removeClass('fa-star-half-o').addClass('fa-star');
		}
		$parent.children('.acas4u-ratings-single-star').each(function(index){
			var eachStarClass = $(this).data('class');
			if (index < starIndex) {
				if (eachStarClass == 'fa-star-o') {
					$(this).find('i.fa').addClass('fa-star').removeClass('fa-star-o');
				} else if (eachStarClass == 'fa-star-half-o') {
					$(this).find('i.fa').addClass('fa-star').removeClass('fa-star-half-o');
				}
			} else if (index > starIndex) {
				if (eachStarClass == 'fa-star') {
					$(this).find('i.fa').addClass('fa-star-o').removeClass('fa-star');
				} else if (eachStarClass == 'fa-star-half-o') {
					$(this).find('i.fa').addClass('fa-star-o').removeClass('fa-star-half-o');
				}
			}
		});
	});

	/* user rating functions */

	$('.acas4u-ratings-single-star').on('mouseleave', function(){
		var $starLink = $(this),
			$parent = $starLink.parent('.acas4u-user-ratings-stars'),
			$star = $starLink.find('i.fa'),
			starClass = $starLink.data('class'),
			starIndex = $starLink.data('index');
		$star.removeClass('fa-star').addClass(starClass);
		$parent.children('.acas4u-ratings-single-star').each(function(index){
			var eachStarClass = $(this).data('class');
			if (index < starIndex) {
				$(this).find('i.fa').removeClass('fa-star').addClass(eachStarClass);
			} else {
				$(this).find('i.fa').removeClass('fa-star-o').addClass(eachStarClass);
			}
		});

	});

	$('.acas4u-user-ratings-stars').on('click', '.acas4u-ratings-single-star', function(e){
		var $starLink = $(this),
			$parent = $starLink.parent('.acas4u-user-ratings-stars'),
			$container = $parent.closest('.acas4u-user-rating-wrapper'),
			$message = $container.find('.acas4u-user-ratings-message'),
			starIndex = $starLink.data('index'),
			totalVotes = $parent.data('votes'),
			averageRating = $parent.data('rating'),
			vars = {
				action: 'do_rating',
				_ajax_nonce: $parent.data('nonce'),
				rating: starIndex + 1,
				post_id: $parent.data('postid')
			};

		$.post(acas4uAjax.ajaxurl, vars, function(response){
			if (!response.success) {
				$message.text('You have already voted for this file!')
			} else {
				var newVotes = totalVotes + 1,
					averageRating = response.data.new_rating,
					formattedRating = number_format(averageRating, 2, '.', ''),
					ratingFloor = Math.floor(averageRating),
					ratingCeil = Math.ceil(averageRating);
				$container.find('.acas4u-ratings-total-votes').text(newVotes);
				$container.find('.acas4u-ratings-average-votes').text(formattedRating);
				$parent.data('votes', newVotes);
				$parent.data('rating', averageRating);
				$message.text('Thank you for your vote!');

				// recalculate stars
				$parent.children('.acas4u-ratings-single-star').each(function(index){
					var starClass;
					if (index == ratingFloor && ratingCeil != ratingFloor) {
						starClass = 'fa-star-half-o';
					} else if (index >= ratingFloor) {
						starClass = 'fa-star-o';
					} else {
						starClass = 'fa-star';
					}

					$(this).find('i.fa').
						removeClass('fa-star').
						removeClass('fa-star-o').
						removeClass('fa-star-half-o').
						addClass(starClass);
					$(this).data('class', starClass);
				});

			}
		});

	});

	function number_format(number, decimals, dec_point, thousands_sep){	// Format a number with grouped thousands
		//
		// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +	 bugfix by: Michael White (http://crestidg.com)

		var i, j, kw, kd, km;

		// input sanitation & defaults
		if (isNaN(decimals = Math.abs(decimals))) {
			decimals = 2;
		}
		if (dec_point == undefined) {
			dec_point = ",";
		}
		if (thousands_sep == undefined) {
			thousands_sep = ".";
		}

		i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

		if ((j = i.length) > 3) {
			j = j % 3;
		} else {
			j = 0;
		}

		km = (j ? i.substr(0, j) + thousands_sep : "");
		kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
		//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
		kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


		return km + kw + kd;
	}

	/* download button */

	/**
	 * Our countdown plugin takes a callback, a duration
	 *
	 * @param callback
	 * @param duration
	 */
	$.fn.countdown = function(callback, duration){
		// Get reference to container, and set initial content
		var container = $(this[0]).html('Wait ' + duration + ' seconds to download');
		// Get reference to the interval doing the countdown
		var countdown = setInterval(function(){
			// If seconds remain
			if (--duration) {
				// Update our container's message
				container.html('Wait ' + duration + ' seconds to download');
				// Otherwise
			} else {
				// Clear the countdown interval
				clearInterval(countdown);
				// And fire the callback passing our container as `this`
				callback.call(container);
			}
			// Run interval every 1000ms (1 second)
		}, 1000);

	};

	var $downloadButton = $('.download-button-wrapper'),
		$buttonMessage = $downloadButton.find('.download-button-message'),
		donator = $downloadButton.data('donator'),
		userID = $downloadButton.data('userid'),
		interval = 30000,   //number of ms between each call
		refresh = function(){
			var $enabledButton = $('.download-button-enabled'),
				$message = $enabledButton.find('.download-button-message'),
				vars = {
					action: 'do_update_downloads_remaining',
					_ajax_nonce: $enabledButton.data('nonce'),
					post_id: $enabledButton.data('postid'),
					user_id: $enabledButton.data('userid')
				};
			$.post(acas4uAjax.ajaxurl, vars, function(response){
				if (response.success) {
					var remaining = response.data.remaining;
					$message.text(remaining);
				} else {
					$message.text('Something went wrong!');
				}
				setTimeout(function(){
					refresh();
				}, interval);
			});
		},
		countdown_callback = function(){
			var $textContainer = $(this),
				$button = $textContainer.closest('.download-button-wrapper'),
				$message = $button.find('.download-button-message'),
				vars = {
					action: 'do_download_link',
					_ajax_nonce: $button.data('nonce'),
					post_id: $button.data('postid'),
					user_id: $button.data('userid')
				};

			$.post(acas4uAjax.ajaxurl, vars, function(response){
				if (response.success) {
					var url = response.data.url,
						remaining = response.data.remaining;
					$button.attr('href', url);
					$message.text(remaining);
					$button.addClass('download-button-enabled');
					refresh();
				} else {
					$message.text('Something went wrong!');
				}
			});
		};

	if (userID) {
		if (donator === true) {
			countdown_callback.call($buttonMessage);
		} else {
			$buttonMessage.countdown(countdown_callback, 60);
		}
	}

});
