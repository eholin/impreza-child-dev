jQuery(document).ready(function($){
	'use strict';

	$('.acas4u-genre-load-more').on('click', function(){
		var $button = $(this),
			$wrapper = $button.closest('.acas4u-downloads-by-genre-wrapper'),
			$loadmore = $wrapper.find('.g-loadmore'),
			$genreResults = $wrapper.find('.acas4u-downloads-by-genre-results'),
			total_posts = $wrapper.data('total'),
			data = {
				_ajax_nonce: $wrapper.data('nonce'),
				action: 'do_genre_scroll',
				page: $wrapper.data('page'),
				genre: $wrapper.data('genre')
			};
		$loadmore.addClass('loading');
		//$genreResults.append('<i class="fa fa-spinner fa-2x fa-spin"></i>');
		$.post(acas4uAjax.ajaxurl, data, function(response){
			if (response.success) {
				var html = response.data.html,
					page = response.data.page,
					current = page * 10;
				//$genreResults.find('.fa-spinner').remove();
				$loadmore.removeClass('loading');
				$genreResults.append(html);
				$wrapper.data('page', page);
				if (total_posts <= current) {
					$button.css('display', 'none');
				}
			}
		}, 'json');

	});

});
