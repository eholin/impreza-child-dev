jQuery(document).ready(function($){
	'use strict';

	$.fn.tooltipster('setDefaults', {
		position: 'top',
		theme: 'tooltipster-light'
	});

	var $form = $('#uploaderform'),
		$progress = $('.acas4u-uploader-progress'),
		$bar = $('.acas4u-uploader-progress-bar'),
		$percent = $('.acas4u-uploader-progress-percent'),
		$message = $("#message"),
		$clear = $('.acas4u-uploader-data-clear'),
		_validFileExtensions = ['.mp3'],
		percentVal,
		clearForm = function(){
			$form.find('.acas4u-uploader-file-input').removeAttr('value');
			$form.find('.acas4u-uploader-file-name').text('');
			$form.find('.acas4u-uploader-data').each(function(){
				$(this).css('display', 'none');
				$(this).find('input').removeAttr('value');
			});
			$form.find('.acas4u-uploader-file-wrapper').each(function(){
				$(this).css('background-color', 'transparent');
			});

		},
		validate = function(formData, jqForm, options){
			$message.html('');
			//console.log(formData);
			for (var i = 0; i < formData.length; i++) {
				if (formData[i].type == 'file' && formData[i].value != '') {
					var sFileName = formData[i].value.name,
						sFileType = formData[i].value.type,
						inputID = jqForm[0][i].id,
						$currentInput = $form.find('#' + inputID),
						$currentContainer = $currentInput.closest('.acas4u-uploader-file-wrapper'),
						$currentArtist = $currentContainer.find('.fl-artist'),
						$currentTrackname = $currentContainer.find('.fl-trackname'),
						artistVal = $currentArtist.val(),
						tracknameVal = $currentTrackname.val();

					//console.log('name:' + sFileName);
					//console.log('type:' + sFileType);
					//console.log('ID:' + inputID);
					if (sFileName.length > 0) {
						var blnValid = false;
						for (var j = 0; j < _validFileExtensions.length; j++) {
							var sCurExtension = _validFileExtensions[j];
							if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
								blnValid = true;
								break;
							}
						}
						if (!blnValid) {
							$message.html('<span style="color:#ff0000;">Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + _validFileExtensions.join(', ') + '</span>');
							formData[i].value = '';
							$currentContainer.css('background-color', '#FFC7C7');
							return false;
						}

					} // end if (sFileName.length > 0)


					if (!artistVal.length) {
						$currentArtist.tooltipster({
							content: $('<span>Please fill this field!</span>')
						});
						$currentArtist.tooltipster('show');
						$currentArtist.css('box-shadow', '0 0 0 2px #d13a7a');
						$currentContainer.css('background-color', '#FFC7C7');
						return false;
					}

					if (!tracknameVal.length) {
						$currentTrackname.tooltipster({
							content: $('<span>Please fill this field!</span>')
						});
						$currentTrackname.tooltipster('show');
						$currentTrackname.css('box-shadow', '0 0 0 2px #d13a7a');
						$currentContainer.css('background-color', '#FFC7C7');
						return false;
					}


				}
			}
		},
		options = {
			url: acas4uAjax.ajaxurl,
			beforeSubmit: validate,
			beforeSend: function(){
				$message.html('');
				$progress.css('display', 'block');
				percentVal = '0%';
				$bar.width(percentVal);
				$percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				percentVal = percentComplete + '%';
				$bar.width(percentVal);
				$percent.html(percentVal);
			},
			success: function(){
				percentVal = '100%';
				$bar.width(percentVal);
				$percent.html(percentVal);
			},
			complete: function(response){
				var obj = jQuery.parseJSON(response.responseText);
				if (typeof obj.data !== 'undefined') {
					if (typeof obj.data.total_files !== 'undefined' && obj.data.total_files.length > 0) {
						$('<div>Total files: ' + obj.data.total_files + '</div>').appendTo($message);
					}
					if (typeof obj.data.messages !== 'undefined' && obj.data.messages.length > 0) {
						var messages = obj.data.messages;
						messages.forEach(function(item, i, messages){
							$('<div>' + item + '</div>').appendTo($message);
						});

					}
				} else {
					$('<div>We are sorry, but something went wrong, your file(s) not uploaded.</div>').appendTo($message);
				}
				clearForm();
			},
			error: function(){
				$message.html('<span style="color:#990000;">ERROR: unable to upload files</span>');

			}

		};


	$form.ajaxForm(options);

	$form.on('click', '.acas4u-uploader-reset-button', clearForm);

	$form.on('change', '.acas4u-uploader-file-input', function(){
		var $this = $(this),
			$wrapper = $this.closest('.acas4u-uploader-file-wrapper'),
			$fnContainer = $wrapper.find('.acas4u-uploader-file-name'),
			fileName = $this.val().replace(/.+[\\\/]/, "");
		if (fileName.length > 0) {
			$fnContainer.text(fileName);
		}
		$wrapper.find('.acas4u-uploader-data').each(function(){
			$(this).css('display', 'inline-block');
			$(this).find('input').css('width', '100px');
		});
	});

	$clear.on('click', function(e){
		e.preventDefault();
		var $this = $(this),
			$wrapper = $this.closest('.acas4u-uploader-file-wrapper'),
			$input = $wrapper.find('.acas4u-uploader-file-input'),
			$fnContainer = $wrapper.find('.acas4u-uploader-file-name');
		$fnContainer.text('');
		$input.removeAttr('value');
		$wrapper.find('.acas4u-uploader-data').each(function(){
			$(this).css('display', 'none');
			$(this).find('input').removeAttr('value');
		});
		$wrapper.css('background-color', 'transparent');
	});

});
