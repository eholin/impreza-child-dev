jQuery(document).ready(function($){
	'use strict';

	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#" + tab_id).addClass('current');
	});

	$('.acas4u-log-table').tablesorter();

	$('.acas4u-userlogs-upload').stacktable();
	$('.acas4u-userlogs-download').stacktable();
});
