jQuery(document).ready(function($){
	'use strict';

	$("#acas4u_popular_count").ionRangeSlider({
		type: "single",
		grid: true,
		min: 0,
		max: 100,
		step: 5,
		from_min: 5
	});

	$('#acas4u_date_from').Zebra_DatePicker({
		format: 'd/m/Y',
		direction: [false, '26/02/2004']
	});

	$('#acas4u_date_to').Zebra_DatePicker({
		format: 'd/m/Y',
		direction: [false, '26/02/2004']
	});

	var $popularTable = $('#acas4u-popular-table'),
		isDonator = $popularTable.data('donator'),
		key2Sorter = true;

	$popularTable.stacktable();

	if (isDonator == '0') {
		key2Sorter = false;
	}

	$popularTable.tablesorter({
		headers: {
			1: {
				sorter: false
			},
			4: {
				sorter: key2Sorter
			}
		},
		textExtraction: {
			'.td-rating': function(node, table, cellIndex){
				return $(node).data('rating');
			}
		}
	});

	$('.acas4u-show-more-popular').on('click', function(){
		var $button = $(this),
			$wrapper = $button.closest('.acas4u-popular-result-wrapper'),
			$loadmore = $wrapper.find('.g-loadmore'),
			total_posts = $wrapper.data('total'),
			total_page = $button.data('page'),
			$searchResultsTable = $wrapper.find('.acas4u-popular-download-result-wrapper.large-only'),
			$searchResultsTableBody = $wrapper.find('.acas4u-popular-download-result-wrapper.large-only tbody'),
			$searchResultsMobileTableBody = $wrapper.find('.acas4u-popular-download-result-wrapper.small-only tbody'),
			data = {
				_ajax_nonce: $wrapper.data('nonce'),
				action: 'do_scroll_popular',
				total: total_posts,
				count: $wrapper.data('count'),
				enddate: $wrapper.data('enddate'),
				startdate: $wrapper.data('startdate'),
				page: total_page,
				offset: $button.data('offset')
			};
		$loadmore.addClass('loading');
		$.post(acas4uAjax.ajaxurl, data, function(response){
			if (response.success) {
				var html_large = response.data.html_large,
					html_small = response.data.html_small,
					offset = response.data.offset,
					newPage = total_page + 1;
				$loadmore.removeClass('loading');
				$searchResultsTableBody.append(html_large);
				$searchResultsMobileTableBody.append(html_small);
				$searchResultsTable.trigger('update');
				$button.data('offset', offset);
				$button.data('page', newPage);
				if (total_posts <= offset) {
					$button.css('display', 'none');
				}
			}
		}, 'json');

	});


});
