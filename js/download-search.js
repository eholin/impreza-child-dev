jQuery(document).ready(function($){
	'use strict';

	var $searchForm = $('#searchform'),
		$searchTable = $('.acas4u-search-results');

	$searchTable.tablesorter({
		headers: {
			0: {
				sorter: false
			},
			3: {
				sorter: key2Sorter
			}
		},
		textExtraction: {
			'.td-rating': function(node, table, cellIndex){
				return $(node).data('rating');
			}
		}
	});

	var isDonator = $searchForm.data('donator'),
		maxBitrateForFree = $searchForm.data('bitratefree'),
		maxBitrate = $searchForm.data('bitratemax'),
		key2Sorter = true,
		do_search_update = function($element){
			var $form = $element.closest('#searchform'),
				searchTerm = $form.find('#searchterm').val(),
				$searchWrapper = $form.closest('#acas4u-download-search-result'),
				$resultsWrapper = $searchWrapper.find('.acas4u-downloads-search-wrapper'),
				$searchResultsTable = $searchWrapper.find('.acas4u-search-results.large-only'),
				$searchResultsTableBody = $searchWrapper.find('.acas4u-search-results.large-only tbody'),
				$searchResultsMobileTableBody = $searchWrapper.find('.acas4u-search-results.small-only tbody'),
				resultsCountVal = $form.find('#search_count').val(),
				$resultsCountDisplay = $form.find('.acas4u-sf-count'),
				formdata = {
					_ajax_nonce: $form.data('nonce'),
					action: 'do_update_search',
					search_term: searchTerm,
					genre: $form.find('#download_genre').val(),
					bpm1: $form.find('#download_bpm1').val(),
					bpm2: $form.find('#download_bpm2').val(),
					bitrate1: $form.find('#download_bitrate1').val(),
					bitrate2: $form.find('#download_bitrate2').val(),
					rating1: $form.find('#download_rating1').val(),
					rating2: $form.find('#download_rating2').val(),
					mode: $form.find('#download_mode').val(),
					key1: $form.find('#download_key1').val(),
					key2: $form.find('#download_key2').val(),
					count: resultsCountVal,
					search_type: $form.data('searchtype')
				};
			$resultsWrapper.data('searchterm', searchTerm);
			$resultsWrapper.addClass('loading');
			$.post(acas4uAjax.ajaxurl, formdata, function(response){
				if (response.success) {
					var html_large = response.data.html['large'],
						html_small = response.data.html['small'],
						total = response.data.html['total'],
						current = response.data.html['current'];
					$resultsWrapper.removeClass('loading');
					$searchResultsTableBody.html(html_large);
					$searchResultsMobileTableBody.html(html_small);
					$searchResultsTable.trigger('update');
					$searchTable.trigger('update');
					$resultsCountDisplay.text(total);
					$resultsWrapper.attr('data-total', total);
				}
			}, 'json');
		};

	$searchTable.stacktable();

	if (isDonator == '0') {
		maxBitrate = maxBitrateForFree;
		key2Sorter = false;
	}

	// rating range
	var $ratingRange = $("#acas4u_download_rating");
	$ratingRange.ionRangeSlider({
		type: "double",
		grid: true,
		grid_snap: true,
		min: 0,
		max: 5,
		step: 1
	});

	$ratingRange.on('change', function(){
		var $this = $(this),
			from = $this.data('from'),
			to = $this.data('to');
		$('#download_rating1').val(from);
		$('#download_rating2').val(to);
		//do_search_update($(this));
	});

	// bitrate range
	var $bitrateRange = $("#acas4u_download_bitrate");
	$bitrateRange.ionRangeSlider({
		type: "double",
		grid: true,
		min: 0,
		max: 320,
		from_max: maxBitrate,
		from_shadow: true,
		to_max: maxBitrate,
		to_shadow: true
	});

	$bitrateRange.on('change', function(){
		var $this = $(this),
			from = $this.data('from'),
			to = $this.data('to');
		$('#download_bitrate1').val(from);
		$('#download_bitrate2').val(to);
		//do_search_update($(this));
	});

	// bpm range
	var $bpmRange = $('#acas4u_download_bpm');
	$bpmRange.ionRangeSlider({
		type: "double",
		grid: true,
		min: 50,
		max: 200
	});

	$bpmRange.on('change', function(){
		var $this = $(this),
			from = $this.data('from'),
			to = $this.data('to');
		$('#download_bpm1').val(from);
		$('#download_bpm2').val(to);
		//do_search_update($(this));
	});

	// count range
	var $countRange = $('#acas4u_search_count');
	$countRange.ionRangeSlider({
		type: "single",
		grid: true,
		min: 0,
		max: 100,
		step: 5,
		from_min: 5,
		onChange: function(data){
			$('#search_count').val(data.from);
		}
	});

	$countRange.on('change', function(){
		var from = $(this).data('from');
		$('#search_count').val(from);
		//do_search_update($(this));
	});

	// select change
	/*
	$('.acas4u-sf-select').on('change', function(){
		do_search_update($(this));
	});
	*/

	$('.acas4u-searchbutton').on('click', function(e){
		e.preventDefault();
		do_search_update($(this));
	});

	$('.acas4u-show-more-results').on('click', function(){
		var $button = $(this),
			$wrapper = $button.closest('.acas4u-downloads-search-wrapper'),
			$loadmore = $wrapper.find('.g-loadmore'),
			totalPosts = $wrapper.data('total'),
			postsCount = $wrapper.data('count'),
			$searchResultsTable = $wrapper.find('.acas4u-search-results.large-only'),
			$searchResultsTableBody = $wrapper.find('.acas4u-search-results.large-only tbody'),
			$searchResultsMobileTableBody = $wrapper.find('.acas4u-search-results.small-only tbody'),
			formdata = {
				_ajax_nonce: $wrapper.data('nonce'),
				action: 'do_scroll',
				page: $wrapper.data('page'),
				search_term: $wrapper.data('searchterm'),
				genre: $wrapper.data('genre'),
				bpm1: $wrapper.data('bpm1'),
				bpm2: $wrapper.data('bpm2'),
				bitrate1: $wrapper.data('bitrate1'),
				bitrate2: $wrapper.data('bitrate2'),
				rating1: $wrapper.data('rating1'),
				rating2: $wrapper.data('rating2'),
				mode: $wrapper.data('mode'),
				key1: $wrapper.data('key1'),
				key2: $wrapper.data('key2'),
				count: postsCount,
				search_type: $wrapper.data('searchtype')
			};
		$loadmore.addClass('loading');
		$.post(acas4uAjax.ajaxurl, formdata, function(response){
			if (response.success) {
				var html_large = response.data.html['large'],
					html_small = response.data.html['small'],
					page = response.data.page,
					current = page * postsCount;
				$loadmore.removeClass('loading');
				$searchResultsTableBody.append(html_large);
				$searchResultsMobileTableBody.append(html_small);
				$searchResultsTable.trigger('update');
				$wrapper.data('page', page);
				$wrapper.attr('data-page', page);
				//console.log('page ' + page);
				if (totalPosts <= current) {
					$button.css('display', 'none');
				}
			}
		}, 'json');

	});

});
