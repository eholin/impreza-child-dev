jQuery(document).ready(function($){
	'use strict';

	$('.acas4u-update-artist-ids').on('click', function(e){
		var $this = $(this),
			$mainWrapper = $this.closest('.acas4u-metabox-wrapper'),
			$thisWrapper = $this.closest('.acas4u-update-artist-wrapper'),
			$artistEnIDInput = $mainWrapper.find('#download_artist1id'),
			$artistSpIDInput = $mainWrapper.find('#download_artist_spotify_id'),
			artistEnID = $this.data('enid'),
			artistSpID = $this.data('spid');
		$artistEnIDInput.val(artistEnID);
		$artistSpIDInput.val(artistSpID);
		$thisWrapper.slideUp('fast');
	});

	$('.acas4u-update-song-ids').on('click', function(e){
		var $this = $(this),
			$mainWrapper = $this.closest('.acas4u-metabox-wrapper'),
			$thisWrapper = $this.closest('.acas4u-update-song-wrapper'),
			$thisLi = $this.closest('.acas4u-update-song-li'),
			$songEnIDInput = $mainWrapper.find('#download_echonestid'),
			$songSpIDInput = $mainWrapper.find('#download_spotifyid'),
			$songASContainer = $thisLi.find('.acas4u-api-info-container'),
			audioSummaryJson = $songASContainer.html(),
			songEnID = $this.data('enid'),
			songSpID = $this.data('spid'),
			audioSummary = jQuery.parseJSON(audioSummaryJson);
		$songEnIDInput.val(songEnID);
		$songSpIDInput.val(songSpID);
		$thisWrapper.slideUp('fast');
		if (audioSummary) {
			$.each(audioSummary, function(key, value){
				var $inputKey = $mainWrapper.find('#' + key);
				$inputKey.val(value);
			});
		}
	});

	$('#acas4u-get-enapi-track-info').on('click', function(e){
		var $button = $(this),
			$metabox = $button.closest('.acas4u-metabox-wrapper'),
			$container = $metabox.find('.acas4u-get-enapi-container'),
			vars = {
				action: 'do_enapi_track_data',
				_ajax_nonce: $button.data('nonce'),
				track_enid: $button.data('enid')
			};

		$.post(acas4uAjax.ajaxurl, vars, function(response){
			if (response.success) {
				var data = response.data,
					audioSummary = response.data['api_info'];
				if (audioSummary) {
					$.each(audioSummary, function(key, value){
						var $inputKey = $metabox.find('#' + key);
						$inputKey.val(value);
					});
				}

			}
		});

	});

});
