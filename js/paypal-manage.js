jQuery(document).ready(function($){
	'use strict';

	$(".acas4u-backend-datepicker").datepicker({
		onSelect: function(){
			var inputID = $(this).attr('id'),
				inputValue = $(this).val().split('/'),
				targetValue = inputValue[2] + '-' + inputValue[0] + '-' + inputValue[1];
			$('.' + inputID).val(targetValue);
		}
	});


});
