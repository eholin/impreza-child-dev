(function($){
	$.toggleShowPassword = function(options){
		var settings = $.extend({
			field: "#password",
			control: "#toggle_show_password",
		}, options);

		var control = $(settings.control),
			field = $(settings.field);

		control.bind('click', function(){
			if (control.is(':checked')) {
				field.attr('type', 'text');
			} else {
				field.attr('type', 'password');
			}
		})
	};
}(jQuery));

jQuery(document).ready(function($){
	'use strict';

	var modalElements = $('.modal-overlay, .modal');

	function adBlockDetected(){
		modalElements.addClass('active');
	}

	$('.close-modal').click(function(){
		modalElements.removeClass('active');
	});

	// Recommended audit because AdBlock lock the file 'blockadblock.js'
	// If the file is not called, the variable does not exist 'blockAdBlock'
	// This means that AdBlock is present
	if (typeof blockAdBlock === 'undefined') {
		adBlockDetected();
	} else {
		blockAdBlock.onDetected(adBlockDetected);
	}


	$('.acas4u-highlignt').on('click', function(){
		$(this).fadeOut('slow');
	});

	var $logTable = $('.acas4u-log-table'),
		$exploreArtistsTable = $('#explore-artists-table');

	$('.acas4u-agree').on('click', function(e){
		e.preventDefault();
		var $button = $(this),
			$container = $button.closest('.acas4u-register-container'),
			$btnWrapper = $container.find('.acas4u-preregister-buttons-wrapper'),
			$formWrapper = $container.find('.acas4u-register-form');
		$btnWrapper.css('display', 'none');
		$formWrapper.css('display', 'table');
	});

	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#" + tab_id).addClass('current');
	});

	$logTable.tablesorter();

	$exploreArtistsTable.tablesorter({
		headers: {
			0: {
				sorter: false
			}
		},
		textExtraction: {
			'.td-rating': function(node, table, cellIndex){
				return $(node).data('rating');
			}
		}
	});

	$exploreArtistsTable.stacktable();
	$('.acas4u-userlogs-upload').stacktable();
	$('.acas4u-userlogs-download').stacktable();

	$('.acas4u-show-more-explore-artists').on('click', function(){
		var $button = $(this),
			$wrapper = $button.closest('.acas4u-explore-artists-wrapper'),
			$loadmore = $wrapper.find('.g-loadmore'),
			totalPosts = $wrapper.data('total'),
			postsCount = $wrapper.data('count'),
			$searchResultsTable = $wrapper.find('.acas4u-search-results.large-only'),
			$searchResultsTableBody = $wrapper.find('.acas4u-search-results.large-only tbody'),
			$searchResultsMobileTableBody = $wrapper.find('.acas4u-search-results.small-only tbody'),
			data = {
				_ajax_nonce: $wrapper.data('nonce'),
				action: 'do_explore_artists_scroll',
				page: $wrapper.data('page'),
				index: $wrapper.data('index'),
				count: postsCount
			};
		$loadmore.addClass('loading');
		$.post(acas4uAjax.ajaxurl, data, function(response){
			if (response.success) {
				var html_large = response.data.html['large'],
					html_small = response.data.html['small'],
					page = response.data.page,
					current = page * postsCount;
				$loadmore.removeClass('loading');
				$searchResultsTableBody.append(html_large);
				$searchResultsMobileTableBody.append(html_small);
				$searchResultsTable.trigger('update');
				$wrapper.data('page', page);
				if (totalPosts <= current) {
					$button.css('display', 'none');
				}
			}
		}, 'json');

	});


	var $loaderContainer = $(document).find('.acas4u-load-acapellas-list');
	if ($loaderContainer.length) {
		var ajaxData = {
			_ajax_nonce: $loaderContainer.data('nonce'),
			action: 'show_our_acapellas_list',
			count: $loaderContainer.data('count'),
			order: $loaderContainer.data('order'),
			orderby: $loaderContainer.data('orderby'),
			links: $loaderContainer.data('links')
		};

		$.ajax({
			method: 'POST',
			url: acas4uAjax.ajaxurl,
			data: ajaxData,
			dataType: 'json',
			success: function(response){
				if (response.success) {
					var acapellasList = response.data.html;
					$loaderContainer.html(acapellasList);
				}
			}
		});

	}

	/*
	 var $registerForm = $(document).find('#acas4u-register-form'),
	 $showPassword = $registerForm.find('#acas4u-regform-show-password');
	 $showPassword.on('click', function(){
	 if ($(this).is(':checked')) {
	 $registerForm.find('#acas4u-register-form').attr('type', 'text');
	 } else {
	 $registerForm.find('#acas4u-register-form').attr('type', 'password');
	 }
	 });
	 */

	$.toggleShowPassword({
		field: '.acas4u-reg-password',
		control: '#acas4u-regform-show-password'
	});

});


