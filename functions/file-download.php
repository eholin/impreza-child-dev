<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/* template redirect to file download */
add_action( 'template_redirect', 'acas4u_mp3_download_template_redirect' );
function acas4u_mp3_download_template_redirect() {
	global $_SERVER, $wp_query, $wpdb;

	$url = explode( "/", substr( $_SERVER ['REQUEST_URI'], 1 ) );
	$user_id = get_current_user_id();

	if ( $url[0] == 'mp3-download' AND $user_id ) {
		$download_path = $url[1];
		$filename = $url[2];
		$acas4u_upload_dir = wp_upload_dir();
		$file_path = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . '/' . $filename;

		if ( file_exists( $file_path ) ) {
			if ( $wp_query->is_404 ) {
				$wp_query->is_404 = FALSE;
			}

			$post_id = acas4u_get_post_id_by_filename( $filename );

			$filehash = hash_file( 'sha256', $file_path );
			$download_unmetered = acas4u_is_download_unmetered( $post_id );

			$max_downloads = acas4u_get_user_max_downloads( $user_id );
			$bonus_downloads = acas4u_get_user_bonus_downloads( $user_id );

			$prev_downloaded = acas4u_get_item_previously_downloaded( $user_id, $filehash );

			if ( ! $download_unmetered AND ! $prev_downloaded ) {
				if ( $bonus_downloads > 0 ) {
					$bonus_downloads = $bonus_downloads - 1;
					$new_credits = $max_downloads;
				} else {
					$new_credits = $max_downloads - 1;
				}
				update_user_meta( $user_id, '_max_credits', $new_credits );
				update_user_meta( $user_id, '_bonus_credits', $bonus_downloads );
			} else {
				$new_credits = $max_downloads;
			}

			if ( $prev_downloaded === FALSE ) {
				$wpdb->insert( 'phpbb_download_logs', array(
					'user_id' => $user_id,
					'filehash' => $filehash,
					'bonus' => $bonus_downloads,
					'prev_credits' => $max_downloads,
					'new_credits' => $new_credits,
					'completed' => 'yes',
					'request_type' => 'GET',
					'request_url' => $_SERVER ['REQUEST_URI'],
					'timestamp' => date( 'Y-m-d H:i:s' ),
				), array(
					'%d',
					'%s',
					'%d',
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
				) );
			}

			//acas4u_write_log_entry( 'File ' . $filename . ' was downloaded now, previously downloaded: ' . $prev_downloaded, 'acas4u_downloaded_files.log' );

			header( "HTTP/1.1 200 OK" );
			header( 'Pragma: public' );   // required
			header( 'Expires: 0' );       // no cache
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s', filemtime( $file_path ) ) . ' GMT' );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Content-Length: ' . filesize( $file_path ) );    // provide file size
			header( 'Cache-Control: private', FALSE );
			header( 'Content-Type: octet/stream' );
			header( 'Content-Disposition: attachment; filename="' . basename( $file_path ) . '"' );
			header( 'Connection: close' );
			set_time_limit( 0 );
			@readfile( $file_path );
			exit;
		}
	} else {
		// include template with message about non existing file
	}
}

add_action( 'wp_ajax_do_download_link', 'acas4u_do_download_link' );
function acas4u_do_download_link() {

	check_ajax_referer( 'acas4u_do_download_link', '_ajax_nonce' );

	$post_id = $_POST['post_id'];
	$user_id = $_POST['user_id'];
	$filename = get_post_meta( $post_id, '_download_filename', TRUE );
	$download_path = get_post_meta( $post_id, '_download_path', TRUE );
	if ( ! $filename ) {
		$filename = get_the_title( $post_id );
	}
	$home_url = esc_url( home_url( '/' ) );

	$max_downloads = acas4u_get_user_max_downloads( $user_id );
	$bonus_downloads = acas4u_get_user_bonus_downloads( $user_id );

	$downloads_remaining = $max_downloads . ' (' . $bonus_downloads . ' bonus) downloads remaining';

	wp_send_json_success( array(
		'url' => $home_url . 'mp3-download/' . $download_path . $filename,
		'remaining' => $downloads_remaining,
	) );
}

add_action( 'wp_ajax_do_update_downloads_remaining', 'acas4u_do_update_downloads_remaining' );
function acas4u_do_update_downloads_remaining() {

	check_ajax_referer( 'acas4u_do_download_link', '_ajax_nonce' );

	$post_id = $_POST['post_id'];
	$user_id = $_POST['user_id'];

	$max_downloads = acas4u_get_user_max_downloads( $user_id );
	$bonus_downloads = acas4u_get_user_bonus_downloads( $user_id );

	$downloads_remaining = $max_downloads . ' (' . $bonus_downloads . ' bonus) downloads remaining';

	wp_send_json_success( array(
		'remaining' => $downloads_remaining,
	) );
}

function acas4u_get_item_previously_downloaded( $user_id, $filehash ) {
	global $wpdb;

	$log_id = intval( $wpdb->get_var( 'SELECT log_id FROM phpbb_download_logs WHERE user_id="' . $user_id . '" AND filehash="' . $filehash . '"' ) );
	$prev_downloaded = ( $log_id > 0 ) ? TRUE : FALSE;

	//acas4u_write_log_entry( 'Log ID: ' . $log_id, 'acas4u_downloaded_files.log' );

	return $prev_downloaded;
}
