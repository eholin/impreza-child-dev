<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
function get_latest_downloads( $count ) {
	$output = '';
	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => 'DESC',
		'orderby' => 'date',
	);
	$latest_downloads = new WP_Query( $args );
	if ( $latest_downloads->have_posts() ) {
		$output .= '<div class="acapella-latest-wrapper">';
		$output .= '<div class="acapella-latest-row">';
		$output .= '<div class="lt-cell">Title</div>';
		$output .= '<div class="lt-cell">Added</div>';
		$output .= '<div class="lt-cell">Listeners</div>';
		$output .= '</div>';
		while ( $latest_downloads->have_posts() ){
			$latest_downloads->the_post();
			$post_id = $latest_downloads->post->ID;
			$permalink = get_the_permalink( $post_id );
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
			$download_count = get_post_meta( $post_id, '_download_count', TRUE );

			$date_uploaded = get_the_date( 'd/m/Y' );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$output .= '<div class="acapella-latest-row">';
			$output .= '<div class="latest-post-title lt-cell"><a href="' . $permalink . '" data-id="' . $post_id . '">' . $title;
			if ( $download_duration ) {
				$output .= ' (' . acas4u_format_duration( $download_duration ) . ')';
			}
			$output .= '</a>';
			$output .= '</div>';
			if ( $download_count > 0 ) {
				number_format( $download_count, 0, ',', ',' );
			} else {
				$download_count = 0;
			}
			$output .= '<div class="latest-post-added lt-cell" title="Date added">' . $date_uploaded . '</div>';
			$output .= '<div class="latest-post-downloads lt-cell" title="Total listeners">' . $download_count . '</div>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();

	return $output;
}

function get_latest_downloads_with_images_from_meta( $count ) {
	global $acas4u_stylesheet_directory_uri;

	$output = '';
	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => 'DESC',
		'orderby' => 'date',
	);
	$posts = new WP_Query( $args );
	if ( $posts->have_posts() ) {
		$output .= '<div class="acas4u-fp-latest-wrapper">';
		while ( $posts->have_posts() ){
			$posts->the_post();
			$post_id = $posts->post->ID;
			$permalink = get_the_permalink();
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );
			$filename = get_post_meta( $post_id, '_download_filename', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
			if ( $artist_thumbnail_id ) {
				$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
				$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
			} else {
				$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
			}
			$artist_thumbnail_img = '<img src="' . $artist_thumbnail_url . '" alt="' . $title . '>" class="img-responsive">';

			$short_title = acas4u_limit_chars( $title, 40 );
			$output .= '<div class="acas4u-fp-latest-single" title="' . $filename . '">';
			$output .= '<a class="acas4u-fp-latest-link" href="' . $permalink . '">';
			$output .= '<div class="acas4u-fp-latest-image">' . $artist_thumbnail_img . '</div>';
			$output .= '<div class="acas4u-fp-latest-inner">';
			$output .= '<div class="acas4u-fp-latest-overlay"></div>';
			$output .= '<div class="acas4u-fp-latest-title">' . $short_title . '</div>';
			$output .= '</div>';
			$output .= '</a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();

	return $output;
}
