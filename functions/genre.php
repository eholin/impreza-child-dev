<?php

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'acas4u_create_genre_taxonomy', 0 );
function acas4u_create_genre_taxonomy() {
	$labels = array(
		'name' => _x( 'Genres', 'taxonomy general name' ),
		'singular_name' => _x( 'Genre', 'taxonomy singular name' ),
		'search_items' => __( 'Search Genres' ),
		'popular_items' => __( 'Popular Genres' ),
		'all_items' => __( 'All Genres' ),
		'parent_item' => NULL,
		'parent_item_colon' => NULL,
		'edit_item' => __( 'Edit Genre' ),
		'update_item' => __( 'Update Genre' ),
		'add_new_item' => __( 'Add New Genre' ),
		'new_item_name' => __( 'New Genre Name' ),
		'separate_items_with_commas' => __( 'Separate genres with commas' ),
		'add_or_remove_items' => __( 'Add or remove genres' ),
		'choose_from_most_used' => __( 'Choose from the most used genres' ),
		'not_found' => __( 'No genres found.' ),
		'menu_name' => __( 'Genres' ),
	);

	$args = array(
		'hierarchical' => FALSE,
		'labels' => $labels,
		'show_ui' => TRUE,
		'show_admin_column' => TRUE,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => TRUE,
		'rewrite' => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'genre', NULL, $args );
}

function acas4u_show_downloads_by_genre( $args ) {
	global $acas4u_stylesheet_directory_uri;

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {

		$output = '';
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();
			$permalink = get_the_permalink();
			$author = get_the_author();

			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );
			$filename = get_post_meta( $post_id, '_download_filename', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
			if ( $artist_thumbnail_id ) {
				$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
				$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
			} else {
				$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
			}

			$download_count = get_post_meta( $post_id, '_download_count', TRUE );
			$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE ); // use 'tempo' instead of 'bpm'
			$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			$date_uploaded = get_the_date();

			$download_comments = get_comments_number( 'No comments', 'One comment', '% comments' );
			if ( $download_comments == 0 ) {
				$comments = __( 'No Comments' );
			} elseif ( $download_comments > 1 ) {
				$comments = $download_comments . __( ' Comments' );
			} else {
				$comments = __( '1 Comment' );
			}
			$output_comments = '<a href="' . get_comments_link() . '">' . $comments . '</a>';

			$output .= '<div class="acas4u-downloads-by-genre">';

			$output .= '<div class="acas4u-dg-artist">';

			$output .= '<div class="acas4u-dg-artist-image">';
			$output .= '<img src="' . $artist_thumbnail_url . '" alt="' . $title . '" class="img-responsive">';
			$output .= '</div>'; // .acas4u-dg-artist-image

			$output .= '<div class="acas4u-dg-artist-info">';
			$output .= '<a class="acas4u-dg-title" href="' . $permalink . '">' . $title . '</a>';
			$output .= '<div class="acas4u-dg-filename">File: ' . $filename . '</div>';

			$output .= '<div class="acas4u-dg-rating">' . acas4u_user_ratings( TRUE ) . '</div>';

			$output .= '</div>'; // .acas4u-dg-artist-info

			$output .= '</div>'; // .acas4u-dg-artist

			$output .= '<div class="acas4u-dg-meta">';
			$output .= '<div class="dg-item-wrapper"><i class="fa fa-user"></i> <a title="Uploaded by: ' . $author . '" href="' . home_url( '/user/' . $author . '/' ) . '">' . $author . '</a></div>';
			$output .= '<div class="dg-item-wrapper"><i class="fa fa-download"></i>	<strong>' . number_format( $download_count, 0, ',', ',' ) . '</strong> downloads</div>';
			$output .= '<div class="dg-item-wrapper"><i class="fa fa-comments-o"></i> ' . $output_comments . '</div>';
			$output .= '<div class="dg-item-wrapper"><i class="fa fa-music"></i> <strong>' . $download_tempo . '</strong> BPM <strong>' . $download_bitrate . '</strong> kbps</div>';
			$output .= '<div class="dg-item-wrapper"><i class="fa fa-calendar"></i> <strong>' . $date_uploaded . '</strong></div>';
			$output .= '</div>'; // .acas4u-dg-meta

			$output .= '</div>'; // .acas4u-downloads-by-genre
		}
	} else {
		$output = __( 'No posts were found.', 'us' );
	}
	wp_reset_query();

	return $output;
}

/**
 * Ajax for infinite scroll
 */
add_action( 'wp_ajax_do_genre_scroll', 'acas4u_do_genre_scroll' );
add_action( 'wp_ajax_nopriv_do_scroll', 'acas4u_do_genre_scroll' );
function acas4u_do_genre_scroll() {
	check_ajax_referer( 'acas4u_do_genre_scroll_nonce', '_ajax_nonce' );

	$default_posts_per_page = get_option( 'posts_per_page' );

	$page = $_POST['page'];
	if ( $page ) {
		$offset = $page * 10;
	} else {
		wp_send_json_error();
	}

	$genre = $_POST['genre'];

	$args = array(
		'post_type' => 'download',
		'order' => 'DESC',
		'orderby' => 'date',
		'post_status' => 'publish',
		'posts_per_page' => $default_posts_per_page,
		'offset' => $offset,
		'tax_query' => array(
			array(
				'taxonomy' => 'genre',
				'field' => 'slug',
				'terms' => $genre,
			),
		),
	);

	$search_results = acas4u_show_downloads_by_genre( $args );

	wp_send_json_success( array(
		'html' => $search_results,
		'page' => $page + 1,
	) );
}

add_action( 'init', 'genres_list_rewrite' );
function genres_list_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%genres%', '([^&]+)' );
	add_rewrite_rule( '^genres/?', 'index.php?pagetype=genres-list', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	//$wp_rewrite->flush_rules();
}

add_action( 'template_redirect', 'genres_list_redirect' );
function genres_list_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'genres-list' ) {

		include( $acas4u_stylesheet_directory . '/templates/genres-list.php' );
		exit;
	}
}

add_filter( 'wp_title', 'genres_list_title' );
function genres_list_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'genres-list' ) {
		$title = 'All Acapellas Genres';
	}

	return $title;
}
