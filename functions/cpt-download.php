<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * declare custom post type - download
 */
add_action( 'init', 'acas4u_custom_post_download', 15 );
function acas4u_custom_post_download() {
	$labels = array(
		'name' => _x( 'Downloads', 'post type general name', 'acapellas4u' ),
		'singular_name' => _x( 'Download', 'post type singular name', 'acapellas4u' ),
		'add_new' => _x( 'Add New', 'download', 'acapellas4u' ),
		'add_new_item' => __( 'Add New Download', 'acapellas4u' ),
		'edit_item' => __( 'Edit Download', 'acapellas4u' ),
		'new_item' => __( 'New Download', 'acapellas4u' ),
		'all_items' => __( 'All Downloads', 'acapellas4u' ),
		'view_item' => __( 'View Download', 'acapellas4u' ),
		'search_items' => __( 'Search Downloads', 'acapellas4u' ),
		'not_found' => __( 'No Downloads found', 'acapellas4u' ),
		'not_found_in_trash' => __( 'No Downloads found in the Trash', 'acapellas4u' ),
		'parent_item_colon' => '',
		'menu_name' => __( 'Downloads', 'acapellas4u' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Create a download file', 'acapellas4u' ),
		'menu_icon' => 'dashicons-media-audio',
		'public' => TRUE,
		'show_in_nav_menus' => FALSE,
		'exclude_from_search' => FALSE,
		'supports' => array( 'title', 'editor', 'page-attributes', 'thumbnail', 'author', 'comments' ),
		'has_archive' => TRUE,
		'taxonomies' => array(
			'genre',
		),
	);
	register_post_type( 'download', $args );
}

/** -------------------------------------------------------------------------------------------------
 * Adds a meta box with an information from Spotify and EchoNest APIs
 * --------------------------------------------------------------------------------------------------
 */
add_action( 'add_meta_boxes', 'acas4u_webapi_information' );
function acas4u_webapi_information() {
	add_meta_box( 'acas4u_webapi_information', __( 'Information from Spotify and EchoNest APIs', 'acapellas4u' ), 'acas4u_webapi_information_box_content', 'download', 'normal', 'default' );
}

function acas4u_webapi_information_box_content( $post ) {
	global $en_api_key, $wpdb;

	/* track meta data */

	$download_artist1 = get_post_meta( $post->ID, '_download_artist1', TRUE );
	$download_artist1id = get_post_meta( $post->ID, '_download_artist1id', TRUE );
	$download_artist2 = get_post_meta( $post->ID, '_download_artist2', TRUE );
	$download_artist2id = get_post_meta( $post->ID, '_download_artist2id', TRUE );
	$download_trackname = get_post_meta( $post->ID, '_download_trackname', TRUE );
	$download_echonestid = get_post_meta( $post->ID, '_download_echonestid', TRUE );
	$download_spotifyid = get_post_meta( $post->ID, '_download_spotifyid', TRUE );

	$acas4u_upload_dir = wp_upload_dir();
	$download_filename = get_post_meta( $post->ID, '_download_filename', TRUE );
	$download_path = get_post_meta( $post->ID, '_download_path', TRUE );
	if ( $download_path ) {
		$filename = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . $download_filename;
	} else {
		$filename = $acas4u_upload_dir['basedir'] . '/tmpacapellas/' . $download_filename;
	}

	/*
	$download_audio_md5 = get_post_meta( $post->ID, '_download_audio_md5', TRUE );
	if ( $download_audio_md5 == '' ) {
		if ( file_exists( $file_path ) ) {
			$download_audio_md5 = md5_file( $filename );
		}
	}
	*/

	$download_samplerate = get_post_meta( $post->ID, '_download_samplerate', TRUE );
	if ( ! $download_samplerate ) {
		$download_samplerate = acas4u_get_file_samplerate( $filename );
	}

	$download_bitrate = get_post_meta( $post->ID, '_download_bitrate', TRUE );
	if ( $download_bitrate == '' ) {
		$download_bitrate = acas4u_get_file_bitrate( $filename );
	}

	$download_tempo = get_post_meta( $post->ID, '_download_tempo', TRUE );
	$download_mode = get_post_meta( $post->ID, '_download_mode', TRUE );

	$download_time_signature = get_post_meta( $post->ID, '_download_time_signature', TRUE );
	$download_duration = get_post_meta( $post->ID, '_download_duration', TRUE );
	$download_keymode_id = get_post_meta( $post->ID, '_download_keymode_id', TRUE );

	$download_energy = get_post_meta( $post->ID, '_download_energy', TRUE );
	$download_loudness = get_post_meta( $post->ID, '_download_loudness', TRUE );
	$download_danceability = get_post_meta( $post->ID, '_download_danceability', TRUE );

	$download_speechness = get_post_meta( $post->ID, '_download_speechness', TRUE );
	$download_acousticness = get_post_meta( $post->ID, '_download_acousticness', TRUE );
	$download_liveness = get_post_meta( $post->ID, '_download_liveness', TRUE );

	$download_instrumentalness = get_post_meta( $post->ID, '_download_instrumentalness', TRUE );

	/* artist meta data */

	$download_artist_id = get_post_meta( $post->ID, '_download_artist_id', TRUE ); // only first artist

	if ( $download_artist_id == '' AND $download_artist1id != '' ) {
		$download_artist_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_download_artist_enid' AND meta_value='" . $download_artist1id . "';" ) );
	}

	if ( $download_artist_id ) {
		$download_artist_spotify_id = get_post_meta( $download_artist_id, '_download_artist_spotify_id', TRUE );
	}

	wp_nonce_field( 'acas4u_webapi_information_save', 'acas4u_webapi_information_box_content_nonce' );

	$output = '<div class="acas4u-metabox-wrapper">';
	$output .= '<input name="download_artist_id" type="hidden" value="' . esc_attr( $download_artist_id ) . '">';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_artist1"><strong>' . __( 'Artist #1 name:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_artist1" id="download_artist1" class="widefat" type="text" value="' . esc_attr( $download_artist1 ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_artist2"><strong>' . __( 'Artist #2 name:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_artist2" id="download_artist2" class="widefat" type="text" value="' . esc_attr( $download_artist2 ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_trackname"><strong>' . __( 'Track title:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_trackname" id="download_trackname" class="widefat" type="text" value="' . esc_attr( $download_trackname ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_artist1id"><strong>' . __( 'Artist #1 EchoNest ID:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_artist1id" id="download_artist1id" class="widefat" type="text" value="' . esc_attr( $download_artist1id ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_artist2id"><strong>' . __( 'Artist #2 EchoNest ID:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_artist2id" id="download_artist2id" class="widefat" type="text" value="' . esc_attr( $download_artist2id ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_echonestid"><strong>' . __( 'Track EchoNest ID:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_echonestid" id="download_echonestid" class="widefat" type="text" value="' . esc_attr( $download_echonestid ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_artist_spotify_id"><strong>' . __( 'Artist #1 Spotify ID:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_artist_spotify_id" id="download_artist_spotify_id" class="widefat" type="text" value="' . esc_attr( $download_artist_spotify_id ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_spotifyid"><strong>' . __( 'Track Spotify ID:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_spotifyid" id="download_spotifyid" class="widefat" type="text" value="' . esc_attr( $download_spotifyid ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	/*
	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_audio_md5"><strong>' . __( 'Audio MD5 hash:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_audio_md5" id="download_audio_md5" class="widefat" type="text" value="' . esc_attr( $download_audio_md5 ) . '">';
	$output .= '</div>';
	$output .= '</div>';
	*/

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	if ( $download_artist1 != '' AND $download_artist1id == '' ) {
		$url = 'http://developer.echonest.com/api/v4/artist/search?api_key=' . $en_api_key . '&name=' . urlencode( $download_artist1 ) . '&format=json&bucket=id:spotify';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			$artists = $json->response->artists;
			$output .= '<div class="acas4u-update-artist-wrapper">';
			$output .= '<p><strong>Update Artist IDs from EchoNest and Spotify APIs</strong></p>';
			$output .= '<ul>';
			foreach ( $artists as $artist ) {
				$foreign_ids = $artist->foreign_ids;
				foreach ( $foreign_ids as $foreign_id ) {
					if ( strstr( $foreign_id->foreign_id, 'spotify' ) ) {
						$spotify_arr = explode( ':', $foreign_id->foreign_id );
						if ( ! empty( $spotify_arr[2] ) ) {
							$spotify_artist_id = $spotify_arr[2];
						}
					}
				}
				if ( $spotify_artist_id != '' AND $artist->id != '' ) {
					$output .= '<li><a href="javascript:void(0);" class="acas4u-update-artist-ids" data-enid="' . $artist->id . '" data-spid="' . $spotify_artist_id . '">' . $artist->name . '</a></li>';
				}
			}
			$output .= '</ul>';
			$output .= '</div>';
		}
	}
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	if ( $download_trackname != '' AND ( $download_echonestid == '' AND $download_spotifyid == '' ) ) {
		$url = 'http://developer.echonest.com/api/v4/song/search?api_key=' . $en_api_key . '&format=json&artist=' . urlencode( $download_artist1 ) . '&title=' . urlencode( $download_trackname ) . '&bucket=id:spotify&bucket=audio_summary&bucket=tracks';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			$songs = $json->response->songs;
			$output .= '<div class="acas4u-update-song-wrapper">';
			if ( $songs ) {
				$output .= '<p><strong>Update Track ID from EchoNest and Spotify API</strong></p>';
				$output .= '<ul>';
				foreach ( $songs as $song ) {
					$tracks = $song->tracks[0];
					if ( $tracks->catalog == 'spotify' ) {
						$spotify_id_arr = explode( ':', $tracks->foreign_id );
					}
					if ( $tracks->id AND $spotify_id_arr[2] ) {
						$audio_summary = $song->audio_summary;
						if ( $audio_summary ) {
							$api_info['download_duration'] = $audio_summary->duration;
							$api_info['download_tempo'] = $audio_summary->tempo;
							$api_info['download_time_signature'] = $audio_summary->time_signature;
							$api_info['download_mode'] = $audio_summary->mode;
							$api_info['download_keymode_id'] = $audio_summary->key;
							$api_info['download_loudness'] = $audio_summary->loudness;
							$api_info['download_speechness'] = $audio_summary->speechiness;
							$api_info['download_danceability'] = $audio_summary->danceability;
							$api_info['download_energy'] = $audio_summary->energy;
							$api_info['download_acousticness'] = $audio_summary->acousticness;
							$api_info['download_liveness'] = $audio_summary->liveness;
							$api_info['download_instrumentalness'] = $audio_summary->instrumentalness;
						}
						$output .= '<li class="acas4u-update-song-li"><a href="javascript:void(0);" class="acas4u-update-song-ids" data-enid="' . $tracks->id . '" data-spid="' . $spotify_id_arr[2] . '">' . $song->artist_name . ' - ' . $song->title . '</a>';
						if ( $api_info ) {
							$output .= '<div class="acas4u-api-info-container">' . json_encode( $api_info ) . '</div>';
						}
						$output .= '</li>';
					}
				}
				$output .= '</ul>';
			} else {
				$output .= '<p>No songs for artist "' . $download_artist1 . '" with name "' . $download_trackname . '" found in EchoNest API.</p>';
			}
			$output .= '</div>';
		}
	}
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<hr>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_samplerate"><strong>' . __( 'Samplerate:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_samplerate" id="download_samplerate" class="widefat" type="text" value="' . esc_attr( $download_samplerate ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_bitrate"><strong>' . __( 'Bitrate:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_bitrate" id="download_bitrate" class="widefat" type="text" value="' . esc_attr( $download_bitrate ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_tempo"><strong>' . __( 'Tempo:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_tempo" id="download_tempo" class="widefat" type="text" value="' . esc_attr( $download_tempo ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_mode"><strong>' . __( 'Mode:', 'acapellas4u' ) . '</strong></label>';
	//$output .= '<input name="download_mode" id="download_mode" class="widefat" type="text" value="' . esc_attr( $download_mode ) . '">';
	$output .= '<select name="download_mode" id="download_mode" class="widefat">';
	$output .= '<option value="">No mode</option>';
	$output .= '<option value="0" "' . selected( '0', $download_mode, FALSE ) . '">Minor</option>';
	$output .= '<option value="1" "' . selected( '1', $download_mode, FALSE ) . '">Major</option>';
	$output .= '<select>';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_time_signature"><strong>' . __( 'Time signature:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_time_signature" id="download_time_signature" class="widefat" type="text" value="' . esc_attr( $download_time_signature ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_duration"><strong>' . __( 'Duration:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_duration" id="download_duration" class="widefat" type="text" value="' . esc_attr( $download_duration ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_keymode_id"><strong>' . __( 'Key:', 'acapellas4u' ) . '</strong></label>';
	//$output .= '<input name="download_keymode_id" id="download_keymode_id" class="widefat" type="text" value="' . esc_attr( $download_keymode_id ) . '">';
	$output .= '<select name="download_keymode_id" id="download_keymode_id" class="widefat">';
	$output .= '<option value="">No key</option>';
	$output .= '<option value="0" "' . selected( '0', $download_keymode_id, FALSE ) . '">C</option>';
	$output .= '<option value="1" "' . selected( '1', $download_keymode_id, FALSE ) . '">C#</option>';
	$output .= '<option value="2" "' . selected( '2', $download_keymode_id, FALSE ) . '">D</option>';
	$output .= '<option value="3" "' . selected( '3', $download_keymode_id, FALSE ) . '">Eb</option>';
	$output .= '<option value="4" "' . selected( '4', $download_keymode_id, FALSE ) . '">E</option>';
	$output .= '<option value="5" "' . selected( '5', $download_keymode_id, FALSE ) . '">F</option>';
	$output .= '<option value="6" "' . selected( '6', $download_keymode_id, FALSE ) . '">F#</option>';
	$output .= '<option value="7" "' . selected( '7', $download_keymode_id, FALSE ) . '">G</option>';
	$output .= '<option value="8" "' . selected( '8', $download_keymode_id, FALSE ) . '">Ab</option>';
	$output .= '<option value="9" "' . selected( '9', $download_keymode_id, FALSE ) . '">A</option>';
	$output .= '<option value="10" "' . selected( '10', $download_keymode_id, FALSE ) . '">Bb</option>';
	$output .= '<option value="11" "' . selected( '11', $download_keymode_id, FALSE ) . '">B</option>';
	$output .= '<select>';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_energy"><strong>' . __( 'Energy:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_energy" id="download_energy" class="widefat" type="text" value="' . esc_attr( $download_energy ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_loudness"><strong>' . __( 'Loudness:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_loudness" id="download_loudness" class="widefat" type="text" value="' . esc_attr( $download_loudness ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_danceability"><strong>' . __( 'Danceability:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_danceability" id="download_danceability" class="widefat" type="text" value="' . esc_attr( $download_danceability ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_speechness"><strong>' . __( 'Speechness:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_speechness" id="download_speechness" class="widefat" type="text" value="' . esc_attr( $download_speechness ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	$output .= '<div class="row">';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_acousticness"><strong>' . __( 'Acousticness:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_acousticness" id="download_acousticness" class="widefat" type="text" value="' . esc_attr( $download_acousticness ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_liveness"><strong>' . __( 'Liveness:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_liveness" id="download_liveness" class="widefat" type="text" value="' . esc_attr( $download_liveness ) . '">';
	$output .= '</div>';
	$output .= '<div class="col-lg-3">';
	$output .= '<label for="download_instrumentalness"><strong>' . __( 'Instrumentalness:', 'acapellas4u' ) . '</strong></label>';
	$output .= '<input name="download_instrumentalness" id="download_instrumentalness" class="widefat" type="text" value="' . esc_attr( $download_instrumentalness ) . '">';
	$output .= '</div>';
	$output .= '</div>';

	if ( $download_echonestid != '' ) {
		$ajax_nonce = wp_create_nonce( 'acas4u_get_enapi_track_info' );

		$output .= '<div class="row">';
		$output .= '<div class="col-lg-3 acas4u-get-enapi-wrapper">';
		$output .= '<a class="button button-primary button-large" id="acas4u-get-enapi-track-info" href="javascript:void(0);" data-nonce="' . $ajax_nonce . '" data-enid="' . $download_echonestid . '">Get info from EN API</a>';
		$output .= '</div>';
		$output .= '</div>';

		$output .= '<div class="row">';
		$output .= '<div class="col-lg-9 acas4u-get-enapi-container">';
		$output .= '</div>';
		$output .= '</div>';
	}

	$output .= '</div>';

	echo $output;
}

/* save information from meta box */

add_action( 'save_post', 'acas4u_download_information_save' );
function acas4u_download_information_save( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) AND DOING_AUTOSAVE ) {
		return;
	}

	if ( ! $_POST AND $_POST['post_type'] != 'download' ) {
		return;
	}

	if ( ! current_user_can( 'edit_page', $post_id ) ) {
		return;
	}

	if ( wp_verify_nonce( $_POST['acas4u_webapi_information_box_content_nonce'], 'acas4u_webapi_information_save' ) ) {

		if ( $_POST['download_artist1'] != '' ) {
			$download_artist1 = sanitize_text_field( $_POST['download_artist1'] );
		} else {
			$download_artist1 = '';
		}
		update_post_meta( $post_id, '_download_artist1', $download_artist1 );

		if ( $_POST['download_artist2'] != '' ) {
			$download_artist2 = sanitize_text_field( $_POST['download_artist2'] );
		} else {
			$download_artist2 = '';
		}
		update_post_meta( $post_id, '_download_artist2', $download_artist2 );

		if ( $_POST['download_trackname'] != '' ) {
			$download_trackname = sanitize_text_field( $_POST['download_trackname'] );
		} else {
			$download_trackname = '';
		}
		update_post_meta( $post_id, '_download_trackname', $download_trackname );

		if ( $_POST['download_artist1id'] != '' ) {
			$download_artist1id = sanitize_text_field( $_POST['download_artist1id'] );
		} else {
			$download_artist1id = '';
		}
		update_post_meta( $post_id, '_download_artist1id', $download_artist1id );

		if ( $_POST['download_artist2id'] != '' ) {
			$download_artist2id = sanitize_text_field( $_POST['download_artist2id'] );
		} else {
			$download_artist2id = '';
		}
		update_post_meta( $post_id, '_download_artist2id', $download_artist2id );

		if ( $_POST['download_echonestid'] != '' ) {
			$download_echonestid = sanitize_text_field( $_POST['download_echonestid'] );
		} else {
			$download_echonestid = '';
		}
		update_post_meta( $post_id, '_download_echonestid', $download_echonestid );

		if ( $_POST['download_artist_id'] != '' ) {
			$artist_post_id = $_POST['download_artist_id'];
			if ( $_POST['download_artist_spotify_id'] != '' ) {
				$download_artist_spotify_id = sanitize_text_field( $_POST['download_artist_spotify_id'] );
			} else {
				$download_artist_spotify_id = '';
			}
			update_post_meta( $artist_post_id, '_download_artist_spotify_id', $download_artist_spotify_id );
		}

		if ( $_POST['download_spotifyid'] != '' ) {
			$download_spotifyid = sanitize_text_field( $_POST['download_spotifyid'] );
		} else {
			$download_spotifyid = '';
		}
		update_post_meta( $post_id, '_download_spotifyid', $download_spotifyid );

		if ( $_POST['download_audio_md5'] != '' ) {
			$download_audio_md5 = sanitize_text_field( $_POST['download_audio_md5'] );
		} else {
			$download_audio_md5 = '';
		}
		update_post_meta( $post_id, '_download_audio_md5', $download_audio_md5 );

		if ( $_POST['download_samplerate'] != '' ) {
			$download_samplerate = sanitize_text_field( $_POST['download_samplerate'] );
		} else {
			$download_samplerate = '';
		}
		update_post_meta( $post_id, '_download_samplerate', $download_samplerate );

		if ( $_POST['download_bitrate'] != '' ) {
			$download_bitrate = sanitize_text_field( $_POST['download_bitrate'] );
		} else {
			$download_bitrate = '';
		}
		update_post_meta( $post_id, '_download_bitrate', $download_bitrate );

		if ( $_POST['download_tempo'] != '' ) {
			$download_tempo = sanitize_text_field( $_POST['download_tempo'] );
		} else {
			$download_tempo = '';
		}
		update_post_meta( $post_id, '_download_tempo', $download_tempo );

		if ( $_POST['download_mode'] != '' ) {
			$download_mode = sanitize_text_field( $_POST['download_mode'] );
		} else {
			$download_mode = '';
		}
		update_post_meta( $post_id, '_download_mode', $download_mode );

		if ( $_POST['download_time_signature'] != '' ) {
			$download_time_signature = sanitize_text_field( $_POST['download_time_signature'] );
		} else {
			$download_time_signature = '';
		}
		update_post_meta( $post_id, '_download_time_signature', $download_time_signature );

		if ( $_POST['download_duration'] != '' ) {
			$download_duration = sanitize_text_field( $_POST['download_duration'] );
		} else {
			$download_duration = '';
		}
		update_post_meta( $post_id, '_download_duration', $download_duration );

		if ( $_POST['download_keymode_id'] != '' ) {
			$download_keymode_id = sanitize_text_field( $_POST['download_keymode_id'] );
		} else {
			$download_keymode_id = '';
		}
		update_post_meta( $post_id, '_download_keymode_id', $download_keymode_id );

		if ( $_POST['download_energy'] != '' ) {
			$download_energy = sanitize_text_field( $_POST['download_energy'] );
		} else {
			$download_energy = '';
		}
		update_post_meta( $post_id, '_download_energy', $download_energy );

		if ( $_POST['download_loudness'] != '' ) {
			$download_loudness = sanitize_text_field( $_POST['download_loudness'] );
		} else {
			$download_loudness = '';
		}
		update_post_meta( $post_id, '_download_loudness', $download_loudness );

		if ( $_POST['download_danceability'] != '' ) {
			$download_danceability = sanitize_text_field( $_POST['download_danceability'] );
		} else {
			$download_danceability = '';
		}
		update_post_meta( $post_id, '_download_danceability', $download_danceability );

		if ( $_POST['download_speechness'] != '' ) {
			$download_speechness = sanitize_text_field( $_POST['download_speechness'] );
		} else {
			$download_speechness = '';
		}
		update_post_meta( $post_id, '_download_speechness', $download_speechness );

		if ( $_POST['download_acousticness'] != '' ) {
			$download_acousticness = sanitize_text_field( $_POST['download_acousticness'] );
		} else {
			$download_acousticness = '';
		}
		update_post_meta( $post_id, '_download_acousticness', $download_acousticness );

		if ( $_POST['download_liveness'] != '' ) {
			$download_liveness = sanitize_text_field( $_POST['download_liveness'] );
		} else {
			$download_liveness = '';
		}
		update_post_meta( $post_id, '_download_liveness', $download_liveness );

		if ( $_POST['download_instrumentalness'] != '' ) {
			$download_instrumentalness = sanitize_text_field( $_POST['download_instrumentalness'] );
		} else {
			$download_instrumentalness = '';
		}
		update_post_meta( $post_id, '_download_instrumentalness', $download_instrumentalness );
	}
}

add_action( 'wp_ajax_do_enapi_track_data', 'acas4u_get_enapi_track_info' );
function acas4u_get_enapi_track_info() {
	global $en_api_key;

	check_ajax_referer( 'acas4u_get_enapi_track_info', '_ajax_nonce' );

	$download_echonestid = $_POST['track_enid'];

	$track_details = 'error';

	if ( $download_echonestid != '' ) {
		$url = 'http://developer.echonest.com/api/v4/track/profile?api_key=' . $en_api_key . '&id=' . $download_echonestid . '&format=json&bucket=audio_summary';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			$audio_md5 = $json->response->track->audio_md5;
			$audio_summary = $json->response->track->audio_summary;
			if ( $audio_summary ) {
				$track_details = 'success';
				$api_info['audio_md5'] = $audio_md5;
				$api_info['download_duration'] = $audio_summary->duration;
				$api_info['download_tempo'] = $audio_summary->tempo;
				$api_info['download_time_signature'] = $audio_summary->time_signature;
				$api_info['download_mode'] = $audio_summary->mode;
				$api_info['download_keymode_id'] = $audio_summary->key;
				$api_info['download_loudness'] = $audio_summary->loudness;
				$api_info['download_speechness'] = $audio_summary->speechiness;
				$api_info['download_danceability'] = $audio_summary->danceability;
				$api_info['download_energy'] = $audio_summary->energy;
				$api_info['download_acousticness'] = $audio_summary->acousticness;
				$api_info['download_liveness'] = $audio_summary->liveness;
				$api_info['download_instrumentalness'] = $audio_summary->instrumentalness;
			}
		} else {
			$message = $json->response->status->message;
		}
	}

	wp_send_json_success( array(
		'track_details' => $track_details,
		'api_message' => $message,
		'api_info' => $api_info,
	) );
}

// add Open Graph for downloads
add_action( 'wp_head', 'acas4u_add_og_microdata', 5 );
function acas4u_add_og_microdata() {
	global $post;

	if ( $post->post_type == 'download' ) {

		$artist1 = get_post_meta( $post->ID, '_download_artist1', TRUE );
		$artist2 = get_post_meta( $post->ID, '_download_artist2', TRUE );
		$trackname = get_post_meta( $post->ID, '_download_trackname', TRUE );
		$artist_id = get_post_meta( $post->ID, '_download_artist_id', TRUE );
		$permalink = get_the_permalink( $post->ID );

		$title = acas4u_create_download_title( $post->ID, $artist1, $artist2, $trackname );
		$output = '<meta property="og:title" content="' . $title . '" />' . "\r\n";
		$output .= '<meta property="og:type" content="article" />' . "\r\n";
		if ( $artist_id ) {
			$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
			if ( $artist_thumbnail_id ) {
				$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
				//$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
				$output .= '<meta property="og:image" content="' . $artist_thumbnail_image[0] . '" />' . "\r\n";
			}
		}

		$output .= '<meta property="og:url" content="' . $permalink . '" />' . "\r\n";
		$output .= '<meta property="og:site_name" content="' . get_bloginfo( 'name' ) . '" />' . "\r\n";

		echo $output;
	}
}

add_action( 'init', 'acas4u_taxonomy_download_index' );
function acas4u_taxonomy_download_index() {
	// create a new taxonomy
	register_taxonomy( 'download-index', 'download', array(
		'label' => __( 'Download index', 'acapellas4u' ),
		'rewrite' => array( 'slug' => 'download-index' ),
		'show_in_nav_menus' => FALSE,
		'exclude_from_search' => TRUE,
		'hierarchical' => TRUE,
		'capabilities' => array(
			'manage_terms' => 'manage_options', //by default only admin
			'edit_terms' => 'manage_options',
			'delete_terms' => 'manage_options',
			'assign_terms' => 'edit_posts'  // means administrator', 'editor', 'author', 'contributor'
		),
	) );
}
