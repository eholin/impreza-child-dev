<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * declare custom post type - artist
 */
add_action( 'init', 'acas4u_custom_post_artist', 15 );
function acas4u_custom_post_artist() {
	$labels = array(
		'name' => _x( 'Artists', 'post type general name', 'acapellas4u' ),
		'singular_name' => _x( 'Artist', 'post type singular name', 'acapellas4u' ),
		'add_new' => _x( 'Add New', 'artist', 'acapellas4u' ),
		'add_new_item' => __( 'Add New Artist', 'acapellas4u' ),
		'edit_item' => __( 'Edit Artist', 'acapellas4u' ),
		'new_item' => __( 'New Artist', 'acapellas4u' ),
		'all_items' => __( 'All Artists', 'acapellas4u' ),
		'view_item' => __( 'View Artist', 'acapellas4u' ),
		'search_items' => __( 'Search Artists', 'acapellas4u' ),
		'not_found' => __( 'No Artists found', 'acapellas4u' ),
		'not_found_in_trash' => __( 'No Artists found in the Trash', 'acapellas4u' ),
		'parent_item_colon' => '',
		'menu_name' => __( 'Artists', 'acapellas4u' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Create an Artist', 'acapellas4u' ),
		'menu_icon' => 'dashicons-id-alt',
		'public' => TRUE,
		'show_in_nav_menus' => FALSE,
		'exclude_from_search' => FALSE,
		'supports' => array( 'title', 'editor', 'page-attributes', 'thumbnail', 'author' ),
		'has_archive' => TRUE,
		'taxonomies' => array(
			'genre',
		),
	);
	register_post_type( 'artist', $args );
}

add_action( 'init', 'acas4u_taxonomy_artist_index' );
function acas4u_taxonomy_artist_index() {
	// create a new taxonomy
	register_taxonomy( 'artist-index', 'artist', array(
		'label' => __( 'Artist index', 'acapellas4u' ),
		'rewrite' => array( 'slug' => 'artist-index' ),
		'show_in_nav_menus' => FALSE,
		'exclude_from_search' => TRUE,
		'hierarchical' => TRUE,
		'capabilities' => array(
			'manage_terms' => 'manage_options', //by default only admin
			'edit_terms' => 'manage_options',
			'delete_terms' => 'manage_options',
			'assign_terms' => 'edit_posts'  // means administrator', 'editor', 'author', 'contributor'
		),
	) );
}

add_filter( 'manage_artist_posts_columns', 'acas4u_artist_admin_columns_head' );
function acas4u_artist_admin_columns_head( $defaults ) {
	$defaults['downloads'] = 'Downloads';

	return $defaults;
}

add_action( 'manage_artist_posts_custom_column', 'acas4u_artist_admin_columns_content', 10, 2 );
function acas4u_artist_admin_columns_content( $column_name, $post_ID ) {
	if ( $column_name == 'downloads' ) {
		$args = array(
			'post_type' => 'download',
			'posts_per_page' => - 1,
			'order' => 'ASC',
			'orderby' => 'title',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => '_download_artist_id',
					'compare' => '=',
					'value' => $post_ID,
				),
			),
		);

		$total_results = acas4u_get_posts_count( $args, 'custom' );

		echo $total_results;
	}
}

function acas4u_get_artist_id_by_enid( $artist_enid ) {
	global $wpdb;

	$artist_id = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_download_artist_enid' AND meta_value='$artist_enid'" );

	return $artist_id;
}

add_action( 'init', 'acas4u_explore_artists_rewrite' );
function acas4u_explore_artists_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%explore-artists%', '([^&]+)' );
	add_rewrite_tag( '%artist_index%', '([^&]+)' );
	add_rewrite_rule( '^explore-artists/([^/]*)?', 'index.php?pagetype=explore-artists&artist_index=$matches[1]', 'top' );
	add_rewrite_rule( '^explore-artists/?', 'index.php?pagetype=explore-artists', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	//$wp_rewrite->flush_rules();
}

//add_action( 'init', 'acas4u_explore_artists_front_rewrite' );
function acas4u_explore_artists_front_rewrite() {
	add_rewrite_tag( '%explore-artists%', '([^&]+)' );
	add_rewrite_rule( '^explore-artists/?', 'index.php?pagetype=explore-artists', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );
}

add_action( 'template_redirect', 'acas4u_explore_artists_redirect' );
function acas4u_explore_artists_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'explore-artists' ) {

		include( $acas4u_stylesheet_directory . '/templates/browse-artists/browse-artists-page.php' );
		exit;
	}
}

add_filter( 'wp_title', 'acas4u_explore_artists_title' );
function acas4u_explore_artists_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'explore-artists' ) {
		$title = 'Explore Artists';
	}

	return $title;
}

function acas4u_show_explore_artists_results( $args ) {
	$explore_artists = new WP_Query( $args );

	if ( $explore_artists->have_posts() ) {
		// Start the loop.
		$output = '';
		$output_mobile = '';
		while ( $explore_artists->have_posts() ){
			$explore_artists->the_post();

			$permalink = get_the_permalink();
			$post_id = get_the_ID();

			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );

			$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
			if ( $download_mode != '' ) {
				$download_mode = ( $download_mode == 1 ) ? 'major' : 'minor';
			}
			// $download_bpm = get_post_meta( $post_id, '_download_bpm', TRUE ); use 'tempo' instead of 'bpm'
			$download_size = get_post_meta( $post_id, '_download_size', TRUE );
			$download_size = formatBytes( $download_size );
			$download_rating = get_post_meta( $post_id, '_download_rating', TRUE );
			if ( $download_rating == '' ) {
				$download_rating = 0;
			}
			$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );

			$download_count = get_post_meta( $post_id, '_download_count', TRUE );
			$download_key = (int) get_post_meta( $post_id, '_download_key', TRUE );
			$download_keymode = (int) get_post_meta( $post_id, '_download_keymode_id', TRUE );
			$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );
			$date_uploaded = get_the_date( 'd/m/Y' );
			$download_filename = get_post_meta( $post_id, '_download_filename', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$rating_stars = acas4u_display_rating_stars( $download_rating );

			// create html for mobile version (stacktable)
			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<th class="st-head-row" colspan="2"><a href="' . $permalink . '" title="Filename: ' . $download_filename . '">' . $title . '</a></th>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="1">Mode</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="2">Key</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-key1">' . acas4u_key_value( $download_key ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="3">Key</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-key2">' . acas4u_key_value( $download_keymode ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="4">BPM</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bpm">' . $download_tempo . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="5">Bitrate</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bitrate">' . $download_bitrate . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="6">Size</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-size">' . $download_size . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="7">Length</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-length">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="8">Listeners</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-listeners">' . number_format( $download_count, 0, ',', ',' ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="9">Added</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="10">Rating</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-rating" title="Total votes: ' . $download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output .= '<tr class="acas4u-search-tr" role="row">';
			$output .= '<td class="acas4u-search-td td-title"><a href="' . $permalink . '" title="Filename: ' . $download_filename . '">' . $title . '</a></td>';
			$output .= '<td class="acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output .= '<td class="acas4u-search-td td-key1">' . acas4u_key_value( $download_key ) . '</td>';
			$output .= '<td class="acas4u-search-td td-key1">' . acas4u_keymode_value( $download_keymode ) . '</td>';
			$output .= '<td class="acas4u-search-td td-bpm">' . $download_tempo . '</td>';
			$output .= '<td class="acas4u-search-td td-bitrate">' . $download_bitrate . '</td>';
			$output .= '<td class="acas4u-search-td td-size">' . $download_size . '</td>';
			$output .= '<td class="acas4u-search-td td-length">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output .= '<td class="acas4u-search-td td-listeners">' . number_format( $download_count, 0, ',', ',' ) . '</td>';
			$output .= '<td class="acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output .= '<td class="acas4u-search-td td-rating" title="Total votes: ' . $download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output .= '</tr>' . "\n";
		}
	} else {
		$output = '<tr class="acas4u-search-tr">';
		$output .= '<td class="acas4u-search-td">No results found.</td>';
		$output .= '</tr>';
	}
	wp_reset_query();

	return array( 'large' => $output, 'small' => $output_mobile );
}

/**
 * Ajax for infinite scroll
 */
add_action( 'wp_ajax_do_explore_artists_scroll', 'acas4u_do_explore_artists_scroll' );
add_action( 'wp_ajax_nopriv_do_explore_artists_scroll', 'acas4u_do_explore_artists_scroll' );
function acas4u_do_explore_artists_scroll() {
	check_ajax_referer( 'acas4u_do_explore_artists_scroll_nonce', '_ajax_nonce' );

	$default_posts_per_page = get_option( 'posts_per_page' );

	$page = $_POST['page'];

	if ( $_POST['count'] ) {
		$count = $_POST['count'];
	} else {
		$count = $default_posts_per_page;
	}

	if ( $page ) {
		$offset = $page * $count;
	} else {
		wp_send_json_error();
	}

	$index = $_POST['index'];

	$args = array(
		'post_type' => 'download',
		'order' => 'ASC',
		'orderby' => 'title',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'offset' => $offset,
		'tax_query' => array(
			array(
				'taxonomy' => 'download-index',
				'field' => 'slug',
				'terms' => $index,
			),
		),
	);

	$search_results = acas4u_show_explore_artists_results( $args );

	wp_send_json_success( array(
		'html' => $search_results,
		'page' => $page + 1,
	) );
}

function acas4u_show_artists_acapellas( $artist_id ) {
	$args = array(
		'post_type' => 'download',
		'posts_per_page' => - 1,
		'order' => 'ASC',
		'orderby' => 'title',
		'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' => '_download_artist_id',
				'compare' => '=',
				'value' => $artist_id,
			),
		),
	);
	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		$output = '<ul class="acas4u-artist-downloads-list">';
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$title = get_the_title();
			$permalink = get_the_permalink();
			$output .= '<li><a href="' . $permalink . '">' . $title . '</a></li>';
		}
		$output .= '<ul>';
	} else {
		$output = '<p>There are no acapellas.</p>';
	}
	wp_reset_query();

	return $output;
}
