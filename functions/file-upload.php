<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
//add_action( 'init', 'file_uploader_rewrite' );
function file_uploader_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%upload%', '([^&]+)' );
	add_rewrite_rule( '^upload/?', 'index.php?upload=upload', 'top' );
	add_rewrite_endpoint( 'upload', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	//$wp_rewrite->flush_rules();
}

//add_action( 'template_redirect', 'file_uploader_redirect' );
function file_uploader_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'upload', $template ) AND $template['upload'] == 'upload' ) {

		include( $acas4u_stylesheet_directory . '/templates/uploader-page.php' );
		exit;
	}
}

//add_filter( 'wp_title', 'file_uploader_title' );
function file_uploader_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'upload', $template ) AND $template['upload'] == 'upload' ) {
		$title = 'Upload Your Acapellas';
	}

	return $title;
}

add_action( 'wp_ajax_do_upload', 'acas4u_do_upload' );
function acas4u_do_upload() {
	global $wpdb, $_FILES;

	check_ajax_referer( 'acas4u_do_file_upload', '_ajax_nonce' );

	$output_dir = WP_CONTENT_DIR . '/uploads/tmpacapellas/';

	if ( ! isset( $_FILES["userfile"] ) ) {
		wp_send_json_error( array( 'message' => 'No file(s)' ) );
	}

	$f = 0;
	$messages = [ ];
	foreach ( $_FILES['userfile']['tmp_name'] as $index => $tmp_name ) {
		//move the uploaded file to uploads folder;
		$file = $_FILES['userfile']['name'][ $index ];

		if ( $_FILES['userfile']['error'][ $index ] > 0 ) {
			$error_text = acas4u_get_error_text( $_FILES['userfile']['error'][ $index ] );
			$messages [] = 'Not Uploaded File: <strong>' . $file . '</strong>. Cause: ' . $error_text;
		} else if ( file_exists( $output_dir . $file ) ) {
			$messages [] = 'File exists: <strong>' . $file . '</strong>';
		} else if ( move_uploaded_file( $tmp_name, $output_dir . $file ) ) {
			$bitrate = acas4u_get_file_bitrate( $output_dir . $file );
			if ( $bitrate < 192 ) {
				$messages [] = 'A bitrate of this file <strong>' . $file . '</strong> is too low: <strong>' . $bitrate . '</strong> kbps. Please upload files with the bitrate <strong>192</strong> kbps and more.';
				// delete uploaded file
				unlink( $output_dir . $file );
			} else {
				$artist = $_POST['artist1'][ $f ];
				$title = $_POST['trackname'][ $f ];
				$featuring = $_POST['featuring'][ $f ];
				$type = $_POST['type'][ $f ];
				$size = $_FILES['userfile']['size'][ $index ];

				// check hash, delete file if hash exists in DB
				// meta_key = _download_filehash
				$hash = hash_file( 'sha256', $output_dir . $file );
				$existing_post = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", '_download_filehash', $hash ) );
				//$existing_post = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_download_filehash' AND meta_value = '$hash'" );
				if ( ! $existing_post ) {
					$new_file = acas4u_rename_uploaded_file( $file, $artist, $title, $featuring, $type );
					$post_name = trim( $new_file, '.mp3' );

					if ( $new_file !== FALSE ) {
						// create new post
						$postarr = array(
							'post_author' => $_POST['uploader'],
							'post_title' => sanitize_text_field( $new_file ),
							'post_content' => '',
							'post_status' => 'pending',
							'post_type' => 'download',
							'post_name' => sanitize_title( $post_name ),
						);

						$post_id = wp_insert_post( $postarr );
						if ( $post_id ) {
							// create post meta for the new post
							add_post_meta( $post_id, '_download_filename', $new_file );
							add_post_meta( $post_id, '_download_filehash', $hash );
							add_post_meta( $post_id, '_download_size', $size );
							add_post_meta( $post_id, '_download_bitrate', $bitrate );
							add_post_meta( $post_id, '_download_count', '0' );
							add_post_meta( $post_id, '_download_rating', '0' );
							add_post_meta( $post_id, '_download_votes', '0' );
							add_post_meta( $post_id, '_download_artist1', sanitize_text_field( $artist ) );
							add_post_meta( $post_id, '_download_artist2', sanitize_text_field( $featuring ) );
							add_post_meta( $post_id, '_download_trackname', sanitize_text_field( $title ) );
							add_post_meta( $post_id, '_download_trackstatus', 'new' );
						}

						$messages [] = 'Uploaded File: <strong>' . $file . '</strong>';
					} else {
						$messages [] = 'File <strong>' . $file . '</strong> can not be renamed or something else went wrong.';
						unlink( $output_dir . $file );
					}
				} else {
					$messages [] = 'File <strong>' . $file . '</strong> is exists in our collection.';
					unlink( $output_dir . $file );
				}
			}
		}
		$f ++;
	}
	wp_send_json_success( array(
		'messages' => $messages,
		'total_files' => $f,
	) );
}

function acas4u_get_error_text( $error ) {

	switch ( $error ) {
		case '1':
			$error_text = 'The uploaded file is too large.';
			break;
		case '2':
			$error_text = 'The uploaded file is too large.';
			break;
		case '3':
			$error_text = 'The uploaded file was only partially uploaded.';
			break;
		case '4':
			$error_text = 'No file was uploaded.';
			break;
		case '5':
			$error_text = 'Missing a temporary folder.';
			break;
		case '6':
			$error_text = 'Failed to write file to disk.';
			break;
		case '7':
			$error_text = 'System stopped the file upload.';
			break;
		default:
			$error_text = 'No error.';
			break;
	}

	return $error_text;
}

function acas4u_mp3fname_clean( $string ) {
	$string = preg_replace( '/[^A-Za-z0-9\- ]/', '', $string ); // Removes special chars.
	return $string;
}

function acas4u_rename_uploaded_file( $file, $artist, $title, $featuring, $type ) {
	$output_dir = WP_CONTENT_DIR . '/uploads/tmpacapellas/';

	$filename = $output_dir . $file;

	// reading and writing id3 tag into mp3 file
	$TaggingFormat = 'UTF-8';
	$getID3 = new getID3;
	$getID3->setOption( array( 'encoding' => $TaggingFormat ) );
	$ThisFileInfo = $getID3->analyze( $filename );
	if ( $ThisFileInfo AND $ThisFileInfo['fileformat'] == 'mp3' ) {
		if ( ! $title ) {
			if ( $ThisFileInfo['id3v2']['title'] ) {
				$title = $ThisFileInfo['id3v2']['title'];
			} else if ( $ThisFileInfo['id3v1']['title'] ) {
				$title = $ThisFileInfo['id3v1']['title'];
			} else {
				$title = 'No title';
			}
		}
		if ( ! $artist ) {
			if ( $ThisFileInfo['id3v2']['artist'] ) {
				$artist = $ThisFileInfo['id3v2']['artist'];
			} else if ( $ThisFileInfo['id3v1']['artist'] ) {
				$artist = $ThisFileInfo['id3v1']['artist'];
			} else {
				$artist = 'No artist';
			}
		}
		if ( $ThisFileInfo['id3v2']['album'] ) {
			$album = $ThisFileInfo['id3v2']['album'];
		} else if ( $ThisFileInfo['id3v1']['album'] ) {
			$album = $ThisFileInfo['id3v1']['album'];
		}
		if ( $ThisFileInfo['id3v2']['track'] ) {
			$track = $ThisFileInfo['id3v2']['track'];
		} else if ( $ThisFileInfo['id3v1']['track'] ) {
			$track = $ThisFileInfo['id3v1']['track'];
		}

		$comment = 'http://acapellas4u.co.uk';

		$TagFormatsToWrite = array( 'id3v1', 'id3v2.3' );

		$tagwriter = new getid3_writetags;
		$tagwriter->filename = $filename;
		$tagwriter->tagformats = $TagFormatsToWrite;
		$tagwriter->overwrite_tags = TRUE;
		$tagwriter->merge_existing_data = TRUE;
		$tagwriter->remove_other_tags = FALSE;
		$tagwriter->tag_encoding = $TaggingFormat;

		$TagData = array(
			'title' => array( acas4u_mp3fname_clean( $title ) ),
			'artist' => array( acas4u_mp3fname_clean( $artist ) ),
			'album' => array( acas4u_mp3fname_clean( $album ) ),
			'track' => array( acas4u_mp3fname_clean( $track ) ),
			'comment' => array( $comment ),
			'url_source' => array( $comment ),
		);

		$tagwriter->tag_data = $TagData;
	}

	if ( $type ) {
		$file_type = '_(' . $type . ')';
	}
	if ( $featuring ) {
		$name_feat = '_ft_' . $featuring . '_';
	}
	if ( $artist AND $title ) {
		$artist = sanitize_file_name( $artist );
		$name_feat = sanitize_file_name( $name_feat );
		$title = sanitize_file_name( $title );
		$new_file = $artist . $name_feat . '_-_' . $title . $file_type . '.mp3';
		$new_file = strtolower( str_replace( ' ', '_', $new_file ) ); // Replaces all spaces with hyphens.
		$new_file = preg_replace( '/[_]+/', '_', $new_file ); // Replaces all multiple underscores by one

		$new_filename = $output_dir . $new_file;
		if ( is_file( $filename ) ) {
			rename( $filename, $new_filename );
		}
	} else {
		$new_file = FALSE;
	}

	return $new_file;
}

/* delete mp3-file when post is deleted */
add_action( 'before_delete_post', 'acas4u_delete_download', 10, 3 );
function acas4u_delete_download( $post_id ) {
	global $post_type;

	//acas4u_write_log_entry( '#------# delete post #------#', 'acas4u_download_ops.log' );
	//acas4u_write_log_entry( 'Type: ' . $post_type . '. Post ID: ' . $post_id, 'acas4u_download_ops.log' );

	if ( $post_type != 'download' ) {
		return;
	}

	$acas4u_upload_dir = wp_upload_dir();

	// get file name and path
	$download_filename = get_post_meta( $post_id, '_download_filename', TRUE );
	$download_path = get_post_meta( $post_id, '_download_path', TRUE );
	$download_preview = update_post_meta( $post_id, '_download_preview', TRUE );

	if ( $download_path ) {
		$file_path = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . $download_filename;
	} else {
		$file_path = $acas4u_upload_dir['basedir'] . '/tmpacapellas/' . $download_filename;
	}

	if ( file_exists( $file_path ) ) {
		unlink( $file_path );
	}

	if ( $download_preview ) {
		$preview_path = $acas4u_upload_dir['basedir'] . '/waveforms/' . $download_preview;
		$preview_png_path = $acas4u_upload_dir['basedir'] . '/waveforms/png/' . trim( $download_preview, 'dat' ) . 'png';

		unlink( $preview_path );
		unlink( $preview_png_path );
	}
}

add_action( 'transition_post_status', 'acas4u_publish_download', 10, 3 );
function acas4u_publish_download( $new_status, $old_status, $post ) {

	if ( $post ) {
		$post_type = $post->post_type;
	} else {
		return;
	}

	if ( $post_type != 'download' ) {
		return;
	}

	//acas4u_write_log_entry( '#------# post change status #------#', 'acas4u_download_ops.log' );
	//acas4u_write_log_entry( 'Type: ' . $post_type . '. Post old status: ' . $old_status . ', post new status: ' . $new_status, 'acas4u_download_ops.log' );

	if ( $new_status !== 'publish' OR $old_status === 'publish' ) {
		return;
	}

	$post_id = $post->ID;

	$acas4u_upload_dir = wp_upload_dir();

	//acas4u_write_log_entry( 'Start publish post #' . $post_id, 'acas4u_download_ops.log' );

	// get file name and path
	$download_filename = get_post_meta( $post_id, '_download_filename', TRUE );
	//acas4u_write_log_entry( 'File: ' . $download_filename, 'acas4u_download_ops.log' );
	if ( $download_filename ) {
		$first_letter = mb_substr( $download_filename, 0, 1 );
		$file_path = $acas4u_upload_dir['basedir'] . '/tmpacapellas/' . $download_filename;
		//acas4u_write_log_entry( 'File path: ' . $file_path, 'acas4u_download_ops.log' );
		if ( file_exists( $file_path ) ) {
			$download_path = mb_strtoupper( $first_letter ) . '/';
			$collection_file_path = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . $download_filename;
			//acas4u_write_log_entry( 'Collection file path: ' . $collection_file_path, 'acas4u_download_ops.log' );
			if ( rename( $file_path, $collection_file_path ) ) {
				update_post_meta( $post_id, '_download_path', $download_path );
				update_post_meta( $post_id, '_download_trackstatus', 'publish' );
			}
		}
	}
}
