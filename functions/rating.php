<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Ajax for rating
 */
add_action( 'wp_ajax_do_rating', 'acas4u_do_rating' );
add_action( 'wp_ajax_nopriv_do_rating', 'acas4u_do_rating' );
function acas4u_do_rating() {
	global $_REQUEST, $_COOKIE, $_POST;

	check_ajax_referer( 'acas4u_user_rating_vote', '_ajax_nonce' );

	if ( ! empty( $_POST['post_id'] ) ) {
		$post_id = intval( $_POST['post_id'] );
		$rating = floatval( $_POST['rating'] );
		if ( empty( $_COOKIE[ 'acas4u_rated_' . $post_id ] ) ) {
			setcookie( 'acas4u_rated_' . $post_id, $rating, time() + 3600 * 24 * 365, "/" );
			$new_rating = acas4u_update_rating( $post_id, $rating );
			wp_send_json_success( array( 'new_rating' => $new_rating ) );
		} else {
			wp_send_json_error();
		}
	} else {
		wp_send_json_error();
	}
}

function acas4u_update_rating( $post_id, $rating ) {
	$download_rating = (float) get_post_meta( $post_id, '_download_rating', TRUE );
	$download_votes = (int) get_post_meta( $post_id, '_download_votes', TRUE );

	$total_votes = $download_votes + 1;
	$total_rating = ( ( $download_rating * $download_votes ) + $rating ) / $total_votes;

	update_post_meta( $post_id, '_download_rating', $total_rating );
	update_post_meta( $post_id, '_download_votes', $total_votes );

	return $total_rating;
}

function acas4u_user_ratings( $readonly ) {
	global $post;

	$voted = $_COOKIE[ 'acas4u_rated_' . $post->ID ];

	$ajax_nonce = wp_create_nonce( 'acas4u_user_rating_vote' );

	$download_rating = (float) get_post_meta( $post->ID, '_download_rating', TRUE );
	$download_votes = (int) get_post_meta( $post->ID, '_download_votes', TRUE );
	if ( ! $download_rating ) {
		$download_rating = 0;
	}
	$rating_floor = floor( $download_rating );
	$rating_ceil = ceil( $download_rating );

	$output = '<div class="acas4u-user-rating-wrapper">';
	if ( ! $readonly ) {
		$output .= '<div class="acas4u-user-ratings-stars" data-nonce="' . $ajax_nonce . '" data-postid="' . $post->ID . '" data-votes="' . $download_votes . '" data-rating="' . $download_rating . '">Rate the track:&nbsp;';
	} else {
		$output .= '<div class="acas4u-user-ratings-stars">Track rating:&nbsp;';
	}
	for ( $i = 0; $i < 5; $i ++ ) {
		if ( $i == $rating_floor AND $rating_ceil != $rating_floor ) {
			$star_class = 'fa-star-half-o';
		} else if ( $i >= $rating_floor ) {
			$star_class = 'fa-star-o';
		} else {
			$star_class = 'fa-star';
		}
		if ( empty( $voted ) AND ! $readonly ) {
			$output .= '<a href="javascript:void(0);" class="acas4u-ratings-single-star" data-index="' . $i . '" data-class="' . $star_class . '"><i class="fa ' . $star_class . '"></i></a>';
		} else {
			$output .= '<i class="fa ' . $star_class . '"></i>';
		}
	}

	$output .= '</div >';

	$output .= '<div itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">' . __( "Total votes", "acas4u" ) . ': <span itemprop="ratingCount" class="acas4u-ratings-total-votes">' . $download_votes . '</span>, ' . __( "average", "acas4u" ) . ': <span itemprop="ratingValue" class="acas4u-ratings-average-votes">' . number_format( $download_rating, 2, ".", "" ) . '</span> ' . __( "out of", "acas4u" ) . ' <span itemprop="bestRating">5</span></div>';
	$output .= '<div class="acas4u-user-rating-total">' . acas4u_get_total_rating( $post->ID ) . '</div>';
	$output .= '<div class="acas4u-user-ratings-message"></div>';
	$output .= '</div >';

	return $output;
}

function acas4u_get_total_rating( $post_id ) {
	global $wpdb;
	$rating_posts_ids = $wpdb->get_results( "SELECT post_id, @rownum := @rownum +1 AS ROW_NUMBER FROM $wpdb->postmeta JOIN (SELECT @rownum :=0) R WHERE meta_key = '_download_count' ORDER BY CAST( meta_value AS unsigned ) DESC" );
	if ( $rating_posts_ids > 0 ) {
		$total = 0;
		foreach ( $rating_posts_ids as $rating_post ) {
			$total ++;
			if ( $rating_post->post_id == $post_id ) {
				$post_position = $total;
			}
		}
	}

	return 'Acapella Ranking: <span class="ur-post-position">#' . number_format( $post_position, 0, ',', ',' ) . '</span> of <span class="ur-post-total">' . number_format( count( $rating_posts_ids ), 0, ',', ',' ) . '</span>';
}
