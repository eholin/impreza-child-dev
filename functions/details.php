<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
function acas4u_show_track_details( $post_id, $track_enid ) {
	global $live_update, $en_api_key;
	$empty_details = FALSE;
	$live_update = FALSE;
	$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
	if ( ! $download_duration ) {
		$empty_details = TRUE;
	}
	$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );
	if ( ! $download_tempo ) {
		$empty_details = TRUE;
	}
	$download_time_signature = get_post_meta( $post_id, '_download_time_signature', TRUE );
	if ( ! $download_time_signature ) {
		$empty_details = TRUE;
	}
	$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
	if ( ! $download_mode ) {
		$empty_details = TRUE;
	}
	$download_key = get_post_meta( $post_id, '_download_key', TRUE );
	if ( ! $download_key ) {
		$empty_details = TRUE;
	}
	$download_loudness = get_post_meta( $post_id, '_download_loudness', TRUE );
	if ( ! $download_loudness ) {
		$empty_details = TRUE;
	}
	$download_speechness = get_post_meta( $post_id, '_download_speechness', TRUE );
	if ( ! $download_speechness ) {
		$empty_details = TRUE;
	}
	$download_danceability = get_post_meta( $post_id, '_download_danceability', TRUE );
	if ( ! $download_danceability ) {
		$empty_details = TRUE;
	}
	$download_energy = get_post_meta( $post_id, '_download_energy', TRUE );
	if ( ! $download_energy ) {
		$empty_details = TRUE;
	}
	$download_acousticness = get_post_meta( $post_id, '_download_acousticness', TRUE );
	if ( ! $download_acousticness ) {
		$empty_details = TRUE;
	}
	$download_liveness = get_post_meta( $post_id, '_download_liveness', TRUE );
	if ( ! $download_liveness ) {
		$empty_details = TRUE;
	}
	$download_instrumentalness = get_post_meta( $post_id, '_download_instrumentalness', TRUE );
	if ( ! $download_instrumentalness AND $download_instrumentalness != NULL ) {
		$empty_details = TRUE;
	}

	if ( $empty_details AND $track_enid != '' AND $live_update ) {
		$url = 'http://developer.echonest.com/api/v4/track/profile?api_key=' . $en_api_key . '&id=' . $track_enid . '&format=json&bucket=audio_summary';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			$audio_md5 = $json->response->track->audio_md5;
			if ( $audio_md5 != '' ) {
				update_post_meta( $post_id, '_download_audio_md5', $audio_md5 );
			}

			$audio_summary = $json->response->track->audio_summary;

			$download_duration = $audio_summary->duration;
			if ( $download_duration != '' ) {
				update_post_meta( $post_id, '_download_duration', $download_duration );
			}
			$download_tempo = $audio_summary->tempo;
			if ( $download_tempo != '' ) {
				update_post_meta( $post_id, '_download_tempo', $download_tempo );
			}
			$download_time_signature = $audio_summary->time_signature;
			if ( $download_time_signature != '' ) {
				update_post_meta( $post_id, '_download_time_signature', $download_time_signature );
			}
			$download_mode = $audio_summary->mode;
			if ( $download_mode != '' ) {
				update_post_meta( $post_id, '_download_mode', $download_mode );
			}
			$download_key = $audio_summary->key;
			if ( $download_key != '' ) {
				update_post_meta( $post_id, '_download_key', $download_key );
			}
			$download_loudness = $audio_summary->loudness;
			if ( $download_loudness != '' ) {
				update_post_meta( $post_id, '_download_loudness', $download_loudness );
			}
			$download_speechness = $audio_summary->speechiness;
			if ( $download_speechness != '' ) {
				update_post_meta( $post_id, '_download_speechness', $download_speechness );
			}
			$download_danceability = $audio_summary->danceability;
			if ( $download_danceability != '' ) {
				update_post_meta( $post_id, '_download_danceability', $download_danceability );
			}
			$download_energy = $audio_summary->energy;
			if ( $download_energy != '' ) {
				update_post_meta( $post_id, '_download_energy', $download_energy );
			}
			$download_acousticness = $audio_summary->acousticness;
			if ( $download_acousticness != '' ) {
				update_post_meta( $post_id, '_download_acousticness', $download_acousticness );
			}
			$download_liveness = $audio_summary->liveness;
			if ( $download_liveness != '' ) {
				update_post_meta( $post_id, '_download_liveness', $download_liveness );
			}
			$download_instrumentalness = $audio_summary->instrumentalness;
			if ( $download_instrumentalness != '' ) {
				update_post_meta( $post_id, '_download_instrumentalness', $download_instrumentalness );
			}
		}
	}
	$output = '';
	if ( $download_duration ) {
		$output .= '<div class="acas4u-details-item det-duration">';
		$output .= '<span class="detail-label">Duration:</span>';
		$output .= acas4u_format_duration( $download_duration );
		$output .= '</div>';
	}

	if ( $download_tempo ) {
		$output .= '<div class="acas4u-details-item det-tempo">';
		$output .= '<span class="detail-label">Tempo:</span>';
		$output .= number_format( $download_tempo, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_time_signature ) {
		$output .= '<div class="acas4u-details-item det-time-signature">';
		$output .= '<span class="detail-label">Time signature:</span>';
		$output .= $download_time_signature;
		$output .= '</div>';
	}

	if ( $download_mode ) {
		$output .= '<div class="acas4u-details-item det-mode">';
		$output .= '<span class="detail-label">Mode:</span>';
		$output .= $download_mode;
		$output .= '</div>';
	}

	if ( $download_key ) {
		$output .= '<div class="acas4u-details-item det-key">';
		$output .= '<span class="detail-label">Key:</span>';
		$output .= $download_key;
		$output .= '</div>';
	}

	if ( $download_loudness ) {
		$output .= '<div class="acas4u-details-item det-loudness">';
		$output .= '<span class="detail-label">Loudness:</span>';
		$output .= number_format( $download_loudness, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_speechness ) {
		$output .= '<div class="acas4u-details-item det-speechness">';
		$output .= '<span class="detail-label">Speechness:</span>';
		$output .= number_format( $download_speechness, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_danceability ) {
		$output .= '<div class="acas4u-details-item det-danceability">';
		$output .= '<span class="detail-label">Danceability:</span>';
		$output .= number_format( $download_danceability, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_energy ) {
		$output .= '<div class="acas4u-details-item det-energy">';
		$output .= '<span class="detail-label">Energy:</span>';
		$output .= number_format( $download_energy, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_acousticness ) {
		$output .= '<div class="acas4u-details-item det-acousticness">';
		$output .= '<span class="detail-label">Acousticness:</span>';
		$output .= number_format( $download_acousticness, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_liveness ) {
		$output .= '<div class="acas4u-details-item det-liveness">';
		$output .= '<span class="detail-label">Liveness:</span>';
		$output .= number_format( $download_liveness, 2, '.', ' ' );
		$output .= '</div>';
	}

	if ( $download_instrumentalness ) {
		$output .= '<div class="acas4u-details-item det-instrumentalness">';
		$output .= '<span class="detail-label">Instrumentalness:</span>';
		$output .= number_format( $download_instrumentalness, 2, '.', ' ' );
		$output .= '</div>';
	}

	return $output;
}
