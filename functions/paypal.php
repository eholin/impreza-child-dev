<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

add_filter( 'paypal_framework_sslverify', '__return_true' );

add_action( 'admin_menu', 'acas4u_register_paypal_menu_page' );
function acas4u_register_paypal_menu_page() {
	global $acas4u_stylesheet_directory_uri;
	add_menu_page( __( 'Manage PayPal Transactions', 'acapellas4u' ), __( 'PayPal', 'acapellas4u' ), 'manage_options', 'manage_paypal', 'acas4u_manage_paypal_transactions', $acas4u_stylesheet_directory_uri . '/img/paypalbw_16x16.png' );
}

function acas4u_manage_paypal_transactions() {
	global $wpdb;

	$limit = 50;

	$orderby = $_GET['orderby'];
	$order = $_GET['order'];
	if ( ! $order ) {
		$orderby = 'timestamp';
		$order = 'DESC';
	}
	if ( $order AND $orderby ) {
		$order_str = ' ORDER BY ' . $orderby . ' ' . $order . ' ';
	}

	if ( $order == 'DESC' ) {
		$new_order = 'ASC';
	} else {
		$new_order = 'DESC';
	}

	$date_start = $_GET['date_start'];
	$date_end = $_GET['date_end'];
	$trans_id = $_GET['trans_id'];
	$paged = $_GET['paged'];
	$payer_email = $_GET['payer_email'];
	$username = $_GET['username'];
	if ( $paged > 1 ) {
		$start = ( $paged - 1 ) * $limit;
	} else {
		$start = 0;
		$paged = 1;
	}

	$filters = [ ];
	if ( $trans_id ) {
		$filters['trans_id'] = $trans_id;
	}

	if ( $payer_email ) {
		$filters['payer_email'] = $payer_email;
	}

	if ( $username ) {
		$userdata_s = get_user_by( 'login', $username );
		$user_id = $userdata_s->ID;
		$filters['user_id'] = $user_id;
	}

	if ( is_array( $filters ) ) {
		$filter_str = implode( ' AND ', array_map( function ( $v, $k ) {
			return sprintf( "%s='%s'", $k, $v );
		}, $filters, array_keys( $filters ) ) );
	}

	if ( $date_start AND $date_end ) {
		$timestamp_start = strtotime( $date_start );
		$timestamp_end = strtotime( $date_end );
		if ( $filter_str ) {
			$filter_str .= ' AND ';
		}
		$filter_str .= 'timestamp >= "' . $timestamp_start . '" AND timestamp <= "' . $timestamp_end . '"';
	}

	$query_count = 'SELECT COUNT(*) AS total FROM phpbb_paypal_transactions';
	if ( $filter_str ) {
		$query_count .= ' WHERE ' . $filter_str;
	}

	$output = '<div class="wrap"><div id="icon-tools" class="icon32"></div>
			<h2>' . __( 'Manage PayPal Transactions', 'acapellas4u' ) . '</h2>';

	$total_transactions = $wpdb->get_var( $wpdb->prepare( $query_count ) );
	$total_pages = ceil( $total_transactions / 50 );

	$output .= '<div class="tablenav top">';
	$output .= '<div class="alignleft actions bulkactions">';
	$output .= '</div>'; // .alignleft
	$output .= '<form method="get" id="acas4u_paypal_transactions_pagination" action="' . admin_url( 'admin.php' ) . '">';
	$output .= '<input type="hidden" name="page" value="manage_paypal" />';
	$output .= '<input type="hidden" name="orderby" value="' . $orderby . '" />';
	$output .= '<input type="hidden" name="order" value="' . $order . '" />';
	$output .= '<input type="hidden" name="date_start" value="' . $date_start . '" />';
	$output .= '<input type="hidden" name="date_end" value="' . $date_end . '" />';
	$output .= '<input type="hidden" name="trans_id" value="' . $trans_id . '" />';
	$output .= '<input type="hidden" name="username" value="' . $username . '" />';
	$output .= '<input type="hidden" name="payer_email" value="' . $payer_email . '" />';
	$output .= '<div class="tablenav-pages">';
	$output .= '<span class="displaying-num">' . number_format( $total_transactions, 0, ',', ',' ) . ' transactions</span>';
	if ( $paged > 2 ) {
		$output .= '<span class="pagination-links"><a href="' . admin_url( 'admin.php?page=manage_paypal' ) . acas4u_append_search_filter( $filter_str, array(
				'page',
				'paged',
			) ) . '" class="first-page"><span class="screen-reader-text">First page</span><span aria-hidden="true">&laquo;</span></a> ';
	} else {
		$output .= '<span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span> ';
	}
	if ( $paged > 1 ) {
		$page_prev = $paged - 1;
		$output .= '<a href="' . admin_url( 'admin.php?page=manage_paypal&paged=' . $page_prev ) . acas4u_append_search_filter( $filter_str, array(
				'page',
				'paged',
			) ) . '" class="prev-page"><span class="screen-reader-text">Previous page</span><span aria-hidden="true">&lsaquo;</span></a> ';
	} else {
		$output .= '<span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span> ';
	}
	$output .= '<span class="paging-input"><label class="screen-reader-text" for="current-page-selector">Current Page</label><input type="text" aria-describedby="table-paging" size="4" value="' . $paged . '" name="paged" id="current-page-selector" class="current-page"> of <span class="total-pages">' . number_format( $total_pages, 0, ',', ',' ) . '</span></span> ';
	if ( $paged < $total_pages ) {
		$page_next = $paged + 1;
		$output .= '<a href="' . admin_url( 'admin.php?page=manage_paypal&paged=' . $page_next ) . acas4u_append_search_filter( $filter_str, array(
				'page',
				'paged',
			) ) . '" class="next-page"><span class="screen-reader-text">Next page</span><span aria-hidden="true">&rsaquo;</span></a> ';
	} else {
		$output .= '<span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span> ';
	}
	if ( $paged < ( $total_pages - 1 ) ) {
		$output .= '<a href="' . admin_url( 'admin.php?page=manage_paypal&paged=' . $total_pages ) . acas4u_append_search_filter( $filter_str, array(
				'page',
				'paged',
			) ) . '" class="last-page"><span class="screen-reader-text">Last page</span><span aria-hidden="true">&raquo;</span></a></span>';
	} else {
		$output .= '<span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>';
	}
	$output .= '</div>'; // .tablenav-pages
	$output .= '</form>';
	$output .= '<br class="clear">';
	$output .= '</div>'; // .tablenav

	if ( $date_start ) {
		$date_start_str = date( 'd/m/Y', strtotime( $date_start ) );
	}
	if ( $date_end ) {
		$date_end_str = date( 'd/m/Y', strtotime( $date_end ) );
	}

	$output .= '<form method="get" action="' . admin_url( 'admin.php' ) . '" id="acas4u_manage_paypal_transactions">
					<input type="hidden" name="page" value="manage_paypal" />
					<input type="hidden" name="paged" value="' . $paged . '" />
					<input type="hidden" name="orderby" value="' . $orderby . '" />
					<input type="hidden" name="order" value="' . $order . '" />

				<table class="acas4u-paypal-transactions">
					<thead>
						<tr>
							<th>#</th>
							<th>Transaction ID</th>
							<th class="acas4u-order-' . strtolower( $order ) . '"><a href="' . admin_url( 'admin.php?orderby=timestamp&order=' . $new_order ) . acas4u_append_search_filter( $filter_str, array(
			'orderby',
			'order',
		) ) . '">Date/Time</a></th>
							<th>Payment Status</th>
							<th>Payment Date</th>
							<th>Payment Gross</th>
							<th>Payment Fee</th>
							<th>Username</th>
							<th>Payer name</th>
							<th>Payer email</th>
							<th>Payer status</th>
							<th>Comments</th>
						</tr>
						<tr>
							<td></td>
							<td><input type="text" value="' . esc_attr( $trans_id ) . '" name="trans_id" class="widefat" /></td>
							<td>
								<input placeholder="Start date" title="Start date" name="date_start" type="text" value="' . $date_start_str . '" class="acas4u-backend-datepicker" id="date_start" />
								<input placeholder="End date" title="End date" name="date_end" type="text" value="' . $date_end_str . '" class="acas4u-backend-datepicker" id="date_end" />
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><input type="text" value="' . esc_attr( $username ) . '" name="username" class="widefat" /></td>
							<td></td>
							<td><input type="text" value="' . esc_attr( $payer_email ) . '" name="payer_email" class="widefat" /></td>
							<td></td>
							<td class="buttons-container">';
	if ( $filter_str ) {
		$output .= '<a class="button-secondary" href="' . admin_url( 'admin.php?page=manage_paypal&paged=' . $paged ) . '">Clear</a>';
	}
	$output .= '<input type="submit" class="button-primary" value="Apply filters" />';
	$output .= '</td>
						</tr>
					</thead>';

	$query = 'SELECT * FROM phpbb_paypal_transactions';
	if ( $filter_str ) {
		$query .= ' WHERE ' . $filter_str;
	}
	if ( $order_str ) {
		$query .= $order_str;
	}
	$query .= ' LIMIT ' . $start . ',' . $limit;

	$transactions = $wpdb->get_results( $query );
	$i = $total_transactions - $start;
	if ( $transactions ) {
		$output .= '<tbody>';
		foreach ( $transactions as $transaction ) {
			if ( $transaction->user_id ) {
				$userdata = get_user_by( 'id', $transaction->user_id );
				$username = $userdata->user_login;
			}

			$output .= '<tr>';
			$output .= '<td class="pt-count">' . $i . '</td>';
			$output .= '<td>' . $transaction->trans_id . '</td>';
			$output .= '<td class="pt-timestamp">' . date( 'd/m/Y H:i', $transaction->timestamp ) . '</td>';
			$output .= '<td>' . $transaction->payment_status . '</td>';
			$output .= '<td class="pt-date">' . $transaction->payment_date . '</td>';
			$output .= '<td class="pt-gross">' . $transaction->payment_gross . '</td>';
			$output .= '<td class="pt-fee">' . $transaction->payment_fee . '</td>';
			if ( $username ) {
				$output .= '<td class="pt-username"><a href="' . admin_url( 'user-edit.php?user_id=' . $transaction->user_id . '&wp_http_referer=/wp-admin/admin.php?page=manage_paypal' ) . '">' . $username . '</a></td>';
			} else {
				$output .= '<td class="pt-username">N/A</td>';
			}
			$output .= '<td class="pt-name">' . $transaction->payer_firstname . ' ' . $transaction->payer_lastname . '</td>';
			$output .= '<td>' . $transaction->payer_email . '</td>';
			$output .= '<td>' . $transaction->payer_status . '</td>';
			$output .= '<td>' . $transaction->comments . '</td>';
			$output .= '</tr>';
			$i --;
		}
		$output .= '</tbody>';
	}
	$output .= '</table>';
	$output .= '</form>';

	$output .= '</div>';

	echo $output;
}

function acas4u_append_search_filter( $exclude_params ) {
	$params = '';
	if ( $_GET ) {
		foreach ( $_GET as $key => $value ) {
			if ( ! in_array( $key, $exclude_params ) ) {
				$params .= '&' . $key . '=' . $value;
			}
		}
	}

	return $params;
}

/*
 * From https://wordpress.org/plugins/paypal-framework/faq/
 * You can use the 'paypal-ipn' action to look at every message you ever get, or hook directly into a 'paypal-{transaction-type}' hook
 * https://developer.paypal.com/docs/classic/ipn/integration-guide/IPNandPDTVariables/
 */
add_action( 'paypal-ipn', 'acas4u_process_ipn' );
function acas4u_process_ipn( $data ) {
	global $wpdb;
	//file_put_contents( '/srv/site/new.acapellas4u.co.uk/www/wp-content/themes/Impreza-child/paypal.log', maybe_serialize( $data ) . "\r\n", FILE_APPEND );

	if ( $data['mc_gross'] ) {
		$gross = $data['mc_gross'];
	} else if ( $data['payment_gross'] > 0 ) {
		$gross = $data['payment_gross'];
	} else {
		$gross = 0;
	}

	if ( $data['mc_fee'] ) {
		$fee = $data['mc_fee'];
	} else if ( $data['payment_fee'] > 0 ) {
		$fee = $data['payment_fee'];
	} else {
		$fee = 0;
	}

	$query = "INSERT INTO phpbb_paypal_transactions (trans_id, parent_trans_id, timestamp, trans_type, payment_status, payment_date, payment_type, payment_gross, payment_fee, payer_firstname, payer_lastname, payer_business_name, payer_email, payer_status, user_id, comments, files, credits) VALUES ('" . $data['txn_id'] . "', '" . $data['parent_txn_id'] . "', '" . time() . "', '" . $data['txn_type'] . "', '" . $data['payment_status'] . "', '" . $data['payment_date'] . "', '" . $data['payment_type'] . "', '" . $gross . "', '" . $fee . "', '" . $data['first_name'] . "', '" . $data['last_name'] . "', '" . $data['business_name'] . "', '" . $data['payer_email'] . "', '" . $data['payer_status'] . "', '" . $data['custom'] . "', '" . $data['memo'] . "', '0', '0')";

	//file_put_contents( '/srv/site/new.acapellas4u.co.uk/www/wp-content/themes/Impreza-child/paypal.log', $query . "\r\n", FILE_APPEND );

	$user_id = $data['custom'];
	$userdata = get_user_by( 'id', $user_id );
	$user_caps = $userdata->caps;

	if ( $data['payment_status'] == 'Completed' ) {
		// payment completed, add user to donator's group
		$user_caps['donator'] = TRUE;
	} else if ( $data['payment_status'] == 'Refunded' ) {
		// payment_refunded, remove user from donator's group
		unset( $user_caps['donator'] );
	}
	update_user_meta( $user_id, 'wp_capabilities', $user_caps );
	$wpdb->query( $query );
}
