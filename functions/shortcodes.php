<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * Small shortcodes for acapellas4u.co.uk
 */

add_shortcode( 'acas4u_count_acapellas', 'acas4u_count_acapellas' );
function acas4u_count_acapellas( $atts ) {
	global $wpdb;

	$acapellas_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_type='download' AND post_status='publish';" ) );

	return number_format( $acapellas_count, 0, ',', ',' );
}

add_shortcode( 'acas4u_count_users', 'acas4u_count_users' );
function acas4u_count_users( $atts ) {
	global $wpdb;

	$user_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->users;" ) );

	return number_format( $user_count, 0, ',', ',' );
}

add_shortcode( 'acas4u_wide_search', 'acas4u_wide_search_box' );
function acas4u_wide_search_box( $atts ) {

	$output = '
	<form class="acas4u-wide-search-form">
		<div class="acas4u-search-input-group">
			<div class="acas4u-sig-select-wrapper">
				<select name="post_type" class="acas4u-sig-select">
					<option value="download" selected="selected">Acapellas</option>
					<option value="all">Website</option>
				</select>
			</div>

			<div class="acas4u-sig-input-wrapper">
				<label for="search-input" class="acas4u-sig-label"><i aria-hidden="true" class="fa fa-search"></i></label>
				<input id="search-input" type="text" name="s" placeholder="search..." class="acas4u-sig-textfield">
			</div>
		</div>

	</form>';

	return $output;
}

add_shortcode( 'acas4u_top10_popular', 'acas4u_top10_popular_acapellas' );
function acas4u_top10_popular_acapellas( $atts ) {
	$a = shortcode_atts( array(
		'count' => 10,
		'period' => 'month',
	), $atts );

	//$html = acas4u_get_popular_downloads_with_images_from_meta( $a['count'], $a['period'] );
	$html = acas4u_get_popular_downloads_with_images_from_logs( $a['count'], $a['period'] );

	return $html;
}

add_shortcode( 'acas4u_top10_latest', 'acas4u_top10_latest_acapellas' );
function acas4u_top10_latest_acapellas( $atts ) {
	$a = shortcode_atts( array(
		'count' => 10,
	), $atts );

	$html = get_latest_downloads_with_images_from_meta( $a['count'] );

	return $html;
}

add_shortcode( 'acas4u_activation_form', 'acas4u_user_activation_form' );
function acas4u_user_activation_form() {

	$error_message = [];
	$user_activated = FALSE;
	$html = '';

	//var_dump( $_POST );

	if ( isset( $_GET['user_id'] ) AND isset( $_GET['key'] ) ) {
		$username = $_GET['user_id'];
		$key = $_GET['key'];

		$user = get_user_by( 'id', $_GET['user_id'] );
		if ( is_wp_error( $user ) OR $user == FALSE ) {
			$error_message['username'] = 'This user is not exist.';
		} else {
			$user_activated_meta = get_user_meta( $user->ID, 'acas4u_has_to_be_activated', TRUE );
			if ( $user_activated_meta == 'active' ) {
				$html .= '<h2>Your account already active! Please log in.</h2>';
			} elseif ( $user_activated_meta == $_GET['key'] ) {
				update_user_meta( $user->ID, 'acas4u_has_to_be_activated', 'active' );
				$user_activated = TRUE;
				$html .= '<h2>Your account activated successfully! Please log in.</h2>';
			} else {
				$error_message['key'] = 'This activation key is not correct.';
			}
		}
	}

	// get User Object by username from POST
	$username_checked = FALSE;
	if ( isset( $_POST['username'] ) AND $_POST['username'] != '' ) {
		$username = $_POST['username'];

		$user = get_user_by( 'id', $username );
		if ( is_wp_error( $user ) OR $user == FALSE ) {
			$username_checked = FALSE;
		} else {
			$username_checked = TRUE;
		}

		if ( $username_checked == FALSE ) {
			$user = get_user_by( 'login', $username );
			if ( is_wp_error( $user ) OR $user == FALSE ) {
				$username_checked = FALSE;
			} else {
				$username_checked = TRUE;
			}
		}

		if ( $username_checked == FALSE ) {
			$user = get_user_by( 'email', $username );
			if ( is_wp_error( $user ) OR $user == FALSE ) {
				$username_checked = FALSE;
			} else {
				$username_checked = TRUE;
			}
		}

		if ( $username_checked == FALSE ) {
			$error_message['username'] = 'This user is not exist.';
		}
	}

	if ( isset( $_POST['button_activate'] ) ) {
		$key = $_POST['key'];

		if ( $username == '' ) {
			$error_message['username'] = 'You must enter an username, email or user ID.';
		}

		if ( $_POST['key'] == '' ) {
			$error_message['key'] = 'You must enter an activation key.';
		}

		if ( $user AND $username_checked == TRUE AND $_POST['key'] != '' ) {
			$user_activated_meta = get_user_meta( $user->ID, 'acas4u_has_to_be_activated', TRUE );
			if ( $user_activated_meta == 'active' ) {
				$error_message['key'] = 'This user already active.';
			} elseif ( $user_activated_meta == $_POST['key'] ) {
				update_user_meta( $user->ID, 'acas4u_has_to_be_activated', 'active' );
				$user_activated = TRUE;
				$html .= '<h2>Your account activated successfully! Please log in.</h2>';
			} else {
				$error_message['key'] = 'This activation key is not correct.';
			}
		}
	} else if ( isset( $_POST['button_resend'] ) ) {
		if ( $username == '' ) {
			$error_message['username'] = 'You must enter an username, email or user ID.';
		}
		if ( $user AND $username_checked == TRUE ) {

			$code = sha1( $user->ID . time() );
			$activation_page_id = 497973;
			$activation_args = add_query_arg( array(
				'key' => $code,
				'user_id' => $user->ID,
			), get_permalink( $activation_page_id ) );

			$activation_link = $activation_args;
			add_user_meta( $user->ID, 'acas4u_has_to_be_activated', $code, TRUE );

			if ( $user->user_email ) {
				wp_mail( trim( $user->user_email ), 'Activate your email on acapellas4u.co.uk', 'Your activation link is: ' . $activation_link );
				$html .= '<h3>Email with activation link was sent to your email ' . $user->user_email . '.</h3>';
			}
		} else {
			$html = '<h3>This user is not exist.</h3>';
		}
	}

	if ( ! is_user_logged_in() AND $user_activated == FALSE ) {
		$html .= '<div class="acas4u-activation-container">';
		$html .= '<div class="acas4u-activation-form">';
		$html .= '<form action="' . home_url( '/activate/' ) . '" method="post">';
		$html .= '<input type="hidden" name="action" value="activate_user">';
		$html .= wp_nonce_field( 'acas4u-do-user-activate', 'do_user_activate', TRUE, FALSE );

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<strong>Username, email or user ID:</strong>&nbsp;';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" class="' . acas4u_err_class( $error_message['username'] ) . '" name="username" value="' . $username . '">';
		if ( $error_message['username'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['username'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<strong>Activation key:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" class="' . acas4u_err_class( $error_message['key'] ) . '" name="key" value="' . $key . '">';
		if ( $error_message['key'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['key'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="submit" name="button_activate" value="Activate">';
		$html .= '&nbsp;&nbsp;<input type="submit" name="button_resend" value="Resend activation e-mail">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '</form>';

		$html .= '</div>'; // .acas4u-activation-form
		$html .= '</div>'; // .acas4u-register-container
	}

	return $html;
}

add_shortcode( 'acas4u_register_form', 'acas4u_user_register_form' );
function acas4u_user_register_form( $atts ) {

	/*
	echo '<pre>';
	var_dump( $_POST );
	echo '</pre>';
	*/

	$html = '';
	$new_user_registered = FALSE;
	$error_message = [];

	if ( isset( $_POST['action'] ) AND $_POST['action'] == 'register_user' ) {

		if ( $_POST['reg_username'] == '' ) {
			$error_message['reg_username'] = 'You must enter a username.';
		}

		if ( username_exists( trim( $_POST['reg_username'] ) ) ) {
			$error_message['reg_username'] = 'A user with this login already exists. Please choose another username.';
		}

		if ( mb_strlen( $_POST['reg_username'], 'UTF-8' ) < 6 ) {
			$error_message['reg_username'] = 'Username is too short';
		}

		if ( mb_strlen( $_POST['username'], 'UTF-8' ) > 20 ) {
			$error_message['reg_username'] = 'Username is too long';
		}

		if ( $_POST['reg_email'] == '' ) {
			$error_message['reg_email'] = 'You have not entered an email address.';
		}

		if ( ! filter_var( trim( $_POST['reg_email'] ), FILTER_VALIDATE_EMAIL ) ) {
			$error_message['reg_email'] = 'You have entered the wrong email.';
		}

		if ( email_exists( trim( $_POST['reg_email'] ) ) ) {
			$error_message['reg_email'] = 'User with this email already exists. Please enter an another email.';
		}

		if ( trim( $_POST['reg_email'] ) != trim( $_POST['reg_email2'] ) ) {
			$error_message['reg_email'] = 'Email and email confirmation must match.';
			$error_message['reg_email2'] = 'Email and email confirmation must match.';
		}

		if ( $_POST['reg_password'] == '' AND $_POST['reg_password2'] == '' ) {
			$error_message['reg_password'] = 'You should enter a password.';
			$error_message['reg_password2'] = 'You should enter a password confirmation.';
		}

		if ( trim( $_POST['reg_password'] ) != trim( $_POST['reg_password2'] ) ) {
			$error_message['reg_password2'] = 'Password and password confirmation must match.';
		}

		if ( mb_strlen( trim( $_POST['reg_password'] ), 'UTF-8' ) < 8 AND mb_strlen( trim( $_POST['reg_password2'] ), 'UTF-8' ) < 8 ) {
			$error_message['reg_password'] = 'The password should be at least 8 characters.';
		}

		if ( $_POST['reg_password'] == '' AND $_POST['reg_password2'] != '' ) {
			$error_message['reg_password'] = 'You must enter your password.';
		}

		if ( $_POST['reg_password'] != '' AND $_POST['reg_password2'] == '' ) {
			$error_message['reg_password2'] = 'You must enter the password confirmation.';
		}

		if ( $_POST['reg_talents'] == '' ) {
			$error_message['reg_talents'] = 'Please introduce yourself.';
		}

		if ( empty( $error_message ) ) {

			$user_id = wp_create_user( trim( $_POST['reg_username'] ), trim( $_POST['reg_password'] ), trim( $_POST['reg_email'] ) );
			if ( is_wp_error( $user_id ) ) {
				$error_message['agree'] = 'We can not register you right now. Please try again later.';
			} else {
				$code = sha1( $user_id . time() );
				$activation_page_id = 497973;
				$activation_args = add_query_arg( array(
					'key' => $code,
					'user_id' => $user_id,
				), get_permalink( $activation_page_id ) );

				//$activation_link = home_url() . $activation_args;
				$activation_link = $activation_args;
				add_user_meta( $user_id, 'acas4u_has_to_be_activated', $code, TRUE );

				// add filter for HTML content-type
				//add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
				wp_mail( trim( $_POST['reg_email'] ), 'Activate your email on acapellas4u.co.uk', 'Your activation link is: ' . $activation_link );
				// Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
				//remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

				// send login and password without integration
				//wp_new_user_notification( $user_id, NULL, 'user' );
			}

			$user = get_user_by( 'id', $user_id );
			$user->set_role( 'subscriber' );
			$userdata = array(
				'ID' => $user_id,
				'nicename' => strtolower( $_POST['reg_username'] ),
				'user_url' => $_POST['reg_url'],
			);
			wp_update_user( $userdata );

			update_user_meta( $user_id, '_bbp_phpbb_user_timezone', $_POST['reg_timezone'] );

			update_user_meta( $user_id, '_bbp_phpbb_pf_talents', $_POST['reg_talents'] );

			if ( $_POST['reg_inspiredby'] != '' ) {
				update_user_meta( $user_id, '_bbp_phpbb_pf_inspiredby', sanitize_text_field( $_POST['reg_inspiredby'] ) );
			}

			if ( $_POST['reg_genres'] != '' ) {
				update_user_meta( $user_id, '_bbp_phpbb_pf_your_genre', sanitize_text_field( $_POST['reg_genres'] ) );
			}

			if ( $_POST['reg_soundcloud'] != '' ) {
				update_user_meta( $user_id, '_bbp_phpbb_pf_soundcloud_page', $_POST['reg_soundcloud'] );
			}

			if ( $_POST['reg_favorite'] != '' ) {
				update_user_meta( $user_id, '_bbp_phpbb_pf_fav_website', $_POST['reg_favorite'] );
			}

			update_user_meta( $user_id, '_bbp_phpbb_pf_testimonials', $_POST['reg_testimonials'] );

			update_user_meta( $user_id, 'wp_capabilities', serialize( array( 'subscriber', 'bbp_participant' ) ) );
			/* user autorization is switched off because of account activation is needed
			wp_set_current_user( $user_id, $user->user_login );
			wp_set_auth_cookie( $user_id );
			do_action( 'wp_login', $user->user_login );
			*/

			$html = '<h3>You are registered!</h3>';
			$html .= '<p>Please activate your account. Activation link was sent you by email.</p>';
			$new_user_registered = TRUE;
		}
	}

	/*
	echo '<pre>';
	var_dump( $error_message );
	echo '</pre>';
	*/

	if ( $new_user_registered !== TRUE ) {

		$html = '<div class="acas4u-register-container">';

		if ( ! isset( $_POST['action'] ) ) {
			$html .= '<div class="acas4u-preregister-buttons-wrapper">';
			$html .= '<a class="w-btn color_primary acas4u-agree" href="javascript:void(0)">I agree to these terms</a>&nbsp;';
			$html .= '<a class="w-btn color_primary style_outlined not-agree" href="' . home_url( '/' ) . '">I do not agree to these terms</a>';
			$html .= '</div>';
			$hidden_form = 'hidden-form';
		} else if ( $_POST['action'] == 'register_user' ) {
			$hidden_form = '';
		}

		$html .= '<div class="acas4u-register-form ' . $hidden_form . '">';
		$html .= '<form action="' . home_url( '/register/' ) . '" method="post" id="acas4u-register-form">';
		$html .= '<input type="hidden" name="action" value="register_user">';
		$html .= wp_nonce_field( 'acas4u-do-user-register', 'do_user_register', TRUE, FALSE );

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<p>Please note that you will need to enter a valid e-mail address before your account is activated. You will receive an e-mail at the address you provide that contains an account activation link.</p>';
		$html .= '<p>The items marked with * are required profile fields and need to be filled out.</p>';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Username: *</strong><br>Length must be between 6 and 20 characters.</p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" class="' . acas4u_err_class( $error_message['reg_username'] ) . '" name="reg_username" maxlength="20" value="' . $_POST['reg_username'] . '">';
		if ( $error_message['reg_username'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_username'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>E-mail address: *</strong></p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" class="' . acas4u_err_class( $error_message['reg_email'] ) . '" name="reg_email" value="' . $_POST['reg_email'] . '">';
		if ( $error_message['reg_email'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_email'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Confirm e-mail address: *</strong></p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" class="' . acas4u_err_class( $error_message['reg_email2'] ) . '" name="reg_email2" value="' . $_POST['reg_email2'] . '">';
		if ( $error_message['reg_email2'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_email2'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Password: *</strong><br>Must be at least 8 characters.</p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="password" class="acas4u-reg-password ' . acas4u_err_class( $error_message['reg_password'] ) . '" name="reg_password">';
		if ( $error_message['reg_password'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_password'] . '</div>';
		}
		$html .= '<label class="acas4u-align-hor" for="acas4u-regform-show-password"><input type="checkbox" class="acas4u-align-hor" id="acas4u-regform-show-password">&nbsp;Show password</label>';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Confirm password: *</strong></p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="password" class="acas4u-reg-password ' . acas4u_err_class( $error_message['reg_password2'] ) . '" name="reg_password2">';
		if ( $error_message['reg_password2'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_password2'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		if ( $_POST['reg_talents'] == '' ) {
			$_POST['reg_talents'] = '0';
		}

		if ( $_POST['reg_timezone'] == '' ) {
			$_POST['reg_timezone'] = '0';
		}

		if ( $_POST['reg_testimonials'] == '' ) {
			$_POST['reg_testimonials'] = '1';
		}

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Timezone:</strong></p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<select name="reg_timezone">';
		$html .= '<option value="-12" ' . selected( '-12', $_POST['reg_timezone'], FALSE ) . ' title="[UTC - 12] Baker Island Time">[UTC - 12] Baker Island Time</option>';
		$html .= '<option value="-11" ' . selected( '-11', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 11] Niue Time, Samoa Standard Time">[UTC - 11] Niue Time, Samoa Standard Time</option>';
		$html .= '<option value="-10" ' . selected( '-10', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time">[UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time</option>';
		$html .= '<option value="-9.5" ' . selected( '-9.5', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 9:30] Marquesas Islands Time">[UTC - 9:30] Marquesas Islands Time</option>';
		$html .= '<option value="-9" ' . selected( '-9', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 9] Alaska Standard Time, Gambier Island Time">[UTC - 9] Alaska Standard Time, Gambier Island Time</option>';
		$html .= '<option value="-8" ' . selected( '-8', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 8] Pacific Standard Time">[UTC - 8] Pacific Standard Time</option>';
		$html .= '<option value="-7" ' . selected( '-7', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 7] Mountain Standard Time">[UTC - 7] Mountain Standard Time</option>';
		$html .= '<option value="-6" ' . selected( '-6', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 6] Central Standard Time">[UTC - 6] Central Standard Time</option>';
		$html .= '<option value="-5" ' . selected( '-5', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 5] Eastern Standard Time">[UTC - 5] Eastern Standard Time</option>';
		$html .= '<option value="-4.5" ' . selected( '-4.5', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 4:30] Venezuelan Standard Time">[UTC - 4:30] Venezuelan Standard Time</option>';
		$html .= '<option value="-4" ' . selected( '-4', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 4] Atlantic Standard Time">[UTC - 4] Atlantic Standard Time</option>';
		$html .= '<option value="-3.5" ' . selected( '-3.5', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 3:30] Newfoundland Standard Time">[UTC - 3:30] Newfoundland Standard Time</option>';
		$html .= '<option value="-3" ' . selected( '-3', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 3] Amazon Standard Time, Central Greenland Time">[UTC - 3] Amazon Standard Time, Central Greenland Time</option>';
		$html .= '<option value="-2" ' . selected( '-2', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time">[UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time</option>';
		$html .= '<option value="-1" ' . selected( '-1', $_POST['reg_timezone'], FALSE ) . 'title="[UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time">[UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time</option>';
		$html .= '<option value="0" ' . selected( '0', $_POST['reg_timezone'], FALSE ) . 'title="[UTC] Western European Time, Greenwich Mean Time">[UTC] Western European Time, Greenwich Mean Time</option>';
		$html .= '<option value="1" ' . selected( '1', $_POST['reg_timezone'], FALSE ) . 'title="[UTC + 1] Central European Time, West African Time">[UTC + 1] Central European Time, West African Time</option>';
		$html .= '<option value="2" ' . selected( '2', $_POST['reg_timezone'], FALSE ) . 'title="[UTC + 2] Eastern European Time, Central African Time">[UTC + 2] Eastern European Time, Central African Time</option>';
		$html .= '<option value="3" ' . selected( '3', $_POST['reg_timezone'], FALSE ) . 'title="[UTC + 3] Moscow Standard Time, Eastern African Time">[UTC + 3] Moscow Standard Time, Eastern African Time</option>';
		$html .= '<option value="3.5" ' . selected( '3.5', $_POST['reg_timezone'], FALSE ) . 'title="[UTC + 3:30] Iran Standard Time">[UTC + 3:30] Iran Standard Time</option>';
		$html .= '<option value="4" ' . selected( '4', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 4] Gulf Standard Time, Samara Standard Time">[UTC + 4] Gulf Standard Time, Samara Standard Time</option>';
		$html .= '<option value="4.5" ' . selected( '4.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 4:30] Afghanistan Time">[UTC + 4:30] Afghanistan Time</option>';
		$html .= '<option value="5" ' . selected( '5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time">[UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time</option>';
		$html .= '<option value="5.5" ' . selected( '5.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 5:30] Indian Standard Time, Sri Lanka Time">[UTC + 5:30] Indian Standard Time, Sri Lanka Time</option>';
		$html .= '<option value="5.75" ' . selected( '5.75', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 5:45] Nepal Time">[UTC + 5:45] Nepal Time</option>';
		$html .= '<option value="6" ' . selected( '6', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time">[UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time</option>';
		$html .= '<option value="6.5" ' . selected( '6.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 6:30] Cocos Islands Time, Myanmar Time">[UTC + 6:30] Cocos Islands Time, Myanmar Time</option>';
		$html .= '<option value="7" ' . selected( '7', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 7] Indochina Time, Krasnoyarsk Standard Time">[UTC + 7] Indochina Time, Krasnoyarsk Standard Time</option>';
		$html .= '<option value="8" ' . selected( '8', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time">[UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time</option>';
		$html .= '<option value="8.75" ' . selected( '8.75', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 8:45] Southeastern Western Australia Standard Time">[UTC + 8:45] Southeastern Western Australia Standard Time</option>';
		$html .= '<option value="9" ' . selected( '9', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time">[UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time</option>';
		$html .= '<option value="9.5" ' . selected( '9.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 9:30] Australian Central Standard Time">[UTC + 9:30] Australian Central Standard Time</option>';
		$html .= '<option value="10" ' . selected( '10', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time">[UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time</option>';
		$html .= '<option value="10.5" ' . selected( '10.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 10:30] Lord Howe Standard Time">[UTC + 10:30] Lord Howe Standard Time</option>';
		$html .= '<option value="11" ' . selected( '11', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 11] Solomon Island Time, Magadan Standard Time">[UTC + 11] Solomon Island Time, Magadan Standard Time</option>';
		$html .= '<option value="11.5" ' . selected( '11.5', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 11:30] Norfolk Island Time">[UTC + 11:30] Norfolk Island Time</option>';
		$html .= '<option value="12" ' . selected( '12', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time">[UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time</option>';
		$html .= '<option value="12.75" ' . selected( '12.75', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 12:45] Chatham Islands Time">[UTC + 12:45] Chatham Islands Time</option>';
		$html .= '<option value="13" ' . selected( '13', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 13] Tonga Time, Phoenix Islands Time">[UTC + 13] Tonga Time, Phoenix Islands Time</option>';
		$html .= '<option value="14" ' . selected( '14', $_POST['reg_timezone'], FALSE ) . ' title="[UTC + 14] Line Island Time">[UTC + 14] Line Island Time</option>';
		$html .= '</select>';
		$html .= '</div>';
		$html .= '</div>';

		// custom fields

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Your website:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" name="reg_url">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Dj or Producer?: *</strong><br>Which of these best describes you [required field]</p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<select class="' . acas4u_err_class( $error_message['reg_talents'] ) . '" name="reg_talents">';
		$html .= '<option value="">please choose...</option>';
		$html .= '<option ' . selected( '2', $_POST['reg_talents'], FALSE ) . ' value="2">Dj - mainly at home/friends</option>';
		$html .= '<option ' . selected( '3', $_POST['reg_talents'], FALSE ) . ' value="3">Dj - played at a club/event</option>';
		$html .= '<option ' . selected( '4', $_POST['reg_talents'], FALSE ) . ' value="4">Dj - hold a few residencies/regularly paid to play</option>';
		$html .= '<option ' . selected( '5', $_POST['reg_talents'], FALSE ) . ' value="5">Music Producer - just starting out</option>';
		$html .= '<option ' . selected( '6', $_POST['reg_talents'], FALSE ) . ' value="6">Music Producer - I know what DAW and latency are all about</option>';
		$html .= '<option ' . selected( '7', $_POST['reg_talents'], FALSE ) . ' value="7">Music Producer - pro with some releases</option>';
		$html .= '<option ' . selected( '8', $_POST['reg_talents'], FALSE ) . ' value="8">Both Dj and Producer</option>';
		$html .= '<option ' . selected( '9', $_POST['reg_talents'], FALSE ) . ' value="9">VJ</option>';
		$html .= '<option ' . selected( '10', $_POST['reg_talents'], FALSE ) . ' value="10">None of the above</option>';
		$html .= '<option ' . selected( '11', $_POST['reg_talents'], FALSE ) . ' value="11">Other</option>';
		$html .= '</select>';
		if ( $error_message['reg_talents'] ) {
			$html .= '<div class="acas4u-error">' . $error_message['reg_talents'] . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Musical influences:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" name="reg_inspiredby">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Music genre(s):</strong><br>Which music genre(s) best explain your style? (max: 50 chars)</p>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" name="reg_genres">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Soundcloud page:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" name="reg_soundcloud">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>My favorite website is:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="text" name="reg_favorite">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '<p><strong>Recommendation:</strong>';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<select name="reg_testimonials">';
		$html .= '<option ' . selected( '1', $_POST['reg_testimonials'], FALSE ) . ' value="1">No</option>';
		$html .= '<option ' . selected( '2', $_POST['reg_testimonials'], FALSE ) . ' value="2">Yes</option>';
		$html .= '</select>';

		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="acas4u-rf-row">';
		$html .= '<div class="acas4u-rf-col w25pc">';
		$html .= '</div>';
		$html .= '<div class="acas4u-rf-col w75pc">';
		$html .= '<input type="submit" value="Submit">';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '</form>';
		$html .= '</div>'; // .acas4u-register-form
		$html .= '</div>'; // .acas4u-register-container
	}

	return $html;
}

add_shortcode( 'acas4u_our_acapellas_list', 'acas4u_our_acapellas_list_html' );
function acas4u_our_acapellas_list_html( $atts ) {
	$a = shortcode_atts( array(
		'count' => - 1,
		'order' => 'ASC',
		'orderby' => 'title',
		'show_link' => 'no',
	), $atts );

	$ajax_nonce = wp_create_nonce( 'acas4u_show_our_acapellas_list_nonce' );

	$html = '<div class="acas4u-load-acapellas-list" data-nonce="' . $ajax_nonce . '" data-links="' . $a['show_link'] . '" data-count="' . $a['count'] . '" data-order="' . $a['order'] . '" data-orderby="' . $a['orderby'] . '">
				<div id="loader-container">
					<div id="loader"></div>
					<p class="acas4u-load-text">Loading...</p>
				</div>
			</div>';

	return $html;
}

/**
 * Ajax for our acapellas list
 */
add_action( 'wp_ajax_show_our_acapellas_list', 'acas4u_show_our_acapellas_list_index' );
add_action( 'wp_ajax_nopriv_show_our_acapellas_list', 'acas4u_show_our_acapellas_list_index' );
function acas4u_show_our_acapellas_list_index() {
	global $wpdb;

	check_ajax_referer( 'acas4u_show_our_acapellas_list_nonce', '_ajax_nonce' );

	if ( ! $_POST['count'] ) {
		wp_send_json_error();
	} else {
		$count = $_POST['count'];
	}

	if ( ! $_POST['order'] ) {
		wp_send_json_error();
	} else {
		$order = $_POST['order'];
	}

	if ( ! $_POST['orderby'] ) {
		wp_send_json_error();
	} else {
		$orderby = $_POST['orderby'];
	}

	if ( ! $_POST['links'] ) {
		wp_send_json_error();
	} else {
		$show_link = $_POST['links'];
	}

	$query = 'SELECT
				t1.download_post_id,
				t1.download_permalink,
				t1.download_post_name,
				t1.download_artist1,
				t1.download_artist2,
				t1.download_trackname,
				t1.download_bitrate,
				t1.download_mode,
				t1.download_size,
				t1.download_rating,
				t1.download_votes,
				t1.download_duration,
				t1.download_count,
				t1.download_key,
				t1.download_key2,
				t1.download_tempo,
				t1.download_filename,
				t1.download_post_date
			  FROM acas4u_download_indexes t1';

	$downloads = $wpdb->get_results( $query );

	$current_downloads = count( $downloads );

	$html = '';
	if ( $current_downloads > 0 ) {

		$html .= '<h2>Total: ' . $current_downloads . ' acapellas</h2>';

		$html .= '<ul class="acas4u-acapellas-list">';
		foreach ( $downloads as $download ) {

			$download_permalink = $download->download_permalink;
			//TODO replace hardcoded URL by function;
			$download_permalink = str_replace( 'http://new.', 'http://', $download_permalink );

			if ( $download->download_artist1 != '' AND $download->download_trackname != '' ) {
				$title = $download->download_artist1 . ' ' . $download->download_trackname;
			} else if ( $download->download_artist2 != '' AND $download->download_trackname != '' ) {
				$title = $download->download_artist2 . ' ' . $download->download_trackname;
			} else {
				$title = $download->download_post_name;
			}

			$html .= '<li>';
			if ( $show_link == 'yes' ) {
				$html .= '<a href="' . $download_permalink . '">' . $title . '</a>';
			} else {
				$html .= $title;
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
	}

	wp_send_json_success( array(
		'html' => $html,
	) );
}

//add_action( 'wp_ajax_show_our_acapellas_list', 'acas4u_show_our_acapellas_list' );
//add_action( 'wp_ajax_nopriv_show_our_acapellas_list', 'acas4u_show_our_acapellas_list' );
function acas4u_show_our_acapellas_list() {
	check_ajax_referer( 'acas4u_show_our_acapellas_list_nonce', '_ajax_nonce' );

	if ( ! $_POST['count'] ) {
		wp_send_json_error();
	} else {
		$count = $_POST['count'];
	}

	if ( ! $_POST['order'] ) {
		wp_send_json_error();
	} else {
		$order = $_POST['order'];
	}

	if ( ! $_POST['orderby'] ) {
		wp_send_json_error();
	} else {
		$orderby = $_POST['orderby'];
	}

	if ( ! $_POST['links'] ) {
		wp_send_json_error();
	} else {
		$show_link = $_POST['links'];
	}

	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => $order,
		'orderby' => $orderby,
	);

	$html = '';
	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {

		$html .= '<h2>Total: ' . $downloads->post_count . ' acapellas</h2>';

		$html .= '<ul class="acas4u-acapellas-list">';
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$html .= '<li>';
			if ( $show_link == 'yes' ) {
				$html .= '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';
			} else {
				$html .= get_the_title();
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
	}
	wp_reset_query();

	wp_send_json_success( array(
		'html' => $html,
	) );
}
