<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

function get_similar_tracks_by_bpm( $bpm, $count ) {
	global $acas4u_stylesheet_directory_uri;

	$bpm_start = ceil( $bpm - ( $bpm * 0.1 ) );
	$bpm_end = ceil( $bpm + ( $bpm * 0.1 ) );

	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => 'DESC',
		'orderby' => 'date',
		'meta_query' => array(
			array(
				'key' => '_download_tempo',
				'value' => array( $bpm_start, $bpm_end ),
				'type' => 'numeric',
				'compare' => 'BETWEEN',
			),
		),
	);
	$posts = new WP_Query( $args );
	if ( $posts->have_posts() ) {
		$output = '<div class="acapella-similar-bpm-wrapper">';
		while ( $posts->have_posts() ){
			$posts->the_post();
			$post_id = $posts->post->ID;
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );
			$filename = get_post_meta( $post_id, '_download_filename', TRUE );
			$permalink = get_the_permalink();

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
			if ( $artist_thumbnail_id ) {
				$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
				$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
			} else {
				$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
			}
			$artist_thumbnail_img = '<img src="' . $artist_thumbnail_url . '" alt="' . $title . '>" class="img-responsive">';

			$short_title = acas4u_limit_chars( $title, 40 );
			$output .= '<div class="acas4u-similar-single" title="' . $filename . '">';
			$output .= '<a class="acas4u-similar-link" href="' . $permalink . '">';
			$output .= '<div class="acas4u-similar-image">' . $artist_thumbnail_img . '</div>';
			$output .= '<div class="acas4u-similar-inner">';
			$output .= '<div class="acas4u-similar-overlay"></div>';
			$output .= '<div class="acas4u-similar-title">' . $short_title . '</div>';
			$output .= '</div>';
			$output .= '</a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();

	return $output;
}

function get_similar_artist_by_id_en_api( $post_id, $artist_enid, $count ) {
	global $en_api_key, $acas4u_stylesheet_directory_uri, $wpdb;

	if ( ! $count ) {
		$count = 10;
	}

	$similar_artists = get_post_meta( $post_id, '_download_similar_artists', TRUE );

	if ( empty( $similar_artists ) ) {
		$url = 'http://developer.echonest.com/api/v4/artist/similar?api_key=' . $en_api_key . '&id=' . $artist_enid . '&format=json&results=' . $count . '&start=0';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			$artists = $json->response->artists;
			foreach ( $artists as $artist ) {
				$similar_artists[] = array(
					'name' => $artist->name,
					'id' => $artist->id,
				);
			}

			if ( ! empty( $similar_artists ) ) {
				update_post_meta( $post_id, '_download_similar_artists', serialize( $similar_artists ) );
			}
		}
	} else {
		$similar_artists = unserialize( $similar_artists );
	}

	$output = '<div class="acapella-similar-artist-wrapper">';
	foreach ( $similar_artists as $artist_id => $artist_data ) {
		$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_artist_enid" AND meta_value= "' . $artist_data['id'] . '"';
		$post_from_meta = $wpdb->get_row( $sqlquery );
		$artist_id = $post_from_meta->post_id;
		$artist_link = get_permalink( $artist_id ) . '#tracks';
		if ( $artist_link == '' ) {
			$artist_link = 'javascript:void(0);';
		}

		$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
		if ( $artist_thumbnail_id ) {
			$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
			$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
		} else {
			$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
		}
		$artist_thumbnail_img = '<img src="' . $artist_thumbnail_url . '" alt="' . $artist_data['name'] . '>" class="img-responsive">';

		$track_link = get_similar_artist_track_link( $artist_data['id'] );
		if ( ! $track_link ) {
			$track_link = 'javascript:void(0);';
		}

		$output .= '<div class="acas4u-similar-single">';
		//$output .= '<a class="acas4u-similar-link" href="' . $track_link . '" title="' . $artist_data['name'] . '">';
		$output .= '<a class="acas4u-similar-link" href="' . $artist_link . '" title="' . $artist_data['name'] . '">';
		$output .= '<div class="acas4u-similar-image">' . $artist_thumbnail_img . '</div>';
		$output .= '<div class="acas4u-similar-inner">';
		$output .= '<div class="acas4u-similar-overlay"></div>';
		$output .= '<div class="acas4u-similar-title">' . $artist_data['name'] . '</div>';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>';
	}
	$output .= '</div>';

	return $output;
}

function get_similar_artist_track_link( $artist_enid ) {

	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'date',
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key' => '_download_artist1id',
				'value' => $artist_enid,
				'compare' => '=',
			),
			array(
				'key' => '_download_artist2id',
				'value' => $artist_enid,
				'compare' => '=',
			),
		),
	);

	$permalink = [];
	$posts = new WP_Query( $args );
	if ( $posts->have_posts() ) {
		while ( $posts->have_posts() ){
			$posts->the_post();
			$permalink = get_the_permalink();
		}
	}
	wp_reset_query();

	return $permalink;
}
