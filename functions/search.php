<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

add_action( 'init', 'acas4u_search_template_rewrite' );
function acas4u_search_template_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%pagetype%', '([^&]+)' );
	add_rewrite_tag( '%searchtype%', '([^&]+)' );
	add_rewrite_rule( '^search/([^/]*)/?', 'index.php?pagetype=search&searchtype=$matches[1]', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	$wp_rewrite->flush_rules();
}

add_action( 'template_redirect', 'acas4u_search_template_redirect' );
function acas4u_search_template_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'search' AND $template['searchtype'] == 'acapellas' ) {

		include( $acas4u_stylesheet_directory . '/templates/downloads-search-results.php' );
		exit;
	}
}

add_filter( 'wp_title', 'acas4u_search_template_title' );
function acas4u_search_template_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'search' AND $template['searchtype'] == 'acapellas' ) {
		$title = 'Search Acapellas';
	}

	return $title;
}

add_filter( 'body_class', 'acas4u_search_body_class' );
function acas4u_search_body_class( $classes ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'search' ) {
		$classes[] = 'search';
	}

	return $classes;
}

add_filter( 'template_include', 'acapella_search_template' );
function acapella_search_template( $template ) {
	global $wp_query;
	$post_type = get_query_var( 'post_type' );
	if ( $wp_query->is_search AND $post_type == 'download' ) {

		return locate_template( 'downloads-search.php' );  //  redirect to downloads-search.php
	}

	return $template;
}

function acas4u_display_rating_stars( $rating ) {
	$rating_floor = floor( $rating );
	$rating_ceil = ceil( $rating );

	$output = '<div class="acas4u-user-ratings-stars">';
	for ( $i = 0; $i < 5; $i ++ ) {
		if ( $i == $rating_floor AND $rating_ceil != $rating_floor ) {
			$star_class = 'fa-star-half-o';
		} else if ( $i >= $rating_floor ) {
			$star_class = 'fa-star-o';
		} else {
			$star_class = 'fa-star';
		}
		$output .= '<i class="fa ' . $star_class . '"></i>';
	}

	$output .= '</div >';

	return $output;
}

/**
 * Ajax for infinite scroll
 */
add_action( 'wp_ajax_do_scroll', 'acas4u_do_scroll' );
add_action( 'wp_ajax_nopriv_do_scroll', 'acas4u_do_scroll' );
function acas4u_do_scroll() {
	check_ajax_referer( 'acas4u_do_scroll_nonce', '_ajax_nonce' );

	$page = $_POST['page'];
	$count = ( $_POST['count'] != '' ) ? $_POST['count'] : 20;
	if ( $page ) {
		$offset = $page * $count;
	} else {
		wp_send_json_error();
	}

	$search_term = $_POST['search_term'];
	$query_genre = $_POST['genre'];
	$query_bpm1 = $_POST['bpm1'];
	$query_bpm2 = $_POST['bpm2'];
	$query_bitrate1 = $_POST['bitrate1'];
	$query_bitrate2 = $_POST['bitrate2'];
	$query_rating1 = $_POST['rating1'];
	$query_rating2 = $_POST['rating2'];
	$query_mode = $_POST['mode'];
	$query_key1 = $_POST['key1'];
	$query_key2 = $_POST['key2'];
	$count = $_POST['count'];
	$search_type = $_POST['search_type'];

	if ( $search_type != 'custom' ) {
		$args = acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, FALSE, $search_type );
		$search_results = acas4u_show_search_results( $args );
	} else {
		$args_custom = array(
			'search_term' => $search_term,
			'offset' => $offset,
			'count' => $count,
			'query_genre' => $query_genre,
			'query_bpm1' => $query_bpm1,
			'query_bpm2' => $query_bpm2,
			'query_bitrate1' => $query_bitrate1,
			'query_bitrate2' => $query_bitrate2,
			'query_rating1' => $query_rating1,
			'query_rating2' => $query_rating2,
			'query_mode' => $query_mode,
			'query_key1' => $query_key1,
			'query_key2' => $query_key2,
		);
		$search_results = acas4u_show_custom_search_results( $args_custom );
		//var_dump( $args_custom );
	}

	wp_send_json_success( array(
		'html' => $search_results,
		'page' => $page + 1,
	) );
}

/**
 * Ajax for search
 */
add_action( 'wp_ajax_do_update_search', 'acas4u_do_update_search' );
add_action( 'wp_ajax_nopriv_do_update_search', 'acas4u_do_update_search' );
function acas4u_do_update_search() {
	check_ajax_referer( 'acas4u_do_update_search_nonce', '_ajax_nonce' );

	$search_term = $_POST['search_term'];
	$query_genre = $_POST['genre'];
	$query_bpm1 = $_POST['bpm1'];
	$query_bpm2 = $_POST['bpm2'];
	$query_bitrate1 = $_POST['bitrate1'];
	$query_bitrate2 = $_POST['bitrate2'];
	$query_rating1 = $_POST['rating1'];
	$query_rating2 = $_POST['rating2'];
	$query_mode = $_POST['mode'];
	$query_key1 = $_POST['key1'];
	$query_key2 = $_POST['key2'];
	$count = $_POST['count'];
	$search_type = $_POST['search_type'];
	$offset = 0;

	if ( $search_type != 'custom' ) {
		$args = acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, FALSE, $search_type );
		$search_results = acas4u_show_search_results( $args );
	} else {
		$args_custom = array(
			'search_term' => $search_term,
			'offset' => $offset,
			'count' => $count,
			'query_genre' => $query_genre,
			'query_bpm1' => $query_bpm1,
			'query_bpm2' => $query_bpm2,
			'query_bitrate1' => $query_bitrate1,
			'query_bitrate2' => $query_bitrate2,
			'query_rating1' => $query_rating1,
			'query_rating2' => $query_rating2,
			'query_mode' => $query_mode,
			'query_key1' => $query_key1,
			'query_key2' => $query_key2,
		);
		$search_results = acas4u_show_custom_search_results( $args_custom );
		//var_dump( $args_custom );
	}

	wp_send_json_success( array(
		'html' => $search_results,
	) );
}

function acas4u_build_custom_search_query( $query_start, $args ) {

	if ( $query_start == '' ) {
		return FALSE;
	}

	$query_args = [];

	$query = $query_start;

	if ( $args['query_genre'] != '' ) {
		$query .= ' LEFT JOIN wp_term_relationships t2 ON t1.download_post_id = t2.object_id';
		$query_args[] = 't2.term_taxonomy_id="' . $args['query_genre'] . '"';
	}

	if ( $args['search_term'] != '' ) {
		if ( strrpos( $args['search_term'], ' ' ) !== FALSE ) {
			$query_args_like = [];
			$search_terms_array = explode( ' ', $args['search_term'] );
			foreach ( $search_terms_array AS $search_term ) {
				$query_args_like[] = 't1.download_post_name LIKE "%' . $search_term . '%"';
			}
			//$query_args[] = '( ' . implode( ' OR ', $query_args_like ) . ' )';
			$query_args[] = implode( ' OR ', $query_args_like );
		} else {
			$query_args[] = 't1.download_post_name LIKE "%' . $args['search_term'] . '%"';
		}
	}

	if ( $args['query_bpm1'] != '' ) {
		$query_args[] = 't1.download_tempo >= "' . $args['query_bpm1'] . '"';
	}

	if ( $args['query_bpm2'] != '' ) {
		$query_args[] = 't1.download_tempo <= "' . $args['query_bpm2'] . '"';
	}

	if ( $args['query_bitrate1'] != '' ) {
		$query_args[] = 't1.download_bitrate >= "' . $args['query_bitrate1'] . '"';
	}

	if ( $args['query_bitrate2'] != '' ) {
		$query_args[] = 't1.download_bitrate <= "' . $args['query_bitrate2'] . '"';
	}

	if ( $args['query_rating1'] != '' ) {
		$query_args[] = 't1.download_rating >= "' . $args['query_rating1'] . '"';
	}

	if ( $args['query_rating2'] != '' ) {
		$query_args[] = 't1.download_rating <= "' . $args['query_rating2'] . '"';
	}

	if ( $args['query_mode'] != '' ) {
		$query_args[] = 't1.download_mode = "' . $args['query_mode'] . '"';
	}

	if ( $args['query_key1'] != '' ) {
		$query_args[] = 't1.download_key = "' . $args['query_key1'] . '"';
	}

	if ( $args['query_key2'] != '' ) {
		$query_args[] = 't1.download_key2 = "' . $args['query_key2'] . '"';
	}

	if ( count( $query_args ) > 0 ) {
		$query .= ' WHERE ' . implode( ' AND ', $query_args );
	}

	$offset = ( intval( $args['offset'] ) > 0 ) ? intval( $args['offset'] ) : 0;
	$count = ( intval( $args['count'] ) > 0 ) ? intval( $args['count'] ) : 20;
	$query .= ' ORDER BY t1.download_post_name ASC LIMIT ' . $offset . ',' . $count;

	/*
	echo '<pre>';
	echo $query;
	echo '</pre>';
	*/

	return $query;
}

function acas4u_show_custom_search_results( $args ) {
	global $wpdb;

	$user_logged = is_user_logged_in();
	$output = '';
	$output_mobile = '';

	$query_start = 'SELECT
				t1.download_post_id,
				t1.download_permalink,
				t1.download_post_name,
				t1.download_artist1,
				t1.download_artist2,
				t1.download_trackname,
				t1.download_bitrate,
				t1.download_mode,
				t1.download_size,
				t1.download_rating,
				t1.download_votes,
				t1.download_duration,
				t1.download_count,
				t1.download_key,
				t1.download_key2,
				t1.download_tempo,
				t1.download_filename,
				t1.download_post_date
			  FROM acas4u_download_indexes t1';

	$query = acas4u_build_custom_search_query( $query_start, $args );

	//$query_log = preg_replace( "/(\/[^>]*>)([^<]*)(<)/", "\\1\\3", $query );
	//acas4u_write_log_entry( 'Query search: ' . $query_log, 'acas4u_search_query.log' );

	$downloads = $wpdb->get_results( $query );
	$total_downloads = acas4u_get_posts_count( $args, 'custom' );
	$current_downloads = count( $downloads );

	if ( $current_downloads > 0 ) {
		foreach ( $downloads as $download ) {

			$download_permalink = $download->download_permalink;
			//TODO replace hardcoded URL by function;
			$download_permalink = str_replace( 'http://new.', 'http://', $download_permalink );

			$download_mode = '';
			if ( $download->download_mode != '' ) {
				$download_mode = ( $download->download_mode == '1' ) ? 'major' : 'minor';
			}
			$download_size = formatBytes( $download->download_size );
			$download_rating = ( $download->download_rating != '' ) ? $download->download_rating : 0;
			$date_uploaded = date( 'd/m/Y', strtotime( $download->download_post_date ) );
			$rating_stars = acas4u_display_rating_stars( $download->download_rating );

			if ( $download->download_artist1 != '' AND $download->download_trackname != '' ) {
				$title = $download->download_artist1 . ' ' . $download->download_trackname;
			} else if ( $download->download_artist2 != '' AND $download->download_trackname != '' ) {
				$title = $download->download_artist2 . ' ' . $download->download_trackname;
			} else {
				$title = $download->download_post_name;
			}

			// create html for mobile version (stacktable)
			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<th class="st-head-row" colspan="2"><a href="' . $download_permalink . '" title="Filename: ' . $download->download_filename . '">' . $title . '</a></th>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="1">Mode</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="2">Key</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-key1">' . acas4u_key_value( $download->download_key ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			if ( $user_logged ) {
				$output_mobile .= '<tr class="acas4u-search-tr">';
				$output_mobile .= '<td class="st-key" data-index="3">Key</td>';
				$output_mobile .= '<td class="st-val acas4u-search-td td-key2">' . acas4u_keymode_value( $download->download_key2 ) . '</td>';
				$output_mobile .= '</tr>' . "\n";
			}

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="4">BPM</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bpm">' . $download->download_tempo . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="5">Bitrate</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bitrate">' . $download->download_bitrate . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="6">Size</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-size">' . $download_size . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="7">Length</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-length">' . acas4u_format_duration( $download->download_duration ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="8">Listeners</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-listeners">' . number_format( $download->download_count, 0, ',', ',' ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="9">Added</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="10">Rating</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-rating" title="Total votes: ' . $download->download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output .= '<tr class="acas4u-search-tr" role="row">';
			$output .= '<td class="acas4u-search-td td-title"><a href="' . $download_permalink . '" title="Filename: ' . $download->download_filename . '">' . $title . '</a></td>';
			$output .= '<td class="acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output .= '<td class="acas4u-search-td td-key1">' . acas4u_key_value( $download->download_key ) . '</td>';
			if ( $user_logged ) {
				$output .= '<td class="acas4u-search-td td-key2">' . acas4u_keymode_value( $download->download_key2 ) . '</td>';
			}
			$output .= '<td class="acas4u-search-td td-bpm">' . $download->download_tempo . '</td>';
			$output .= '<td class="acas4u-search-td td-bitrate">' . $download->download_bitrate . '</td>';
			$output .= '<td class="acas4u-search-td td-size">' . $download_size . '</td>';
			$output .= '<td class="acas4u-search-td td-length">' . acas4u_format_duration( $download->download_duration ) . '</td>';
			$output .= '<td class="acas4u-search-td td-listeners">' . number_format( $download->download_count, 0, ',', ',' ) . '</td>';
			$output .= '<td class="acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output .= '<td class="acas4u-search-td td-rating" title="Total votes: ' . $download->download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output .= '</tr>' . "\n";
		}
	} else {
		$output .= 'No results found';
	}

	return array( 'large' => $output, 'small' => $output_mobile, 'total' => $total_downloads, 'current' => $current_downloads );
}

function acas4u_show_search_results( $args ) {
	$search_results = new WP_Query( $args );
	$user_logged = is_user_logged_in();

	if ( $search_results->have_posts() ) {
		// Start the loop.
		$output = '';
		$output_mobile = '';
		while ( $search_results->have_posts() ){
			$search_results->the_post();

			$permalink = get_the_permalink();
			$post_id = get_the_ID();

			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );

			$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
			if ( $download_mode != '' ) {
				$download_mode = ( $download_mode == 1 ) ? 'major' : 'minor';
			}
			// $download_bpm = get_post_meta( $post_id, '_download_bpm', TRUE ); use 'tempo' instead of 'bpm'
			$download_size = get_post_meta( $post_id, '_download_size', TRUE );
			$download_size = formatBytes( $download_size );
			$download_rating = get_post_meta( $post_id, '_download_rating', TRUE );
			if ( $download_rating == '' ) {
				$download_rating = 0;
			}
			$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );

			$download_count = get_post_meta( $post_id, '_download_count', TRUE );
			$download_key = (int) get_post_meta( $post_id, '_download_key', TRUE );
			$download_keymode = (int) get_post_meta( $post_id, '_download_keymode_id', TRUE );
			$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );
			$date_uploaded = get_the_date( 'd/m/Y' );
			$download_filename = get_post_meta( $post_id, '_download_filename', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$rating_stars = acas4u_display_rating_stars( $download_rating );

			// create html for mobile version (stacktable)
			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<th class="st-head-row" colspan="2"><a href="' . $permalink . '" title="Filename: ' . $download_filename . '">' . $title . '</a></th>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="1">Mode</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="2">Key</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-key1">' . acas4u_key_value( $download_key ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			if ( $user_logged ) {
				$output_mobile .= '<tr class="acas4u-search-tr">';
				$output_mobile .= '<td class="st-key" data-index="3">Key</td>';
				$output_mobile .= '<td class="st-val acas4u-search-td td-key2">' . acas4u_keymode_value( $download_keymode ) . '</td>';
				$output_mobile .= '</tr>' . "\n";
			}

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="4">BPM</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bpm">' . $download_tempo . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="5">Bitrate</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-bitrate">' . $download_bitrate . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="6">Size</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-size">' . $download_size . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="7">Length</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-length">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="8">Listeners</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-listeners">' . number_format( $download_count, 0, ',', ',' ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="9">Added</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-search-tr">';
			$output_mobile .= '<td class="st-key" data-index="10">Rating</td>';
			$output_mobile .= '<td class="st-val acas4u-search-td td-rating" title="Total votes: ' . $download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output .= '<tr class="acas4u-search-tr" role="row">';
			$output .= '<td class="acas4u-search-td td-title"><a href="' . $permalink . '" title="Filename: ' . $download_filename . '">' . $title . '</a></td>';
			$output .= '<td class="acas4u-search-td td-mode">' . $download_mode . '</td>';
			$output .= '<td class="acas4u-search-td td-key1">' . acas4u_key_value( $download_key ) . '</td>';
			if ( $user_logged ) {
				$output .= '<td class="acas4u-search-td td-key2">' . acas4u_keymode_value( $download_keymode ) . '</td>';
			}
			$output .= '<td class="acas4u-search-td td-bpm">' . $download_tempo . '</td>';
			$output .= '<td class="acas4u-search-td td-bitrate">' . $download_bitrate . '</td>';
			$output .= '<td class="acas4u-search-td td-size">' . $download_size . '</td>';
			$output .= '<td class="acas4u-search-td td-length">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output .= '<td class="acas4u-search-td td-listeners">' . number_format( $download_count, 0, ',', ',' ) . '</td>';
			$output .= '<td class="acas4u-search-td td-added">' . $date_uploaded . '</td>';
			$output .= '<td class="acas4u-search-td td-rating" title="Total votes: ' . $download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output .= '</tr>' . "\n";
		}
	} else {
		$output = '<tr class="acas4u-search-tr">';
		$output .= '<td class="acas4u-search-td">No results found.</td>';
		$output .= '</tr>';
	}
	wp_reset_query();

	return array( 'large' => $output, 'small' => $output_mobile );
}

function acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, $no_found_rows ) {
	$count = ( $count != '' ) ? $count : 20;

	$args = array(
		'post_type' => 'download',
		'posts_per_page' => $count,
		'order' => 'ASC',
		'orderby' => 'title',
		'post_status' => 'publish',
		'no_found_rows' => $no_found_rows,
	);

	if ( $search_term ) {
		$args['s'] = $search_term;
	}

	if ( $offset != '' ) {
		$args['offset'] = $offset;
	}

	if ( $query_genre != '' ) {
		$genre_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'genre',
					'field' => 'slug',
					'terms' => $query_genre,
				),
			),
		);
		$args = $args + $genre_args;
	}

	$args['meta_query']['relation'] = 'AND';

	if ( $query_bpm1 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_tempo',
			'type' => 'numeric',
			'value' => $query_bpm1,
			'compare' => '>=',
		);
	}

	if ( $query_bpm2 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_tempo',
			'type' => 'numeric',
			'value' => $query_bpm2,
			'compare' => '<=',
		);
	}

	if ( $query_bitrate1 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_bitrate',
			'type' => 'numeric',
			'value' => $query_bitrate1,
			'compare' => '>=',
		);
	}

	if ( $query_bitrate2 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_bitrate',
			'type' => 'numeric',
			'value' => $query_bitrate2,
			'compare' => '<=',
		);
	}

	if ( $query_rating1 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_rating',
			'type' => 'numeric',
			'value' => $query_rating1,
			'compare' => '>=',
		);
	}

	if ( $query_rating2 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_rating',
			'type' => 'numeric',
			'value' => $query_rating2,
			'compare' => '<=',
		);
	}

	if ( $query_mode != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_mode',
			'type' => 'numeric',
			'value' => $query_mode,
			'compare' => '=',
		);
	}

	if ( $query_key1 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_key',
			'type' => 'numeric',
			'value' => $query_key1,
			'compare' => '=',
		);
	}

	if ( $query_key2 != '' ) {
		$args['meta_query'][] = array(
			'key' => '_download_keymode_id',
			'type' => 'numeric',
			'value' => $query_key2,
			'compare' => '=',
		);
	}

	return $args;
}

function acas4u_save_search_tag( $search_tag, $user_id, $user_ip ) {
	global $wpdb;

	$existing_tags = $wpdb->get_var( "SELECT tag_id FROM acas4u_search_tags WHERE user_id = '" . $user_id . "' AND search_tag = '" . trim( $search_tag ) . "'" );
	if ( ! $existing_tags ) {
		$current_date = date( 'Y-m-d H:i:s' );
		$user_id = ( $user_id != '' ) ? $user_id : 0;

		$tag_add = (int) $wpdb->get_var( "SELECT tag_id FROM acas4u_search_tags WHERE search_tag = '" . trim( $search_tag ) . "'" );
		if ( $tag_add > 0 ) {
			$wpdb->query( "UPDATE acas4u_search_tags SET tag_quantity = tag_quantity + 1 WHERE tag_id = '" . $tag_add . "'" );
		} else {
			$wpdb->insert( 'acas4u_search_tags', array(
				'tag_quantity' => 1,
				'user_id' => $user_id,
				'user_ip' => $user_ip,
				'search_date' => $current_date,
				'search_tag' => trim( $search_tag ),
			), array( '%d', '%d', '%s', '%s', '%s' ) );
		}
	}
}

function acas4u_get_tag_class( $tag_quantity ) {
	$tag_class = 'tag-xs';
	if ( $tag_quantity > 5 AND $tag_quantity < 10 ) {
		$tag_class = 'tag-s';
	} else if ( $tag_quantity >= 10 AND $tag_quantity < 15 ) {
		$tag_class = 'tag-m';
	} else if ( $tag_quantity >= 15 AND $tag_quantity < 20 ) {
		$tag_class = 'tag-l';
	} else if ( $tag_quantity >= 20 AND $tag_quantity < 25 ) {
		$tag_class = 'tag-xl';
	} else if ( $tag_quantity >= 25 ) {
		$tag_class = 'tag-xxl';
	}

	return $tag_class;
}

?>
