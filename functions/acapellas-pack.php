<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * declare custom post type - acapellas pack
 */
add_action( 'init', 'acas4u_custom_post_acapellas_pack' );
function acas4u_custom_post_acapellas_pack() {
	$labels = array(
		'name' => _x( 'Acapellas packs', 'post type general name', 'acapellas4u' ),
		'singular_name' => _x( 'Acapellas pack', 'post type singular name', 'acapellas4u' ),
		'add_new' => _x( 'Add New', 'pack', 'acapellas4u' ),
		'add_new_item' => __( 'Add New pack', 'acapellas4u' ),
		'edit_item' => __( 'Edit Acapellas pack', 'acapellas4u' ),
		'new_item' => __( 'New Acapellas pack', 'acapellas4u' ),
		'all_items' => __( 'All Acapellas packs', 'acapellas4u' ),
		'view_item' => __( 'View Acapellas pack', 'acapellas4u' ),
		'search_items' => __( 'Search Acapellas packs', 'acapellas4u' ),
		'not_found' => __( 'No Acapellas packs found', 'acapellas4u' ),
		'not_found_in_trash' => __( 'No Acapellas packs found in the Trash', 'acapellas4u' ),
		'parent_item_colon' => '',
		'menu_name' => __( 'Acapellas packs', 'acapellas4u' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Add an acapellas pack link', 'acapellas4u' ),
		'menu_icon' => 'dashicons-playlist-audio',
		'public' => TRUE,
		'show_in_nav_menus' => FALSE,
		'exclude_from_search' => TRUE,
		'supports' => array( 'title', 'editor', 'page-attributes', 'thumbnail', 'author', 'comments' ),
		'has_archive' => TRUE,
	);
	register_post_type( 'acapellas_packs', $args );
}

function acas4u_get_best_acapellas_pack( $count, $order ) {
	$args = array(
		'post_type' => 'acapellas_packs',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => $order,
		'orderby' => 'date',
	);
	$posts = new WP_Query( $args );
	$output = '';
	if ( $posts->have_posts() ) {
		$output = '<div class="acapella-packs-wrapper">';
		while ( $posts->have_posts() ){
			$posts->the_post();
			$post_id = $posts->post->ID;
			$title = get_the_title();
			$post_external_url = get_post_meta( $post_id, 'ExternalURL', TRUE );

			$post_image = '';
			$post_thumbnail_id = get_post_thumbnail_id( $post_id );
			if ( $post_thumbnail_id ) {
				$post_image = get_the_post_thumbnail( $post_id, array( 140, 140 ) );
			}

			$output .= '<div class="acapella-pack-item">';
			$output .= '<a class="acapella-pack-link" href="' . esc_url( $post_external_url ) . '" target="_blank" title="' . $title . '">';
			$output .= $post_image;
			$output .= '</a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();

	return $output;
}
