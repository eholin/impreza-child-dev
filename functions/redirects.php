<?php
add_action( 'template_redirect', 'acas4u_do_redirect_on_404' );
function acas4u_do_redirect_on_404() {
	global $wp_query, $wpdb;

	$type = '';

	$url = $_SERVER['REQUEST_URI'];
	if ( is_404() ) {

		$matches = array();
		preg_match_all( '!\d+!', $url, $matches );

		if ( is_array( $matches ) ) {
			$_id = $matches[0][0];
			$_page = $matches[0][1];

			if ( isset( $_GET['p'] ) AND $_GET['p'] != '' ) {
				$_id = $_GET['p'];
			} else if ( isset( $_GET['u'] ) AND $_GET['mode'] == 'viewprofile' ) {
				$_id = $_GET['u'];
			}
		}

		if ( stripos( $url, 'forum' ) !== FALSE ) {
			$post_id = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_bbp_old_forum_id' AND meta_value='$_id'" );
		} else if ( stripos( $url, 'topic' ) !== FALSE ) {
			$post_id = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_bbp_old_topic_id' AND meta_value='$_id'" );
		} else if ( stripos( $url, 'post' ) !== FALSE ) {
			$post_id = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_bbp_old_post_id' AND meta_value='$_id'" );
		} else if ( stripos( $url, 'member' ) !== FALSE ) {
			$type = 'user';
			$post_id = $_id;
		}

		if ( $post_id ) {
			if ( $type == 'user' ) {
				$author = get_userdata( $post_id );
				$author_name = $author->user_nicename;
				$permalink = home_url( '/users/' . $author_name . '/' );
			} else {
				$permalink = get_permalink( $post_id );

				if ( $_page ) {
					$page = (int) $_page / 30;
					$permalink .= 'page/' . $page . '/';
				}
			}

			//echo '<pre>';
			//var_dump( $post_id );
			//var_dump( $matches );
			//var_dump( $permalink );
			//var_dump( $author );
			//echo '</pre>';

			if ( $permalink ) {
				$wp_query->is_404 = FALSE;
				wp_redirect( $permalink, 301 );
				exit;
			}
		}
	}
}

