<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
add_action( 'init', 'user_profile_rewrite' );
function user_profile_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%user%', '([^&]+)' );
	add_rewrite_tag( '%user_nicename%', '([^&]+)' );
	//add_rewrite_rule( '^user/?', 'index.php?pagetype=user', 'top' );
	add_rewrite_rule( '^user/([^/]*)/?', 'index.php?pagetype=user&user_nicename=$matches[1]', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	$wp_rewrite->flush_rules();
}

add_action( 'template_redirect', 'user_profile_redirect' );
function user_profile_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'user' ) {

		include( $acas4u_stylesheet_directory . '/templates/user-profile-page.php' );
		exit;
	}
}

add_filter( 'wp_title', 'user_profile_title' );
function user_profile_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'user' ) {
		$title = 'User Profile';
	}

	return $title;
}

/*
add_action( 'init', 'user_register_page_rewrite' );
function user_register_page_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%register%', '([^&]+)' );
	add_rewrite_rule( '^register/?', 'index.php?pagetype=register', 'top' );
	add_rewrite_endpoint( 'pagetype', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	$wp_rewrite->flush_rules();
}

add_action( 'template_redirect', 'user_register_page_redirect' );
function user_register_page_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'register' ) {

		include( $acas4u_stylesheet_directory . '/templates/user-register-page.php' );
		exit;
	}
}

add_filter( 'wp_title', 'user_register_page_title' );
function user_register_page_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'register' ) {
		$title = 'Register a New User';
	}

	return $title;
}
*/

function acas4u_get_user_credits( $user_id ) {
	global $wpdb;

	/*
	$user_credits = $wpdb->get_row( 'SELECT * FROM acas4u_phpbb.phpbb_download_credits WHERE user_id="' . $user_id . '"' );
	$max_downloads = $user_credits->files;
	$credit_period = $user_credits->days;
	$bonus_credits = $user_credits->credits;


	if ( ! $max_downloads ) {
		$max_downloads = us_get_option( 'default_max_credits' );
	}
	if ( ! $credit_period ) {
		$credit_period = us_get_option( 'default_credit_period' );
	}
	if ( ! $bonus_credits ) {
		$bonus_credits = 0;
	}

	*/

	$max_downloads = us_get_option( 'default_max_credits' );
	$credit_period = us_get_option( 'default_credit_period' );
	$bonus_credits = acas4u_get_user_bonus_downloads( $user_id );

	// credits used
	$startdate = strftime( "%Y-%m-%d, %H:%M:%S", time() - ( 86400 * $credit_period ) );
	$credit_used = $wpdb->get_row( 'SELECT count(*) AS count FROM acas4u_wpnew.phpbb_download_logs WHERE user_id="' . $user_id . '" AND bonus="0" AND completed!="failed" AND timestamp>="' . $startdate . '"' );
	$credits_used = $credit_used->count;

	$user_credits = array(
		'max_downloads' => $max_downloads,
		'credit_period' => $credit_period,
		'credits_used' => $credits_used,
		'credit_remaining' => $max_downloads - $credits_used,
		'bonus_credits' => $bonus_credits,
	);

	return $user_credits;
}

function acas4u_is_user_donator( $user_id ) {
	$donator = FALSE;

	$user_donator = get_user_meta( $user_id, '_bbp_phpbb_user_donator', TRUE );
	if ( $user_donator == 'TRUE' ) {
		$donator = TRUE;
	}

	$user_roles = get_user_meta( $user_id, 'wp_capabilities', TRUE );
	$roles_result = array_key_exists( 'donator', $user_roles );
	if ( $roles_result ) {
		$donator = TRUE;
	}

	return $donator;
}

function acas4u_get_user_max_downloads( $user_id ) {
	global $wpdb;

	$max_downloads = get_user_meta( $user_id, '_max_credits', TRUE );

	if ( ! $max_downloads ) {
		$user_credits = $wpdb->get_row( 'SELECT * FROM acas4u_wpnew.phpbb_download_credits WHERE user_id="' . $user_id . '"' );
		$max_downloads = $user_credits->files;
	}

	if ( ! $max_downloads ) {
		$max_downloads = us_get_option( 'default_max_credits' );
	}

	return $max_downloads;
}

function acas4u_get_user_bonus_downloads( $user_id ) {
	global $wpdb;

	$bonus_downloads = get_user_meta( $user_id, '_bonus_credits', TRUE );

	if ( ! $bonus_downloads ) {
		$user_credits = $wpdb->get_row( 'SELECT * FROM phpbb_download_credits WHERE user_id="' . $user_id . '"' );
		$bonus_downloads = $user_credits->credits;
	}

	if ( ! $bonus_downloads ) {
		$bonus_downloads = us_get_option( 'default_bonus_credits' );
	}

	return $bonus_downloads;
}

function acas4u_get_user_upload_log( $user_id ) {
	global $wpdb;

	$limit = 50;
	$output = '';

	$uploads = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) AS uploads FROM phpbb_download_uploads WHERE user_id = '" . $user_id . "';" ) );

	$uploaded_files = $wpdb->get_results( 'SELECT filehash, timestamp FROM phpbb_download_uploads WHERE user_id = "' . $user_id . '" ORDER BY timestamp DESC LIMIT 0,' . $limit );

	if ( $uploaded_files ) {
		if ( $uploads > $limit ) {
			$output .= '<p>Found ' . number_format( $uploads, 0, ',', ',' ) . ' results total. Showed only first ' . $limit . ' results.</p>';
		}
		$output .= '<table class="acas4u-userlogs-upload acas4u-log-table">';
		$output .= '<thead>';
		$output .= '<tr>';
		$output .= '<th>Date</th>';
		$output .= '<th>Title</th>';
		$output .= '<th data-sorter="false">Bitrate</th>';
		$output .= '<th data-sorter="false">Size</th>';
		$output .= '<th data-sorter="false">Duration</th>';
		$output .= '<th data-sorter="false">Added</th>';
		$output .= '<th data-sorter="false">Rating</th>';
		$output .= '</tr>';
		$output .= '</thead>';
		$output .= '<tbody>';
		foreach ( $uploaded_files as $u_file ) {

			$upload_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_download_filehash' AND meta_value='" . $u_file->filehash . "';" ) );
			if ( $upload_id ) {
				$artist1 = get_post_meta( $upload_id, '_download_artist1', TRUE );
				$artist2 = get_post_meta( $upload_id, '_download_artist2', TRUE );
				$trackname = get_post_meta( $upload_id, '_download_trackname', TRUE );
				$post_permalink = get_the_permalink( $upload_id );

				$title = acas4u_create_download_title( $upload_id, $artist1, $artist2, $trackname );

				$download_duration = get_post_meta( $upload_id, '_download_duration', TRUE );
				$download_bitrate = get_post_meta( $upload_id, '_download_bitrate', TRUE );
				$download_size = get_post_meta( $upload_id, '_download_size', TRUE );
				$download_size = formatBytes( $download_size );
				$date_uploaded = get_the_date( 'd/m/Y', $upload_id );

				$download_rating = get_post_meta( $upload_id, '_download_rating', TRUE );
				if ( $download_rating == '' ) {
					$download_rating = 0;
				}
				$rating_stars = acas4u_display_rating_stars( $download_rating );
				$download_votes = get_post_meta( $upload_id, '_download_votes', TRUE );

				$output .= '<tr>';
				$output .= '<td class="td-log-date">' . date( "d/m/Y", $u_file->timestamp ) . '</td>';
				$output .= '<td class="td-log-title"><a href="' . $post_permalink . '">' . $title . '</a></td>';
				$output .= '<td class="td-log-bitrate">' . $download_bitrate . '</td>';
				$output .= '<td class="td-log-size">' . $download_size . '</td>';
				$output .= '<td class="td-log-duration">' . acas4u_format_duration( $download_duration ) . '</td>';
				$output .= '<td class="td-log-added">' . $date_uploaded . '</td>';
				$output .= '<td class="td-log-rating" title="Total votes: ' . $download_votes . '">' . $rating_stars . '</td>';
				$output .= '</tr>';
			}
		}
		$output .= '</tbody>';
		$output .= '</table>';
	}

	return $output;
}

function acas4u_get_user_download_log( $user_id ) {
	global $wpdb;

	$limit = 50;
	$output = '';

	$downloads = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) AS downloads FROM phpbb_download_logs WHERE user_id = '" . $user_id . "' AND completed='yes';" ) );

	$downloaded_files = $wpdb->get_results( 'SELECT filehash, timestamp FROM phpbb_download_logs WHERE user_id = "' . $user_id . '" AND completed="yes" ORDER BY timestamp DESC LIMIT 0,' . $limit );

	if ( $downloaded_files ) {
		if ( $downloads > $limit ) {
			$output .= '<p>Found ' . number_format( $downloads, 0, ',', ',' ) . ' results total. Showed only first ' . $limit . ' results.</p>';
		}
		$output .= '<table class="acas4u-userlogs-download acas4u-log-table tablesorter" data-userid="' . $user_id . '">';
		$output .= '<thead>';
		$output .= '<tr>';
		$output .= '<th>Date</th>';
		$output .= '<th>Title</th>';
		$output .= '<th data-sorter="false">Bitrate</th>';
		$output .= '<th data-sorter="false">Size</th>';
		$output .= '<th data-sorter="false">Duration</th>';
		$output .= '<th data-sorter="false">Added</th>';
		$output .= '<th data-sorter="false">Rating</th>';
		$output .= '</tr>';
		$output .= '</thead>';
		$output .= '<tbody>';
		foreach ( $downloaded_files as $d_file ) {

			$download_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_download_filehash' AND meta_value='" . $d_file->filehash . "';" ) );
			if ( $download_id ) {
				$artist1 = get_post_meta( $download_id, '_download_artist1', TRUE );
				$artist2 = get_post_meta( $download_id, '_download_artist2', TRUE );
				$trackname = get_post_meta( $download_id, '_download_trackname', TRUE );
				$post_permalink = get_the_permalink( $download_id );

				$title = acas4u_create_download_title( $download_id, $artist1, $artist2, $trackname );

				$download_duration = get_post_meta( $download_id, '_download_duration', TRUE );

				$download_bitrate = get_post_meta( $download_id, '_download_bitrate', TRUE );
				$download_size = get_post_meta( $download_id, '_download_size', TRUE );
				$download_size = formatBytes( $download_size );

				$date_uploaded = get_the_date( 'd/m/Y', $download_id );

				$download_rating = get_post_meta( $download_id, '_download_rating', TRUE );
				if ( $download_rating == '' ) {
					$download_rating = 0;
				}
				$rating_stars = acas4u_display_rating_stars( $download_rating );
				$download_votes = get_post_meta( $download_id, '_download_votes', TRUE );

				$output .= '<tr data-downloadid="' . $download_id . '">';
				$output .= '<td class="td-log-date">' . date( "d/m/Y", strtotime( $d_file->timestamp ) ) . '</td>';
				$output .= '<td class="td-log-title"><a href="' . $post_permalink . '">' . $title . '</a></td>';
				$output .= '<td class="td-log-bitrate">' . $download_bitrate . '</td>';
				$output .= '<td class="td-log-size">' . $download_size . '</td>';
				$output .= '<td class="td-log-duration">' . acas4u_format_duration( $download_duration ) . '</td>';
				$output .= '<td class="td-log-added">' . $date_uploaded . '</td>';
				$output .= '<td class="td-log-rating" title="Total votes: ' . $download_votes . '">' . $rating_stars . '</td>';
				$output .= '</tr>';
			}
		}
		$output .= '</tbody>';
		$output .= '</table>';
	}

	return $output;
}

// Apply filter
add_filter( 'get_avatar', 'acas4u_custom_avatar', 1, 5 );
function acas4u_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
	$user = FALSE;
	$avatar_prefix = '4755f1c8cf5acbbb352bce9759405761';
	$upload_info = wp_upload_dir();
	$upload_dir = $upload_info['basedir'];
	$upload_url = $upload_info['baseurl'];

	if ( is_numeric( $id_or_email ) ) {

		$id = (int) $id_or_email;
		$user = get_user_by( 'id', $id );
	} elseif ( is_object( $id_or_email ) ) {

		if ( ! empty( $id_or_email->user_id ) ) {
			$id = (int) $id_or_email->user_id;
			$user = get_user_by( 'id', $id );
		}
	} else {
		$user = get_user_by( 'email', $id_or_email );
	}

	if ( $user && is_object( $user ) ) {

		$old_avatar = get_user_meta( $user->data->ID, '_bbp_phpbb_user_avatar', TRUE );
		if ( $old_avatar ) {
			if ( stripos( $old_avatar, 'http' ) !== FALSE ) {
				$avatar = $old_avatar;
				$avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
			} else {
				$avatar_dir = $upload_dir . '/avatars/upload/' . $avatar_prefix . '_' . esc_attr( $old_avatar );
				$avatar_path = $upload_url . '/avatars/upload/' . $avatar_prefix . '_' . esc_attr( $old_avatar );
				if ( file_exists( $avatar_dir ) ) {
					$avatar = $avatar_path;
					$avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
				}
			}
		}
	}

	return $avatar;
}

// logs a member in after submitting a form from a widget
add_action( 'init', 'acas4u_login_member_from_widget' );
function acas4u_login_member_from_widget() {
	global $widget_errors;
	$widget_errors = [ ];

	//var_dump( $_POST );

	if ( isset( $_POST['action'] ) AND $_POST['action'] == 'login_user' ) {

		if ( ! isset( $_POST['_acas4u_widget_login_nonce'] ) AND wp_verify_nonce( $_POST['_acas4u_widget_login_nonce'], 'acas4u-do-login-from-widget' ) === FALSE ) {
			return;
		}

		// this returns the user ID and other info from the user name
		$user = get_user_by( 'login', $_POST['login_username'] );
		//var_dump( $user );

		if ( ! $user ) {
			// if the user name doesn't exist
			$widget_errors['login_username'] = 'Invalid Username';
		}

		if ( $user ) {
			$user_activated_meta = get_user_meta( $user->ID, 'acas4u_has_to_be_activated', TRUE );

			// for old accounts without activation meta field
			if ( $user_activated_meta == '' ) {
				$user_activated_meta = 'active';
			}

			if ( $user_activated_meta != 'active' ) {
				$activation_page_id = 497973;
				$activation_link = get_permalink( $activation_page_id );
				$widget_errors['login_username'] = 'This user is not active. Please <a href="' . $activation_link . '">activate</a> your account.';
			}
		}

		if ( ! isset( $_POST['login_password'] ) || $_POST['login_password'] == '' ) {
			// if no password was entered
			$widget_errors['login_password'] = 'Please enter a password';
		}

		// check the user's login with their password
		if ( ! wp_check_password( $_POST['login_password'], $user->user_pass, $user->ID ) ) {
			// if the password is incorrect for the specified user
			$widget_errors['login_password'] = 'Incorrect password';
		}

		//var_dump( $widget_errors );

		// only log the user in if there are no errors
		if ( empty( $widget_errors ) ) {

			wp_setcookie( $_POST['login_username'], $_POST['login_password'], TRUE );
			wp_set_current_user( $user->ID, $_POST['login_username'] );
			do_action( 'wp_login', $_POST['login_username'] );

			wp_redirect( home_url( $_POST['_wp_http_referer'] ) );
			exit;
		}
	}
}
