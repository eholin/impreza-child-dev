<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/* Register and load the widget */
add_action( 'widgets_init', 'acas4u_load_widget' );
function acas4u_load_widget() {
	register_widget( 'acas4u_user_login_widget' );
	register_widget( 'acas4u_genres_taxonomy_widget' );
	register_widget( 'acas4u_downloads_index_widget' );
}

add_action( 'widgets_init', 'acas4u_register_genre_sidebars' );
function acas4u_register_genre_sidebars() {
	register_sidebar( array(
		'name' => __( 'Genre Sidebar', 'acapellas4u' ),
		'id' => 'genre_sidebar',
		'description' => __( 'Widgets in this area will be shown at the right or left of a genres taxonomy page.', 'acapellas4u' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	) );
}

add_action( 'widgets_init', 'acas4u_register_explore_artists_sidebars' );
function acas4u_register_explore_artists_sidebars() {
	register_sidebar( array(
		'name' => __( 'Explore Artists Sidebar', 'acapellas4u' ),
		'id' => 'explore_artists_sidebar',
		'description' => __( 'Widgets in this area will be shown at the right or left of a Explore Artists page.', 'acapellas4u' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	) );
}

/**
 * Widget for user login or info
 *
 * Outputs the widget with login form or current user information if the user is already logged
 *
 */
class acas4u_user_login_widget extends WP_Widget {

	function __construct() {

		$widget_ops = array(
			'classname' => 'acas4u_user_login_widget',
			'description' => __( 'Outputs the widget with tabs on the front page.', 'ratinglite' ),
		);
		parent::__construct( 'acas4u-user-login-widget', __( '(Acas4u) User Login Widget', 'acapellas4u' ), $widget_ops );
		$this->alt_option_name = 'acas4u_user_login_widget';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		global $widget_errors;
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'acas4u_user_login_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];

			return;
		}

		ob_start();
		echo $args['before_widget'];

		/* get widget data */

		$show_one_click_buttons = isset( $instance['show_one_click_buttons'] ) ? (bool) $instance['show_one_click_buttons'] : TRUE;

		$show_forgot_link = isset( $instance['show_forgot_link'] ) ? (bool) $instance['show_forgot_link'] : TRUE;
		$forgot_link_text = isset( $instance['forgot_link_text'] ) ? esc_attr( $instance['forgot_link_text'] ) : __( 'I forgot my password', 'acapellas4u' );
		$forgot_link_url = isset( $instance['forgot_link_url'] ) ? esc_url( $instance['forgot_link_url'] ) : '';

		$show_resend_link = isset( $instance['show_resend_link'] ) ? (bool) $instance['show_resend_link'] : TRUE;
		$resend_link_text = isset( $instance['resend_link_text'] ) ? esc_attr( $instance['resend_link_text'] ) : __( 'Resend activation e-mail', 'acapellas4u' );
		$resend_link_url = isset( $instance['resend_link_url'] ) ? esc_url( $instance['resend_link_url'] ) : '';

		/* start frontend output */

		$user_logged = is_user_logged_in();

		if ( $show_one_click_buttons AND ! $user_logged ) {
			echo do_action( 'wordpress_social_login' );
		}

		if ( $user_logged ) {
			$user_id = get_current_user_id();
			$userdata = get_userdata( $user_id );

			$username = $userdata->user_login;
			$first_name = $userdata->first_name;
			$last_name = $userdata->last_name;
			$nicename = $userdata->user_nicename;
			if ( $first_name AND $last_name ) {
				$username = $first_name . ' ' . $last_name;
			} else if ( $nicename ) {
				$username = $nicename;
			}

			$user_credits = acas4u_get_user_credits( $user_id );

			$output = '<div class="acas4u-wl-user-logged">';
			$output .= __( 'Welcome', 'acapellas4u' ) . ', ' . $username . '!';

			$output .= '<ul>';
			$output .= '<li>You\'re allowed <strong>' . $user_credits['max_downloads'] . '</strong> downloads in a <strong>' . $user_credits['credit_period'] . '</strong> days.</li>';
			$output .= '<li>You\'ve downloaded <strong>' . $user_credits['credits_used'] . '</strong> file(s) in the last <strong>' . $user_credits['credit_period'] . '</strong> days.</li>';
			$output .= '<li>You have <strong>' . $user_credits['credit_remaining'] . '</strong> download(s)	remaining.</li>';
			$output .= '<li>You have <strong>' . $user_credits['bonus_credits'] . '</strong> bonus download(s) remaining.</li>';
			$output .= '<li><a href="' . wp_logout_url( '/' ) . '">Logout</a></li>';
			$output .= '</ul>';

			$output .= '</div>';
		} else {
			$login_username_class = '';
			if ( $widget_errors['login_username'] != '' ) {
				$login_username_class = 'class="acas4u-input-error"';
			}

			$login_password_class = '';
			if ( $widget_errors['login_password'] != '' ) {
				$login_password_class = 'class="acas4u-input-error"';
			}
			$output = '<div class="acas4u-wl-login-form">';
			$output .= '<form method="post" action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '">';
			$output .= '<input type="hidden" name="action" value="login_user">';
			$output .= wp_nonce_field( 'acas4u-do-login-from-widget', '_acas4u_widget_login_nonce', TRUE, FALSE );
			$output .= '<p><input type="text" name="login_username" placeholder="Username" ' . $login_username_class . ' value="' . $_POST['login_username'] . '" /></p>';
			if ( $widget_errors['login_username'] != '' ) {
				$output .= '<p class="acas4u-error">' . $widget_errors['login_username'] . '</p>';
			}
			$output .= '<p><input type="password" name="login_password" placeholder="Password" ' . $login_password_class . ' /></p>';
			if ( $widget_errors['login_password'] != '' ) {
				$output .= '<p class="acas4u-error">' . $widget_errors['login_password'] . '</p>';
			}
			$output .= '<p><input type="checkbox" name="remember_login" value="true" checked="checked"/> Remember Me</p>';
			$output .= '<p><input type="submit" name="login_submit" value="Login" />&nbsp;<a class="w-btn color_primary" href="' . home_url( '/register/' ) . '">Register</a></p>';
			$output .= '</form>';
			$output .= '</div>';

			if ( $show_forgot_link ) {
				if ( $forgot_link_url == '' ) {
					$forgot_link_url = home_url( '/wp-login.php?action=lostpassword' );
				}
				$output .= '<div class="acas4u-wl-forgot-link"><a href="' . $forgot_link_url . '">' . $forgot_link_text . '</a></div>';
			}

			if ( $show_resend_link ) {
				if ( $resend_link_url == '' ) {
					$resend_link_url = home_url( '/activate/' );
				}
				$output .= '<div class="acas4u-wl-resend-link"><a href="' . $resend_link_url . '">' . $resend_link_text . '</a></div>';
			}
		}

		echo $output;
		echo $args['after_widget'];

		/* end frontend output */

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'rl_frontpage_tabs_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	/* --------------------------------- Widget Backend --------------------------------- */
	public function form( $instance ) {

		$show_one_click_buttons = $instance['show_one_click_buttons'];

		$show_forgot_link = (bool) $instance['show_forgot_link'];
		$forgot_link_text = esc_attr( $instance['forgot_link_text'] );
		$forgot_link_url = esc_url( $instance['forgot_link_url'] );

		$show_resend_link = (bool) $instance['show_resend_link'];
		$resend_link_text = esc_attr( $instance['resend_link_text'] );
		$resend_link_url = esc_attr( $instance['resend_link_url'] );

		// Widget admin form
		?>
		<p class="acas4u-widget-row-delimeter">
			<input class="checkbox" type="checkbox" <?php checked( $show_one_click_buttons ); ?> id="<?php echo $this->get_field_id( 'show_one_click_buttons' ); ?>" name="<?php echo $this->get_field_name( 'show_one_click_buttons' ); ?>"/>
			<label for="<?php echo $this->get_field_id( 'show_one_click_buttons' ); ?>"><?php _e( 'Show one click login buttons', 'acapellas4u' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_forgot_link ); ?> id="<?php echo $this->get_field_id( 'show_forgot_link' ); ?>" name="<?php echo $this->get_field_name( 'show_forgot_link' ); ?>"/>
			<label for="<?php echo $this->get_field_id( 'show_forgot_link' ); ?>"><?php _e( 'Show &quot;Forgot password...&quot; link', 'acapellas4u' ); ?></label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'forgot_link_text' ); ?>"><?php _e( 'Link text:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'forgot_link_text' ); ?>" name="<?php echo $this->get_field_name( 'forgot_link_text' ); ?>" type="text" value="<?php echo $forgot_link_text; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'forgot_link_url' ); ?>"><?php _e( 'Link URL:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'forgot_link_url' ); ?>" name="<?php echo $this->get_field_name( 'forgot_link_url' ); ?>" type="text" value="<?php echo $forgot_link_url; ?>"/>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_resend_link ); ?> id="<?php echo $this->get_field_id( 'show_resend_link' ); ?>" name="<?php echo $this->get_field_name( 'show_resend_link' ); ?>"/>
			<label for="<?php echo $this->get_field_id( 'show_resend_link' ); ?>"><?php _e( 'Show &quot;Resend activation email...&quot; link', 'acapellas4u' ); ?></label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'resend_link_text' ); ?>"><?php _e( 'Link text:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'resend_link_text' ); ?>" name="<?php echo $this->get_field_name( 'resend_link_text' ); ?>" type="text" value="<?php echo $resend_link_text; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'resend_link_url' ); ?>"><?php _e( 'Link URL:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'resend_link_url' ); ?>" name="<?php echo $this->get_field_name( 'resend_link_url' ); ?>" type="text" value="<?php echo $resend_link_url; ?>"/>
		</p>

		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['show_one_click_buttons'] = (bool) $new_instance['show_one_click_buttons'];

		$instance['show_forgot_link'] = (bool) $new_instance['show_forgot_link'];
		$instance['forgot_link_text'] = strip_tags( $new_instance['forgot_link_text'] );
		$instance['forgot_link_url'] = strip_tags( $new_instance['forgot_link_url'] );

		$instance['show_resend_link'] = (bool) $new_instance['show_resend_link'];
		$instance['resend_link_text'] = strip_tags( $new_instance['resend_link_text'] );
		$instance['resend_link_url'] = strip_tags( $new_instance['resend_link_url'] );

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['acas4u_user_login_widget'] ) ) {
			delete_option( 'acas4u_user_login_widget' );
		}

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete( 'acas4u_user_login_widget', 'widget' );
	}
} // Class wpb_widget ends here

// #----------------------------------# Widget for list of genres (taxonomy genre) #----------------------------------#

class acas4u_genres_taxonomy_widget extends WP_Widget {

	function __construct() {

		$widget_ops = array(
			'classname' => 'acas4u_genres_taxonomy_widget',
			'description' => __( 'Outputs the widget with list of genres.', 'acapellas4u' ),
		);
		parent::__construct( 'acas4u-genres-taxonomy_widget', __( '(Acas4u) Genres Widget', 'acapellas4u' ), $widget_ops );
		$this->alt_option_name = 'acas4u_genres_taxonomy_widget';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		global $widget_errors;
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'acas4u_genres_taxonomy_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];

			return;
		}

		ob_start();
		echo $args['before_widget'];

		/* get widget data */

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Popular Genres', 'acapellas4u' );

		$all_genres_title = isset( $instance['all_genres_title'] ) ? esc_attr( $instance['all_genres_title'] ) : '';
		$title_url = isset( $instance['title_url'] ) ? esc_url( $instance['title_url'] ) : '';

		$number_of_items = isset( $instance['number_of_items'] ) ? esc_attr( $instance['number_of_items'] ) : '30';
		$smallest_font_size = isset( $instance['smallest_font_size'] ) ? esc_attr( $instance['smallest_font_size'] ) : '12';
		$largest_font_size = isset( $instance['largest_font_size'] ) ? esc_attr( $instance['largest_font_size'] ) : '22';

		$order_by = isset( $instance['order_by'] ) ? esc_attr( $instance['order_by'] ) : 'count';
		$order = isset( $instance['order'] ) ? esc_attr( $instance['order'] ) : 'ASC';

		/* start frontend output */

		$output = '<div class="acas4u-wl-popular-genres">';

		$output .= $args['before_title'] . $title . $args['after_title'];

		$tag_cloud_args = array(
			'smallest' => $smallest_font_size,
			'largest' => $largest_font_size,
			'unit' => 'px',
			'format' => 'flat',
			'separator' => "\n",
			'number' => $number_of_items,
			'orderby' => $order_by,
			'order' => $order,
			'taxonomy' => 'genre',
			'echo' => FALSE,
		);

		$output .= wp_tag_cloud( $tag_cloud_args );

		$output .= '</div>';

		if ( $title_url != '' ) {
			if ( $all_genres_title != '' ) {
				$output .= '<a class="acas4u-all-genres-link" href="' . $title_url . '">' . $all_genres_title . '</a>';
			} else {
				$output .= '<a class="acas4u-all-genres-link" href="' . $title_url . '">' . __( 'View all genres', 'acapellas4u' ) . '</a>';
			}
		}

		echo $output;

		echo $args['after_widget'];

		/* end frontend output */

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'acas4u_genres_taxonomy_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	/* --------------------------------- Widget Backend --------------------------------- */
	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Popular Genres', 'acapellas4u' );
		$all_genres_title = isset( $instance['all_genres_title'] ) ? esc_attr( $instance['all_genres_title'] ) : '';
		$title_url = isset( $instance['title_url'] ) ? esc_url( $instance['title_url'] ) : '';

		$number_of_items = isset( $instance['number_of_items'] ) ? esc_attr( $instance['number_of_items'] ) : '30';
		$smallest_font_size = isset( $instance['smallest_font_size'] ) ? esc_attr( $instance['smallest_font_size'] ) : '12';
		$largest_font_size = isset( $instance['largest_font_size'] ) ? esc_attr( $instance['largest_font_size'] ) : '22';

		$order_by = isset( $instance['order_by'] ) ? esc_attr( $instance['order_by'] ) : 'count';
		$order = isset( $instance['order'] ) ? esc_attr( $instance['order'] ) : 'ASC';

		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'all_genres_title' ); ?>"><?php _e( '&quot;View all genres&quot; link title:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'all_genres_title' ); ?>" name="<?php echo $this->get_field_name( 'all_genres_title' ); ?>" type="text" value="<?php echo $all_genres_title; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'title_url' ); ?>"><?php _e( 'Title URL:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title_url' ); ?>" name="<?php echo $this->get_field_name( 'title_url' ); ?>" type="text" value="<?php echo $title_url; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'number_of_items' ); ?>"><?php _e( 'Number of items to show:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_items' ); ?>" name="<?php echo $this->get_field_name( 'number_of_items' ); ?>" type="text" value="<?php echo $number_of_items; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'smallest_font_size' ); ?>"><?php _e( 'Smallest font size:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'smallest_font_size' ); ?>" name="<?php echo $this->get_field_name( 'smallest_font_size' ); ?>" type="text" value="<?php echo $smallest_font_size; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'largest_font_size' ); ?>"><?php _e( 'Largest font size:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'largest_font_size' ); ?>" name="<?php echo $this->get_field_name( 'largest_font_size' ); ?>" type="text" value="<?php echo $largest_font_size; ?>"/>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'order_by' ); ?>"><?php _e( 'Order by:', 'acapellas4u' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'order_by' ); ?>" name="<?php echo $this->get_field_name( 'order_by' ); ?>">
				<option <?php selected( 'count', $order_by, TRUE ); ?> value="count"><?php _e( 'Number of posts', 'acapellas4u' ); ?></option>
				<option <?php selected( 'name', $order_by, TRUE ); ?> value="name"><?php _e( 'Tag name', 'acapellas4u' ); ?></option>
			</select>
		</p>

		<p class="acas4u-widget-row-delimeter">
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order:', 'acapellas4u' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<option <?php selected( 'ASC', $order, TRUE ); ?> value="ASC"><?php _e( 'Ascending', 'acapellas4u' ); ?></option>
				<option <?php selected( 'DESC', $order, TRUE ); ?> value="DESC"><?php _e( 'Descending', 'acapellas4u' ); ?></option>
				<option <?php selected( 'RAND', $order, TRUE ); ?> value="RAND"><?php _e( 'Random', 'acapellas4u' ); ?></option>
			</select>
		</p>


		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['all_genres_title'] = strip_tags( $new_instance['all_genres_title'] );
		$instance['title_url'] = strip_tags( $new_instance['title_url'] );

		$instance['number_of_items'] = strip_tags( $new_instance['number_of_items'] );
		$instance['smallest_font_size'] = strip_tags( $new_instance['smallest_font_size'] );
		$instance['largest_font_size'] = strip_tags( $new_instance['largest_font_size'] );

		$instance['order_by'] = strip_tags( $new_instance['order_by'] );
		$instance['order'] = strip_tags( $new_instance['order'] );

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['acas4u_genres_taxonomy_widget'] ) ) {
			delete_option( 'acas4u_genres_taxonomy_widget' );
		}

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete( 'acas4u_genres_taxonomy_widget', 'widget' );
	}
} // Class wpb_widget ends here

// #----------------------------------# Widget for list of downloads indexes (Explore Artists) #----------------------------------#

class acas4u_downloads_index_widget extends WP_Widget {

	function __construct() {

		$widget_ops = array(
			'classname' => 'acas4u_downloads_index_widget',
			'description' => __( 'Outputs the widget with list of downloads indexes.', 'acapellas4u' ),
		);
		parent::__construct( 'acas4u-downloads-index_widget', __( '(Acas4u) Downloads Index Widget', 'acapellas4u' ), $widget_ops );
		$this->alt_option_name = 'acas4u_downloads_index_widget';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		global $widget_errors;
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'acas4u_downloads_index_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];

			return;
		}

		/* start frontend output */
		ob_start();
		echo $args['before_widget'];

		/* get widget data */

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Downloads Index', 'acapellas4u' );

		$default_taxonomy_slug = 'a';

		global $wp;
		$query_taxonomy_slug = urldecode( $wp->query_vars['artist_index'] );

		$index = ( $query_taxonomy_slug != '' ) ? $query_taxonomy_slug : $default_taxonomy_slug;

		$output = '<div class="acas4u-wl-downloads-index">';

		$output .= $args['before_title'] . $title . $args['after_title'];

		$excluded_taxonomies_ids = array();

		$unknown_term = get_term_by( 'slug', $index, 'artist-index' );
		$unknown_term_id = $unknown_term->term_taxonomy_id;
		$unknown_term_parent_id = $unknown_term->parent;

		if ( $unknown_term->parent > 0 ) {
			$parent_term = get_term_by( 'id', $unknown_term_parent_id, 'download-index' );
			$index = $parent_term->slug;
		}

		$primary_terms = get_terms( array(
			'taxonomy' => 'download-index',
			'hide_empty' => FALSE,
			'parent' => 0,
			'exclude' => implode( ',', $excluded_taxonomies_ids ),
		) );
		if ( ! empty( $primary_terms ) AND ! is_wp_error( $primary_terms ) ) {
			$output .= '<ul class="acas4u-widget-artists-index-list">';
			foreach ( $primary_terms as $primary_term ) {
				$primary_term_count = '';
				$children_terms_count = acas4u_count_term_children_posts( $primary_term->term_id, 'download-index' );
				$all_posts_count = $primary_term->count + $children_terms_count;
				if ( $all_posts_count > 0 ) {
					$all_posts_count_str = '(' . $all_posts_count . ')';
				}
				if ( $index == $primary_term->slug ) {
					$output .= '<li class="active" id="term-' . $primary_term->term_id . '"><span>' . $primary_term->name . ' ' . $all_posts_count_str . '</span>';

					$parent = get_term_by( 'slug', $index, 'download-index' );
					$parent_id = $parent->term_taxonomy_id;

					$index_secondary = ( strlen( $query_taxonomy_slug ) > 1 ) ? $query_taxonomy_slug : $index;

					$secondary_terms = get_terms( array(
						'taxonomy' => 'download-index',
						'hide_empty' => FALSE,
						'parent' => $parent_id,
						'exclude' => implode( ',', $excluded_taxonomies_ids ),
					) );
					if ( ! empty( $secondary_terms ) AND ! is_wp_error( $secondary_terms ) ) {
						$output .= '<ul class="acas4u-widget-artists-index-list-child">';
						foreach ( $secondary_terms as $secondary_term ) {
							if ( $index_secondary == $secondary_term->slug ) {
								$output .= '<li class="active"><span>' . $secondary_term->name . '</span></li>';
							} else {
								$output .= '<li><a href="' . home_url( '/explore-artists/' ) . $secondary_term->slug . '/"><span>' . $secondary_term->name . ' (' . $secondary_term->count . ')</span></a></li>';
							}
						}
						$output .= '</ul>';
					}

					$output .= '</li>';
				} else {
					$output .= '<li id="term-' . $primary_term->term_id . '"><a href="' . home_url( '/explore-artists/' ) . $primary_term->slug . '/"><span>' . $primary_term->name . ' ' . $all_posts_count_str . '</span></a></li>';
				}
			}
			$output .= '</ul>';
		}

		$output .= '</div>';

		echo $output;

		echo $args['after_widget'];

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'acas4u_genres_taxonomy_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
		/* end frontend output */
	}

	/* --------------------------------- Widget Backend --------------------------------- */
	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Downloads Index', 'acapellas4u' );

		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'acapellas4u' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
		</p>

		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['acas4u_downloads_index_widget'] ) ) {
			delete_option( 'acas4u_downloads_index_widget' );
		}

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete( 'acas4u_downloads_index_widget', 'widget' );
	}
} // Class wpb_widget ends here
