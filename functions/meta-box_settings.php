<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
// External Link Metabox for pages
add_action( 'admin_init', 'acas4u_register_meta_boxes', 9 );
function acas4u_register_meta_boxes() {
	global $meta_boxes;
	$meta_boxes[] = array(
		'id' => 'us_acapellas_packs_url',
		'title' => __( 'Acapellas pack link', 'us' ),
		'pages' => array( 'acapellas_packs' ),
		'context' => 'normal',
		'priority' => 'default',
		'fields' => array(
			array(
				'name' => 'External URL',
				'id' => 'ExternalURL',
				'type' => 'text',
			),
		),
	);

	return $meta_boxes;
}
