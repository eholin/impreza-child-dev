<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
add_action( 'init', 'popular_downloads_rewrite' );
function popular_downloads_rewrite() {
	global $wp_rewrite;

	add_rewrite_tag( '%popular-acapellas%', '([^&]+)' );
	add_rewrite_rule( '^popular-acapellas/?', 'index.php?popular=popular-acapellas', 'top' );
	add_rewrite_endpoint( 'popular', EP_PERMALINK | EP_PAGES );

	//flush rules to get this to work properly (do this once, then comment out)
	$wp_rewrite->flush_rules();
}

add_action( 'template_redirect', 'popular_downloads_redirect' );
function popular_downloads_redirect() {
	global $wp, $acas4u_stylesheet_directory;

	$template = $wp->query_vars;

	if ( array_key_exists( 'popular', $template ) AND $template['popular'] == 'popular-acapellas' ) {

		include( $acas4u_stylesheet_directory . '/templates/popular-downloads-page.php' );
		exit;
	}
}

add_filter( 'wp_title', 'popular_acapellas_title' );
function popular_acapellas_title( $title ) {
	global $wp;

	$template = $wp->query_vars;

	if ( array_key_exists( 'popular', $template ) AND $template['popular'] == 'popular-acapellas' ) {
		$title = 'Popular Acapellas';
	}

	return $title;
}

add_action( 'update_popular_acapellas', 'acas4u_cron_update_popular_acapellas_cache' );
function acas4u_cron_update_popular_acapellas_cache() {
	/*
	$html_meta = get_popular_downloads_from_meta( 10, 'month' );
	if ( $html_meta ) {
		$html_meta = addslashes( $html_meta );
		update_option( 'acas4u_top10_popular_acapellas', $html_meta, FALSE );
	}
	*/

	$html_logs = acas4u_get_popular_downloads_from_logs( 10, 'month' );
	if ( $html_logs ) {
		$html_logs = addslashes( $html_logs );
		update_option( 'acas4u_top10_popular_acapellas', $html_logs, FALSE );
	}

	$html_logs_images = acas4u_get_popular_downloads_with_images_from_logs( 10, 'month' );
	if ( $html_logs_images ) {
		$html_logs_images = addslashes( $html_logs_images );
		update_option( 'acas4u_top10_popular_acapellas_images', $html_logs_images, FALSE );
	}
}

function get_popular_downloads_from_meta( $count, $period ) {
	$output = '';
	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => 'DESC',
		'orderby' => 'meta_value',
		'meta_query' => array(
			array(
				'key' => '_download_count',
				'type' => 'numeric',
			),
		),
	);
	if ( $period == 'month' ) {
		$args['date_query'] = array(
			array(
				'after' => '1 month ago',
			),
		);
	} else if ( $period == 'week' ) {
		$args['date_query'] = array(
			array(
				'after' => '1 week ago',
			),
		);
	}
	$posts = new WP_Query( $args );
	if ( $posts->have_posts() ) {
		$output .= '<div class="acapella-popular-wrapper">';
		$output .= '<div class="acapella-popular-row">';
		$output .= '<div class="pp-cell">Title</div>';
		$output .= '<div class="pp-cell">Listeners</div>';
		$output .= '</div>';
		while ( $posts->have_posts() ){
			$posts->the_post();
			$post_id = $posts->post->ID;
			$permalink = get_the_permalink();
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
			$download_count = get_post_meta( $post_id, '_download_count', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$output .= '<div class="acapella-popular-row">';
			$output .= '<div class="popular-post-title pp-cell"><a href="' . $permalink . '">' . $title;
			if ( $download_duration ) {
				$output .= ' (' . acas4u_format_duration( $download_duration ) . ')';
			}
			$output .= '</a>';
			$output .= '</div>';
			$output .= '<div class="popular-post-downloads pp-cell" title="Downloads count">' . number_format( $download_count, 0, ',', ',' ) . '</div>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();
	return $output;
}

function acas4u_get_popular_downloads_from_logs( $count, $period ) {
	global $wpdb;

	$period_str = '30 days';

	if ( $period == 'month' ) {
		$period_str = '30 days';
	} else if ( $period == 'week' ) {
		$period_str = '7 days';
	}

	$last_date = $wpdb->get_var( 'SELECT timestamp FROM phpbb_download_logs ORDER BY timestamp DESC LIMIT 0,1' );

	$end_date = date( 'Y-m-d', strtotime( $last_date ) ) . ' 23:59:59';
	$start_date = date( 'Y-m-d', strtotime( $last_date . ' - ' . $period_str ) ) . ' 00:00:00';

	$d_query = 'SELECT count(log_id) AS downloads_count, filehash FROM phpbb_download_logs WHERE timestamp >= "' . $start_date . '" AND timestamp <= "' . $end_date . '" GROUP BY filehash ORDER BY downloads_count DESC LIMIT 0,' . ( $count + 2 );

	$downloads = $wpdb->get_results( $d_query );

	$output = '';
	if ( count( $downloads ) > 0 ) {
		$output .= '<div class="acapella-popular-wrapper">';
		$output .= '<div class="acapella-popular-row">';
		$output .= '<div class="pp-cell">Title</div>';
		$output .= '<div class="pp-cell">Listeners</div>';
		$output .= '</div>';
		$i = 0;
		foreach ( $downloads as $download ) {
			if ( $i >= $count ) {
				break;
			}
			$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_filehash" AND meta_value= "' . $download->filehash . '"';
			$post_from_meta = $wpdb->get_row( $sqlquery );
			$post_id = $post_from_meta->post_id;
			if ( $post_id ) {
				$permalink = get_the_permalink( $post_id );
				$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
				$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
				$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
				$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );

				$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

				$output .= '<div class="acapella-popular-row">';
				$output .= '<div class="popular-post-title pp-cell"><a href="' . $permalink . '">' . $title;
				if ( $download_duration ) {
					$output .= ' (' . acas4u_format_duration( $download_duration ) . ')';
				}
				$output .= '</a>';
				$output .= '</div>';
				$output .= '<div class="popular-post-downloads pp-cell" title="Downloads count">' . number_format( $download->downloads_count, 0, ',', ',' ) . '</div>';
				$output .= '</div>';
				$i ++;
			}
		}
		$output .= '</div>';
	}

	return $output;
}

function acas4u_get_popular_downloads_with_images_from_meta( $count, $period ) {
	global $acas4u_stylesheet_directory_uri;

	$output = '';
	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => $count,
		'order' => 'DESC',
		'orderby' => 'meta_value',
		'meta_query' => array(
			array(
				'key' => '_download_count',
				'type' => 'numeric',
			),
		),
	);
	if ( $period == 'month' ) {
		$args['date_query'] = array(
			array(
				'after' => '1 month ago',
			),
		);
	} else if ( $period == 'week' ) {
		$args['date_query'] = array(
			array(
				'after' => '1 week ago',
			),
		);
	}
	$posts = new WP_Query( $args );
	if ( $posts->have_posts() ) {
		$output .= '<div class="acas4u-fp-popular-wrapper">';
		while ( $posts->have_posts() ){
			$posts->the_post();
			$post_id = $posts->post->ID;
			$permalink = get_the_permalink();
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );

			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

			$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
			if ( $artist_thumbnail_id ) {
				$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
				$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
			} else {
				$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
			}
			$artist_thumbnail_img = '<img src="' . $artist_thumbnail_url . '" alt="' . $title . '>" class="img-responsive">';

			$title = acas4u_limit_chars( $title, 40 );

			$output .= '<div class="acas4u-fp-popular-single">';
			$output .= '<a class="acas4u-fp-popular-link" href="' . $permalink . '">';
			$output .= '<div class="acas4u-fp-popular-image">' . $artist_thumbnail_img . '</div>';
			$output .= '<div class="acas4u-fp-popular-title">' . $title . '</div>';
			$output .= '</a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	wp_reset_query();

	return $output;
}

function acas4u_get_popular_downloads_table_from_logs( $start_date, $end_date, $count ) {
	global $wpdb;
	$total_query = 'SELECT count(*) AS downloads_count FROM phpbb_download_logs WHERE timestamp >= "' . $start_date . '" AND timestamp <= "' . $end_date . '" GROUP BY filehash ORDER BY downloads_count DESC';
	$total_downloads = $wpdb->get_var( $total_query );

	$limit = ceil( $count * 1.5 );

	$query = 'SELECT count(log_id) AS downloads_count, filehash FROM phpbb_download_logs WHERE timestamp >= "' . $start_date . '" AND timestamp <= "' . $end_date . '" GROUP BY filehash ORDER BY downloads_count DESC LIMIT 0, ' . $limit;

	$downloads = $wpdb->get_results( $query );

	$user_id = get_current_user_id();
	$user_donator = acas4u_is_user_donator( $user_id );
	if ( $user_donator ) {
		$data_donator = '1';
		$tooltip_key2 = '';
	} else {
		$tooltip_key2 = 'data-tooltip="Sorting by this field available for donators" data-tooltip-position="top"';
		$data_donator = '0';
	}

	$output = '<div class="acas4u-popular-download-form">';

	$output .= '<form action="' . site_url( '/popular-acapellas/' ) . '" method="POST" id="popularform">';
	$output .= '<div class="acas4u-sf-row acas4u-sr-border">';

	$output .= '<div class="acas4u-sf-col w25pc">';
	$output .= '<label for="acas4u_date_from">Date from: </label>';
	$output .= '<input type="text" name="date_from" id="acas4u_date_from" value="' . date( 'd/m/Y', strtotime( $start_date ) ) . '">';
	$output .= '</div>';

	$output .= '<div class="acas4u-sf-col w25pc">';
	$output .= '<label for="acas4u_date_to">Date to: </label>';
	$output .= '<input type="text" name="date_to" id="acas4u_date_to" value="' . date( 'd/m/Y', strtotime( $end_date ) ) . '">';
	$output .= '</div>';

	$count_from = ( $count != '' ) ? $count : 25;

	$output .= '<div class="acas4u-sf-col w50pc">';
	$output .= '<label for="acas4u_popular_count">Results count: </label>';
	$output .= '<input type="text" name="count" id="acas4u_popular_count" data-from="' . $count_from . '" value="' . $count . '">';
	$output .= '</div>';

	$output .= '</div>';

	$output .= '<div class="acas4u-sf-row"><input type="submit" alt="Filter" value="Filter"/></div>';

	$output .= '</form>';
	$output .= '</div>';

	if ( count( $downloads ) > 0 ) {

		$ajax_nonce = wp_create_nonce( 'acas4u_do_scroll_popular_nonce' );

		$output .= '<div class="acas4u-popular-result-wrapper"
			data-nonce="' . $ajax_nonce . '"
			data-total="' . $total_downloads . '"
			data-count="' . $count . '"
			data-enddate="' . $end_date . '"
			data-startdate="' . $start_date . '">';
		$output .= '<table class="acas4u-popular-download-result-wrapper" id="acas4u-popular-table" data-donator="' . $data_donator . '">';

		$output .= '<thead>';
		$output .= '<tr class="acas4u-popular-download-row">';
		$output .= '<th class="pd-cell-title pd-count">#</th>';
		$output .= '<th class="pd-cell-title pd-link">Title</th>';
		$output .= '<th class="pd-cell-title pd-mode">Mode</th>';
		$output .= '<th class="pd-cell-title pd-key1">Key 1</th>';
		$output .= '<th class="pd-cell-title pd-key2"><span ' . $tooltip_key2 . '>Key 2</span></th>';
		$output .= '<th class="pd-cell-title pd-bpm">BPM</th>';
		$output .= '<th class="pd-cell-title pd-bitrate">Bitrate</th>';
		$output .= '<th class="pd-cell-title pd-size">Size</th>';
		$output .= '<th class="pd-cell-title pd-duration">Duration</th>';
		$output .= '<th class="pd-cell-title pd-count" title="Listeners from ' . date( 'd/m/Y', strtotime( $start_date ) ) . ' to ' . date( 'd/m/Y', strtotime( $end_date ) ) . '">Listeners</th>';
		$output .= '<th class="pd-cell-title pd-added">Added</th>';
		$output .= '<th class="pd-cell-title pd-rating">Rating</th>';
		$output .= '</tr>';
		$output .= '</thead>';

		$output .= '<tbody>';
		$popular_results = acas4u_get_popular_downloads_table_body_from_logs( $downloads, 0, $count, 1 );
		$offset = $popular_results['offset'];
		$output .= $popular_results['large'];
		$output .= '</tbody>';
		$output .= '</table>';
		if ( $total_downloads > $count ) {
			$output .= '<div class="g-loadmore">';
			$output .= '<div class="g-loadmore-btn acas4u-show-more-popular" data-offset="' . $offset . '" data-page="' . 2 . '">';
			$output .= '<span>' . __( 'Load More', 'us' ) . '</span>';
			$output .= '</div>';
			$output .= '<div class="g-preloader type_1"></div>';
			$output .= '</div>';
		}
		$output .= '</div>';
	} else {
		$output .= '<div>No popular acapellas found in this period.</div>';
	}

	return $output;
}

function acas4u_get_popular_downloads_table_body_from_logs( $downloads, $offset, $count, $page ) {
	global $wpdb;

	$i = 0;
	$last_result = ( $page - 1 ) * $count;
	$output = '';
	$output_mobile = '';
	foreach ( $downloads as $download ) {
		$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_filehash" AND meta_value= "' . $download->filehash . '"';
		$post_from_meta = $wpdb->get_row( $sqlquery );
		$post_id = $post_from_meta->post_id;
		if ( $post_id ) {
			$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$download_trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
			$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $download_trackname );

			$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
			if ( $download_mode != '' ) {
				$download_mode = ( $download_mode == 1 ) ? 'major' : 'minor';
			}
			$download_key = (int) get_post_meta( $post_id, '_download_key', TRUE );
			$download_keymode = (int) get_post_meta( $post_id, '_download_keymode_id', TRUE );
			$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );

			$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			// $download_bpm = get_post_meta( $post_id, '_download_bpm', TRUE ); // use 'tempo' instead of 'bpm'
			$download_size = get_post_meta( $post_id, '_download_size', TRUE );
			$download_size = formatBytes( $download_size );

			$date_uploaded = get_the_date( 'd/m/Y', $post_id );

			$download_rating = get_post_meta( $post_id, '_download_rating', TRUE );
			if ( $download_rating == '' ) {
				$download_rating = 0;
			}
			$rating_stars = acas4u_display_rating_stars( $download_rating );
			$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );

			$permalink = get_the_permalink( $post_id );
			$i ++;

			// create html for mobile version (stacktable)
			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<th class="st-head-row" colspan="2">' . ( $i + $last_result ) . '</th>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="1">Title</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-link"><a href="' . $permalink . '">' . $title . '</a></td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="2">Mode</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-mode">' . $download_mode . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="3">Key 1</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-key1">' . acas4u_key_value( $download_key ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="4">Key 2</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-key2">' . acas4u_keymode_value( $download_key ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="5">BPM</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-bpm">' . $download_tempo . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="6">Bitrate</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-bitrate">' . $download_bitrate . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="7">Size</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-size">' . $download_size . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="8">Length</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-length">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="9">Listeners</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-count">' . number_format( $download->downloads_count, 0, ',', ',' ) . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="10">Added</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-added">' . $date_uploaded . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output_mobile .= '<tr class="acas4u-popular-download-row">';
			$output_mobile .= '<td class="st-key" data-index="11">Rating</td>';
			$output_mobile .= '<td class="st-val pd-cell pd-rating" title="Total votes: ' . $download_votes . '" data-rating="' . $download_rating . '">' . $rating_stars . '</td>';
			$output_mobile .= '</tr>' . "\n";

			$output .= '<tr class="acas4u-popular-download-row">';
			$output .= '<td class="pd-cell pd-count">' . ( $i + $last_result ) . '</td>';
			$output .= '<td class="pd-cell pd-link"><a href="' . $permalink . '">' . $title . '</a></td>';

			$output .= '<td class="pd-cell pd-mode">' . $download_mode . '</td>';
			$output .= '<td class="pd-cell pd-key1">' . acas4u_key_value( $download_key ) . '</td>';
			$output .= '<td class="pd-cell pd-key2">' . acas4u_keymode_value( $download_keymode ) . '</td>';

			$output .= '<td class="pd-cell pd-bpm">' . $download_tempo . '</td>';
			$output .= '<td class="pd-cell pd-bitrate">' . $download_bitrate . '</td>';
			$output .= '<td class="pd-cell pd-size">' . $download_size . '</td>';

			$output .= '<td class="pd-cell pd-duration">' . acas4u_format_duration( $download_duration ) . '</td>';
			$output .= '<td class="pd-cell pd-count">' . number_format( $download->downloads_count, 0, ',', ',' ) . '</td>';
			$output .= '<td class="pd-cell pd-added">' . $date_uploaded . '</td>';
			$output .= '<td class="pd-cell pd-rating" data-rating="' . $download_rating . '" title="Total votes: ' . $download_votes . '">' . $rating_stars . '</td>';
			$output .= '</tr>';
		}
		$offset ++;
		if ( $i >= $count ) {
			break;
		}
	}

	return array( 'large' => $output, 'small' => $output_mobile, 'offset' => $offset );
}

/**
 * Ajax for infinite scroll
 */
add_action( 'wp_ajax_do_scroll_popular', 'acas4u_do_scroll_popular' );
add_action( 'wp_ajax_nopriv_do_scroll_popular', 'acas4u_do_scroll_popular' );
function acas4u_do_scroll_popular() {
	global $wpdb;
	check_ajax_referer( 'acas4u_do_scroll_popular_nonce', '_ajax_nonce' );

	$end_date = $_POST['enddate'];
	$start_date = $_POST['startdate'];
	$page = $_POST['page'];
	$total = $_POST['total'];
	$offset = $_POST['offset'];
	$count = $_POST['count'];

	$query = 'SELECT count(log_id) AS downloads_count, filehash FROM phpbb_download_logs WHERE timestamp >= "' . $start_date . '" AND timestamp <= "' . $end_date . '" GROUP BY filehash ORDER BY downloads_count DESC LIMIT ' . $offset . ', ' . $count * 1.5;
	$downloads = $wpdb->get_results( $query );

	if ( ! $downloads ) {
		wp_send_json_error();
	}

	$popular_results = acas4u_get_popular_downloads_table_body_from_logs( $downloads, $offset, $count, $page );

	wp_send_json_success( array(
		'html_large' => $popular_results['large'],
		'html_small' => $popular_results['small'],
		'offset' => $popular_results['offset'],
	) );
}

function acas4u_get_popular_downloads_with_images_from_logs( $count, $period ) {
	global $wpdb, $acas4u_stylesheet_directory_uri;

	$period_str = '30 days';

	if ( $period == 'month' ) {
		$period_str = '30 days';
	} else if ( $period == 'week' ) {
		$period_str = '7 days';
	}

	$last_date = $wpdb->get_var( 'SELECT timestamp FROM phpbb_download_logs ORDER BY timestamp DESC LIMIT 0,1' );

	$end_date = date( 'Y-m-d', strtotime( $last_date ) ) . ' 23:59:59';
	$start_date = date( 'Y-m-d', strtotime( $last_date . ' - ' . $period_str ) ) . ' 00:00:00';

	$d_query = 'SELECT count(log_id) AS downloads_count, filehash FROM phpbb_download_logs WHERE timestamp >= "' . $start_date . '" AND timestamp <= "' . $end_date . '" GROUP BY filehash ORDER BY downloads_count DESC LIMIT 0,' . ( $count + 2 );

	$downloads = $wpdb->get_results( $d_query );

	$output = '';
	if ( count( $downloads ) > 0 ) {
		$output .= '<div class="acas4u-fp-latest-wrapper">';
		$i = 0;
		foreach ( $downloads as $download ) {
			if ( $i >= $count ) {
				break;
			}
			$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_filehash" AND meta_value= "' . $download->filehash . '"';
			$post_from_meta = $wpdb->get_row( $sqlquery );
			$post_id = $post_from_meta->post_id;
			if ( $post_id ) {
				$permalink = get_the_permalink( $post_id );
				$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
				$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
				$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
				$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );
				$filename = get_post_meta( $post_id, '_download_filename', TRUE );

				$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

				$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
				if ( $artist_thumbnail_id ) {
					$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
					$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
				} else {
					$artist_thumbnail_url = $acas4u_stylesheet_directory_uri . '/img/placeholder_130x130.png';
				}
				$artist_thumbnail_img = '<img src="' . $artist_thumbnail_url . '" alt="' . $title . '>" class="img-responsive">';

				$short_title = acas4u_limit_chars( $title, 40 );
				$output .= '<div class="acas4u-fp-latest-single" title="' . $filename . '">';
				$output .= '<a class="acas4u-fp-latest-link" href="' . $permalink . '">';
				$output .= '<div class="acas4u-fp-latest-image">' . $artist_thumbnail_img . '</div>';
				$output .= '<div class="acas4u-fp-latest-inner">';
				$output .= '<div class="acas4u-fp-latest-overlay"></div>';
				$output .= '<div class="acas4u-fp-latest-title">' . $short_title . '</div>';
				$output .= '</div>';
				$output .= '</a>';
				$output .= '</div>';
				$i ++;
			}
		}
		$output .= '</div>';
	}

	return $output;
}
