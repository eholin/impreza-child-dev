<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

// add custom interval
add_filter( 'cron_schedules', 'cron_add_5minutes' );
function cron_add_5minutes( $schedules ) {
	// Adds once every 5 minutes to the existing schedules.
	$schedules['everyfiveminutes'] = array(
		'interval' => 300,
		'display' => __( 'Once Every 5 Minutes' ),
	);

	// Adds once every one minute to the existing schedules.
	$schedules['everyminute'] = array(
		'interval' => 60,
		'display' => __( 'Once Every One Minute' ),
	);

	return $schedules;
}

//remove unused cron events
//add_action('init', 'clear_crons_left');
function clear_crons_left() {
	wp_clear_scheduled_hook( 'sm_ping' );
}

/* ---------------------------------- start of cron jobs activation ---------------------------------- */

/* add cron job for audio waveform preview generation */
add_action( 'wp', 'acas4u_add_update_downloads_count_to_cron' );
function acas4u_add_update_downloads_count_to_cron() {
	if ( ! wp_next_scheduled( 'update_downloads_count' ) ) {
		wp_schedule_event( time(), 'daily', 'update_downloads_count' );
	}
}

/* add cron job for audio waveform preview generation */
add_action( 'wp', 'acas4u_add_generate_preview_to_cron' );
function acas4u_add_generate_preview_to_cron() {
	if ( ! wp_next_scheduled( 'generate_preview' ) ) {
		wp_schedule_event( time(), 'hourly', 'generate_preview' );
	}
}

// add cron job for artists index update
add_action( 'wp', 'acas4u_add_update_artists_index_to_cron' );
function acas4u_add_update_artists_index_to_cron() {
	if ( ! wp_next_scheduled( 'update_artists_index' ) ) {
		wp_schedule_event( time(), 'daily', 'update_artists_index' );
	}
}

// add cron job for downloads index update
add_action( 'wp', 'acas4u_add_update_downloads_index_to_cron' );
function acas4u_add_update_downloads_index_to_cron() {
	if ( ! wp_next_scheduled( 'update_downloads_index' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_downloads_index' );
	}
}

// add cron job for downloads terms update
//add_action( 'wp', 'acas4u_add_update_downloads_terms_to_cron' );
function acas4u_add_update_downloads_terms_to_cron() {
	if ( ! wp_next_scheduled( 'update_downloads_terms' ) ) {
		wp_schedule_event( time(), 'everyminute', 'update_downloads_terms' );
	}
}

// add cron job for update download indexes
add_action( 'wp', 'acas4u_add_update_search_indexes_to_cron' );
function acas4u_add_update_search_indexes_to_cron() {
	if ( ! wp_next_scheduled( 'update_search_indexes' ) ) {
		wp_schedule_event( time(), 'daily', 'update_search_indexes' );
	}
}

// TEMP add cron job for update download indexes
//add_action( 'wp', 'acas4u_add_temp_update_search_indexes_to_cron' );
function acas4u_add_temp_update_search_indexes_to_cron() {
	if ( ! wp_next_scheduled( 'temp_update_search_indexes' ) ) {
		wp_schedule_event( time(), 'everyminute', 'temp_update_search_indexes' );
	}
}

// add cron job for downloads update
// connect downloads with artist by artist_id
add_action( 'wp', 'acas4u_add_update_downloads_to_cron' );
function acas4u_add_update_downloads_to_cron() {
	if ( ! wp_next_scheduled( 'update_downloads' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_downloads' );
	}
}

/* add cron job for artists genres update from Spotify */
add_action( 'wp', 'acas4u_add_update_artist_genres_to_cron' );
function acas4u_add_update_artist_genres_to_cron() {
	if ( ! wp_next_scheduled( 'update_artist_genres' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_artist_genres' );
	}
}

/* add cron job for download genres update from artists */
add_action( 'wp', 'acas4u_add_update_download_genres_to_cron' );
function acas4u_add_update_download_genres_to_cron() {
	if ( ! wp_next_scheduled( 'update_download_genres' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_download_genres' );
	}
}

/* add cron job for download genres update from artists */
add_action( 'wp', 'acas4u_add_update_popular_acapellas_to_cron' );
function acas4u_add_update_popular_acapellas_to_cron() {
	if ( ! wp_next_scheduled( 'update_popular_acapellas' ) ) {
		wp_schedule_event( time(), 'daily', 'update_popular_acapellas' );
	}
}

/* Spotify API not contains Artist Biography */

/* add cron job for artists meta (images etc - but only meta values, not real image files!!!) update from Spotify */
add_action( 'wp', 'acas4u_add_update_artist_meta_to_cron' );
function acas4u_add_update_artist_meta_to_cron() {
	if ( ! wp_next_scheduled( 'update_artist_meta' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_artist_meta' );
	}
}

/* add cron job for related artists - update from Spotify */
add_action( 'wp', 'acas4u_add_update_related_artist_to_cron' );
function acas4u_add_update_related_artist_to_cron() {
	if ( ! wp_next_scheduled( 'update_related_artists' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_related_artists' );
	}
}

/* add cron job for artist top tracks - update from Spotify */
add_action( 'wp', 'acas4u_add_update_artist_top_tracks_to_cron' );
function acas4u_add_update_artist_top_tracks_to_cron() {
	if ( ! wp_next_scheduled( 'update_artist_top_tracks' ) ) {
		wp_schedule_event( time(), 'hourly', 'update_artist_top_tracks' );
	}
}

/* add cron job for artists images download from Spotify sources */
add_action( 'wp', 'acas4u_add_download_images_to_cron' );
function acas4u_add_download_images_to_cron() {
	if ( ! wp_next_scheduled( 'download_images' ) ) {
		wp_schedule_event( time(), 'hourly', 'download_images' );
	}
}

/* add cron job for update acepellas list by cron */
//add_action( 'wp', 'acas4u_add_update_acapellas_list_to_cron' );
function acas4u_add_update_acapellas_list_to_cron() {
	if ( ! wp_next_scheduled( 'update_acapellas_list' ) ) {
		wp_schedule_event( time(), 'daily', 'update_acapellas_list' );
	}
}

/* #########-------------------- start of not active cron jobs ---------------------######### */

/* add cron job for update users meta (capabilities) */
/*
add_action( 'wp', 'acas4u_add_update_users_meta_to_cron' );
function acas4u_add_update_users_meta_to_cron() {
	if ( ! wp_next_scheduled( 'update_users_meta' ) ) {
		wp_schedule_event( time(), 'everyminute', 'update_users_meta' );
	}
}
*/

/* #########-------------------- end of not active cron jobs ---------------------######### */

/* #########-------------------- start of Echonest deprecated cron jobs ---------------------######### */

/* add cron job for artist update
add_action( 'wp', 'acas4u_add_update_artists_to_cron' );
function acas4u_add_update_artists_to_cron() {
	if ( ! wp_next_scheduled( 'update_artists' ) ) {
		wp_schedule_event( time(), 'everyminute', 'update_artists' );
	}
}
*/

/* add cron job for genres update from EchoNest
add_action( 'wp', 'acas4u_add_update_genres_to_cron' );
function acas4u_add_update_genres_to_cron() {
	if ( ! wp_next_scheduled( 'update_genres' ) ) {
		wp_schedule_event( time(), 'everyfiveminutes', 'update_genres' );
	}
}
*/

/* add cron job for artists bio update from EchoNest
add_action( 'wp', 'acas4u_add_update_bio_to_cron' );
function acas4u_add_update_bio_to_cron() {
	if ( ! wp_next_scheduled( 'update_bio' ) ) {
		wp_schedule_event( time(), 'everyfiveminutes', 'update_bio' );
	}
}
*/

/* #########-------------------- end of Echonest deprecated cron jobs ---------------------######### */

/* ---------------------------------- end of cron jobs activation ---------------------------------- */

add_action( 'update_artists_index', 'acas4u_cron_update_artists_index' );
function acas4u_cron_update_artists_index() {

	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 5,
		'order' => 'DESC',
		'orderby' => 'rand',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'artist-index',
				'field' => 'term_id',
				'operator' => 'NOT IN',
				'terms' => get_terms( 'artist-index', array(
					'fields' => 'ids',
				) ),
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();
			$post_title = get_the_title();

			if ( mb_strtolower( mb_substr( $post_title, 0, 4, 'UTF-8' ) ) == 'the ' ) {
				$post_title = mb_substr( $post_title, 4, NULL, 'UTF-8' );
			}

			$artist_filtered_name = preg_replace( "/[^ \w]+/", "", $post_title );

			$artist_fl = mb_strtolower( mb_substr( $post_title, 0, 1, 'UTF-8' ) );

			if ( mb_substr( $artist_filtered_name, 1, 1, 'UTF-8' ) == ' ' ) {
				$artist_filtered_name = mb_substr( $artist_filtered_name, 0, 1, 'UTF-8' ) . mb_substr( $artist_filtered_name, 2, NULL, 'UTF-8' );
			}

			$artist_dl = mb_strtolower( mb_substr( $artist_filtered_name, 0, 2, 'UTF-8' ) );
			$artist_dl = mb_convert_case( $artist_dl, MB_CASE_TITLE, 'UTF-8' );
			$terms = [];
			if ( is_numeric( $artist_fl ) ) {
				$parent_term = get_term_by( 'name', '0-9', 'artist-index' );
				$terms[] = $parent_term->term_taxonomy_id;
			} else {
				$parent_term = get_term_by( 'slug', $artist_fl, 'artist-index' );

				if ( ! $parent_term ) {
					$parent_term = get_term_by( 'name', '_OTHERS_', 'artist-index' );
					$terms[] = $parent_term->term_taxonomy_id;
				} else {
					$child_term = get_term_by( 'slug', $artist_dl, 'artist-index' );
					if ( ! $child_term ) {
						$new_term = wp_insert_term( $artist_dl, 'artist-index', array(
							'slug' => mb_strtolower( $artist_dl ),
							'parent' => $parent_term->term_taxonomy_id,
						) );
						$terms[] = $new_term['term_id'];
					} else {
						$terms[] = $child_term->term_taxonomy_id;
					}
				}
			}

			if ( count( $terms ) > 0 ) {
				$terms = array_map( 'intval', $terms );
				$post_terms_ids = wp_set_object_terms( $post_id, $terms, 'artist-index', TRUE );
			}
		}
	}
	wp_reset_query();
}

add_action( 'temp_update_search_indexes', 'acas4u_cron_temp_update_download_indexes' );
function acas4u_cron_temp_update_download_indexes() {
	global $wpdb;

	$query = 'SELECT download_post_id FROM acas4u_download_indexes WHERE download_permalink="" LIMIT 0,15';
	$downloads = $wpdb->get_results( $query );

	if ( count( $downloads ) > 0 ) {
		foreach ( $downloads as $download ) {
			$post_id = $download->download_post_id;
			$permalink = get_the_permalink( $post_id );
			$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
			$download_count = get_post_meta( $post_id, '_download_count', TRUE );
			$date_uploaded = get_the_date( 'Y-m-d H:i:s', $post_id );
			$query_update = 'UPDATE acas4u_download_indexes SET
										download_permalink = "' . $permalink . '",
										download_votes="' . $download_votes . '",
										download_duration="' . $download_duration . '",
										download_count="' . $download_count . '",
										download_post_date="' . $date_uploaded . '"
									 WHERE download_post_id = "' . $post_id . '"';
			$wpdb->query( $query_update );
		}
	}
}

add_action( 'update_search_indexes', 'acas4u_cron_update_download_indexes' );
function acas4u_cron_update_download_indexes() {
	global $wpdb;

	$args = array(
		'post_type' => 'download',
		'posts_per_page' => 15,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_index_updated',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_artists_genres.log' );
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();
			$post_name = get_the_title();
			$permalink = get_the_permalink();
			$download_post_date = get_the_date( 'Y-m-d H:i:s' );
			$download_updated = date( 'Y-m-d H:i:s' );
			$download_filename = get_post_meta( $post_id, '_download_filename', TRUE );
			$download_size = get_post_meta( $post_id, '_download_size', TRUE );
			$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );
			$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			$download_rating = get_post_meta( $post_id, '_download_rating', TRUE );
			$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
			$download_live = get_post_meta( $post_id, '_download_live', TRUE );
			$download_artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
			$download_artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
			$download_trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
			$download_samplerate = get_post_meta( $post_id, '_download_samplerate', TRUE );
			$download_key = get_post_meta( $post_id, '_download_key', TRUE );
			$download_key2 = get_post_meta( $post_id, '_download_keymode_id', TRUE );
			$download_time_signature = get_post_meta( $post_id, '_download_time_signature', TRUE );
			$download_term_id = get_post_meta( $post_id, '_download_term_id', TRUE );
			$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
			$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
			$download_count = get_post_meta( $post_id, '_download_count', TRUE );

			$query = 'INSERT INTO acas4u_download_indexes (
							download_post_id,
							download_post_name,
							download_filename,
							download_size,
							download_tempo,
							download_bitrate,
							download_rating,
							download_mode,
							download_live,
							download_artist1,
							download_artist2,
							download_trackname,
							download_samplerate,
							download_key,
							download_key2,
							download_time_signature,
							download_term_id,
							index_last_updated,
							download_permalink,
							download_votes,
							download_duration,
							download_count,
							download_post_date
							) VALUES (
							"' . $post_id . '",
							"' . $post_name . '",
							"' . esc_attr( $download_filename ) . '",
							"' . $download_size . '",
							"' . $download_tempo . '",
							"' . $download_bitrate . '",
							"' . $download_rating . '",
							"' . $download_mode . '",
							"' . $download_live . '",
							"' . esc_attr( $download_artist1 ) . '",
							"' . esc_attr( $download_artist2 ) . '",
							"' . esc_attr( $download_trackname ) . '",
							"' . $download_samplerate . '",
							"' . $download_key . '",
							"' . $download_key2 . '",
							"' . $download_time_signature . '",
							"' . $download_term_id . '",
							"' . $download_updated . '",
							"' . $permalink . '",
							"' . $download_votes . '",
							"' . $download_duration . '",
							"' . $download_count . '",
							"' . $download_post_date . '"
							)';
			$wpdb->query( $query );
			update_post_meta( $post_id, '_download_index_updated', $download_updated );
		}
	}
	wp_reset_query();
}

add_action( 'update_download_genres', 'acas4u_cron_update_download_genres' );
function acas4u_cron_update_download_genres() {
	$args = array(
		'post_type' => 'download',
		'posts_per_page' => 5,
		'order' => 'ASC',
		'orderby' => 'title',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'genre',
				'field' => 'term_id',
				'operator' => 'NOT IN',
				'terms' => get_terms( 'genre', array(
					'fields' => 'ids',
				) ),
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist_id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_download_genre_last_update',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------' );
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();

			$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );

			$artist_taxonomies = wp_get_post_terms( $artist_id, 'genre', array(
				'orderby' => 'name',
				'order' => 'ASC',
				'fields' => 'ids',
			) );

			if ( count( $artist_taxonomies ) > 0 ) {

				$artist_taxonomies = array_map( 'intval', $artist_taxonomies );
				$artist_taxonomies = array_unique( $artist_taxonomies );
				$term_taxonomy_ids = wp_set_object_terms( $post_id, $artist_taxonomies, 'genre', TRUE );
			}

			$timestamp = time();
			update_post_meta( $post_id, '_download_genre_last_update', $timestamp );

			$message = 'Added ' . count( $term_taxonomy_ids ) . ' terms for post #' . $post_id . ' (artist ID ' . $artist_id . ')' . "\r\n";
			//acas4u_write_log_entry( $message );
		}
	} else {
		//acas4u_write_log_entry( 'No posts without terms' );
	}
	wp_reset_query();
}

/* actual function, data from Spotify API */
add_action( 'update_artist_genres', 'acas4u_cron_update_artist_genres' );
function acas4u_cron_update_artist_genres() {

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_artists_genres.log' );

	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'genre',
				'field' => 'term_id',
				'operator' => 'NOT IN',
				'terms' => get_terms( 'genre', array(
					'fields' => 'ids',
				) ),
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist_spotify_id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_artist_genre_last_update',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_artists_genres.log' );
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();

			$artist_spotify_id = get_post_meta( $post_id, '_download_artist_spotify_id', TRUE );

			if ( $artist_spotify_id ) {

				$url = 'https://api.spotify.com/v1/artists/' . $artist_spotify_id;
				//acas4u_write_log_entry( 'Our Spotify API URL: ' . $url, 'acas4u_artists_genres.log' );
				$response = wp_remote_get( $url );
				$body = wp_remote_retrieve_body( $response );
				$json = json_decode( $body );

				$genres = $json->genres;
				if ( count( $genres ) > 0 ) {
					$t = 0;
					acas4u_write_log_entry( 'All genres: ' . serialize( $genres ), 'acas4u_artists_genres.log' );
					foreach ( $genres as $genre ) {
						$artist_genres[] = $genre;

						$term_slug = acas4u_clean_slug( $genre );
						$is_term_exists = acas4u_term_exists_by_slug( $term_slug, 'genre' );
						if ( $is_term_exists ) {
							$artist_taxonomies[] = $is_term_exists['term_taxonomy_id'];
						} else {
							$term_inserted = wp_insert_term( $genre, 'genre', array( 'slug' => $term_slug ) );
							//acas4u_write_log_entry( 'WP insert term response: ' . serialize( $term_inserted, 'acas4u_artists_genres.log' ) );
							$artist_taxonomies[] = $term_inserted['term_taxonomy_id'];
						}
						$t ++;
					}
					$artist_taxonomies = array_map( 'intval', $artist_taxonomies );
					$artist_taxonomies = array_unique( $artist_taxonomies );
					$artist_term_taxonomy_ids = wp_set_object_terms( $post_id, $artist_taxonomies, 'genre', TRUE );
				}
			}

			$timestamp = time();
			update_post_meta( $post_id, '_artist_genre_last_update', $timestamp );

			$message = 'Added ' . count( $artist_term_taxonomy_ids ) . ' terms for post #' . $post_id . ' (artist ID: ' . $artist_spotify_id . ', timestamp: ' . $timestamp . ')' . "\r\n";
			acas4u_write_log_entry( $message, 'acas4u_artists_genres.log' );
		}
	} else {
		acas4u_write_log_entry( 'No artists without terms', 'acas4u_artists_genres.log' );
	}
	wp_reset_query();
}

add_action( 'update_artist_top_tracks', 'acas4u_cron_spotify_update_top_tracks' );
function acas4u_cron_spotify_update_top_tracks() {
	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 5,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist_spotify_id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_artist_top_tracks_last_update',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();

			$artist_spotify_id = get_post_meta( $post_id, '_download_artist_spotify_id', TRUE );

			if ( $artist_spotify_id ) {

				$url = 'https://api.spotify.com/v1/artists/' . $artist_spotify_id . '/top-tracks?country=GB';
				$response = wp_remote_get( $url );
				$body = wp_remote_retrieve_body( $response );
				$json = json_decode( $body );

				$artist_top_tracks = $json->tracks;
				if ( count( $artist_top_tracks ) > 0 ) {
					echo '<p>' . $post_id . ' / ' . $artist_spotify_id . '</p>';
					$t = 0;
					$artist_tracks = [];
					foreach ( $artist_top_tracks as $single_track ) {
						$artist_tracks[ $t ]['spotify_track_id'] = $single_track->id;
						$artist_tracks[ $t ]['spotify_track_popularity'] = $single_track->popularity;
						$artist_tracks[ $t ]['spotify_track_name'] = $single_track->name;
						$t ++;
					}

					if ( ! empty( $artist_tracks[0] ) ) {
						update_post_meta( $post_id, '_artist_spotify_top_tracks', serialize( $artist_tracks ) );
					}
				}
			}

			$timestamp = time();
			update_post_meta( $post_id, '_artist_top_tracks_last_update', $timestamp );

			$message = 'Added ' . count( $artist_tracks ) . ' top tracks for post #' . $post_id . ' (artist ID: ' . $artist_spotify_id . ', timestamp: ' . $timestamp . ')' . "\r\n";
			acas4u_write_log_entry( $message, 'acas4u_artist_top_tracks.log' );
		}
	} else {
		acas4u_write_log_entry( 'No artist matched this criteries', 'acas4u_artist_top_tracks.log' );
	}
	wp_reset_query();
}

add_action( 'update_related_artists', 'acas4u_cron_spotify_update_related_artists' );
function acas4u_cron_spotify_update_related_artists() {
	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 5,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist_spotify_id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_artist_related_artists_last_update',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_artists_genres.log' );
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();

			$artist_spotify_id = get_post_meta( $post_id, '_download_artist_spotify_id', TRUE );

			if ( $artist_spotify_id ) {

				$url = 'https://api.spotify.com/v1/artists/' . $artist_spotify_id . '/related-artists';
				//acas4u_write_log_entry( 'Our Spotify API URL: ' . $url, 'acas4u_artists_genres.log' );
				$response = wp_remote_get( $url );
				$body = wp_remote_retrieve_body( $response );
				$json = json_decode( $body );

				$related_artists = $json->artists;
				if ( count( $related_artists ) > 0 ) {
					$t = 0;
					$related_artists_ids = [];
					foreach ( $related_artists as $single_artist ) {
						$related_artists_ids[] = $single_artist->id;
					}

					if ( ! empty( $related_artists_ids[0] ) ) {
						update_post_meta( $post_id, '_artist_related_artists_ids', maybe_serialize( $related_artists_ids ) );
					}
				}
			}

			$timestamp = time();
			update_post_meta( $post_id, '_artist_related_artists_last_update', $timestamp );

			$message = 'Added ' . count( $related_artists_ids ) . ' related artists for post #' . $post_id . ' (artist ID: ' . $artist_spotify_id . ', timestamp: ' . $timestamp . ')' . "\r\n";
			acas4u_write_log_entry( $message, 'acas4u_related_artists.log' );
		}
	} else {
		acas4u_write_log_entry( 'No artists without terms', 'acas4u_related_artists.log' );
	}
	wp_reset_query();
}

add_action( 'update_artist_meta', 'acas4u_cron_spotify_update_artist_meta' );
function acas4u_cron_spotify_update_artist_meta() {

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_images.log' );

	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist_spotify_id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_download_artist_images',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_images.log' );
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();

			$artist_spotify_id = get_post_meta( $post_id, '_download_artist_spotify_id', TRUE );

			if ( $artist_spotify_id ) {

				$url = 'https://api.spotify.com/v1/artists/' . $artist_spotify_id;
				acas4u_write_log_entry( 'Our Spotify API URL: ' . $url, 'acas4u_images.log' );
				$response = wp_remote_get( $url );
				$body = wp_remote_retrieve_body( $response );
				$json = json_decode( $body );

				// artist images
				$images = $json->images;
				$artist_images = [];
				foreach ( $images as $image ) {
					$artist_images[] = $image->url;
				}
				if ( ! empty( $artist_images[0] ) ) {
					update_post_meta( $post_id, '_download_artist_images', serialize( $artist_images ) );
				}

				// artist popularity
				$artist_popularity = $json->popularity;
				if ( ! empty( $artist_popularity ) ) {
					update_post_meta( $post_id, '_download_artist_popularity', $artist_popularity );
				}
			}

			$message = 'Added ' . count( $artist_images ) . ' images for post #' . $post_id . ' (artist ID ' . $artist_spotify_id . ')' . "\r\n";
			acas4u_write_log_entry( $message, 'acas4u_images.log' );
		}
	} else {
		acas4u_write_log_entry( 'No posts without images', 'acas4u_images.log' );
	}
	wp_reset_query();
}

add_action( 'update_downloads', 'acas4u_cron_update_downloads' );
function acas4u_cron_update_downloads() {

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_images.log' );

	$args = array(
		'post_type' => 'download',
		'posts_per_page' => 10,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_artist1id',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_download_artist_id',
				'compare' => 'NOT EXISTS',
			),
		),
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_downloads.log' );
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();

			$artist_enid = get_post_meta( $post_id, '_download_artist1id', TRUE );

			if ( $artist_enid ) {
				$artist_id = acas4u_get_artist_id_by_enid( $artist_enid );
				if ( $artist_id ) {
					update_post_meta( $post_id, '_download_artist_id', $artist_id );

					$message = 'Updated post #' . $post_id . ', artist ID: ' . $artist_id . ')' . "\r\n";
					acas4u_write_log_entry( $message, 'acas4u_downloads.log' );
				}
			}
		}
	} else {
		//acas4u_write_log_entry( 'No posts without artist ID', 'acas4u_downloads.log' );
	}
	wp_reset_query();
}

add_action( 'download_images', 'acas4u_cron_download_artists_images' );
function acas4u_cron_download_artists_images() {

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_download_images.log' );

	require_once( ABSPATH . 'wp-admin/includes/media.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	$args = array(
		'post_type' => 'artist',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_thumbnail_id',
				'compare' => 'NOT EXISTS',
			),
			array(
				'key' => '_download_artist_images',
				'compare' => 'EXISTS',
			),
		),
	);

	$artists = new WP_Query( $args );
	if ( $artists->have_posts() ) {
		acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_download_images.log' );
		while ( $artists->have_posts() ){
			$artists->the_post();
			$post_id = get_the_ID();

			$images = get_post_meta( $post_id, '_download_artist_images', TRUE );

			$images = maybe_unserialize( $images );

			$post_has_thumbnail = FALSE;
			$total_images = 0;
			foreach ( $images as $image ) {

				$response = wp_remote_get( $image );

				if ( is_array( $response ) ) {

					$type = wp_remote_retrieve_header( $response, 'content-type' );
					$ext = FALSE;
					if ( $type == 'image/gif' ) {
						$ext = '.gif';
					} else if ( $type == 'image/jpeg' ) {
						$ext = '.jpg';
					} else if ( $type == 'image/png' ) {
						$ext = '.png';
					}
					if ( $ext ) {
						$uniqid = uniqid( 'img_' );
						$file = $uniqid . $ext;
						$mirror = wp_upload_bits( $file, '', wp_remote_retrieve_body( $response ) );

						$attachment = array(
							'post_title' => basename( $image ),
							'post_mime_type' => $type,
						);
						$attachment_id = wp_insert_attachment( $attachment, $mirror['file'], $post_id );

						if ( $attachment_id AND ! $post_has_thumbnail ) {
							set_post_thumbnail( $post_id, $attachment_id );
							update_post_meta( $post_id, '_thumbnail_id', $attachment_id );
							$post_has_thumbnail = TRUE;
							acas4u_write_log_entry( 'Post ID ' . $post_id . ', New featured image: ' . $mirror['file'], 'acas4u_download_images.log' );
						}
						$total_images ++;
					}
				}
			}
			$message = 'Post ID ' . $post_id . ', added new images: ' . $total_images . "\r\n";
			acas4u_write_log_entry( $message, 'acas4u_download_images.log' );
		}
	} else {
		acas4u_write_log_entry( 'No posts with images in meta', 'acas4u_download_images.log' );
	}
	wp_reset_query();
}

add_action( 'generate_preview', 'acas4u_cron_generate_audiowave_preview' );
function acas4u_cron_generate_audiowave_preview() {

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_preview_dat.log' );

	$acas4u_upload_dir = wp_upload_dir();

	$args = array(
		'post_type' => 'download',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'ID',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_download_preview',
				'compare' => 'NOT EXISTS',
			),
			array(
				'key' => '_download_filename',
				'compare' => 'EXISTS',
			),
			array(
				'key' => '_download_path',
				'compare' => 'EXISTS',
			),
		),
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_preview_dat.log' );
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();

			$download_path = get_post_meta( $post_id, '_download_path', TRUE );
			$filename = get_post_meta( $post_id, '_download_filename', TRUE );

			$download_preview = $post_id . '_preview.dat';
			$preview_path = $acas4u_upload_dir['basedir'] . '/waveforms/' . $download_preview;
			$preview_png_path = $acas4u_upload_dir['basedir'] . '/waveforms/png/' . trim( $download_preview, 'dat' ) . 'png';
			$file_path = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . $filename;
			$output_dat = shell_exec( 'audiowaveform -i "' . $file_path . '" -o "' . $preview_path . '" -z 256 -b 8' );
			$output_png = shell_exec( 'audiowaveform -i "' . $preview_path . '" -o "' . $preview_png_path . '" -s 45.0 -e 60.0 -w 780 -h 200' );
			update_post_meta( $post_id, '_download_preview', $download_preview );

			//acas4u_write_log_entry( '#' . $post_id . ', file: ' . $file_path, 'acas4u_preview_dat.log' );
			//acas4u_write_log_entry( '#' . $post_id . ', preview.dat: ' . $preview_path, 'acas4u_preview_dat.log' );
			//acas4u_write_log_entry( '#' . $post_id . ', preview.png: ' . $preview_png_path, 'acas4u_preview_dat.log' );
			acas4u_write_log_entry( 'Post ID ' . $post_id . ', added new preview: ' . $download_preview . "\r\n", 'acas4u_preview_dat.log' );
		}
	} else {
		acas4u_write_log_entry( 'No posts without preview', 'acas4u_preview_dat.log' );
	}
	wp_reset_query();
}

add_action( 'update_downloads_count', 'acas4u_cron_update_downloads_count' );
function acas4u_cron_update_downloads_count() {
	/* downloads count for donators */
	$args = acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, FALSE );

	$downloads_for_donators = acas4u_get_posts_count( $args, 'custom' );
	update_option( 'acas4u_downloads_for_donators', $downloads_for_donators );

	/* downloads count for non donators */
	$query_bitrate2 = (int) us_get_option( 'max_bitrate_for_non_donators' );
	$args = acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, FALSE );

	$downloads_for_non_donators = acas4u_get_posts_count( $args, 'custom' );
	update_option( 'acas4u_downloads_for_non_donators', $downloads_for_non_donators );
}

//add_action( 'update_acapellas_list', 'acas4u_cron_update_acapellas_list' );
function acas4u_cron_update_acapellas_list() {
	$acas4u_upload_dir = wp_upload_dir();

	$fp = fopen( $acas4u_upload_dir['basedir'] . '/acapellas4u_list.txt', 'w' );

	$args = array(
		'post_type' => 'download',
		'post_status' => 'publish',
		'posts_per_page' => - 1,
		'order' => 'ASC',
		'orderby' => 'title',
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {

		//fwrite( $fp, '<h2>Total: ' . $downloads->post_count . ' acapellas</h2>' . "\r\n" );

		//fwrite( $fp, '<ul class="acas4u-acapellas-list">' . "\r\n" );
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			fwrite( $fp, get_the_title() . "\r\n" );
			//fwrite( $fp, '<li>' . get_the_title() . '</li>' . "\r\n" );
		}
		//fwrite( $fp, '</ul>' . "\r\n" );
	}
	wp_reset_query();

	fclose( $fp );
}

add_action( 'update_downloads_index', 'acas4u_update_downloads_index' );
function acas4u_update_downloads_index() {
	$args = array(
		'post_type' => 'download',
		'posts_per_page' => 10,
		'order' => 'DESC',
		'orderby' => 'rand',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'download-index',
				'field' => 'term_id',
				'operator' => 'NOT IN',
				'terms' => get_terms( 'download-index', array(
					'fields' => 'ids',
				) ),
			),
		),
	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();
			$post_title = get_the_title();

			$download_path = get_post_meta( $post_id, '_download_path', TRUE );
			$artist1_name = get_post_meta( $post_id, '_download_artist1', TRUE );
			$artist2_name = get_post_meta( $post_id, '_download_artist2', TRUE );
			if ( $artist1_name != '' ) {
				$artist_name = $artist1_name;
			} else if ( $artist1_name == '' AND $artist2_name != '' ) {
				$artist_name = $artist2_name;
			} else {
				$artist_name = $post_title;
			}

			if ( mb_strtolower( mb_substr( $artist_name, 0, 4, 'UTF-8' ) ) == 'the ' ) {
				$artist_name = mb_substr( $artist_name, 4, NULL, 'UTF-8' );
			}

			$artist_filtered_name = preg_replace( "/[^ \w]+/", "", $artist_name );

			$artist_fl = mb_strtolower( mb_substr( $artist_name, 0, 1, 'UTF-8' ) );

			if ( mb_substr( $artist_filtered_name, 1, 1, 'UTF-8' ) == ' ' ) {
				$artist_filtered_name = mb_substr( $artist_filtered_name, 0, 1, 'UTF-8' ) . mb_substr( $artist_filtered_name, 2, NULL, 'UTF-8' );
			}

			$artist_dl = mb_strtolower( mb_substr( $artist_filtered_name, 0, 2, 'UTF-8' ) );
			$artist_dl = mb_convert_case( $artist_dl, MB_CASE_TITLE, 'UTF-8' );
			$terms = [];

			$special_term = FALSE;

			if ( stripos( $download_path, 'DJ_TOOLS' ) !== FALSE ) {
				$parent_term = get_term_by( 'name', '_DJ_TOOLS_', 'download-index' );
				$terms[] = $parent_term->term_taxonomy_id;
				$special_term = TRUE;
			}

			if ( stripos( $download_path, 'ROCKAPELLAS_COVERS' ) !== FALSE ) {
				$parent_term = get_term_by( 'name', '_ROCKAPELLAS_COVERS_', 'download-index' );
				$terms[] = $parent_term->term_taxonomy_id;
				$special_term = TRUE;
			}

			if ( stripos( $download_path, 'OTHERS' ) !== FALSE ) {
				$parent_term = get_term_by( 'name', '_OTHERS_', 'download-index' );
				$terms[] = $parent_term->term_taxonomy_id;
				$special_term = TRUE;
			}

			if ( stripos( $download_path, 'UNKNOWNS' ) !== FALSE ) {
				$parent_term = get_term_by( 'name', '_UNKNOWNS_', 'download-index' );
				$terms[] = $parent_term->term_taxonomy_id;
				$special_term = TRUE;
			}

			if ( stripos( $download_path, 'VOCAL_SHOTS_MISC' ) !== FALSE ) {
				$parent_term = get_term_by( 'name', '_VOCAL_SHOTS_MISC_', 'download-index' );
				$terms[] = $parent_term->term_taxonomy_id;
				$special_term = TRUE;
			}

			if ( $special_term === FALSE ) {

				if ( is_numeric( $artist_fl ) ) {
					$parent_term = get_term_by( 'name', '0-9', 'download-index' );
					$terms[] = $parent_term->term_taxonomy_id;
				} else {
					$parent_term = get_term_by( 'slug', $artist_fl, 'download-index' );

					if ( ! $parent_term ) {
						$parent_term = get_term_by( 'name', '_UNKNOWNS_', 'download-index' );
						$terms[] = $parent_term->term_taxonomy_id;
					} else {
						$child_term = get_term_by( 'slug', $artist_dl, 'download-index' );
						if ( ! $child_term ) {
							$new_term = wp_insert_term( $artist_dl, 'download-index', array(
								'slug' => mb_strtolower( $artist_dl ),
								'parent' => $parent_term->term_taxonomy_id,
							) );
							$terms[] = $new_term['term_id'];
						} else {
							$terms[] = $child_term->term_taxonomy_id;
						}
					}
				}
			}

			if ( count( $terms ) > 0 ) {
				$terms = array_map( 'intval', $terms );
				$post_terms_ids = wp_set_object_terms( $post_id, $terms, 'download-index', TRUE );
			}
		}
	}
	wp_reset_query();
}

add_action( 'update_downloads_terms', 'acas4u_update_downloads_terms' );
function acas4u_update_downloads_terms() {
	$term_name = '_OTHERS_';
	$count = 5;

	$args = array(
		'post_type' => 'download',
		'posts_per_page' => $count,
		'order' => 'ASC',
		'orderby' => 'title',
		'post_status' => 'publish',
		'meta_key' => '_download_path',
		'meta_value' => $term_name . '/',
		'tax_query' => array(
			array(
				'taxonomy' => 'download-index',
				'field' => 'name',
				'operator' => 'NOT IN',
				'terms' => array( $term_name ),
			),
		),

	);

	$downloads = new WP_Query( $args );
	if ( $downloads->have_posts() ) {
		echo '<ul>';
		while ( $downloads->have_posts() ){
			$downloads->the_post();
			$post_id = get_the_ID();
			$post_title = get_the_title();
			echo '<li>' . $post_title . '</li>';

			$parent_term = get_term_by( 'name', $term_name, 'download-index' );
			$terms[] = $parent_term->term_taxonomy_id;
			if ( count( $terms ) > 0 ) {
				$terms = array_map( 'intval', $terms );
				$post_terms_ids = wp_set_object_terms( $post_id, $terms, 'download-index' );
			}
		}
		echo '</ul>';
		acas4u_write_log_entry( 'Updated ' . $count . ' downloads with term ' . $term_name, 'acas4u_update_downloads_terms.log' );
	} else {
		acas4u_write_log_entry( 'No downloads for term ' . $term_name, 'acas4u_update_downloads_terms.log' );
	}

	wp_reset_query();
}

/* #######################-------------------- start of not active cron jobs ---------------------####################### */

add_action( 'update_users_meta', 'acas4u_update_users_meta' );
function acas4u_update_users_meta() {
	$args = array(
		'number' => '30',
		'role' => 'donator',
		'meta_query' => array(
			array(
				'key' => '_bbp_phpbb_user_donator',
				'compare' => 'NOT EXISTS',
			),
		),
	);
	$blogusers = get_users( $args );
	foreach ( $blogusers as $user ) {
		update_user_meta( $user->ID, '_bbp_phpbb_user_donator', 'TRUE' );
	}
}

/* #######################-------------------- end of not active cron jobs ---------------------####################### */

/* #######################-------------------- start of Echonest deprecated cron jobs ---------------------####################### */

/*
add_action( 'update_genres', 'acas4u_cron_update_downloads_genres' );
function acas4u_cron_update_downloads_genres() {
	global $en_api_key;

	//acas4u_write_log_entry( 'Cron job started' );

	if ( $en_api_key ) {
		$args = array(
			'post_type' => 'download',
			'posts_per_page' => 1,
			'order' => 'ASC',
			'orderby' => 'title',
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'genre',
					'field' => 'term_id',
					'operator' => 'NOT IN',
					'terms' => get_terms( 'genre', array(
						'fields' => 'ids',
					) ),
				),
			),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'relation' => 'OR',
					array(
						'key' => '_download_artist1id',
						'compare' => 'EXISTS',
					),
					array(
						'key' => '_download_artist2id',
						'compare' => 'EXISTS',
					),
				),
				array(
					'key' => '_download_artist_id',
					'compare' => 'EXISTS',
				),
			),
		);

		$downloads = new WP_Query( $args );
		if ( $downloads->have_posts() ) {
			//acas4u_write_log_entry( '------------------------INSERT------------------------' );
			while ( $downloads->have_posts() ){
				$downloads->the_post();
				$post_id = get_the_ID();

				$artist1_enid = get_post_meta( $post_id, '_download_artist1id', TRUE );
				$artist2_enid = get_post_meta( $post_id, '_download_artist2id', TRUE );

				if ( $artist1_enid ) {
					$artist_enid = $artist1_enid;
				} else if ( ! $artist1_enid AND $artist2_enid ) {
					$artist_enid = $artist1_enid;
				}

				if ( $artist_enid ) {
					$artist_post_id = acas4u_get_post_id_by_artist_enid( $artist_enid );

					$url = 'http://developer.echonest.com/api/v4/artist/terms?api_key=' . $en_api_key . '&id=' . $artist_enid . '&format=json&type=style';
					//acas4u_write_log_entry( 'Our EN API URL: ' . $url );
					$response = wp_remote_get( $url );
					$body = wp_remote_retrieve_body( $response );
					$json = json_decode( $body );
					$success = $json->response->status->code;
					if ( $success == 0 ) {
						//acas4u_write_log_entry( 'Success code: ' . $success );
						$terms = $json->response->terms;
						$artist_taxonomies = [ ];
						$term_taxonomy_ids = [ ];
						$t = 0;
						foreach ( $terms as $term ) {
							$term_slug = acas4u_clean_slug( $term->name );
							$is_term_exists = acas4u_term_exists_by_slug( $term_slug, 'genre' );
							//acas4u_write_log_entry( 'Term name: ' . $term->name );
							//acas4u_write_log_entry( 'Term exist response: ' . serialize( $is_term_exists ) );
							if ( $is_term_exists ) {
								//if ( $is_term_exists !== 0 AND $is_term_exists !== NULL ) {
								$artist_taxonomies[] = $is_term_exists['term_taxonomy_id'];
							} else {
								$term_inserted = wp_insert_term( $term->name, 'genre', array( 'slug' => $term_slug ) );
								//acas4u_write_log_entry( 'WP insert term response: ' . serialize( $term_inserted ) );
								$artist_taxonomies[] = $term_inserted['term_taxonomy_id'];
							}
							$t ++;
						}
						//acas4u_write_log_entry( 'Terms processed: ' . $t );
						$artist_taxonomies = array_map( 'intval', $artist_taxonomies );
						$artist_taxonomies = array_unique( $artist_taxonomies );
						$term_taxonomy_ids = wp_set_object_terms( $post_id, $artist_taxonomies, 'genre', TRUE );
						$artist_term_taxonomy_ids = wp_set_object_terms( $artist_post_id, $artist_taxonomies, 'genre', TRUE );
					}
				}

				$message = 'Added ' . count( $term_taxonomy_ids ) . ' terms for post #' . $post_id . ' (artist ID ' . $artist_enid . ', api key ' . $en_api_key . ')' . "\r\n";
				//$message .= serialize( $term_taxonomy_ids ) . "\r\n";
				acas4u_write_log_entry( $message );
			}
		} else {
			acas4u_write_log_entry( 'No posts without terms' );
		}
		wp_reset_query();
	}
}
*/

/* add_action( 'update_bio', 'acas4u_cron_update_artists_biographies' );
function acas4u_cron_update_artists_biographies() {
	global $en_api_key;

	//acas4u_write_log_entry( 'Cron job started', 'acas4u_bio.log' );

	if ( $en_api_key ) {
		$args = array(
			'post_type' => 'artist',
			'posts_per_page' => 1,
			'order' => 'ASC',
			'orderby' => 'title',
			'post_status' => 'publish',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => '_download_artist_enid',
					'compare' => 'EXISTS',
				),
				array(
					'key' => '_download_artist_biography',
					'compare' => 'NOT EXISTS',
				),
			),
		);

		$artists = new WP_Query( $args );
		if ( $artists->have_posts() ) {
			//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_bio.log' );
			while ( $artists->have_posts() ){
				$artists->the_post();
				$post_id = get_the_ID();

				$artist_enid = get_post_meta( $post_id, '_download_artist_enid', TRUE );

				if ( $artist_enid ) {

					$url = 'http://developer.echonest.com/api/v4/artist/biographies?api_key=' . $en_api_key . '&id=' . $artist_enid . '&format=json&results=1&start=0&license=cc-by-sa';
					//acas4u_write_log_entry( 'Our EN API URL: ' . $url, 'acas4u_bio.log' );
					$response = wp_remote_get( $url );
					$body = wp_remote_retrieve_body( $response );
					$json = json_decode( $body );
					$success = $json->response->status->code;
					if ( $success == 0 ) {
						if ( $json->response->total > 0 ) {
							$biographies = $json->response->biographies;
							foreach ( $biographies as $biography ) {
								$artist_bio[] = array(
									'text' => $biography->text,
									'url' => $biography->url,
								);
							}
						}
						if ( ! empty( $artist_bio[0] ) ) {
							$artist_biography = $artist_bio[0];
							update_post_meta( $post_id, '_download_artist_biography', serialize( $artist_biography ) );
						}
					}
				}

				$message = 'Added biography for post #' . $post_id . ' (artist ID ' . $artist_enid . ', api key ' . $en_api_key . ')' . "\r\n";
				acas4u_write_log_entry( $message, 'acas4u_bio.log' );
			}
		} else {
			acas4u_write_log_entry( 'No posts without bio' );
		}
		wp_reset_query();
	}
}
*/

/*
add_action( 'update_artists', 'acas4u_update_artist_from_downloads' );
function acas4u_update_artist_from_downloads() {
	global $en_api_key;

	//acas4u_write_log_entry( 'Cron job started' );

	if ( $en_api_key ) {
		$args = array(
			'post_type' => 'download',
			'posts_per_page' => 1,
			'order' => 'DESC',
			'orderby' => 'ID',
			'post_status' => 'publish',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'relation' => 'OR',
					array(
						'key' => '_download_artist1id',
						'compare' => 'EXISTS',
					),
					array(
						'key' => '_download_artist2id',
						'compare' => 'EXISTS',
					),
				),
				array(
					'key' => '_download_artist_id',
					'compare' => 'NOT EXISTS',
				),
			),
		);

		$downloads = new WP_Query( $args );
		if ( $downloads->have_posts() ) {
			$total_artists = 0;
			while ( $downloads->have_posts() ){
				//acas4u_write_log_entry( '------------------------INSERT------------------------', 'acas4u_artists.log' );
				$downloads->the_post();
				$post_id = get_the_ID();

				$artist1_name = get_post_meta( $post_id, '_download_artist1', TRUE );
				$artist1_enid = get_post_meta( $post_id, '_download_artist1id', TRUE );
				$artist2_name = get_post_meta( $post_id, '_download_artist2', TRUE );
				$artist2_enid = get_post_meta( $post_id, '_download_artist2id', TRUE );

				$has_name = TRUE;
				if ( $artist1_enid ) {
					$artist_enid = $artist1_enid;
					$artist_name = $artist1_name;
				} else if ( ! $artist1_enid AND $artist2_enid ) {
					$artist_enid = $artist2_enid;
					$artist_name = $artist2_name;
				} else {
					$has_name = FALSE;
				}

				acas4u_write_log_entry( 'Download ID: ' . $post_id, 'acas4u_artists.log' );
				acas4u_write_log_entry( 'Artist EN ID: ' . $artist_enid, 'acas4u_artists.log' );
				acas4u_write_log_entry( 'Artist name: ' . $artist_name, 'acas4u_artists.log' );

				if ( $artist_enid AND $has_name ) {
					$artist_post_id = acas4u_get_post_id_by_artist_enid( $artist_enid );
					if ( $artist_post_id > 0 ) {
						update_post_meta( $post_id, '_download_artist_id', $artist_post_id );
					} else {
						// Create post object
						$artist_post = array(
							'post_title' => wp_strip_all_tags( $artist_name ),
							'post_content' => '',
							'post_status' => 'publish',
							'post_author' => 1,
							'post_type' => 'artist',
						);
						$artist_id = wp_insert_post( $artist_post );
						if ( $artist_id ) {
							acas4u_write_log_entry( 'Artist ID: ' . $artist_id, 'acas4u_artists.log' );

							// get spotify id from echonest
							$url = 'http://developer.echonest.com/api/v4/artist/profile?api_key=' . $en_api_key . '&id=' . $artist_enid . '&bucket=id:spotify&format=json';
							$response = wp_remote_get( $url );
							$body = wp_remote_retrieve_body( $response );
							$json = json_decode( $body );
							$success = $json->response->status->code;
							if ( $success == 0 ) {
								$foreign_ids = $json->response->artist->foreign_ids;
								foreach ( $foreign_ids as $foreign_id ) {
									if ( strstr( $foreign_id->foreign_id, 'spotify' ) ) {
										$spotify_arr = explode( ':', $foreign_id->foreign_id );
										if ( ! empty( $spotify_arr[2] ) ) {
											update_post_meta( $artist_id, '_download_artist_spotify_id', $spotify_arr[2] );
											acas4u_write_log_entry( 'Artist Spotify ID: ' . $spotify_arr[2], 'acas4u_artists.log' );
										}
									}
								}
							}

							update_post_meta( $artist_id, '_download_artist_enid', $artist_enid );
							update_post_meta( $post_id, '_download_artist_id', $artist_id );
						}
						$total_artists ++;
						$message = 'Added ' . $total_artists . ' artists' . "\r\n";
						acas4u_write_log_entry( $message, 'acas4u_artists.log' );
					}
				}
			}
		} else {
			acas4u_write_log_entry( 'No not inserted artists', 'acas4u_artists.log' );
		}
		wp_reset_query();
	}
}

*/

/* #######################-------------------- end of Echonest deprecated cron jobs ---------------------####################### */
