<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/* Template for front page */

global $wpdb;

?>

<section class="fp-section fp-search-bar" id="searchfield">
	<form>
		<div class="acas4u-search-input-group">
			<div class="acas4u-sig-select-wrapper">
				<select name="post_type" class="acas4u-sig-select">
					<option value="download">Acapellas</option>
					<option value="all">Website</option>
				</select>
			</div>

			<div class="acas4u-sig-input-wrapper">
				<label for="search-input" class="acas4u-sig-label"><i aria-hidden="true" class="fa fa-search"></i></label>
				<input id="search-input" type="text" name="s" placeholder="search..." class="acas4u-sig-textfield">
			</div>
		</div>

	</form>
</section>
<section class="fp-section fp-info-bar" id="infobar">
	<div class="acas4u-fp-total-wrapper">
		<div class="acas4u-fp-total-acapellas">
			<?php
			$acapellas_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_type='download' AND post_status='publish';" ) );
			echo '<i class="fa fa-music fa-lg" aria-hidden="true"></i>' . number_format( $acapellas_count, 0, ',', ',' ) . ' Acapellas!';
			?>
		</div>
		<div class="acas4u-fp-total-users">
			<?php
			$user_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->users;" ) );
			echo '<i class="fa fa-user fa-lg" aria-hidden="true"></i>' . number_format( $user_count, 0, ',', ',' ) . ' Registered users!';
			?>
		</div>
	</div>
</section>
<section class="fp-section fp-main-text" id="maintext">
	<div class="acas4u-main-text-header">You've found the original #1 resource <br/>for free acapellas, vocal loops,
		samples & dj tools.
	</div>
	<div class="acas4u-main-text-subheader">ACAPELLAS4u are officially supported by superstar DJs, remixers, producers &
		mashup artists worldwide.
	</div>
	<div class="acas4u-main-text-descr">
		Membership is completely free and provides you with access to more than 30,000 acapellas, dj tools and vocals.
	</div>
</section>
<section class="fp-section fp-latest-news" id="latestnews">
	<?php
	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => '4',
		'order' => 'DESC',
		'orderby' => 'date',
		'category__in' => array( 269 ),
	);

	$news = new WP_Query( $args );
	if ( $news->have_posts() ) {

		echo '<h2>Latest News</h2>';
		echo '<div class="acas4u-latest-news-wrapper">';
		while ( $news->have_posts() ){
			$news->the_post();
			$post_id = get_the_ID();
			$post_title = get_the_title();
			$post_permalink = get_the_permalink();
			$day = get_the_date( 'j' );
			$month = get_the_date( 'M.Y' );
			echo '<div class="acas4u-ln-wrapper">';
			echo '<div class="acas4u-ln-date"></div>';
			echo '<div class="acas4u-ln-header"><a href="' . $post_permalink . '">' . $post_title . '</a></div>';
			echo '</div>';
		}
		echo '</div>';
	}
	wp_reset_query();

	?>
</section>
