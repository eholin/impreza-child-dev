<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying archive pages
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
$us_layout->titlebar = ( us_get_option( 'titlebar_content', 'all' ) == 'hide' ) ? 'none' : 'default';
$us_layout->sidebar_pos = us_get_option( 'popular_sidebar', 'right' );
get_header();

// Creating .l-titlebar
us_load_template( 'templates/titlebar', array(
	'title' => 'User profile',
) );

?>
	<!-- MAIN -->
	<div class="l-main">
		<div class="l-main-h i-cf">

			<div class="l-content g-html">

				<section <?php post_class( 'l-section for_popular_downloads' ) ?>>
					<div class="l-section-h i-cf">

						<ul class="tabs">
							<li class="tab-link current" data-tab="tab-download">Download log</li>
							<li class="tab-link" data-tab="tab-upload">Upload log</li>
						</ul>

						<?php

						//var_dump( $template );

						if ( $template['user_nicename'] ) {
							//$user_obj = get_user_by( 'user_nicename', $template['user_nicename'] );
							$args = array(
								'search' => $template['user_nicename'],
								'search_fields' => array( 'user_login', 'user_nicename', 'display_name' ),
							);
							$user_obj = new WP_User_Query( $args );
							/*
							echo '<pre>';
							var_dump( $user_obj->results );
							echo '</pre>';
							*/
							if ( $user_obj->results ) {
								$user_id = $user_obj->results[0]->ID;
							}
						}
						if ( $user_id == '' ) {
							$user_id = get_current_user_id();
						}
						//$user_obj = get_user_by( 'id', $user_id );
						//var_dump( $user_id );

						/* download log */
						$download_log = acas4u_get_user_download_log( $user_id );

						echo '<div id="tab-download" class="tab-content current">' . $download_log . '</div>';

						/* upload log */
						$upload_log = acas4u_get_user_upload_log( $user_id );

						echo '<div id="tab-upload" class="tab-content">' . $upload_log . '</div>';

						?>
					</div>
				</section>
			</div>

			<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
				<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
					<?php dynamic_sidebar( 'default_sidebar' ) ?>
				</aside>
			<?php endif; ?>

		</div>
	</div>

<?php
get_footer();
