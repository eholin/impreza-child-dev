<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * Outputs one single acapella.
 *
 */

global $acas4u_stylesheet_directory_uri, $en_api_key, $spotify_access_token;

?>
<div itemscope itemtype="http://schema.org/AudioObject">
	<section <?php post_class( 'l-section for_artistinfo' ) ?>>
		<div class="l-section-h l-section-nopadding i-cf">

			<div class="acas4u-row">
				<div class="acas4u-column-fluid">
					<div id="acas4u-section1" class="acas4u-content-section artist-info">
						<?php
						$post_id = get_the_ID();
						$post_title = get_the_title();
						$post_permalink = get_the_permalink();

						$user_id = get_current_user_id();

						$artist_id = get_post_meta( $post_id, '_download_artist_id', TRUE );
						// artist and track name
						$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
						$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
						$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );
						$filename = get_post_meta( $post_id, '_download_filename', TRUE );
						$filehash = get_post_meta( $post_id, '_download_filehash', TRUE );
						$download_path = get_post_meta( $post_id, '_download_path', TRUE );
						if ( ! $filename ) {
							$filename = $post_title;
						}
						$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
						$download_size = get_post_meta( $post_id, '_download_size', TRUE );
						$home_url = esc_url( home_url( '/' ) );
						$acas4u_upload_dir = wp_upload_dir();
						$file_url = $acas4u_upload_dir['baseurl'] . '/collection/' . $download_path . $filename;
						$file_path = $acas4u_upload_dir['basedir'] . '/collection/' . $download_path . $filename;

						$download_unmetered = acas4u_is_download_unmetered( $post_id );
						$download_unmetered_str = ( $download_unmetered ) ? 'true' : 'false';

						// get preview from post meta
						$download_preview = get_post_meta( $post_id, '_download_preview', TRUE );
						if ( $download_preview ) {
							$download_preview_url = $acas4u_upload_dir['baseurl'] . '/waveforms/' . $download_preview;
							$download_preview_img = $acas4u_upload_dir['baseurl'] . '/waveforms/png/' . trim( $download_preview, 'dat' ) . 'png';
						} else {
							// or generate preview from mp3 file
							/*
							if ( file_exists( $file_path ) ) {
								$download_preview = $post_id . '_preview.dat';
								$preview_path = $acas4u_upload_dir['basedir'] . '/waveforms/' . $download_preview;
								$download_preview_url = $acas4u_upload_dir['baseurl'] . '/waveforms/' . $download_preview;
								$output = shell_exec( 'audiowaveform -i "' . $file_path . '" -o "' . $preview_path . '" -z 256 -b 8' );
								update_post_meta( $post_id, '_download_preview', $download_preview );
							}
							*/
						}

						$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

						// EchoNest API IDs
						$artist1_enid = get_post_meta( $post_id, '_download_artist1id', TRUE );
						$artist2_enid = get_post_meta( $post_id, '_download_artist2id', TRUE );
						$track_enid = get_post_meta( $post_id, '_download_echonestid', TRUE );

						if ( $artist1_enid ) {
							$artist_enid = $artist1_enid;
						} else if ( ! $artist1_enid AND $artist2_enid ) {
							$artist_enid = $artist1_enid;
						}

						/*
						if ( ! $artist_id ) {
							$artist_id = acas4u_get_artist_id_by_enid( $artist_enid );
							if ( $artist_id ) {
								update_post_meta( $post_id, '_download_artist_id', $artist_id );
							}
						}
						*/

						// artist biography
						//$artist_biography = unserialize( get_post_meta( $post_id, '_download_artist_biography', TRUE ) );

						?>
						<h3><?php echo $title; ?></h3>

						<div class="acas4u-artist-info-wrapper">

							<?php
							$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
							if ( $artist_thumbnail_id ) {
								$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
								$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
								?>
								<div class="acas4u-artist-image">
									<img src="<?php echo $artist_thumbnail_url; ?>" alt="<?php echo $title; ?>" class="img-responsive">
								</div>
								<?php
							}
							?>

							<div class="acas4u-artist-styles-wrapper">
								<div class="acas4u-artist-styles">
									<?php
									$before = '<p><strong>Artist genres:</strong> ';
									$sep = ', ';
									$after = '</p>';
									echo get_the_term_list( $post_id, 'genre', $before, $sep, $after );
									?>
								</div>
								<div class="acas4u-download-author">
									Uploaded by:
									<?php
									$author = get_the_author();
									echo '<a href="' . home_url( '/users/' . $author . '/' ) . '">' . $author . '</a>';

									?>
								</div>
							</div>

						</div>
						<?php
						$artist_spotify_id = get_post_meta( $artist_id, '_download_artist_spotify_id', TRUE );
						if ( $artist_spotify_id != '' ) {
							?>
							<div class="acas4u-spotify-followers">
								<iframe src="https://embed.spotify.com/follow/1/?uri=spotify:artist:<?php echo $artist_spotify_id; ?>&size=basic&theme=light" width="200" height="25" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
							</div>
						<?php } ?>
						<div class="acas4u-artist-bio">
							<?php
							if ( ! empty( $artist_biography['text'] ) ) {
								echo '<div class="acas4u-artist-bio-text">' . acas4u_limit_words( $artist_biography['text'], 55 ) . '</div>';
								echo '<a class="acas4u-artist-bio-url" href="' . $artist_biography['url'] . '" target="_blank">View all</a>';
							}
							?>
						</div>

						<div class="acas4u-download-sharing"><?php echo do_shortcode( '[ultimatesocial networks="facebook,twitter,google" count="true" align="left" skin="simple"]' ); ?></div>
					</div>
				</div>
				<?php
				$user_id = get_current_user_id();
				$user_donator = acas4u_is_user_donator( $user_id );
				$user_donator_str = ( $user_donator ) ? 'true' : 'false';
				$non_donator_title = ( $user_donator ) ? '' : 'title=""';
				?>
				<div class="acas4u-column-fluid">
					<div id="acas4u-section2" class="acas4u-content-section track-preview" <?php
					if ( is_user_logged_in() ) {
						echo 'data-waveform="' . $download_preview_url . '"';
					}
					?>>
						<span class="acas4u-hidden" itemprop="name"><?php echo $filename; ?></span>
						<?php if ( $file_url AND file_exists( $file_path ) ) : ?>
							<?php if ( $user_donator === TRUE ) : ?>
								<div id="peaks-container"></div>
								<div class="peaks-control">
									<div class="peaks-audio">
										<audio id="peaks-audio" controls=controls>
											<source src="<?php echo $file_url; ?>" type="audio/mpeg">
											Your browser does not support the audio element.
										</audio>
									</div>
									<button id="zoomIn" class="acas4u-track-preview-control">Zoom In</button>
									<button id="zoomOut" class="acas4u-track-preview-control">Zoom Out</button>
									<button id="segment" class="acas4u-track-preview-control">Create Segment</button>
								</div>
							<?php else :
								if ( acas4u_url_exists( $download_preview_img ) ) {
									echo '<img src="' . $download_preview_img . '" alt="Interactive track preview and downloads without any delay are available for our dear Donators">';
									echo '<div class="acas4u-highlignt">Interactive track preview and downloads without any delay are available for our dear Donators!</div>';
								}
							endif; ?>
						<?php endif; ?>
					</div>
					<div id="acas4u-section3" class="acas4u-content-section track-rating-download">
						<div class="acas4u-column-fluid">
							<?php
							$user_logged = is_user_logged_in();
							$disable_rating_action = ( $user_logged === TRUE ) ? FALSE : TRUE;
							echo acas4u_user_ratings( $disable_rating_action );
							?>
						</div>
						<?php if ( $user_logged ) : ?>
							<div class="acas4u-column-fluid download-button-container">
								<?php $ajax_nonce = wp_create_nonce( 'acas4u_do_download_link' );
								if ( $user_donator ) {
									$max_downloads = acas4u_get_user_max_downloads( $user_id );
									$bonus_downloads = acas4u_get_user_bonus_downloads( $user_id );

									$downloads_remaining = $max_downloads . ' (' . $bonus_downloads . ' bonus) downloads remaining';

									?>
									<a class="download-button-wrapper button download-button-enabled" href="javascript:void(0);" data-postid="<?php echo $post_id; ?>" data-nonce="<?php echo $ajax_nonce; ?>" data-userid="<?php echo $user_id; ?>" data-donator="<?php echo $user_donator_str; ?>" data-unmetered="<?php echo $download_unmetered_str; ?>">
										<div class="download-button-icon"><i class="fa fa-download fa-2x"></i></div>
										<div class="download-button-text">
											<div class="download-button-label">Download</div>
											<div class="download-button-message"><?php echo $downloads_remaining; ?></div>
										</div>
									</a>
								<?php } else { ?>
									<a class="download-button-wrapper button" href="javascript:void(0);" data-postid="<?php echo $post_id; ?>" data-nonce="<?php echo $ajax_nonce; ?>" data-userid="<?php echo $user_id; ?>" data-donator="<?php echo $user_donator_str; ?>" data-unmetered="<?php echo $download_unmetered_str; ?>">
										<div class="download-button-icon"><i class="fa fa-download fa-2x"></i></div>
										<div class="download-button-text">
											<div class="download-button-label">Download</div>
											<div class="download-button-message">Wait 60 seconds to download</div>
										</div>
									</a>
									<div class="acas4u-highlignt">You could be downloading this straight away if you support the site</div>
								<?php } ?>

							</div>
						<?php else : ?>
							<div class="acas4u-column-fluid download-button-container">
								<a class="download-button-wrapper download-button-enabled button" title="You could be downloading this straight away if you support the site" href="<?php echo home_url( '/register/' ); ?>">
									<div class="download-button-icon"><i class="fa fa-download fa-2x"></i></div>
									<div class="download-button-text">
										<div class="download-button-label">Register to Download</div>
										<div class="download-button-message"></div>
									</div>
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section <?php post_class( 'l-section for_infobar' ) ?>>
		<div class="l-section-h l-section-nopadding i-cf">
			<div class="acas4u-row">
				<div class="acas4u-column-fluid w100p">
					<div id="acas4u-section4" class="acas4u-content-section info-bar">
						<?php
						$download_count = get_post_meta( $post_id, '_download_count', TRUE );
						$download_votes = get_post_meta( $post_id, '_download_votes', TRUE );
						// $download_bpm = get_post_meta( $post_id, '_download_bpm', TRUE ); // use 'tempo' instead of 'bpm'
						$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE ); // use 'tempo' instead of 'bpm'
						$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
						$date_uploaded = get_the_date();

						?>
						<div class="acas4u-info-bar-item ib-downloads">
							<div class="ib-item-wrapper">
								<i class="fa fa-download"></i>
								<strong><?php echo number_format( $download_count, 0, ',', ',' ); ?></strong> Total
								Listeners
							</div>
						</div>
						<div class="acas4u-info-bar-item ib-votes">
							<div class="ib-item-wrapper">
								<i class="fa fa-star"></i>
								<strong><?php echo $download_votes; ?></strong> votes
							</div>
						</div>
						<div class="acas4u-info-bar-item ib-comments">
							<div class="ib-item-wrapper">
								<i class="fa fa-comments-o"></i> <?php comments_number( 'No comments', 'One comment', '% comments' ); ?>
							</div>
						</div>
						<div class="acas4u-info-bar-item ib-bpm">
							<div class="ib-item-wrapper">
								<i class="fa fa-music"></i>
								<strong><?php echo $download_tempo; ?></strong>
								BPM <strong><?php echo $download_bitrate; ?></strong> kbps
							</div>
						</div>
						<div class="acas4u-info-bar-item ib-last ib-date">
							<div class="ib-item-wrapper">
								<i class="fa fa-calendar"></i>
								<strong><?php echo $date_uploaded; ?></strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	$download_duration_formatted = acas4u_time_to_iso8601_duration( $download_duration );
	$download_size_formatted = formatBytes( $download_size );
	?>
	<meta itemprop="encodingFormat" content="mp3"/>
	<meta itemprop="duration" content="<?php echo $download_duration_formatted; ?>"/>
	<meta itemprop="bitrate" content="<?php echo $download_bitrate; ?>"/>
	<meta itemprop="contentSize" content="<?php echo $download_size_formatted; ?>"/>
	<meta itemprop="uploadDate" content="<?php echo $date_uploaded; ?>"/>
</div>

<section <?php post_class( 'l-section for_details' ) ?>>
	<div class="l-section-h l-section-nopadding i-cf">

		<div class="acas4u-row">
			<div class="acas4u-column-fluid">
				<div id="acas4u-section5" class="acas4u-content-section acapella-details">
					<h3>Details</h3>
					<?php
					if ( $user_donator ) {
						echo acas4u_show_track_details( $post_id, $track_enid );
					} else {
						the_ad(498144);
					}
					?>
				</div>
			</div>
			<div class="acas4u-column-fluid">
				<div id="acas4u-section6" class="acas4u-content-section acapella-popular">
					<h3><a href="<?php echo esc_url( home_url( '/popular-downloads/' ) ); ?>">Popular Acapellas</a></h3>
					<?php
					$top10_popular = get_option( 'acas4u_top10_popular_acapellas' );
					if ( $top10_popular ) {
						echo stripslashes( $top10_popular );
					} else {
						echo acas4u_get_popular_downloads_from_logs( 10, 'month' );
					}

					?>

				</div>
			</div>
			<div class="acas4u-column-fluid">
				<div id="acas4u-section7" class="acas4u-content-section acapella-latest">
					<h3>Latest Acapellas</h3>
					<?php echo get_latest_downloads( 10 ); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section <?php post_class( 'l-section for_similar' ) ?>>
	<div class="l-section-h l-section-nopadding i-cf">
		<?php the_ad(498137); ?>

		<div class="acas4u-row">
			<div class="acas4u-column-fluid">
				<div id="acas4u-section9" class="acas4u-content-section acapella-best">
					<div class="similar-header">
						<h3>Download some of the best Acapella Pack</h3>
					</div>
					<?php echo acas4u_get_best_acapellas_pack( 6, 'DESC' ); ?>
				</div>
			</div>
			<div class="acas4u-column-fluid">
				<div id="acas4u-section10" class="acas4u-content-section acapella-similar-genre">
					<div class="similar-header">
						<h3>Similar BPM</h3>
					</div>
					<?php
					echo get_similar_tracks_by_bpm( $download_tempo, 6 );
					?>
				</div>
			</div>
			<div class="acas4u-column-fluid">
				<div id="acas4u-section11" class="acas4u-content-section acapella-similar-artists">
					<div class="similar-header">
						<h3>Similar Artists</h3>
					</div>
					<?php echo get_similar_artist_by_id_en_api( $post_id, $artist1_enid, 6 ); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if ( comments_open() OR get_comments_number() != '0' ): ?>
	<section class="l-section for_comments">
		<div class="l-section-h i-cf">
			<?php wp_enqueue_script( 'comment-reply' ) ?>
			<?php comments_template() ?>
		</div>
	</section>
<?php endif; ?>
