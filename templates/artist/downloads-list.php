<section <?php post_class( 'l-section for_downloads' ) ?>>
	<div class="l-section-h i-cf">
		<div class="w-blog">
			<a id="tracks"></a>

			<h2>Artist's acapellas:</h2>

			<?php
			$artist_id = get_the_ID();
			$output = acas4u_show_artists_acapellas( $artist_id );
			echo $output;

			?>
		</div>
	</div>
</section>
