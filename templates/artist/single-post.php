<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Outputs one single post.
 *
 * (!) Should be called after the current $wp_query is already defined
 *
 * @var $metas array Meta data that should be shown: array('date', 'author', 'categories', 'comments')
 * @var $show_tags boolean Should we show tags?
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post'
 * @action After the template: 'us_after_template:templates/blog/single-post'
 * @filter Template variables: 'us_template_vars:templates/blog/single-post'
 */

$us_layout = US_Layout::instance();

// Filling and filtering parameters
$default_metas = array( 'date', 'author', 'categories', 'comments' );
$metas = ( isset( $metas ) AND is_array( $metas ) ) ? array_intersect( $metas, $default_metas ) : $default_metas;
if ( ! isset( $show_tags ) ) {
	$show_tags = TRUE;
}

$post_format = get_post_format() ? get_post_format() : 'standard';

// Note: it should be filtered by 'the_content' before processing to output
$the_content = get_the_content();

$preview_type = rwmb_meta( 'us_post_preview_layout' );
if ( $preview_type == '' ) {
	$preview_type = us_get_option( 'post_preview_layout', 'basic' );
}

$preview_html = '';
$preview_bg = '';
if ( $preview_type != 'none' AND ! post_password_required() ) {
	$post_thumbnail_id = get_post_thumbnail_id();
	if ( $preview_type == 'basic' ) {
		if ( $post_format == 'video' ) {
			$preview_html = us_get_post_preview( $the_content, TRUE );
			if ( $preview_html == '' AND $post_thumbnail_id ) {
				$preview_html = wp_get_attachment_image( $post_thumbnail_id, 'large' );
			}
		} else {
			if ( $post_thumbnail_id ) {
				$preview_html = wp_get_attachment_image( $post_thumbnail_id, 'large' );
			} else {
				// Retreiving preview HTML from the post content
				$preview_html = us_get_post_preview( $the_content, TRUE );
			}
		}
	} elseif ( $preview_type == 'modern' ) {
		if ( $post_thumbnail_id ) {
			$image = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
			$preview_bg = $image[0];
		} elseif ( $post_format == 'image' ) {
			// Retreiving image from post content to use it as preview background
			$preview_bg_html = us_get_post_preview( $the_content, TRUE );
			if ( preg_match( '~src=\"([^\"]+)\"~u', $preview_bg_html, $matches ) ) {
				$preview_bg = $matches[1];
			}
		}
	}
}

if ( ! post_password_required() ) {
	$the_content = apply_filters( 'the_content', $the_content );
}

// The post itself may be paginated via <!--nextpage--> tags
$pagination = us_wp_link_pages( array(
	'before' => '<div class="g-pagination"><nav class="navigation pagination" role="navigation">',
	'after' => '</nav></div>',
	'next_or_number' => 'next_and_number',
	'nextpagelink' => '>',
	'previouspagelink' => '<',
	'link_before' => '<span>',
	'link_after' => '</span>',
	'echo' => 0,
) );

// If content has no sections, we'll create them manually
$has_own_sections = ( strpos( $the_content, ' class="l-section' ) !== FALSE );
if ( ! $has_own_sections ) {
	$the_content = '<section class="l-section"><div class="l-section-h i-cf">' . $the_content . $pagination . '</div></section>';
} elseif ( ! empty( $pagination ) ) {
	$the_content .= '<section class="l-section"><div class="l-section-h i-cf">' . $pagination . '</div></section>';
}

// Meta => certain html in a proper order
$meta_html = array_fill_keys( $metas, '' );

// Preparing post metas separately because we might want to order them inside the .w-blog-post-meta in future
$meta_html['date'] = '<time class="w-blog-post-meta-date date updated';
if ( ! in_array( 'date', $metas ) ) {
	// Hiding from users but not from search engines
	$meta_html['date'] .= ' hidden';
}
$meta_html['date'] .= '">' . get_the_date() . '</time>';

$meta_html['author'] = '<span class="w-blog-post-meta-author vcard author';
if ( ! in_array( 'author', $metas ) ) {
	$meta_html['author'] .= ' hidden';
}
$meta_html['author'] .= '">';
if ( get_the_author_meta( 'url' ) ) {
	$meta_html['author'] .= '<a href="' . esc_url( get_the_author_meta( 'url' ) ) . '" class="fn">' . get_the_author() . '</a>';
} else {
	$meta_html['author'] .= '<span class="fn">' . get_the_author() . '</span>';
}
$meta_html['author'] .= '</span>';

if ( in_array( 'categories', $metas ) ) {
	$meta_html['categories'] = get_the_category_list( ', ' );
	if ( ! empty( $meta_html['categories'] ) ) {
		$meta_html['categories'] = '<span class="w-blog-post-meta-category">' . $meta_html['categories'] . '</span>';
	}
}
/*
$comments_number = get_comments_number();
if ( in_array( 'comments', $metas ) AND ! ( $comments_number == 0 AND ! comments_open() ) ) {
	$meta_html['comments'] .= '<span class="w-blog-post-meta-comments">';
	// TODO Replace with get_comments_popup_link() when https://core.trac.wordpress.org/ticket/17763 is resolved
	ob_start();
	$comments_label = sprintf( _n( '%s comment', '%s comments', $comments_number, 'us' ), $comments_number );
	comments_popup_link( __( 'No comments', 'us' ), $comments_label, $comments_label );
	$meta_html['comments'] .= ob_get_clean();
	$meta_html['comments'] .= '</span>';
}
*/

if ( us_get_option( 'post_nav' ) ) {
	$prevnext = us_get_post_prevnext();
}

if ( $show_tags ) {
	$the_tags = get_the_tag_list( '', ', ', '' );
}

$meta_html = apply_filters( 'us_single_post_meta_html', $meta_html, get_the_ID() );

$meta_html = '';

?>
<section <?php post_class( 'l-section for_blogpost preview_' . $preview_type ) ?>>
	<div class="l-section-h i-cf">
		<div class="w-blog">
			<div class="w-blog-post-body">
				<h1 class="w-blog-post-title entry-title"><?php the_title() ?></h1>

				<?php
				if ( $meta_html ) :
					?>
					<div class="w-blog-post-meta<?php echo empty( $metas ) ? ' hidden' : '' ?>">
						<?php echo implode( '', $meta_html ) ?>
					</div>
					<?php
				endif;
				?>

				<div class="w-blog-post-body-row">
					<div class="w-blog-post-body-col first">
						<?php if ( ! empty( $preview_bg ) ): ?>
							<div class="w-blog-post-preview" style="background-image: url(<?php echo $preview_bg ?>)"></div>
						<?php elseif ( ! empty( $preview_html ) OR $preview_type == 'modern' ): ?>
							<div class="w-blog-post-preview">
								<?php echo $preview_html ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="w-blog-post-body-col second">
						<a id="tracks"></a>

						<h2>Artist's acapellas:</h2>

						<?php
						$artist_id = get_the_ID();
						$output = acas4u_show_artists_acapellas( $artist_id );
						echo $output;

						?>

					</div>
				</div>

				<?php
				$post_id = get_the_ID();
				$artist_spotify_id = get_post_meta( $post_id, '_download_artist_spotify_id', TRUE );
				if ( $artist_spotify_id != '' ) {
					?>
					<div class="acas4u-spotify-followers">
						<iframe src="https://embed.spotify.com/follow/1/?uri=spotify:artist:<?php echo $artist_spotify_id; ?>&size=detail&theme=light" width="300" height="56" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<?php echo $the_content ?>

<section class="l-section for_related_artists">
	<div class="l-section-h i-cf">

		<div class="w-blog-post-body-row">
			<?php
			// Artist Top tracks
			$artist_top_tracks_arr = get_post_meta( $post_id, '_artist_spotify_top_tracks', TRUE );
			$artist_top_tracks = maybe_unserialize( $artist_top_tracks_arr );
			if ( ! empty( $artist_top_tracks[0] ) ) {
				$class = 'first';
				?>

				<div class="w-blog-post-body-col <?php echo $class; ?>">
					<h2>Artist top tracks:</h2>

					<div class="acas4u-related-artists">
						<ul>
							<?php
							foreach ( $artist_top_tracks AS $top_track ) {
								/*
								echo '<pre>';
								var_dump( $artist_top_tracks );
								echo '</pre>';
								*/
								echo '<li><a target="_blank" rel="nofollow" href="http://open.spotify.com/track/' . $top_track['spotify_track_id'] . '">' . $top_track['spotify_track_name'] . '</a></li>';
							}
							?>
						</ul>
					</div>
				</div>
				<?php
			}

			// Related Artists

			$related_artists_arr = get_post_meta( $post_id, '_artist_related_artists_ids', TRUE );
			$related_artists = maybe_unserialize( $related_artists_arr );
			if ( ! empty( $related_artists[0] ) ) {
				global $wpdb;
				$related_artists_ids = [ ];
				foreach ( $related_artists as $related_artist_spotify_id ) {
					$artist_id = $wpdb->get_var( "select post_id from $wpdb->postmeta where meta_key='_download_artist_spotify_id' AND meta_value = '" . $related_artist_spotify_id . "'" );
					if ( $artist_id != '' ) {
						$related_artists_ids[] = $artist_id;
					}
				}

				if ( ! empty( $related_artists_ids[0] ) ) {
					$class = ( $class == 'first' ) ? 'second' : 'first';
					?>
					<div class="w-blog-post-body-col <?php echo $class; ?>">
						<h2>Related artists:</h2>

						<div class="acas4u-related-artists">
							<?php
							foreach ( $related_artists_ids as $artist_id ) {
								$artist = get_post( $artist_id );
								$artist_title = $artist->post_title;
								$artist_permalink = get_permalink( $artist_id );
								$artist_thumbnail_id = get_post_thumbnail_id( $artist_id );
								if ( $artist_thumbnail_id ) {
									$artist_thumbnail_image = wp_get_attachment_image_src( $artist_thumbnail_id, 'full' );
									$artist_thumbnail_url = acas4u_get_resized_thumbnail_image_url( $artist_thumbnail_image, 130, 130 );
									echo '<div class="acas4u-single-related-artists">';

									$artist_thumbnail_img = '<a class="acas4u-single-related-artists-image" href="' . $artist_permalink . '"><img src="' . $artist_thumbnail_url . '" alt="' . $artist_title . '>"></a>';
									echo $artist_thumbnail_img;
									echo '<a class="acas4u-single-related-artists-link" href="' . $artist_permalink . '">' . $artist_title . '</a>';
									echo '</div>';
								}
							}
							?>
						</div>
					</div>
					<?php
				}
			}

			?>
		</div>
	</div>
</section>

<?php if ( $show_tags AND ! empty( $the_tags ) ): ?>
	<section class="l-section for_tags">
		<div class="l-section-h i-cf">
			<div class="g-tags">
				<span class="g-tags-title"><?php _e( 'Tags', 'us' ) ?>:</span>
				<?php echo $the_tags ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( us_get_option( 'post_sharing' ) ) : ?>
	<section class="l-section for_sharing">
		<div class="l-section-h i-cf">
			<?php
			$sharing_providers = (array) us_get_option( 'post_sharing_providers' );
			$us_sharing_atts = array(
				'type' => us_get_option( 'post_sharing_type', 'simple' ),
			);
			foreach ( array( 'facebook', 'twitter', 'linkedin', 'gplus', 'pinterest' ) as $provider ) {
				$us_sharing_atts[ $provider ] = in_array( $provider, $sharing_providers );
			}
			us_load_template( 'shortcodes/us_sharing', array( 'atts' => $us_sharing_atts ) );
			?>
		</div>
	</section>
<?php endif; ?>



<?php if ( us_get_option( 'post_author_box' ) ): ?>
	<?php us_load_template( 'templates/blog/single-post-author' ) ?>
<?php endif; ?>

<?php if ( us_get_option( 'post_nav' ) AND ! empty( $prevnext ) ): ?>
	<section class="l-section for_blognav">
		<div class="l-section-h i-cf">
			<div class="w-blognav">
				<?php foreach ( $prevnext as $key => $item ): ?>
					<a class="w-blognav-<?php echo $key ?>" href="<?php echo $item['link'] ?>">
						<span class="w-blognav-meta"><?php echo $item['meta'] ?></span>
						<span class="w-blognav-title"><?php echo $item['title'] ?></span>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( us_get_option( 'post_related', TRUE ) ): ?>
	<?php us_load_template( 'templates/blog/single-post-related' ) ?>
<?php endif; ?>
