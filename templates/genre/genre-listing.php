<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Output a single blog listing. Universal template that is used by all the possible blog posts listings.
 */

global $acas4u_stylesheet_directory_uri, $wp_query;
$default_posts_per_page = get_option( 'posts_per_page' );

if ( ! empty( $el_class ) ) {
	$classes .= ' ' . $el_class;
}
?>
<div class="w-blog<?php echo $classes; ?>">
	<div class="w-blog-list">
		<?php

		$genre = get_query_var( 'genre' );

		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		if ( $genre ) {

			$args = array(
				'post_type' => 'download',
				'order' => 'DESC',
				'orderby' => 'date',
				'post_status' => 'publish',
				'posts_per_page' => $default_posts_per_page,
				'paged' => $paged,
				'tax_query' => array(
					array(
						'taxonomy' => 'genre',
						'field' => 'slug',
						'terms' => $genre,
					),
				),
			);

			$total_results = acas4u_get_posts_count( $args, 'custom' );
			$ajax_nonce = wp_create_nonce( 'acas4u_do_genre_scroll_nonce' );

			?>
			<div class="acas4u-downloads-by-genre-wrapper" data-genre="<?php echo $genre; ?>" data-page="1" data-total="<?php echo $total_results; ?>" data-nonce="<?php echo $ajax_nonce; ?>">

				<h3>Found <?php echo $total_results; ?> downloads</h3>

				<div class="acas4u-downloads-by-genre-results">
					<?php echo acas4u_show_downloads_by_genre( $args ); ?>
				</div>


				<div class="g-pagination">
					<?php
					if ( $total_results > $default_posts_per_page ) {
						?>
						<div class="g-loadmore">
							<div class="g-loadmore-btn acas4u-genre-load-more">
								<span><?php _e( 'Load More', 'us' ) ?></span>
							</div>
							<div class="g-preloader type_1"></div>
						</div>
					<?php } ?>

				</div>
			</div> <!-- .acas4u-downloads-by-genre-wrapper -->
			<?php
		} else {
			_e( 'No posts were found.', 'us' );

			return;
		}

		?>

	</div>
	<?php

	?>
</div>
