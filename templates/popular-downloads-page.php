<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying archive pages
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
$us_layout->titlebar = ( us_get_option( 'titlebar_content', 'all' ) == 'hide' ) ? 'none' : 'default';
$us_layout->sidebar_pos = us_get_option( 'popular_sidebar', 'none' );
get_header();

// Creating .l-titlebar
us_load_template( 'templates/titlebar', array(
	'title' => 'Popular downloads',
) );

?>
	<!-- MAIN -->
	<div class="l-main">
		<div class="l-main-h i-cf">

			<div class="l-content g-html">

				<section <?php post_class( 'l-section for_popular_downloads' ) ?>>
					<div class="l-section-h i-cf">
						<?php

						if ( ! $_POST['date_from'] ) {
							$date_from = date( 'Y-m-d', strtotime( '-30 days' ) ) . ' 00:00:00';
						} else {
							list( $day, $month, $year ) = sscanf( $_POST['date_from'], '%D/%D/%D' );
							$date_from = $year . '-' . $month . '-' . $day . ' 00:00:00';
						}
						if ( ! $_POST['date_to'] ) {
							$date_to = date( 'Y-m-d' ) . ' 23:59:59';
						} else {
							list( $day, $month, $year ) = sscanf( $_POST['date_to'], '%D/%D/%D' );
							$date_to = $year . '-' . $month . '-' . $day . ' 00:00:00';
						}
						if ( ! $_POST['count'] ) {
							$count = 25;
						} else {
							$count = $_POST['count'];
						}
						echo acas4u_get_popular_downloads_table_from_logs( $date_from, $date_to, $count );

						?>
					</div>
				</section>
			</div>

			<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
				<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
					<?php dynamic_sidebar( 'default_sidebar' ) ?>
				</aside>
			<?php endif; ?>

		</div>
	</div>

<?php
get_footer();
