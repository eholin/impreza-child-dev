<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

$args = array(
	'post_type' => 'download',
	'post_status' => 'publish',
	'posts_per_page' => '300',
	'order' => 'ASC',
	'orderby' => 'title',
);

/*
$args = array(
	'post_type' => 'download',
	'posts_per_page' => 1,
	'order' => 'ASC',
	'orderby' => 'title',
	'post_status' => 'publish',
	'meta_query' => array(
		'relation' => 'AND',
		array(
			'key' => '_download_artist1id',
			'compare' => 'EXISTS',
		),
		array(
			'key' => '_download_artist1_spid',
			'compare' => 'NOT EXISTS',
		),
	),
);

echo '<pre>';
var_dump( $args );
echo '</pre>';
*/

$downloads = new WP_Query( $args );
if ( $downloads->have_posts() ) {

	echo '<p>Found ' . $downloads->found_posts . ' posts</p>';

	echo '<ol>';
	while ( $downloads->have_posts() ){
		$downloads->the_post();
		$post_id = get_the_ID();
		echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
		/*
		$download_artist1id = get_post_meta( $post_id, '_download_artist1id', TRUE );
		$url = 'http://developer.echonest.com/api/v4/song/profile?api_key=AK3XQJRGSJ0HSXIIY&id=' . $download_artist1id . '&format=json&bucket=id:Spotify';
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		$success = $json->response->status->code;
		if ( $success == 0 ) {
			if ( $json->response->total > 0 ) {
				$biographies = $json->response->biographies;
				foreach ( $biographies as $biography ) {
					$artist_bio[] = array(
						'text' => $biography->text,
						'url' => $biography->url,
					);
				}
			}
			if ( ! empty( $artist_bio[0] ) ) {
				$artist_biography = $artist_bio[0];
				update_post_meta( $post_id, '_download_artist_biography', serialize( $artist_biography ) );
			}
		}
		*/
	}
	echo '</ol>';
} else {
}
wp_reset_query();
