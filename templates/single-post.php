<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Outputs one single post.
 *
 * (!) Should be called after the current $wp_query is already defined
 *
 * @var $metas array Meta data that should be shown: array('date', 'author', 'categories', 'comments')
 * @var $show_tags boolean Should we show tags?
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post'
 * @action After the template: 'us_after_template:templates/blog/single-post'
 * @filter Template variables: 'us_template_vars:templates/blog/single-post'
 */

$us_layout = US_Layout::instance();

// Filling and filtering parameters
$default_metas = array( 'date', 'author', 'categories', 'comments' );
$metas = ( isset( $metas ) AND is_array( $metas ) ) ? array_intersect( $metas, $default_metas ) : $default_metas;
if ( ! isset( $show_tags ) ) {
	$show_tags = TRUE;
}

$post_format = get_post_format() ? get_post_format() : 'standard';

// Note: it should be filtered by 'the_content' before processing to output
$the_content = get_the_content();

$preview_type = rwmb_meta( 'us_post_preview_layout' );
if ( $preview_type == '' ) {
	$preview_type = us_get_option( 'post_preview_layout', 'basic' );
}

$preview_html = '';
$preview_bg = '';
if ( $preview_type != 'none' AND ! post_password_required() ) {
	$post_thumbnail_id = get_post_thumbnail_id();
	if ( $preview_type == 'basic' ) {
		if ( $post_format == 'video' ) {
			$preview_html = us_get_post_preview( $the_content, TRUE );
			if ( $preview_html == '' AND $post_thumbnail_id ) {
				$preview_html = wp_get_attachment_image( $post_thumbnail_id, 'large' );
			}
		} else {
			if ( $post_thumbnail_id ) {
				$preview_html = wp_get_attachment_image( $post_thumbnail_id, 'large' );
			} else {
				// Retreiving preview HTML from the post content
				$preview_html = us_get_post_preview( $the_content, TRUE );
			}
		}
	} elseif ( $preview_type == 'modern' ) {
		if ( $post_thumbnail_id ) {
			$image = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
			$preview_bg = $image[0];
		} elseif ( $post_format == 'image' ) {
			// Retreiving image from post content to use it as preview background
			$preview_bg_html = us_get_post_preview( $the_content, TRUE );
			if ( preg_match( '~src=\"([^\"]+)\"~u', $preview_bg_html, $matches ) ) {
				$preview_bg = $matches[1];
			}
		}
	}
}

if ( ! post_password_required() ) {
	$the_content = apply_filters( 'the_content', $the_content );
}

// The post itself may be paginated via <!--nextpage--> tags
$pagination = us_wp_link_pages( array(
	'before' => '<div class="g-pagination"><nav class="navigation pagination" role="navigation">',
	'after' => '</nav></div>',
	'next_or_number' => 'next_and_number',
	'nextpagelink' => '>',
	'previouspagelink' => '<',
	'link_before' => '<span>',
	'link_after' => '</span>',
	'echo' => 0,
) );

// If content has no sections, we'll create them manually
$has_own_sections = ( strpos( $the_content, ' class="l-section' ) !== FALSE );
if ( ! $has_own_sections ) {
	$the_content = '<section class="l-section"><div class="l-section-h i-cf">' . $the_content . $pagination . '</div></section>';
} elseif ( ! empty( $pagination ) ) {
	$the_content .= '<section class="l-section"><div class="l-section-h i-cf">' . $pagination . '</div></section>';
}

// Meta => certain html in a proper order
$meta_html = array_fill_keys( $metas, '' );

// Preparing post metas separately because we might want to order them inside the .w-blog-post-meta in future
$meta_html['date'] = '<time class="w-blog-post-meta-date date updated';
if ( ! in_array( 'date', $metas ) ) {
	// Hiding from users but not from search engines
	$meta_html['date'] .= ' hidden';
}
$meta_html['date'] .= '">' . get_the_date() . '</time>';

$meta_html['author'] = '<span class="w-blog-post-meta-author vcard author';
if ( ! in_array( 'author', $metas ) ) {
	$meta_html['author'] .= ' hidden';
}
$meta_html['author'] .= '">';
if ( get_the_author_meta( 'url' ) ) {
	$meta_html['author'] .= '<a href="' . esc_url( get_the_author_meta( 'url' ) ) . '" class="fn">' . get_the_author() . '</a>';
} else {
	$meta_html['author'] .= '<span class="fn">' . get_the_author() . '</span>';
}
$meta_html['author'] .= '</span>';

if ( in_array( 'categories', $metas ) ) {
	$meta_html['categories'] = get_the_category_list( ', ' );
	if ( ! empty( $meta_html['categories'] ) ) {
		$meta_html['categories'] = '<span class="w-blog-post-meta-category">' . $meta_html['categories'] . '</span>';
	}
}

$comments_number = get_comments_number();
if ( in_array( 'comments', $metas ) AND ! ( $comments_number == 0 AND ! comments_open() ) ) {
	$meta_html['comments'] .= '<span class="w-blog-post-meta-comments">';
	// TODO Replace with get_comments_popup_link() when https://core.trac.wordpress.org/ticket/17763 is resolved
	ob_start();
	$comments_label = sprintf( _n( '%s comment', '%s comments', $comments_number, 'us' ), $comments_number );
	comments_popup_link( __( 'No comments', 'us' ), $comments_label, $comments_label );
	$meta_html['comments'] .= ob_get_clean();
	$meta_html['comments'] .= '</span>';
}

if ( us_get_option( 'post_nav' ) ) {
	$prevnext = us_get_post_prevnext();
}

if ( $show_tags ) {
	$the_tags = get_the_tag_list( '', ', ', '' );
}

$meta_html = apply_filters( 'us_single_post_meta_html', $meta_html, get_the_ID() );

?>
<section <?php post_class( 'l-section for_blogpost preview_' . $preview_type ) ?>>
	<div class="l-section-h i-cf">
		<div class="w-blog">
			<?php if ( ! empty( $preview_bg ) ): ?>
				<div class="w-blog-post-preview" style="background-image: url(<?php echo $preview_bg ?>)"></div>
			<?php elseif ( ! empty( $preview_html ) OR $preview_type == 'modern' ): ?>
				<div class="w-blog-post-preview">
					<?php echo $preview_html ?>
				</div>
			<?php endif; ?>
			<div class="w-blog-post-body">
				<h1 class="w-blog-post-title entry-title"><?php the_title() ?></h1>

				<div class="w-blog-post-meta<?php echo empty( $metas ) ? ' hidden' : '' ?>">
					<?php echo implode( '', $meta_html ) ?>
				</div>
			</div>
		</div>
	</div>
</section>


<?php
$user_id = get_current_user_id();
if ( $user_id ) :

	$user_credits = acas4u_get_user_credits( $user_id );

	?>
	<section <?php post_class( 'l-section for_account' ) ?>>
		<div class="l-section-h i-cf">
			<div class="acas4u-account-details">
				<h2>Account details</h2>
				<ul>
					<li>You're allowed <strong><?php echo $user_credits['max_downloads']; ?></strong> downloads in a
						<strong><?php echo $user_credits['credit_period']; ?></strong> day period.
					</li>
					<li>You've downloaded <strong><?php echo $user_credits['credits_used']; ?></strong> file(s) in the
						last
						<strong><?php echo $user_credits['credit_period']; ?></strong> days.
					</li>
					<li>You have <strong><?php echo $user_credits['credit_remaining']; ?></strong> download(s)
						remaining.
					</li>
					<li>You have <strong><?php echo $user_credits['bonus_credits']; ?></strong> bonus download(s)
						remaining.
					</li>
				</ul>
				<?php

				?>
			</div>
		</div>
	</section>
<?php endif; ?>

<section <?php post_class( 'l-section for_album' ) ?>>
	<?php
	$post_id = get_the_ID();
	$albuminfo = [ ];

	// get keywords - artist and track name
	$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
	$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
	$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );

	// get stored amazon meta
	$albuminfo['url'] = get_post_meta( $post_id, '_amazon_url', TRUE );
	$albuminfo['mediumimage'] = get_post_meta( $post_id, '_amazon_mediumimage', TRUE );

	if ( $albuminfo['url'] == '' AND $albuminfo['mediumimage'] == '' ) {
		$keywords = $artist1 . ' ' . $trackname;
		$albuminfo = getAlbumInfo( $keywords );

		if ( $albuminfo['url'] != '' ) {
			update_post_meta( $post_id, '_amazon_url', esc_url( $albuminfo['url'] ) );
		}
		if ( $albuminfo['mediumimage'] != '' ) {
			update_post_meta( $post_id, '_amazon_mediumimage', esc_url( $albuminfo['mediumimage'] ) );
		}
		if ( $albuminfo['artist'] != '' ) {
			update_post_meta( $post_id, '_amazon_artist', esc_attr( $albuminfo['artist'] ) );
		}
		if ( $albuminfo['brand'] != '' ) {
			update_post_meta( $post_id, '_amazon_brand', esc_attr( $albuminfo['brand'] ) );
		}
		if ( $albuminfo['catalog'] != '' ) {
			update_post_meta( $post_id, '_amazon_catalog', esc_attr( $albuminfo['catalog'] ) );
		}
		if ( $albuminfo['publisher'] != '' ) {
			update_post_meta( $post_id, '_amazon_publisher', esc_attr( $albuminfo['publisher'] ) );
		}
		if ( $albuminfo['pubdate'] != '' ) {
			update_post_meta( $post_id, '_amazon_pubdate', esc_attr( $albuminfo['pubdate'] ) );
		}
		if ( $albuminfo['studio'] != '' ) {
			update_post_meta( $post_id, '_amazon_studio', esc_attr( $albuminfo['studio'] ) );
		}
		if ( $albuminfo['title'] != '' ) {
			update_post_meta( $post_id, '_amazon_title', esc_attr( $albuminfo['title'] ) );
		}
		if ( $albuminfo['label'] != '' ) {
			update_post_meta( $post_id, '_amazon_label', esc_attr( $albuminfo['label'] ) );
		}
	} else {
		$albuminfo['title'] = get_post_meta( $post_id, '_amazon_title', TRUE );
		$albuminfo['artist'] = get_post_meta( $post_id, '_amazon_artist', TRUE );
		$albuminfo['brand'] = get_post_meta( $post_id, '_amazon_brand', TRUE );
		$albuminfo['catalog'] = get_post_meta( $post_id, '_amazon_catalog', TRUE );
		$albuminfo['publisher'] = get_post_meta( $post_id, '_amazon_publisher', TRUE );
		$albuminfo['pubdate'] = get_post_meta( $post_id, '_amazon_pubdate', TRUE );
		$albuminfo['studio'] = get_post_meta( $post_id, '_amazon_studio', TRUE );
		$albuminfo['label'] = get_post_meta( $post_id, '_amazon_label', TRUE );
	}

	if ( $albuminfo['url'] != '' ) {
		$output = '<div class="l-section-h i-cf">
					<div class="acas4u-download-file-album">
						<h2>Album details</h2>
						<div class="acas4u-album-details">';
		$output .= '<div class="acas4u-album-mediumimage"><img src="' . $albuminfo['mediumimage'] . '" alt="' . $albuminfo['title'] . '" title="' . $albuminfo['title'] . '"></div>';
		$output .= '<div class="acas4u-album-details">';
		if ( $albuminfo['artist'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-artist">
									<div class="acas4u-label">Artist:</div>
									<div class="acas4u-descr">' . $albuminfo['artist'] . '</div>
								</div>';
		}
		if ( $albuminfo['title'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-title">
									<div class="acas4u-label">Title:</div>
									<div class="acas4u-descr">' . $albuminfo['title'] . '</div>
								</div>';
		}
		if ( $albuminfo['catalog'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-catalog">
									<div class="acas4u-label">Catalog number:</div>
									<div class="acas4u-descr">' . $albuminfo['catalog'] . '</div>
								</div>';
		}
		if ( $albuminfo['label'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-label">
									<div class="acas4u-label">Label:</div>
									<div class="acas4u-descr">' . $albuminfo['label'] . '</div>
								</div>';
		}
		if ( $albuminfo['brand'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-brand">
									<div class="acas4u-label">Brand:</div>
									<div class="acas4u-descr">' . $albuminfo['brand'] . '</div>
								</div>';
		}
		if ( $albuminfo['studio'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-studio">
									<div class="acas4u-label">Studio:</div>
									<div class="acas4u-descr">' . $albuminfo['studio'] . '</div>
								</div>';
		}
		if ( $albuminfo['publisher'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-publisher">
									<div class="acas4u-label">Publisher:</div>
									<div class="acas4u-descr">' . $albuminfo['publisher'] . '</div>
								</div>';
		}
		if ( $albuminfo['pubdate'] ) {
			$output .= '<div class="acas4u-album-details-row acas4u-album-details-pubdate">
									<div class="acas4u-label">Publishing date:</div>
									<div class="acas4u-descr">' . $albuminfo['pubdate'] . '</div>
								</div>';
		}
		$output .= '</div>';
		$output .= '<p class="acas4u-amazon-reflink">Please consider supporting the artist - <a href="' . $albuminfo['url'] . '">purchase this from Amazon</a></p>';
		$output .= '</div>
				</div>
				</div>';
		echo $output;
	}
	?>
</section>

<section <?php post_class( 'l-section for_download' ) ?>>
	<div class="l-section-h i-cf">
		<div class="acas4u-download-file-meta">
			<?php
			$post_id = get_the_ID();
			$date_added = get_the_date();
			$filename = get_the_title();
			$filesize = (int) get_post_meta( $post_id, '_download_size', TRUE );
			$bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
			$samplerate = get_post_meta( $post_id, '_download_samplerate', TRUE );
			$downloaded = get_post_meta( $post_id, '_download_count', TRUE );
			$member_rating = get_post_meta( $post_id, '_download_rating', TRUE );
			$member_votes = get_post_meta( $post_id, '_download_votes', TRUE );

			$meta_values = '<h2>File information</h2>';
			$meta_values .= '<div class="acas4u-download-date-added">Date added: ' . $date_added . '</div>';
			$meta_values .= '<div class="acas4u-download-mamber-rating">Overall member rating: ' . $member_rating . ' - ' . $member_votes . ' vote(s)</div>';
			$meta_values .= '<div class="acas4u-download-filename">Filename: <strong>' . $filename . '</strong> [' . formatBytes( $filesize ) . ']</div>';
			$meta_values .= '<div class="acas4u-download-bitrate">Bitrate: ' . $bitrate . '</div>';
			$meta_values .= '<div class="acas4u-download-bitrate">Sample rate: ' . $samplerate . '</div>';
			$meta_values .= '<div class="acas4u-download-bitrate">Downloaded: ' . $downloaded . '</div>';

			echo $meta_values;
			?>
		</div>
		<p>Please don't just download, we'd appreciate it if you hovered over the stars below and clicked to choose you
			rating.</p>
	</div>
</section>


<?php echo $the_content ?>

<?php if ( $show_tags AND ! empty( $the_tags ) ): ?>
	<section class="l-section for_tags">
		<div class="l-section-h i-cf">
			<div class="g-tags">
				<span class="g-tags-title"><?php _e( 'Tags', 'us' ) ?>:</span>
				<?php echo $the_tags ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( us_get_option( 'post_sharing' ) ) : ?>
	<section class="l-section for_sharing">
		<div class="l-section-h i-cf">
			<?php
			$sharing_providers = (array) us_get_option( 'post_sharing_providers' );
			$us_sharing_atts = array(
				'type' => us_get_option( 'post_sharing_type', 'simple' ),
			);
			foreach ( array( 'facebook', 'twitter', 'linkedin', 'gplus', 'pinterest' ) as $provider ) {
				$us_sharing_atts[ $provider ] = in_array( $provider, $sharing_providers );
			}
			us_load_template( 'shortcodes/us_sharing', array( 'atts' => $us_sharing_atts ) );
			?>
		</div>
	</section>
<?php endif; ?>

<?php if ( us_get_option( 'post_author_box' ) ): ?>
	<?php us_load_template( 'templates/blog/single-post-author' ) ?>
<?php endif; ?>

<?php if ( us_get_option( 'post_nav' ) AND ! empty( $prevnext ) ): ?>
	<section class="l-section for_blognav">
		<div class="l-section-h i-cf">
			<div class="w-blognav">
				<?php foreach ( $prevnext as $key => $item ): ?>
					<a class="w-blognav-<?php echo $key ?>" href="<?php echo $item['link'] ?>">
						<span class="w-blognav-meta"><?php echo $item['meta'] ?></span>
						<span class="w-blognav-title"><?php echo $item['title'] ?></span>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( us_get_option( 'post_related', TRUE ) ): ?>
	<?php us_load_template( 'templates/blog/single-post-related' ) ?>
<?php endif; ?>

<?php if ( comments_open() OR get_comments_number() != '0' ): ?>
	<section class="l-section for_comments">
		<div class="l-section-h i-cf">
			<?php wp_enqueue_script( 'comment-reply' ) ?>
			<?php comments_template() ?>
		</div>
	</section>
<?php endif; ?>
