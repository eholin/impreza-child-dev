<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Output one single acapella from acapellas listing.
 *
 * (!) Should be called in WP_Query fetching loop only.
 * @link https://codex.wordpress.org/Class_Reference/WP_Query#Standard_Loop
 *
 */

$permalink = get_the_permalink();
$post_id = get_the_ID();

$artist1 = get_post_meta( $post_id, '_download_artist1', TRUE );
$artist2 = get_post_meta( $post_id, '_download_artist2', TRUE );
$trackname = get_post_meta( $post_id, '_download_trackname', TRUE );

$download_bitrate = get_post_meta( $post_id, '_download_bitrate', TRUE );
$download_mode = get_post_meta( $post_id, '_download_mode', TRUE );
if ( $download_mode != '' ) {
	$download_mode = ( $download_mode == 1 ) ? 'major' : 'minor';
}
// $download_bpm = get_post_meta( $post_id, '_download_bpm', TRUE ); // use 'tempo' instead of 'bpm'
$download_size = get_post_meta( $post_id, '_download_size', TRUE );
$download_size = formatBytes( $download_size );
$download_rating = get_post_meta( $post_id, '_download_rating', TRUE );
$download_duration = get_post_meta( $post_id, '_download_duration', TRUE );
$download_count = get_post_meta( $post_id, '_download_count', TRUE );
$download_key = (int) get_post_meta( $post_id, '_download_key', TRUE );
$download_tempo = get_post_meta( $post_id, '_download_tempo', TRUE );
$date_uploaded = get_the_date( 'd/m/Y' );

$title = acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname );

$rating_stars = acas4u_display_rating_stars( $download_rating );

$output = '<div class="acas4u-search-tr">';
$output .= '<div class="acas4u-search-td td-title"><a href="' . $permalink . '">' . $title . '</a></div>';
$output .= '<div class="acas4u-search-td td-mode">' . $download_mode . '</div>';
$output .= '<div class="acas4u-search-td td-key2">' . acas4u_key_value( $download_key ) . '</div>';
$output .= '<div class="acas4u-search-td td-bpm">' . $download_tempo . '</div>';
$output .= '<div class="acas4u-search-td td-bitrate">' . $download_bitrate . '</div>';
$output .= '<div class="acas4u-search-td td-size">' . $download_size . '</div>';
$output .= '<div class="acas4u-search-td td-length">' . acas4u_format_duration( $download_duration ) . '</div>';
$output .= '<div class="acas4u-search-td td-listeners">' . number_format( $download_count, 0, ',', ',' ) . '</div>';
$output .= '<div class="acas4u-search-td td-added">' . $date_uploaded . '</div>';
$output .= '<div class="acas4u-search-td td-rating">' . $rating_stars . '</div>';
$output .= '</div>';

echo $output;

?>
