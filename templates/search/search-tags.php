<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * search tags cloud
 */

?>

	<h3 class="acas4u-search-tags-title">This month's most popular tags</h3>

<?php
global $wpdb;

function array_orderby() {
	$args = func_get_args();
	$data = array_shift( $args );
	foreach ( $args as $n => $field ) {
		if ( is_string( $field ) ) {
			$tmp = array();
			foreach ( $data as $key => $row ) {
				$tmp[ $key ] = $row[ $field ];
			}
			$args[ $n ] = $tmp;
		}
	}
	$args[] = &$data;
	call_user_func_array( 'array_multisort', $args );

	return array_pop( $args );
}

$current_date = date( 'Y-m-d' );
$earlier_date = date( 'Y-m-d', strtotime( '-30 days' ) );

$tags_query = "SELECT tag_quantity, search_tag FROM acas4u_search_tags WHERE search_date >= '" . $earlier_date . "' ORDER BY tag_quantity DESC LIMIT 0,50";
$search_tags = $wpdb->get_results( $tags_query, ARRAY_A );
//echo '<pre>';
//var_dump( $search_tags );
//echo '</pre>';
$output_tags = [ ];
$output_tags_str = '';
if ( is_array( $search_tags ) ) {

	$sorted_search_tags = array_orderby( $search_tags, 'search_tag', SORT_ASC, 'tag_quantity', SORT_ASC );

	foreach ( $sorted_search_tags as $search_tag ) {

		$tag_class = acas4u_get_tag_class( $search_tag['tag_quantity'] );
		$output_tags[] = '<a class="' . $tag_class . '" href="' . home_url( '?s=' ) . urlencode( $search_tag['search_tag'] ) . '&post_type=download">' . $search_tag['search_tag'] . '</a>';
	}
}

if ( count( $output_tags ) > 0 ) {
	$output_tags_str = implode( ', ', $output_tags );
}

echo '<div class="acas4u-search-tags">' . $output_tags_str . '</div>';
