<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Output a single blog listing. Universal template that is used by all the possible blog posts listings.
 *
 * (!) $query_args should be filtered before passing to this template.
 *
 * @var $query_args array Arguments for the new WP_Query. If not set, current global $wp_query will be used instead.
 * @var $layout_type string Blog layout: large / smallcircle / smallsquare / grid / masonry / compact / related / latest
 * @var $columns int Number of columns 1 / 2 / 3 / 4
 * @var $metas array Meta data that should be shown: array('date', 'author', 'categories', 'tags', 'comments')
 * @var $content_type string Content type: 'excerpt' / 'content' / 'none'
 * @var $show_read_more boolean Show "Read more" link after the excerpt?
 * @var $pagination string Pagination type: regular / none / ajax
 * @var $el_class string Additional classes that will be appended to the main .w-blog container
 *
 * @action Before the template: 'us_before_template:templates/blog/listing'
 * @action After the template: 'us_after_template:templates/blog/listing'
 * @filter Template variables: 'us_template_vars:templates/blog/listing'
 */

global $acas4u_stylesheet_directory;

// Variables defaults and filtering
$layout_type = isset( $layout_type ) ? $layout_type : 'large';
$columns = ( isset( $columns ) AND $layout_type == 'latest' ) ? intval( $columns ) : 1;
$default_metas = array( 'date', 'author', 'categories', 'tags', 'comments' );
$metas = ( isset( $metas ) AND is_array( $metas ) ) ? array_intersect( $metas, $default_metas ) : $default_metas;
$content_type = isset( $content_type ) ? $content_type : 'excerpt';
$show_read_more = isset( $show_read_more ) ? $show_read_more : TRUE;
$pagination = isset( $pagination ) ? $pagination : 'regular';
$el_class = isset( $el_class ) ? $el_class : '';

// .w-blog container additional classes and inner CSS-styles
$classes = '';
$inner_css = '';

?>
<div class="w-blog<?php echo $classes ?>">
	<div class="w-blog-list"><?php

		$template_vars = array(
			'layout_type' => $layout_type,
			'metas' => $metas,
			'content_type' => $content_type,
			'show_read_more' => $show_read_more,
		);

		$search_type = 'custom';

		$user_id = get_current_user_id();
		$user_donator = acas4u_is_user_donator( $user_id );

		$search_term = get_search_query();
		$offset = esc_attr( $_GET['offset'] );
		$count = esc_attr( $_GET['count'] );
		$query_genre = esc_attr( $_GET['download_genre'] );
		$query_bpm1 = esc_attr( $_GET['download_bpm1'] );
		$query_bpm2 = esc_attr( $_GET['download_bpm2'] );
		$query_bitrate1 = esc_attr( $_GET['download_bitrate1'] );
		$query_bitrate2 = esc_attr( $_GET['download_bitrate2'] );
		$query_rating1 = esc_attr( $_GET['download_rating1'] );
		$query_rating2 = esc_attr( $_GET['download_rating2'] );
		$query_mode = esc_attr( $_GET['download_mode'] );
		$query_key1 = esc_attr( $_GET['download_key1'] );
		$query_key2 = esc_attr( $_GET['download_key2'] );
		$filter = ( $_GET['filter'] != '' ) ? esc_attr( $_GET['filter'] ) : 'no';

		if ( $query_bitrate2 == '' ) {
			if ( $user_donator === FALSE ) {
				$query_bitrate2 = (int) us_get_option( 'max_bitrate_for_non_donators' );
			} else {
				$query_bitrate2 = 320;
			}
		}

		$query_bitrate1 = ( $query_bitrate1 != '' ) ? $query_bitrate1 : 0;

		if ( $search_term != '' ) {
			acas4u_save_search_tag( $search_term, $user_id, $_SERVER['REMOTE_ADDR'] );
		}

		$count_from = ( $count != '' ) ? $count : 20;
		$count = ( $count != '' ) ? $count : 20;

		if ( $search_type != 'custom' ) {
			$args = acas4u_build_search_args( $search_term, $offset, $count, $query_genre, $query_bpm1, $query_bpm2, $query_bitrate1, $query_bitrate2, $query_rating1, $query_rating2, $query_mode, $query_key1, $query_key2, FALSE, $search_type );
		} else {
			$args = array(
				'search_term' => $search_term,
				'offset' => $offset,
				'count' => $count,
				'query_genre' => $query_genre,
				'query_bpm1' => $query_bpm1,
				'query_bpm2' => $query_bpm2,
				'query_bitrate1' => $query_bitrate1,
				'query_bitrate2' => $query_bitrate2,
				'query_rating1' => $query_rating1,
				'query_rating2' => $query_rating2,
				'query_mode' => $query_mode,
				'query_key1' => $query_key1,
				'query_key2' => $query_key2,
			);
		}
		/*
		echo '<pre>';
		var_dump($args);
		echo '</pre>';
		*/

		if ( $filter == 'yes' ) {
			$total_results = acas4u_get_posts_count( $args, $search_type );
		} else {
			if ( $user_donator === FALSE ) {
				$total_results = get_option( 'acas4u_downloads_for_non_donators' );
			} else {
				$total_results = get_option( 'acas4u_downloads_for_donators' );
			}
		}

		if ( $user_donator ) {
			$input_disabled = '';
			$class_disabled = '';
			$tooltip_disabled = '';
			$data_donator = '1';
			$tooltip_bitrate = '';
			$tooltip_key2 = '';
		} else {
			$class_disabled = 'acas4u-disabled';
			$input_disabled = 'disabled="disabled"';
			$tooltip_key2 = 'data-tooltip="Sorting by this field available for donators" data-tooltip-position="top"';
			$tooltip_bitrate = 'data-tooltip="Maximum bitrate available for donators" data-tooltip-position="top"';
			$tooltip_disabled = 'data-tooltip="Advanced search available for donators" data-tooltip-position="top"';
			$data_donator = '0';
		}

		$ajax_form_nonce = wp_create_nonce( 'acas4u_do_update_search_nonce' );
		?>

		<form role="search" action="<?php echo site_url( '/' ); ?>" id="searchform"
			  data-donator="<?php echo $data_donator; ?>"
			  data-bitratefree="192"
			  data-bitratemax="320"
			  data-nonce="<?php echo $ajax_form_nonce; ?>"
			  data-searchtype="<?php echo $search_type; ?>"
		>
			<div class="acas4u-sf-row acas4u-sr-border">
				<div class="acas4u-sf-col acas4u-sr-header">
					<h3>Search results for:</h3>
				</div>
				<?php
				if ( $search_term != '' ) {
					echo '<div class="acas4u-sf-col acas4u-sr-results">"' . $search_term . '"</div>';
				}
				?>
				<div class="acas4u-sf-col acas4u-sr-found">Found <span class="acas4u-sf-count"><?php echo $total_results; ?></span> results</div>
			</div>
			<div class="acas4u-sf-row">
				<div class="acas4u-sf-col acas4u-sr-query">
					<input type="text" name="s" placeholder="Search Acapellas" value="<?php echo $search_term; ?>" class="acas4u-sf-search-field" id="searchterm"/>
				</div>
				<div class="acas4u-sf-col acas4u-sr-button">
					<a href="javascript:void(0);" class="acas4u-searchbutton w-btn color_default">Search</a>
					<a href="<?php echo home_url( '/search/acapellas/' ); ?>" class="acas4u-clearbutton w-btn color_primary">Clear results</a>
					<input type="hidden" name="post_type" value="download"/>
				</div>
			</div>
			<div class="acas4u-sf-row">
				<div class="acas4u-sf-col w25pc">
					<label for="download_genre">Genre:</label>
					<?php
					$genres = get_terms( 'genre', array(
						'orderby' => 'title',
						'hide_empty' => 1,
					) );
					if ( ! empty( $genres ) AND ! is_wp_error( $genres ) ) {
						echo '<select id="download_genre" name="download_genre" class="acas4u-sf-select">';
						echo '<option value="">Any</option>';
						foreach ( $genres as $genre ) {
							$genre_value = ( $search_type == 'custom' ) ? $genre->term_id : $genre->slug;
							echo '<option value="' . $genre_value . '" ' . selected( $genre_value, $query_genre, FALSE ) . '>' . $genre->name . '</option>';
						}
						echo '</select>';
					}
					?>
				</div>
				<div class="acas4u-sf-col w25pc">
					<label for="acas4u_download_bpm">Beats per Minute:</label>
					<input type="text" data-from="<?php echo ( $query_bpm1 != '' ) ? $query_bpm1 : 50; ?>" data-to="<?php echo ( $query_bpm2 != '' ) ? $query_bpm2 : 200; ?>" id="acas4u_download_bpm"/>
					<input type="hidden" id="download_bpm1" name="download_bpm1" value="<?php echo $query_bpm1; ?>"/>
					<input type="hidden" id="download_bpm2" name="download_bpm2" value="<?php echo $query_bpm2; ?>">
				</div>
				<div class="acas4u-sf-col w25pc" <?php echo $tooltip_bitrate; ?>>
					<label for="acas4u_download_bitrate">BitRate:</label>
					<input type="text" data-from="<?php echo ( $query_bitrate1 != '' ) ? $query_bitrate1 : 0; ?>" data-to="<?php echo ( $query_bitrate2 != '' ) ? $query_bitrate2 : 192; ?>" id="acas4u_download_bitrate"/>
					<input type="hidden" id="download_bitrate1" name="download_bitrate1" value="<?php echo $query_bitrate1; ?>"/>
					<input type="hidden" id="download_bitrate2" name="download_bitrate2" value="<?php echo $query_bitrate2; ?>">
				</div>
				<div class="acas4u-sf-col w25pc">
					<label for="acas4u_download_rating">Rating:</label>
					<input type="text" data-from="<?php echo ( $query_rating1 != '' ) ? $query_rating1 : 0; ?>" data-to="<?php echo ( $query_rating2 != '' ) ? $query_rating2 : 5; ?>" id="acas4u_download_rating"/>
					<input type="hidden" name="download_rating1" id="download_rating1" value="<?php echo $query_rating1; ?>">
					<input type="hidden" name="download_rating2" id="download_rating2" value="<?php echo $query_rating2; ?>">
				</div>
			</div>
			<div class="acas4u-sf-row">
				<div class="acas4u-sf-col w25pc <?php echo $class_disabled; ?>" <?php echo $tooltip_disabled; ?>>
					<label for="download_mode">Mode:</label>
					<select class="acas4u-sf-select" id="download_mode" name="download_mode" <?php echo $input_disabled; ?>>
						<option value="" <?php selected( '', $query_mode, TRUE ); ?>>Any</option>
						<option value="1" <?php selected( '1', $query_mode, TRUE ); ?>>Major</option>
						<option value="0" <?php selected( '0', $query_mode, TRUE ); ?>>Minor</option>
					</select>
				</div>
				<div class="acas4u-sf-col w25pc">
					<label for="download_key1">Key 1:</label>
					<select class="acas4u-sf-select" id="download_key1" name="download_key1">
						<option value="" <?php selected( '', $query_key1, TRUE ); ?>>N/A</option>
						<option value="0" <?php selected( '0', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 0 ); ?></option>
						<option value="1" <?php selected( '1', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 1 ); ?></option>
						<option value="2" <?php selected( '2', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 2 ); ?></option>
						<option value="3" <?php selected( '3', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 3 ); ?></option>
						<option value="4" <?php selected( '4', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 4 ); ?></option>
						<option value="5" <?php selected( '5', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 5 ); ?></option>
						<option value="6" <?php selected( '6', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 6 ); ?></option>
						<option value="7" <?php selected( '7', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 7 ); ?></option>
						<option value="8" <?php selected( '8', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 8 ); ?></option>
						<option value="9" <?php selected( '9', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 9 ); ?></option>
						<option value="10" <?php selected( '10', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 10 ); ?></option>
						<option value="11" <?php selected( '11', $query_key1, TRUE ); ?>><?php echo acas4u_key_value( 11 ); ?></option>
					</select>

				</div>
				<div class="acas4u-sf-col w25pc <?php echo $class_disabled; ?>" <?php echo $tooltip_disabled; ?>>
					<label for="download_key2">Key 2:</label>
					<select class="acas4u-sf-select" id="download_key2" name="download_key2" <?php echo $input_disabled; ?>>
						<option value="" <?php selected( '', $query_key2, TRUE ); ?>>N/A</option>
						<option value="1" <?php selected( '1', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 1 ); ?></option>
						<option value="2" <?php selected( '2', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 2 ); ?></option>
						<option value="3" <?php selected( '3', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 3 ); ?></option>
						<option value="4" <?php selected( '4', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 4 ); ?></option>
						<option value="5" <?php selected( '5', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 5 ); ?></option>
						<option value="6" <?php selected( '6', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 6 ); ?></option>
						<option value="7" <?php selected( '7', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 7 ); ?></option>
						<option value="8" <?php selected( '8', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 8 ); ?></option>
						<option value="9" <?php selected( '9', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 9 ); ?></option>
						<option value="10" <?php selected( '10', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 10 ); ?></option>
						<option value="11" <?php selected( '11', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 11 ); ?></option>
						<option value="12" <?php selected( '12', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 12 ); ?></option>
						<option value="13" <?php selected( '13', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 13 ); ?></option>
						<option value="14" <?php selected( '14', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 14 ); ?></option>
						<option value="15" <?php selected( '15', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 15 ); ?></option>
						<option value="16" <?php selected( '16', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 16 ); ?></option>
						<option value="17" <?php selected( '17', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 17 ); ?></option>
						<option value="18" <?php selected( '18', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 18 ); ?></option>
						<option value="19" <?php selected( '19', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 19 ); ?></option>
						<option value="20" <?php selected( '20', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 20 ); ?></option>
						<option value="21" <?php selected( '21', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 21 ); ?></option>
						<option value="22" <?php selected( '22', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 22 ); ?></option>
						<option value="23" <?php selected( '23', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 23 ); ?></option>
						<option value="24" <?php selected( '24', $query_key2, TRUE ); ?>><?php echo acas4u_keymode_value( 24 ); ?></option>
					</select>
				</div>
				<div class="acas4u-sf-col w25pc">
					<label for="acas4u_search_count">Show Results: </label>
					<input type="text" id="acas4u_search_count" data-from="<?php echo $count_from; ?>">
					<input type="hidden" id="search_count" name="count" value="<?php echo $count; ?>"/>
				</div>

			</div>
			<input type="hidden" name="filter" value="yes"/>
		</form>

		<?php
		// include search tags
		//require_once $acas4u_stylesheet_directory . '/templates/search/search-tags.php';

		the_ad( 498013 );

		/* search results starts here */
		$ajax_nonce = wp_create_nonce( 'acas4u_do_scroll_nonce' );
		?>
		<div class="acas4u-downloads-search-wrapper"
			 data-page="1"
			 data-total="<?php echo $total_results; ?>"
			 data-nonce="<?php echo $ajax_nonce; ?>"
			 data-searchterm="<?php echo $search_term; ?>"
			 data-genre="<?php echo $query_genre; ?>"
			 data-bpm1="<?php echo $query_bpm1; ?>"
			 data-bpm2="<?php echo $query_bpm2; ?>"
			 data-bitrate1="<?php echo $query_bitrate1; ?>"
			 data-bitrate2="<?php echo $query_bitrate2; ?>"
			 data-rating1="<?php echo $query_rating1; ?>"
			 data-rating2="<?php echo $query_rating2; ?>"
			 data-mode="<?php echo $query_mode; ?>"
			 data-key1="<?php echo $query_key1; ?>"
			 data-key2="<?php echo $query_key2; ?>"
			 data-count="<?php echo $count; ?>"
			 data-searchtype="<?php echo $search_type; ?>"
		>
			<div class="g-preloader type_1"></div>
			<table class="acas4u-search-results tablesorter" id="search-results-table">
				<thead>
				<tr class="acas4u-search-th">
					<th class="acas4u-search-td td-title">Title</th>
					<th class="acas4u-search-td td-mode">Mode</th>
					<th class="acas4u-search-td td-key1">Key 1</th>
					<?php if ( is_user_logged_in() ) { ?>
						<th class="acas4u-search-td td-key2"><span <?php echo $tooltip_key2; ?>>Key 2</span></th>
					<?php } ?>
					<th class="acas4u-search-td td-bpm">BPM</th>
					<th class="acas4u-search-td td-bitrate">Bitrate</th>
					<th class="acas4u-search-td td-size">Size</th>
					<th class="acas4u-search-td td-length">Length</th>
					<th class="acas4u-search-td td-listeners">Listeners</th>
					<th class="acas4u-search-td td-added">Added</th>
					<th class="acas4u-search-td td-rating">Rating</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if ( $search_type == 'custom' ) {
					$output = acas4u_show_custom_search_results( $args );
				} else {
					$output = acas4u_show_search_results( $args );
				}
				echo $output['large'];
				?>
				</tbody>
			</table>
			<?php if ( $total_results > $count ) {
				?>
				<div class="g-loadmore">
					<div class="g-loadmore-btn acas4u-show-more-results">
						<span><?php _e( 'Load More', 'us' ) ?></span>
					</div>
					<div class="g-preloader type_1"></div>
				</div>
				<?php
			}
			?>
		</div>
		<!-- .acas4u-downloads-search-wrapper -->
		<?php

		/* search results ends here */
		?>
	</div>
<?php
