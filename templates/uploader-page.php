<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying uploader form
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
$us_layout->titlebar = ( us_get_option( 'titlebar_content', 'all' ) == 'hide' ) ? 'none' : 'default';
$us_layout->sidebar_pos = us_get_option( 'popular_sidebar', 'none' );
get_header();

// Creating .l-titlebar
us_load_template( 'templates/titlebar', array(
	'title' => 'Upload files',
) );

$current_user = wp_get_current_user();

?>
	<!-- MAIN -->
	<div class="l-main">
		<div class="l-main-h i-cf">

			<div class="l-content g-html">

				<section <?php post_class( 'l-section for_file_uploader' ) ?>>
					<div class="l-section-h i-cf">
						<?php

						if ( is_user_logged_in() ) {
							?>
							<form id="uploaderform" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="do_upload"/>
								<input type="hidden" name="uploader" value="<?php echo $current_user->ID; ?>"/>
								<input type="hidden" name="_ajax_nonce" value="<?php echo wp_create_nonce( 'acas4u_do_file_upload' ); ?>"/>

								<?php for ( $i = 1; $i <= 5; $i ++ ) { ?>
									<div class="acas4u-uploader-file-wrapper">
										<input type="file" name="userfile[]" id="userfile<?php echo $i; ?>" class="acas4u-uploader-file-input"/>
										<label for="userfile<?php echo $i; ?>" class="acas4u-uploader-file-label"><i class="fa fa-file-audio-o" aria-hidden="true"></i>Choose
											a file</label>

										<div class="acas4u-uploader-file-name"></div>
										<div class="acas4u-uploader-data acas4u-uploader-file-artist1-wrapper">
											<input class="acas4u-uploader-data-input fl-artist" type="text" name="artist1[]" placeholder="Artist 1"/>
										</div>
										<div class="acas4u-uploader-data acas4u-uploader-file-trackname-wrapper">
											<input class="acas4u-uploader-data-input fl-trackname" type="text" name="trackname[]" placeholder="Trackname"/>
										</div>
										<div class="acas4u-uploader-data acas4u-uploader-file-featuring-wrapper">
											<input class="acas4u-uploader-data-input fl-featuring" type="text" name="featuring[]" placeholder="Featuring"/>
										</div>
										<div class="acas4u-uploader-data acas4u-uploader-file-type-wrapper">
											<select class="acas4u-uploader-data-input fl-type" name="type[]">
												<option value="acapella">Acapella</option>
												<option value="better_quality_acapella">Better Quality</option>
												<option value="clean_acapella">Clean</option>
												<option value="dirty_acapella">Dirty</option>
												<option value="diy_acapella">DIY</option>
												<option value="dj_tool">DJ Tool</option>
												<option value="foreign_acapella">Foreign</option>
												<option value="full_acapella">Full</option>
												<option value="incomplete_acapella">Incomplete</option>
												<option value="remix_acapella">Remix</option>
											</select>
										</div>
										<div class="acas4u-uploader-data acas4u-uploader-file-times-wrapper">
											<a href="javascript:void(0);" class="acas4u-uploader-data-clear"><i class="fa fa-times" aria-hidden="true"></i></a>
										</div>
									</div>
								<?php } ?>

								<div class="acas4u-uploader-button-container">
									<button type="submit" class="acas4u-uploader-file-button">
										<i class="fa fa-upload" aria-hidden="true"></i>Upload Files
									</button>
									<button type="reset" class="acas4u-uploader-reset-button">
										<i class="fa fa-ban" aria-hidden="true"></i>Clear
									</button>
								</div>
								<div class="acas4u-uploader-progress-container">
									<div class="acas4u-uploader-progress">
										<div class="acas4u-uploader-progress-bar"></div>
										<div class="acas4u-uploader-progress-percent">0%</div>
									</div>

								</div>

							</form>

							<div id="message"></div>

							<?php
						} else {
							echo '<p>File uploading available only for registered users. Please login or register.</p>';
						}

						$upload_page_html = us_get_option( 'upload_page_html', '' );

						if ( $upload_page_html ) {
							echo '<div class="acas4u-upload-page-text">';
							echo $upload_page_html;
							echo '</div>';
						}

						?>
					</div>
				</section>
			</div>

			<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
				<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
					<?php dynamic_sidebar( 'default_sidebar' ) ?>
				</aside>
			<?php endif; ?>

		</div>
	</div>

<?php
get_footer();
