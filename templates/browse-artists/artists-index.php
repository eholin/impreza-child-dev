<?php
// Template for artists index

global $wp;
?>
<div class="acas4u-artists-index">
	<?php
	/*
	$excluded_taxonomies_ids = array(
		'380',
		'438',
		'381',
		'382',
		'383',
	);
	*/

	$excluded_taxonomies_ids = array();

	$default_taxonomy_slug = 'a';
	$query_taxonomy_slug = urldecode( $wp->query_vars['artist_index'] );

	$index = ( $query_taxonomy_slug != '' ) ? $query_taxonomy_slug : $default_taxonomy_slug;

	$unknown_term = get_term_by( 'slug', $index, 'artist-index' );
	$unknown_term_id = $unknown_term->term_taxonomy_id;
	$unknown_term_parent_id = $unknown_term->parent;

	if ( $unknown_term->parent > 0 ) {
		$parent_term = get_term_by( 'id', $unknown_term_parent_id, 'download-index' );
		$index = $parent_term->slug;
	}

	$primary_terms = get_terms( array(
		'taxonomy' => 'download-index',
		'hide_empty' => FALSE,
		'parent' => 0,
		'exclude' => implode( ',', $excluded_taxonomies_ids ),
	) );
	if ( ! empty( $primary_terms ) AND ! is_wp_error( $primary_terms ) ) {
		?>
		<ul class="acas4u-artists-index-list">
			<?php
			foreach ( $primary_terms as $primary_term ) {
				$primary_term_count = '';
				$children_terms_count = acas4u_count_term_children_posts( $primary_term->term_id, 'download-index' );
				$all_posts_count = $primary_term->count + $children_terms_count;
				if ( $all_posts_count > 0 ) {
					$all_posts_count_str = '(' . $all_posts_count . ')';
				}
				if ( $index == $primary_term->slug ) {
					echo '<li class="active" id="term-' . $primary_term->term_id . '"><span>' . $primary_term->name . ' ' . $all_posts_count_str . '</span></li>';
				} else {
					echo '<li id="term-' . $primary_term->term_id . '"><a href="' . home_url( '/explore-artists/' ) . $primary_term->slug . '/"><span>' . $primary_term->name . ' ' . $all_posts_count_str . '</span></a></li>';
				}
			}
			?>
		</ul>

		<?php
	}

	$parent = get_term_by( 'slug', $index, 'download-index' );
	$parent_id = $parent->term_taxonomy_id;

	$index = ( strlen( $query_taxonomy_slug ) > 1 ) ? $query_taxonomy_slug : $index;

	$secondary_terms = get_terms( array(
		'taxonomy' => 'download-index',
		'hide_empty' => FALSE,
		'parent' => $parent_id,
		'exclude' => implode( ',', $excluded_taxonomies_ids ),
	) );
	if ( ! empty( $secondary_terms ) AND ! is_wp_error( $secondary_terms ) ) {
		?>
		<ul class="acas4u-artists-index-list-child">
			<?php
			foreach ( $secondary_terms as $secondary_term ) {
				if ( $index == $secondary_term->slug ) {
					echo '<li class="active"><span>' . $secondary_term->name . '</span></li>';
				} else {
					echo '<li><a href="' . home_url( '/explore-artists/' ) . $secondary_term->slug . '/"><span>' . $secondary_term->name . ' (' . $secondary_term->count . ')</span></a></li>';
				}
			}
			?>
		</ul>
		<?php
	}

	?>
</div>
