<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying uploader form
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
$us_layout->titlebar = ( us_get_option( 'titlebar_content', 'all' ) == 'hide' ) ? 'none' : 'default';
$us_layout->show_horizontal_artists_index = us_get_option( 'show_horizontal_artists_index', 'no' );

get_header();

// Creating .l-titlebar
us_load_template( 'templates/titlebar', array(
	'title' => 'Explore Artists',
) );

global $acas4u_stylesheet_directory;

?>
	<!-- MAIN -->
	<div class="l-main acas4u-browse-artists">
		<div class="l-main-h i-cf">

			<div class="l-content g-html">

				<section <?php post_class( 'l-section for_browse_artists' ) ?>>
					<div class="l-section-h i-cf">
						<?php
						$default_taxonomy_slug = 'a';
						$query_taxonomy_slug = urldecode( $wp->query_vars['artist_index'] );

						$index = ( $query_taxonomy_slug != '' ) ? $query_taxonomy_slug : $default_taxonomy_slug;

						$count = 20;

						if ( $us_layout->show_horizontal_artists_index == 'yes' ) {
							include_once( $acas4u_stylesheet_directory . '/templates/browse-artists/artists-index.php' );
						}

						the_ad( 498133 );

						$args = array(
							'post_type' => 'download',
							'posts_per_page' => $count,
							'order' => 'ASC',
							'orderby' => 'title',
							'post_status' => 'publish',
							'tax_query' => array(
								array(
									'taxonomy' => 'download-index',
									'field' => 'slug',
									'terms' => $index,
								),
							),
						);

						$total_results = acas4u_get_posts_count( $args, 'custom' );
						$ajax_nonce = wp_create_nonce( 'acas4u_do_explore_artists_scroll_nonce' );

						?>
						<div class="acas4u-explore-artists-wrapper"
						     data-page="1"
						     data-index="<?php echo $index; ?>"
						     data-nonce="<?php echo $ajax_nonce; ?>"
						     data-total="<?php echo $total_results; ?>"
						     data-count="<?php echo $count; ?>"
							>
							<table class="acas4u-search-results tablesorter" id="explore-artists-table">
								<thead>
								<tr class="acas4u-search-th">
									<th class="acas4u-search-td td-title">Title</th>
									<th class="acas4u-search-td td-mode">Mode</th>
									<th class="acas4u-search-td td-key1">Key 1</th>
									<th class="acas4u-search-td td-key2">Key 2</th>
									<th class="acas4u-search-td td-bpm">BPM</th>
									<th class="acas4u-search-td td-bitrate">Bitrate</th>
									<th class="acas4u-search-td td-size">Size</th>
									<th class="acas4u-search-td td-length">Length</th>
									<th class="acas4u-search-td td-listeners">Listeners</th>
									<th class="acas4u-search-td td-added">Added</th>
									<th class="acas4u-search-td td-rating">Rating</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$output = acas4u_show_explore_artists_results( $args );
								echo $output['large'];
								?>
								</tbody>
							</table>
							<?php if ( $total_results > $count ) {
								?>
								<div class="g-loadmore">
									<div class="g-loadmore-btn acas4u-show-more-explore-artists">
										<span><?php _e( 'Load More', 'us' ) ?></span>
									</div>
									<div class="g-preloader type_1"></div>
								</div>
								<?php
							}
							?>
						</div>

					</div>
				</section>
			</div>

			<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
				<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
					<?php dynamic_sidebar( 'explore_artists_sidebar' ) ?>
				</aside>
			<?php endif; ?>

		</div>
	</div>

<?php
get_footer();
