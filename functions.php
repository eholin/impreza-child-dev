<?php
$acas4u_stylesheet_directory = get_stylesheet_directory();
$acas4u_stylesheet_directory_uri = get_stylesheet_directory_uri();
$acas4u_template_directory_uri = get_template_directory_uri();
//$en_api_key = us_get_option( 'en_api_key' );
$en_api_key = 'AK3XQJRGSJ0HSXIIY';
$spotify_access_token = 'BQBzwgrZfxybj5TMt3wpkmcHW_UKO81Z_aePnRPrEEpPmwi8-h7MJhc0c1OdBUjP0_jTpjw0ifEJYuq4MuWy_ha0YWk1v8PbJpTyynz-aEO2RtksdOP121UKx33E-7Z5cZ-u9XcoRbHUmQsHPe93bNwDUV7OOcVCO2ioMQ2UUwuHh-LDyyl5bcCUmWR501OWhqnHXBltYDf0CJWu4oFxQlZTSTKZFBKrsDv9iXaPa4NGKFfX5nsbIMIfKgB2lfwSpyggtNuB3F2zk3VAWMq6YznK5v-Ee57y1LQOcQ1JKnLlv952vHW0';

/**
 * Include aq_resizer
 */
require $acas4u_stylesheet_directory . '/vendor/aq_resizer/aq_resizer.php';

/**
 * Include Spotify Web API Class
 */
require $acas4u_stylesheet_directory . '/vendor/spotify-web-api/Request.php';
require $acas4u_stylesheet_directory . '/vendor/spotify-web-api/Session.php';
require $acas4u_stylesheet_directory . '/vendor/spotify-web-api/SpotifyWebAPI.php';

/**
 * Include Custom Post Type Artist functions
 */
require $acas4u_stylesheet_directory . '/functions/cpt-artists.php';

/**
 * Include Custom Post Type Download functions
 */
require $acas4u_stylesheet_directory . '/functions/cpt-download.php';

/**
 * Include user rating functions
 */
require $acas4u_stylesheet_directory . '/functions/rating.php';

/**
 * Include shortcodes
 */
require $acas4u_stylesheet_directory . '/functions/shortcodes.php';

/**
 * Include widgets
 */
require $acas4u_stylesheet_directory . '/functions/widgets.php';

/**
 * Include file downloading functions
 */
require $acas4u_stylesheet_directory . '/functions/file-download.php';

/**
 * Include detail info functions
 */
require $acas4u_stylesheet_directory . '/functions/details.php';

/**
 * Include user functions
 */
require $acas4u_stylesheet_directory . '/functions/users.php';

/**
 * Include acapellas packs functions
 */
require $acas4u_stylesheet_directory . '/functions/acapellas-pack.php';

/**
 * Include functions for popular downloads
 */
require $acas4u_stylesheet_directory . '/functions/popular.php';

/**
 * Include functions for file uploader
 */
require $acas4u_stylesheet_directory . '/functions/file-upload.php';

/**
 * Include functions for latest downloads
 */
require $acas4u_stylesheet_directory . '/functions/latest.php';

/**
 * Include functions for similar tracks, artists etc
 */
require $acas4u_stylesheet_directory . '/functions/similar.php';

/**
 * Include metabox settings
 */
require $acas4u_stylesheet_directory . '/functions/meta-box_settings.php';

/**
 * Include ads functions
 */
require $acas4u_stylesheet_directory . '/functions/ads.php';

/**
 * Include Cron Jobs functions
 */
require $acas4u_stylesheet_directory . '/functions/cron-jobs.php';

/**
 * Include functions for search by downloads
 */
require $acas4u_stylesheet_directory . '/functions/search.php';

/**
 * Include functions for paypal functions
 */
require $acas4u_stylesheet_directory . '/functions/paypal.php';

/**
 * Include functions for custom taxonomy - genre
 */
require $acas4u_stylesheet_directory . '/functions/genre.php';

/**
 * Include functions for redirects from old website to the new
 */
require_once $acas4u_stylesheet_directory . '/functions/redirects.php';

/**
 * Include functions for mp3 tagging
 */
require_once( $acas4u_stylesheet_directory . '/vendor/getid3/getid3.php' );
require_once( $acas4u_stylesheet_directory . '/vendor/getid3/write.php' );

/* flush rewrite rules for custom post type url */
add_action( 'after_swith_theme', 'acas4u_flush_rewrites' );
//add_action( 'init', 'acas4u_flush_rewrites' );
function acas4u_flush_rewrites() {
	flush_rewrite_rules();
}

/* add new capability donator */
add_action( 'init', 'acas4u_add_donator_capability' );
function acas4u_add_donator_capability() {
	add_role( 'donator', 'Donator', array(
		'read' => TRUE,
		'edit_posts' => FALSE,
		'delete_posts' => FALSE,
	) );
}

// remove comments from all pages
// not working - why???
//add_filter( 'comments_open', 'acas4u_close_page_comments', 10, 2 );
function acas4u_close_page_comments( $open, $post_id ) {
	$post = get_post( $post_id );
	if ( $post->post_type == 'page' ) {

		$open = FALSE;
	}

	return $open;
}

// remove embediing of pages from the same website in the link post format
remove_filter( 'the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );

add_filter( 'author_link', 'acas4u_change_author_link_to_bbpress_profile', 10, 3 );
function acas4u_change_author_link_to_bbpress_profile( $link, $author_id, $author_nicename ) {
	$link = home_url( '/users/' . $author_nicename . '/' );

	return $link;
}

add_action( 'admin_enqueue_scripts', 'acas4u_backend_assets' );
function acas4u_backend_assets() {
	global $acas4u_stylesheet_directory_uri;
	wp_enqueue_style( 'admin-style', $acas4u_stylesheet_directory_uri . '/admin-style.css' );

	// TODO include only in download edit page
	wp_register_script( 'download-edit', $acas4u_stylesheet_directory_uri . '/js/download-edit.js', array( 'jquery' ), FALSE, TRUE );
	wp_localize_script( 'download-edit', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'download-edit' );

	// TODO include only in paypal page
	wp_enqueue_script( 'jquery-ui-datepicker' );

	wp_register_style( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
	wp_enqueue_style( 'jquery-ui' );

	wp_register_script( 'paypal-manage', $acas4u_stylesheet_directory_uri . '/js/paypal-manage.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'paypal-manage' );
}

add_action( 'wp_enqueue_scripts', 'acas4u_frontend_assets' );
function acas4u_frontend_assets() {
	global $acas4u_stylesheet_directory_uri, $post, $wp;

	$template = $wp->query_vars;

	wp_register_script( 'blockadblock', $acas4u_stylesheet_directory_uri . '/vendor/2b5dda92628bf57b4da5d276c82c2fc1192fa5856db1301835c3895bc5a97caa/file.js', array(), FALSE, TRUE );
	wp_enqueue_script( 'blockadblock' );

	wp_register_script( 'tablesorter-fork', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/js/jquery.tablesorter.combined.min.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'tablesorter-fork' );

	wp_enqueue_style( 'tablesorter-default', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/css/theme.default.min.css' );

	wp_register_script( 'stacktable', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'stacktable' );

	wp_enqueue_style( 'stacktable-style', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.css' );

	// custom scripts and styles
	if ( is_single() AND $post->post_type == 'download' ) {

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', $acas4u_stylesheet_directory_uri . '/vendor/jquery/jquery.js', array(), '2.0.3', FALSE );
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'lodash', $acas4u_stylesheet_directory_uri . '/vendor/lodash/dist/lodash.compat.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'lodash' );

		wp_register_script( 'kineticjs', $acas4u_stylesheet_directory_uri . '/vendor/KineticJS/index.js', array(), FALSE, TRUE );
		wp_enqueue_script( 'kineticjs' );

		wp_register_script( 'eventEmitter', $acas4u_stylesheet_directory_uri . '/vendor/eventEmitter/EventEmitter.js', array(), FALSE, TRUE );
		wp_enqueue_script( 'eventEmitter' );

		wp_register_script( 'waveform-data', $acas4u_stylesheet_directory_uri . '/vendor/waveform-data/dist/waveform-data.all.min.js', array(), FALSE, TRUE );
		wp_enqueue_script( 'waveform-data' );

		wp_register_script( 'peaks', $acas4u_stylesheet_directory_uri . '/vendor/peaks/peaks.min.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'peaks' );

		wp_enqueue_style( 'peaks-style', $acas4u_stylesheet_directory_uri . '/vendor/peaks/peaks.css' );

		wp_register_script( 'acas4u-main', $acas4u_stylesheet_directory_uri . '/js/track-preview.js', array( 'peaks' ), FALSE, TRUE );
		wp_localize_script( 'acas4u-main', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'acas4u-main' );
	}

	if ( is_home() OR is_front_page() ) {
		wp_register_script( 'acas4u-frontpage', $acas4u_stylesheet_directory_uri . '/js/front-page.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'acas4u-frontpage' );
	}

	$taxonomy = get_query_var( 'taxonomy' );
	if ( $taxonomy == 'genre' ) {
		wp_register_script( 'download-genre', $acas4u_stylesheet_directory_uri . '/js/download-genre.js', array( 'jquery' ), FALSE, TRUE );
		wp_localize_script( 'download-genre', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'download-genre' );
	}

	if ( array_key_exists( 'searchtype', $template ) AND $template['searchtype'] == 'acapellas' ) {
		wp_register_script( 'rangeslider', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'rangeslider' );

		wp_enqueue_style( 'rangeslider-style', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/css/ion.rangeSlider.css' );
		wp_enqueue_style( 'rangeslider-skin', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/css/ion.rangeSlider.skinImpreza.css' );

		//wp_register_script( 'tablesorter-fork', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/js/jquery.tablesorter.combined.min.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'tablesorter-fork' );

		//wp_enqueue_style( 'tablesorter-default', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/css/theme.default.min.css' );

		wp_register_script( 'download-search', $acas4u_stylesheet_directory_uri . '/js/download-search.js', array( 'jquery' ), FALSE, TRUE );
		wp_localize_script( 'download-search', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'download-search' );

		//wp_register_script( 'stacktable', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'stacktable' );

		//wp_enqueue_style( 'stacktable-style', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.css' );
	}

	if ( array_key_exists( 'pagetype', $template ) AND $template['pagetype'] == 'user' ) {
		//wp_register_script( 'theme-user-profile', $acas4u_stylesheet_directory_uri . '/js/user-profile.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'theme-user-profile' );

		//wp_register_script( 'tablesorter-fork', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/js/jquery.tablesorter.combined.min.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'tablesorter-fork' );

		//wp_enqueue_style( 'tablesorter-default', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/css/theme.default.min.css' );

		//wp_register_script( 'stacktable', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'stacktable' );

		//wp_enqueue_style( 'stacktable-style', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.css' );
	}

	if ( array_key_exists( 'popular', $template ) AND $template['popular'] == 'popular-acapellas' ) {
		wp_register_script( 'zebra-datepicker', $acas4u_stylesheet_directory_uri . '/vendor/zebra-datepicker/javascript/zebra_datepicker.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'zebra-datepicker' );

		wp_enqueue_style( 'zebra-datepicker-style', $acas4u_stylesheet_directory_uri . '/vendor/zebra-datepicker/css/impreza.css' );

		wp_register_script( 'popular-acapellas', $acas4u_stylesheet_directory_uri . '/js/popular-acapellas.js', array( 'jquery' ), FALSE, TRUE );
		wp_localize_script( 'popular-acapellas', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'popular-acapellas' );

		wp_register_script( 'rangeslider', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js', array( 'jquery' ), FALSE, TRUE );
		wp_enqueue_script( 'rangeslider' );

		wp_enqueue_style( 'rangeslider-style', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/css/ion.rangeSlider.css' );
		wp_enqueue_style( 'rangeslider-skin', $acas4u_stylesheet_directory_uri . '/vendor/ion.rangeslider/css/ion.rangeSlider.skinImpreza.css' );

		//wp_register_script( 'tablesorter-fork', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/js/jquery.tablesorter.combined.min.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'tablesorter-fork' );

		//wp_enqueue_style( 'tablesorter-default', $acas4u_stylesheet_directory_uri . '/vendor/tablesorter-fork/css/theme.default.min.css' );

		//wp_register_script( 'stacktable', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.js', array( 'jquery' ), FALSE, TRUE );
		//wp_enqueue_script( 'stacktable' );

		//wp_enqueue_style( 'stacktable-style', $acas4u_stylesheet_directory_uri . '/vendor/stacktable/stacktable.css' );
	}

	wp_register_script( 'jquery-form', $acas4u_stylesheet_directory_uri . '/vendor/jquery.form/jquery.form.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'jquery-form' );

	wp_register_script( 'tooltipster', $acas4u_stylesheet_directory_uri . '/vendor/tooltipster/js/jquery.tooltipster.min.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'tooltipster' );

	wp_enqueue_style( 'tooltipster-basic', $acas4u_stylesheet_directory_uri . '/vendor/tooltipster/css/tooltipster.css' );
	wp_enqueue_style( 'tooltipster-light', $acas4u_stylesheet_directory_uri . '/vendor/tooltipster/css/themes/tooltipster-light.css' );

	wp_register_script( 'fileupload-custom', $acas4u_stylesheet_directory_uri . '/js/fileupload-custom.js', array( 'jquery' ), FALSE, TRUE );
	wp_localize_script( 'fileupload-custom', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'fileupload-custom' );

	// common scripts and styles
	wp_register_script( 'acas4u-common', $acas4u_stylesheet_directory_uri . '/js/common.js', array( 'jquery' ), FALSE, TRUE );
	wp_localize_script( 'acas4u-common', 'acas4uAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'acas4u-common' );
}

function formatBytes( $bytes, $precision = 2 ) {
	$units = array( 'B', 'KB', 'MB', 'GB', 'TB' );

	$bytes = max( $bytes, 0 );
	$pow = floor( ( $bytes ? log( $bytes ) : 0 ) / log( 1024 ) );
	$pow = min( $pow, count( $units ) - 1 );

	$bytes /= pow( 1024, $pow );

	return round( $bytes, $precision ) . ' ' . $units[ $pow ];
}

function getRequest( $secretKey, $request, $accessKeyID = "", $version = "2009-03-01" ) {
	// Get host and url
	$url = parse_url( $request );

	// Get Parameters of request
	$request = $url['query'];
	$parameters = array();
	parse_str( $request, $parameters );
	$parameters["Timestamp"] = gmdate( "Y-m-d\TH:i:s\Z" );
	$parameters["Version"] = $version;
	if ( $accessKeyID != '' ) {
		$parameters["AWSAccessKeyId"] = $accessKeyID;
	}

	// Sort paramters
	ksort( $parameters );

	// re-build the request
	$request = array();
	foreach ( $parameters as $parameter => $value ) {
		$parameter = str_replace( "_", ".", $parameter );
		$parameter = str_replace( "%7E", "~", rawurlencode( $parameter ) );
		$value = str_replace( "%7E", "~", rawurlencode( $value ) );
		$request[] = $parameter . "=" . $value;
	}
	$request = implode( "&", $request );

	$signatureString = "GET\n" . $url['host'] . "\n" . $url['path'] . "\n" . $request;

	$signature = urlencode( base64_encode( hash_hmac( 'sha256', $signatureString, $secretKey, TRUE ) ) );

	$request = "http://" . $url[ host ] . $url['path'] . "?" . $request . "&Signature=" . $signature;

	return $request;
}

function getAlbumInfo( $keyword ) {

	$keyid = "1A3NQ7TNHQYGDXES9002";
	$secret = "O3VMDZdnRAhvd6EwLh/oDmxrBpRJYGoPXHambZ2U";
	$assoctag = "acapellas4u-21";

	$request = "http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService" . "&AWSAccessKeyId=" . $keyid . "&AssociateTag=" . $assoctag . "&Operation=ItemSearch&SearchIndex=Music" . "&ResponseGroup=Medium&Keywords=" . urlencode( $keyword );

	$request = getRequest( $secret, $request );

	$response = wp_remote_get( $request );
	$body = wp_remote_retrieve_body( $response );
	$xml = simplexml_load_string( $body );

	if ( $xml->Items->TotalResults == 0 ) {
		if ( $xml->Items->Request->Errors->Message ) {
			$albuminfo['error'] = $xml->Items->Request->Errors->Message;
		} else {
			$albuminfo['error'] = 'Unknown error';
		}
	} else {
		$albuminfo['url'] = $xml->Items->Item->DetailPageURL;
		$albuminfo['mediumimage'] = $xml->Items->Item->MediumImage->URL;
		$albuminfo['artist'] = $xml->Items->Item->ItemAttributes->Artist;
		$albuminfo['brand'] = $xml->Items->Item->ItemAttributes->Brand;
		$albuminfo['catalog'] = $xml->Items->Item->ItemAttributes->CatalogNumberList->CatalogNumberListElement;
		$albuminfo['publisher'] = $xml->Items->Item->ItemAttributes->Publisher;
		$albuminfo['pubdate'] = $xml->Items->Item->ItemAttributes->PublicationDate;
		$albuminfo['studio'] = $xml->Items->Item->ItemAttributes->Studio;
		$albuminfo['title'] = $xml->Items->Item->ItemAttributes->Title;
		$albuminfo['label'] = $xml->Items->Item->ItemAttributes->Label;
	}

	return $albuminfo;
}

function acas4u_limit_words( $words, $limit, $append ) {
	if ( ! $append ) {
		$append = ' &hellip;';
	}
	$limit = $limit + 1;
	$words = explode( ' ', $words, $limit );
	array_pop( $words );
	$words = implode( ' ', $words ) . $append;

	return $words;
}

function acas4u_create_download_title( $post_id, $artist1, $artist2, $trackname ) {
	if ( $artist1 AND $artist2 AND $trackname ) {
		$title = $artist1 . ' ft. ' . $artist2 . ' - ' . $trackname;
	} else if ( $artist1 AND $trackname ) {
		$title = $artist1 . ' - ' . $trackname;
	} else {
		$title = get_the_title( $post_id );
	}

	return $title;
}

function acas4u_url_exists( $url ) {
	$headers = get_headers( $url );

	// switched off because HTTP auth is ON
	//return stripos( $headers[0], "200 OK" ) ? TRUE : FALSE;
	return TRUE;
}

function acas4u_is_download_unmetered( $post_id ) {
	$download_path = get_post_meta( $post_id, '_download_path', TRUE );
	$option_path = 'unmetered_' . trim( $download_path, '/' );
	$folder_option = us_get_option( $option_path );

	return ( $folder_option == '1' ) ? TRUE : FALSE;
}

function acas4u_get_post_id_by_filename( $filename ) {
	global $wpdb;

	$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_filename" AND meta_value = "' . $filename . '"';
	$meta_row = $wpdb->get_row( $sqlquery );
	if ( count( $meta_row ) > 0 ) {
		$post_id = $meta_row->post_id;
	}

	if ( ! $post_id ) {
		$sqlquery = 'SELECT ID FROM ' . $wpdb->posts . ' WHERE post_title = "' . $filename . '" AND post_status = "publish"';
		$post_row = $wpdb->get_row( $sqlquery );
		if ( count( $post_row ) > 0 ) {
			$post_id = $post_row->ID;
		}
	}

	return $post_id;
}

function acas4u_get_post_id_by_artist_enid( $artist_enid ) {
	global $wpdb;

	$post_id = 0;
	$sqlquery = 'SELECT post_id FROM ' . $wpdb->postmeta . ' WHERE meta_key = "_download_artist_enid" AND meta_value = "' . $artist_enid . '"';
	$meta_row = $wpdb->get_row( $sqlquery );
	if ( count( $meta_row ) > 0 ) {
		$post_id = $meta_row->post_id;
	}

	return $post_id;
}

function acas4u_clean_slug( $slug ) {
	$slug = str_replace( '&amp;', '_and_', $slug );
	$slug = str_replace( '&', '_and_', $slug );
	$slug = str_replace( ' ', '_', $slug );
	$slug = preg_replace( '/[^a-z0-9\_]/', '', $slug );

	return $slug;
}

function acas4u_write_log_entry( $string, $filename ) {
	global $acas4u_stylesheet_directory;

	if ( ! $filename ) {
		$filename = 'acas4u.log';
	}

	$log_string = '[' . date( 'H:i:s d.m.Y' ) . '] ' . $string . "\r\n";

	$log = $acas4u_stylesheet_directory . '/' . $filename;
	file_put_contents( $log, $log_string, FILE_APPEND );
}

function acas4u_term_exists_by_slug( $term_slug, $term_taxonomy ) {
	$term = get_term_by( 'slug', $term_slug, $term_taxonomy, ARRAY_A );

	return $term;
}

function acas4u_key_value( $key_id ) {
	$keys = array(
		0 => 'C',
		1 => 'C#',
		2 => 'D',
		3 => 'Eb',
		4 => 'E',
		5 => 'F',
		6 => 'F#',
		7 => 'G',
		8 => 'Ab',
		9 => 'A',
		10 => 'Bb',
		11 => 'B',
	);

	return $keys[ $key_id ];
}

function acas4u_keymode_value( $key_id ) {
	$keys = array(
		0 => 'N/A',
		1 => 'C minor',
		2 => 'Db minor',
		3 => 'D minor',
		4 => 'Eb minor',
		5 => 'E minor',
		6 => 'F minor',
		7 => 'F# minor',
		8 => 'G minor',
		9 => 'Ab minor',
		10 => 'A minor',
		11 => 'Bb minor',
		12 => 'B minor',
		13 => 'C major',
		14 => 'Db major',
		15 => 'D major',
		16 => 'Eb major',
		17 => 'E major',
		18 => 'F major',
		19 => 'F# major',
		20 => 'G major',
		21 => 'Ab major',
		22 => 'A major',
		23 => 'Bb major',
		24 => 'B major',
	);

	$keys_mik1 = array(
		0 => 'N/A',
		1 => '5A',
		2 => '12A',
		3 => '7A',
		4 => '2A',
		5 => '9A',
		6 => '4A',
		7 => '11A',
		8 => '6A',
		9 => '1A',
		10 => '8A',
		11 => '3A',
		12 => '10A',
		13 => '8B',
		14 => '3B',
		15 => '10B',
		16 => '5B',
		17 => '12B',
		18 => '7B',
		19 => '2B',
		20 => '9B',
		21 => '4B',
		22 => '11B',
		23 => '6B',
		24 => '1B',
	);

	return $keys_mik1[ $key_id ];
}

function acas4u_get_posts_count( $args, $search_type ) {
	global $wpdb;
	if ( $search_type == 'custom' ) {
		unset( $args['count'] );
		unset( $args['offset'] );
		$query_start = 'SELECT COUNT(t1.download_post_id) FROM acas4u_download_indexes t1';
		$query = acas4u_build_custom_search_query( $query_start, $args );
		/*
		echo '<pre>';
		echo $query;
		echo '</pre>';
		*/
		$count = intval( $wpdb->get_var( $query ) );
	} else {
		$result = new WP_Query( $args );
		$count = $result->found_posts;
		wp_reset_query();
	}

	return $count;
}

function acas4u_limit_chars( $string, $limit ) {
	if ( mb_strlen( $string, 'UTF-8' ) > $limit ) {
		$string = mb_substr( $string, 0, $limit, 'UTF-8' ) . '&#133;';
	}

	return $string;
}

function acas4u_format_duration( $download_duration ) {
	if ( $download_duration AND $download_duration != 0 ) {
		$download_duration = intval( gmdate( 'i', $download_duration ) ) . ':' . gmdate( 's', $download_duration );
	}

	return $download_duration;
}

function acas4u_get_file_bitrate( $filename ) {
	$TaggingFormat = 'UTF-8';
	$getID3 = new getID3;
	$getID3->setOption( array( 'encoding' => $TaggingFormat ) );
	$ThisFileInfo = $getID3->analyze( $filename );

	if ( $ThisFileInfo['audio']['bitrate'] ) {
		$bitrate = floor( $ThisFileInfo['audio']['bitrate'] / 1000 );
	} else {
		$bitrate = 0;
	}

	return $bitrate;
}

function acas4u_get_file_samplerate( $filename ) {
	$TaggingFormat = 'UTF-8';
	$getID3 = new getID3;
	$getID3->setOption( array( 'encoding' => $TaggingFormat ) );
	$ThisFileInfo = $getID3->analyze( $filename );

	if ( $ThisFileInfo['audio']['sample_rate'] ) {
		$sample_rate = $ThisFileInfo['audio']['sample_rate'];
	} else {
		$sample_rate = 0;
	}

	return $sample_rate;
}

function acas4u_get_resized_thumbnail_image_url( $thumbnail_image, $width, $height ) {
	if ( $thumbnail_image[1] < $width AND $thumbnail_image[2] < $height ) {
		$thumbnail_url = aq_resize( $thumbnail_image[0], $width, $height, TRUE, TRUE, FALSE );
	} else if ( $thumbnail_image[1] > $width AND $thumbnail_image[2] > $height ) {
		$thumbnail_url = aq_resize( $thumbnail_image[0], $width, $height, TRUE, TRUE, FALSE );
	} else if ( $thumbnail_image[1] > $width OR $thumbnail_image[2] > $height ) {
		$thumbnail_url = aq_resize( $thumbnail_image[0], $width, $height, FALSE, TRUE, FALSE );
	} else {
		$thumbnail_url = $thumbnail_image[0];
	}

	return $thumbnail_url;
}

add_filter( 'bbp_get_forum_content', 'embed_bbcode', 5 );
add_filter( 'bbp_get_reply_content', 'embed_bbcode', 5 );
function embed_bbcode( $content ) {
	$upload_dir = wp_upload_dir();

	$smilies_path = $upload_dir['baseurl'] . '/phpbb_smilies';

	$content = str_replace( '{SMILIES_PATH}', $smilies_path, $content );

	$content = preg_replace( '/\[img:(.+?)](.+?)\[\/img:(.+?)]/i', '<img src="$2" />', $content );
	$content = preg_replace( '/\[img](.+?)\[\/img]/i', '<img src="$1" />', $content );

	$content = preg_replace( '/\[url=(.+?):(.+?)](.+?)\[\/url:(.+?)]/i', '<a href="$1" />$3</a>', $content );
	$content = preg_replace( '/\[url=(.+?)](.+?)\[\/url]/i', '<a href="$1" />$2</a>', $content );

	$content = preg_replace( '/\[youtube:(.+?)](.+?)\[\/youtube:(.+?)]/i', '<a href="$2" target="_blank">$2</a>', $content );
	$content = preg_replace( '/\[youtube](.+?)\[\/youtube]/i', '<a href="$1" target="_blank">$1</a>', $content );

	$content = preg_replace( '/\[b:(.+?)](.+?)\[\/b:(.+?)]/i', '<strong>$2</strong>', $content );
	$content = preg_replace( '/\[b](.+?)\[\/b]/i', '<strong>$1</strong>', $content );

	$content = preg_replace( '/\[u:(.+?)](.+?)\[\/u:(.+?)]/i', '<u>$2</u>', $content );
	$content = preg_replace( '/\[u](.+?)\[\/u]/i', '<u>$1</u>', $content );

	$content = preg_replace( '/\[i:(.+?)](.+?)\[\/i:(.+?)]/i', '<em>$2</em>', $content );
	$content = preg_replace( '/\[i](.+?)\[\/i]/i', '<em>$1</em>', $content );

	$content = preg_replace( '/\[color=(.+?):(.+?)](.+?)\[\/color:(.+?)]/i', '<span style="color:$1">$3</span>', $content );
	$content = preg_replace( '/\[color=(.+?)](.+?)\[\/color]/i', '<span style="color:$1">$2</span>', $content );

	$content = preg_replace( '/\[size=(.+?):(.+?)](.+?)\[\/size:(.+?)]/i', '<span class="bbp-size-$1">$3</span>', $content );
	$content = preg_replace( '/\[size=(.+?)](.+?)\[\/size]/i', '<span class="bbp-size-$1">$2</span>', $content );

	$content = preg_replace( '/\[quote=(&quot;|")(.+?)(&quot;|"):(.+?)](.+?)\[\/quote:(.+?)]/i', '<blockquote><div class="acas4u-quote-author">$2 wrote:</div><div class="acas4u-quote-content">$5</div></blockquote>', $content );

	return $content;
}

function acas4u_genre_tag_text_callback( $count ) {
	return sprintf( _n( '%s genre', '%s genres', $count ), number_format_i18n( $count ) );
}

function acas4u_err_class( $value ) {
	if ( $value != '' ) {
		$err_class = 'acas4u-input-error';
	} else {
		$err_class = '';
	}

	return $err_class;
}

// Removing bbp widget from admin dashboard
add_action( 'wp_dashboard_setup', 'acas4u_dashboard_setup', 11 );
function acas4u_dashboard_setup() {
	global $wp_meta_boxes;
	if ( ! isset( $wp_meta_boxes ) OR ! is_array( $wp_meta_boxes ) ) {
		return;
	}
	unset( $wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['bbp-dashboard-right-now'] );
}

add_filter( 'login_redirect', 'acas4u_login_redirect', 10, 3 );
function acas4u_login_redirect( $redirect_to, $request, $user ) {
	//return ( is_array( $user->roles ) AND in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url();
	return ( is_array( $user->roles ) AND in_array( 'administrator', $user->roles ) ) ? admin_url() : $request;
}

add_action( 'set_current_user', 'acas4u_hide_admin_bar' );
function acas4u_hide_admin_bar() {
	if ( ! current_user_can( 'manage_options' ) ) {
		show_admin_bar( FALSE );
	}
}

function get_custom_meta( $meta_value_id, $meta_key ) {
	global $wpdb;

	$meta_value = $wpdb->get_var( "SELECT meta_value FROM wp_custommeta WHERE meta_value_id = '" . $meta_value_id . "' AND meta_key = '" . $meta_key . "'" );

	return $meta_value;
}

// Fixing login loading time
add_action( 'admin_init', 'acas4u_remove_maybe_update_core', 9 );
function acas4u_remove_maybe_update_core() {
	remove_action( 'admin_init', '_maybe_update_core' );
}

function acas4u_time_to_iso8601_duration( $time ) {
	$units = array(
		"Y" => 365 * 24 * 3600,
		"D" => 24 * 3600,
		"H" => 3600,
		"M" => 60,
		"S" => 1,
	);

	$str = "P";
	$istime = FALSE;

	foreach ( $units as $unitName => &$unit ) {
		$quot = intval( $time / $unit );
		$time -= $quot * $unit;
		$unit = $quot;
		if ( $unit > 0 ) {
			if ( ! $istime && in_array( $unitName, array( "H", "M", "S" ) ) ) { // There may be a better way to do this
				$str .= "T";
				$istime = TRUE;
			}
			$str .= strval( $unit ) . $unitName;
		}
	}

	return $str;
}

add_filter( 'wp_nav_menu_items', 'acas4u_add_login_logout_link', 10, 2 );
function acas4u_add_login_logout_link( $items, $args ) {

	if ( $args->theme_location == 'us_main_menu' ) {

		$redirect_url = home_url() . $_SERVER['REQUEST_URI'];
		$loginout_link = wp_loginout( $redirect_url, 0 );
		$items .= '<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item w-nav-item level_1">' . $loginout_link . '</li>';
	}

	return $items;
}

add_filter( 'wp_authenticate_user', 'myplugin_auth_login', 10, 2 );
function myplugin_auth_login( $user, $password ) {

	$user_active = get_user_meta( $user->ID, 'acas4u_has_to_be_activated', TRUE );

	// for old accounts without activation meta field
	if ( $user_active == '' ) {
		$user_active = 'active';
	}

	if ( $user != NULL AND $user_active != 'active' ) {
		$user = new WP_Error( 'activation_failed', __( '<strong>ERROR</strong>: User is not activated.' ) );
	}

	return $user;
}

add_action( 'profile_update', 'acas4u_profile_update', 10, 2 );
function acas4u_profile_update( $user_id, $old_user_data ) {
	$user_info = get_userdata( $user_id );

	$new_roles = $user_info->roles;
	$old_roles = $old_user_data->caps;

	if ( in_array( 'donator', $new_roles ) AND ! array_key_exists( 'donator', $old_roles ) ) {
		update_user_meta( $user_id, '_bbp_phpbb_user_donator', 'TRUE' );
	} else if ( ! in_array( 'donator', $new_roles ) AND array_key_exists( 'donator', $old_roles ) ) {
		delete_user_meta( $user_id, '_bbp_phpbb_user_donator' );
	}
}

function acas4u_count_term_children_posts( $parent_term_id, $taxonomy_name ) {
	$term_childrens = get_term_children( $parent_term_id, $taxonomy_name );
	$count_of_childrens = 0;
	foreach ( $term_childrens as $term_child ) {
		$term = get_term_by( 'id', $term_child, $taxonomy_name );
		$count_of_childrens = $count_of_childrens + $term->count;
	}

	return $count_of_childrens;
}

add_filter( 'wp_mail_from', 'acas4u_filter_wp_mail_from' );
function acas4u_filter_wp_mail_from( $email ) {
	return 'no-reply@acapellas4u.co.uk';
}

add_filter( 'wp_mail_from_name', 'acas4u_filter_wp_mail_from_name' );
function acas4u_filter_wp_mail_from_name( $from_name ) {
	return 'Acapellas4U';
}

// some magic, I'm not sure that this will not affect the site's performance
add_filter( 'query', 'acas4u_filter_slow_query', 999 );
function acas4u_filter_slow_query( $query ) {
	//set an array of functions which run the slow queries:
	$banned_functions = array( 'count_users', 'bbp_get_statistics', 'bpbbpst_support_statistics' );

	foreach ( $banned_functions as $banned_function ) {
		if ( in_array( $banned_function, wp_list_pluck( debug_backtrace(), 'function' ) ) ) {
			return "SELECT 1 ";
		}
	}

	return $query;
}


add_filter( 'additional_capabilities_display', 'remove_additional_capabilities_func' );
function remove_additional_capabilities_func() {
	return FALSE;
}


