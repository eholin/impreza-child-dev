<?php

/**
 * User Profile
 *
 * @package bbPress
 * @subpackage Theme
 */

global $wp_query;
$user_nicename = get_query_var( 'bbp_user' );

$args = array(
	'search' => $user_nicename,
	'search_fields' => array( 'user_login', 'user_nicename', 'display_name' ),
);
$user = new WP_User_Query( $args );
if ( $user->results ) {
	$user_id = $user->results[0]->ID;
	$user_url = $user->results[0]->user_url;
	$user_registered = $user->results[0]->user_registered;
}

if ( $user_id ) {
	$user_talents = get_user_meta( $user_id, '_bbp_phpbb_pf_talents', TRUE );
	$user_inspiredby = get_user_meta( $user_id, '_bbp_phpbb_pf_inspiredby', TRUE );
	$user_genre = get_user_meta( $user_id, '_bbp_phpbb_pf_your_genre', TRUE );
	$user_soundcloud = get_user_meta( $user_id, '_bbp_phpbb_pf_soundcloud_page', TRUE );
	$user_fav_website = get_user_meta( $user_id, '_bbp_phpbb_pf_fav_website', TRUE );
	$user_testimonials = get_user_meta( $user_id, '_bbp_phpbb_pf_testimonials', TRUE );

	if ( $user_talents != '' AND $user_talents != '0' ) {
		$user_talents_str = get_custom_meta( $user_talents, '_bbp_phpbb_pf_talents' );
	}
	if ( $user_testimonials != '' ) {
		$user_testimonials_str = get_custom_meta( $user_testimonials, '_bbp_phpbb_pf_testimonials' );
	}
}

?>

<?php do_action( 'bbp_template_before_user_profile' ); ?>

<div id="bbp-user-profile" class="bbp-user-profile">
	<div class="bbp-user-section">

		<?php if ( bbp_get_displayed_user_field( 'description' ) ) : ?>

			<p class="bbp-user-description"><?php bbp_displayed_user_field( 'description' ); ?></p>

		<?php endif; ?>

		<p class="bbp-user-forum-role"><?php printf( __( 'Forum Role: %s', 'bbpress' ), bbp_get_user_display_role() ); ?></p>

		<p class="bbp-user-topic-count"><?php printf( __( 'Topics Started: %s', 'bbpress' ), bbp_get_user_topic_count_raw() ); ?></p>

		<p class="bbp-user-reply-count"><?php printf( __( 'Replies Created: %s', 'bbpress' ), bbp_get_user_reply_count_raw() ); ?></p>

		<p class="bbp-custom-profile bbp-custom-user-website"><?php printf( __( "User's website: %s", "acas4u" ), $user_url ); ?></p>

		<?php if ( $user_talents_str ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-talents"><?php printf( __( 'Dj or Producer?: %s', 'acas4u' ), $user_talents_str ); ?></p>
		<?php endif ?>

		<?php if ( $user_inspiredby ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-inspired"><?php printf( __( 'Musical influences: %s', 'acas4u' ), $user_inspiredby ); ?></p>
		<?php endif ?>

		<?php if ( $user_genre ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-genre"><?php printf( __( 'Music genre(s): %s', 'acas4u' ), $user_genre ); ?></p>
		<?php endif; ?>

		<?php if ( $user_soundcloud ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-soundcloud"><?php printf( __( 'Soundcloud Page: %s', 'acas4u' ), $user_soundcloud ); ?></p>
		<?php endif; ?>

		<?php if ( $user_fav_website ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-fav-website"><?php printf( __( 'My favorite website is: %s', 'acas4u' ), $user_fav_website ); ?></p>
		<?php endif; ?>

		<?php if ( $user_testimonials_str ) : ?>
			<p class="bbp-custom-profile bbp-custom-user-testimonials"><?php printf( __( 'Recommendation: %s', 'acas4u' ), $user_testimonials_str ); ?></p>
		<?php endif; ?>
	</div>

	<h2 class="entry-title"><?php _e( 'Profile', 'bbpress' ); ?></h2>
</div><!-- #bbp-author-topics-started -->

<?php do_action( 'bbp_template_after_user_profile' ); ?>

<div class="acas4u-user-logs-tabs">
	<?php the_ad(498013); ?>
	<h2 class="entry-title">User logs</h2>
	<ul class="tabs">
		<li class="tab-link current" data-tab="tab-download">Download log</li>
		<li class="tab-link" data-tab="tab-upload">Upload log</li>
	</ul>

	<?php
	/* download log */
	$download_log = acas4u_get_user_download_log( $user_id );

	/* upload log */
	$upload_log = acas4u_get_user_upload_log( $user_id );
	?>

	<div id="tab-download" class="tab-content current"><?php echo $download_log; ?></div>

	<div id="tab-upload" class="tab-content"><?php echo $upload_log; ?></div>

</div>
