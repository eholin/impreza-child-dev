<?php
error_reporting( E_ERROR & E_WARNING & ~E_NOTICE );

function getRequest( $secretKey, $request, $accessKeyID = "", $version = "2009-03-01" ) {
	// Get host and url
	$url = parse_url( $request );

	// Get Parameters of request
	$request = $url['query'];
	$parameters = array();
	parse_str( $request, $parameters );
	$parameters["Timestamp"] = gmdate( "Y-m-d\TH:i:s\Z" );
	$parameters["Version"] = $version;
	if ( $accessKeyID != '' ) {
		$parameters["AWSAccessKeyId"] = $accessKeyID;
	}

	// Sort paramters
	ksort( $parameters );

	// re-build the request
	$request = array();
	foreach ( $parameters as $parameter => $value ) {
		$parameter = str_replace( "_", ".", $parameter );
		$parameter = str_replace( "%7E", "~", rawurlencode( $parameter ) );
		$value = str_replace( "%7E", "~", rawurlencode( $value ) );
		$request[] = $parameter . "=" . $value;
	}
	$request = implode( "&", $request );

	$signatureString = "GET\n" . $url['host'] . "\n" . $url['path'] . "\n" . $request;

	//if (function_exists("hash_hmac"))
	$signature = urlencode( base64_encode( hash_hmac( 'sha256', $signatureString, $secretKey, TRUE ) ) );
	/*else
		$signature = urlencode(base64_encode(hash($secretKey, $signatureString, 'sha256')));*/

	$request = "http://" . $url[ host ] . $url['path'] . "?" . $request . "&Signature=" . $signature;

	return $request;
}

function getAlbumInfo( $keyword ) {

	//$keyid = "16EG583XZ0CZH1T5KYR2";
	$keyid = "1A3NQ7TNHQYGDXES9002";
	$secret = "O3VMDZdnRAhvd6EwLh/oDmxrBpRJYGoPXHambZ2U";
	$assoctag = "acapellas4u-21";

	//$request="http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService&".
	// "AWSAccessKeyId=".$keyid."&AssociateTag=".$assoctag."&Version=2009-03-01&".
	//"Operation=ItemSearch&ResponseGroup=Medium,Offers&SearchIndex=Music&".
	//"Keywords=".urlencode($keyword)."&ItemPage=1";

	$request = "http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService" . "&AWSAccessKeyId=" . $keyid . "&AssociateTag=" . $assoctag . "&Operation=ItemSearch&SearchIndex=Music" . "&ResponseGroup=Medium&Keywords=" . urlencode( $keyword );

	$request = getRequest( $secret, $request );//, '', "2009-01-06");

	$session = curl_init( $request );
	curl_setopt( $session, CURLOPT_HEADER, FALSE );
	curl_setopt( $session, CURLOPT_RETURNTRANSFER, TRUE );
	$response = curl_exec( $session );
	curl_close( $session );

	/*header("Content-type: text/plain");
	var_dump($response);
	die();*/

	// now we have xml file
	$xml = xml_parser_create();
	xml_parse_into_struct( $xml, $response, $vals, $index );
	xml_parser_free( $xml );

	$albuminfo = array();
	$albuminfo = getDetails( $index, $vals );
	$albuminfo['IMAGE'] = getImage( $index, $vals );
	$albuminfo['URL'] = getURL( $index, $vals );

	return $albuminfo;
}

function getURL( $index, $vals ) {
	return $vals[ $index['DETAILPAGEURL'][0] ]['value'];
}

function getImage( $index, $vals ) {
	return $vals[ $index['MEDIUMIMAGE'][0] + 1 ]['value'];
}

function getDetails( $index, $vals ) {

	$start = $index['ITEMATTRIBUTES'][0];
	$end = $index['ITEMATTRIBUTES'][1];
	$album_info = array();

	for ( $i = $start; $i < $end; $i ++ ) {

		if ( $vals[ $i ]['type'] == 'complete' ) {
			if ( $vals[ $i ]['tag'] == "RELEASEDATE" ) {
				$vals[ $i ]['value'] = date( "jS F Y", strtotime( $vals[ $i ]['value'] ) );
			}
			$album_info[ $vals[ $i ]['tag'] ] = $vals[ $i ]['value'];
		}
	}

	return $album_info;
}

?>
