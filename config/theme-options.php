<?php
/**
 * Child Theme's Theme Options config
 *
 * @var $config array Framework-based theme options config
 *
 * @return array Changed config
 */

global $acas4u_stylesheet_directory_uri;

$config['acas4uoptions'] = array(
	'title' => __( 'Acapellas4u.co.uk', 'us' ),
	'icon' => $acas4u_stylesheet_directory_uri . '/img/favicon.png',
	'fields' => array(
		'default_max_credits' => array(
			'title' => __( 'Default max download credits', 'us' ),
			'description' => __( 'Set default max download credit value.', 'us' ),
			'type' => 'slider',
			'std' => 30,
			'min' => 1,
			'max' => 100,
		),
		'default_credit_period' => array(
			'title' => __( 'Default download credits period', 'us' ),
			'description' => __( 'Set default download credit period in days.', 'us' ),
			'type' => 'slider',
			'std' => 30,
			'min' => 1,
			'max' => 90,
		),
		'default_bonus_credits' => array(
			'title' => __( 'Default bonus download credits', 'us' ),
			'description' => __( 'Set default bonus download credit value.', 'us' ),
			'type' => 'slider',
			'std' => 30,
			'min' => 1,
			'max' => 100,
		),
		'max_bitrate_for_non_donators' => array(
			'title' => __( 'Max bitrate', 'us' ),
			'description' => __( 'Set maximum bitrate, available for non donators.', 'us' ),
			'type' => 'slider',
			'std' => 192,
			'min' => 0,
			'max' => 320,
		),
		'en_api_key' => array(
			'title' => __( 'EchoNest API key', 'us' ),
			'description' => __( 'Developer key for EchoNest API.', 'us' ),
			'type' => 'text',
			'std' => '',
		),
		'genre_bar' => array(
			'title' => __( 'Genres Bar', 'us' ),
			'type' => 'radio',
			'options' => array(
				'show' => __( 'Show Bar', 'us' ),
				'hide' => __( 'Hide Bar', 'us' ),
			),
			'std' => 'show',
		),
		'upload_page_html' => array(
			'title' => __( 'Upload Page Text', 'us' ),
			'description' => __( 'Paste your text for Upload Page here, it will be added after form inputs. You can use html code.', 'us' ),
			'type' => 'html',
			'classes' => 'title_top desc_2',
		),
	),
);

$config['acas4u_unmetered'] = array(
	'title' => __( 'Unmetered folders', 'us' ),
	'icon' => $acas4u_stylesheet_directory_uri . '/img/favicon.png',
	'fields' => array(
		'unmetered_0-9' => array(
			'title' => __( '0-9', 'us' ),
			'type' => 'switch',
			'std' => 1,
		),
		'unmetered_A' => array(
			'title' => __( 'A', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_B' => array(
			'title' => __( 'B', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_C' => array(
			'title' => __( 'C', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_D' => array(
			'title' => __( 'D', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_E' => array(
			'title' => __( 'E', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_F' => array(
			'title' => __( 'F', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_G' => array(
			'title' => __( 'G', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_H' => array(
			'title' => __( 'H', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_I' => array(
			'title' => __( 'I', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_J' => array(
			'title' => __( 'J', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_K' => array(
			'title' => __( 'K', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_L' => array(
			'title' => __( 'L', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_M' => array(
			'title' => __( 'M', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_N' => array(
			'title' => __( 'N', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_O' => array(
			'title' => __( 'O', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_P' => array(
			'title' => __( 'P', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_Q' => array(
			'title' => __( 'Q', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_R' => array(
			'title' => __( 'R', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_S' => array(
			'title' => __( 'S', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_T' => array(
			'title' => __( 'T', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_U' => array(
			'title' => __( 'U', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_V' => array(
			'title' => __( 'V', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_W' => array(
			'title' => __( 'W', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_X' => array(
			'title' => __( 'X', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_Y' => array(
			'title' => __( 'Y', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered_Z' => array(
			'title' => __( 'Z', 'us' ),
			'type' => 'switch',
			'std' => 0,
		),
		'unmetered__DJ_TOOLS_' => array(
			'title' => __( '_DJ_TOOLS_', 'us' ),
			'type' => 'switch',
			'std' => 1,
		),
		'unmetered__ROCKAPELLAS_Covers_' => array(
			'title' => __( '_ROCKAPELLAS_Covers_', 'us' ),
			'type' => 'switch',
			'std' => 1,
		),
		'unmetered__UNKNOWNS_' => array(
			'title' => __( '_UNKNOWNS_', 'us' ),
			'type' => 'switch',
			'std' => 1,
		),
		'unmetered__VOCAL_SHOTS_MISC_' => array(
			'title' => __( '_VOCAL_SHOTS_MISC_', 'us' ),
			'type' => 'swith',
			'std' => 1,
		),
	),
);

return $config;
